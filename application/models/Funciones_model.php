<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funciones_model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}

	public function obtenerDatosTabla($tabla)	{
		$query = $this->db->get($tabla); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerPasantias_por_periodo($periodo_id)	{
		$this->db->where('inscripcion_periodo_id',$periodo_id);
		$this->db->from('pasantias');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerUltimos($campo_id, $limite, $tabla) {
		$this->db->order_by($campo_id, 'DESC');
		$this->db->limit($limite);
		$query = $this->db->get($tabla);
		if ($query->num_rows() > 0) {
			if ($limite == 1) {
				return $query->row();
			}
			else return $query->result();
		}
		else return false;
	}
}