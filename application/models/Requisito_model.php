<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requisito_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function obtenerRequisitos()	{
		$query = $this->db->get('requisitos');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerRequisito($id) {
		$this->db->where('requisito_id',$id);
		$query = $this->db->get('requisitos');
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerRequisito_por_nombre($nombre)	{
		$this->db->where('nombre',$nombre);
		$query = $this->db->get('requisitos'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function crearRequisito($datos) {
		$this->db->insert('requisitos', $datos);

		$requisito = $this->obtenerRequisito_por_nombre($datos['nombre']);
		
		if ($requisito) {
			return $requisito->requisito_id;
		}
		else {
			return false;
		}
	}

	public function actualizarRequisito($id, $datos) {
		$this->db->set($datos);
		$this->db->where('requisito_id', $id);
		$this->db->update('requisitos');

		$requisito = $this->obtenerRequisito($id);

		if ($requisito) {
			return $requisito->requisito_id;
		}
		else {
			return false;
		}
	}

	public function eliminarRequisito($id) {
		if ($this->db->delete('requisitos', array('requisito_id' => $id))) {
			return true;
		}
		else {
			return false;
		}
	}

}
?>