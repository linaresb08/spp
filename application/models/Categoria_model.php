<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function obtenerCategorias()	{
		$query = $this->db->get('categorias');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerCategoria($id) {
		$this->db->where('categoria_id',$id);
		$query = $this->db->get('categorias');
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerCategoria_por_nombre($nombre)	{
		$this->db->where('nombre',$nombre);
		$query = $this->db->get('categorias'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function crearCategoria($datos) {
		$this->db->insert('categorias', $datos);

		$categoria = $this->obtenerCategoria_por_nombre($datos['nombre']);
		
		if ($categoria) {
			return $categoria->categoria_id;
		}
		else {
			return false;
		}
	}

	public function actualizarCategoria($id, $datos) {
		$this->db->set($datos);
		$this->db->where('categoria_id', $id);
		$this->db->update('categorias');

		$categoria = $this->obtenerCategoria($id);

		if ($categoria) {
			return $categoria->categoria_id;
		}
		else {
			return false;
		}
	}

	public function eliminarCategoria($id) {
		if ($this->db->delete('categorias', array('categoria_id' => $id))) {
			return true;
		}
		else {
			return false;
		}
	}
}
?>