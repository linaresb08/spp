<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informacion_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function obtenerInformacion($id)	{
		$this->db->where('informacion_id',$id);
		$query = $this->db->get('informacion_contacto');
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function actualizarInformacion($id, $datos) {
		$this->db->set($datos);
		$this->db->where('informacion_id', $id);
		$this->db->update('informacion_contacto');

		$informacion = $this->obtenerInformacion($id);

		if ($informacion) {
			return $informacion->informacion_id;
		}
		else {
			return false;
		}
	}
}
?>