<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasantia_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function obtenerPasantias()	{
		$query = $this->db->get('pasantias'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerPasantia($id)	{
		$this->db->where('pasantia_id',$id);
		$query = $this->db->get('pasantias'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerPasantia_por_estudiante($estudiante_id)	{
		$this->db->where('estudiante_id',$estudiante_id);
		$query = $this->db->get('pasantias'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerPasantias_por_estudiante($estudiante_id)	{
		$this->db->where('estudiante_id',$estudiante_id);
		$query = $this->db->get('pasantias'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerPasantias_habilitadas_por_estudiante($estudiante_id)	{
		$this->db->where('estado','Habilitada');
		$this->db->where('estudiante_id',$estudiante_id);
		$query = $this->db->get('pasantias'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerPasantias_aprobadas_por_estudiante($estudiante_id)	{
		$this->db->where('estado','Aprobada');
		$this->db->where('estudiante_id',$estudiante_id);
		$query = $this->db->get('pasantias');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerPasantias_por_TutorAcademico($tutor_academico_id)	{
		$this->db->where('tutor_academico_id',$tutor_academico_id);
		$query = $this->db->get('pasantias'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerPasantias_por_periodo($periodo_id,$seccion)	{
		$this->db->where('inscripcion_periodo_id',$periodo_id);
		$this->db->where('seccion',$seccion);
		$this->db->from('pasantias');
		$this->db->join('notas', 'notas.pasantia_id = pasantias.pasantia_id');
		$this->db->join('usuarios', 'usuarios.usuario_id = pasantias.estudiante_id');
    $this->db->join('perfiles', 'perfiles.usuario_id = usuarios.usuario_id');
		$this->db->order_by('ci', 'ASC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function crearPasantia($datos) {
		$this->db->insert('pasantias', $datos);

		$pasantia = $this->obtenerPasantia_por_estudiante($datos['estudiante_id']);
		
		if ($pasantia) {
			return $pasantia->pasantia_id;
		}
		else {
			return false;
		}
	}

	public function actualizarPasantia($id, $datos) {
		$this->db->set($datos);
		$this->db->where('pasantia_id', $id);
		$this->db->update('pasantias');

		$pasantia = $this->obtenerPasantia($id);

		if ($pasantia) {
			return $pasantia->pasantia_id;
		}
		else {
			return false;
		}
	}

	public function eliminarPasantia($id)	{
		$this->db->where('pasantia_id',$id);
		if ($this->db->delete('pasantias')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function asignarTutorAcademico($pasantia_id, $tutor_academico_id) {
		$this->db->set(array('tutor_academico_id' => $tutor_academico_id));
		$this->db->where('pasantia_id', $pasantia_id);
		if ($this->db->update('pasantias')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function adjuntarInforme($pasantia_id, $informe) {
		$this->db->set(array('informe' => $informe));
		$this->db->where('pasantia_id', $pasantia_id);
		if ($this->db->update('pasantias'))	{
			return true;
		}
		else {
			return false;
		}
	}

	public function aprobarPasantia($pasantia_id)	{
		$this->db->set(array('estado' => 'Aprobada'));
		$this->db->where('pasantia_id', $pasantia_id);
		$this->db->update('pasantias');
	}

	public function habilitarPasantia($pasantia_id)	{
		$this->db->set(array('estado' => 'Habilitada'));
		$this->db->where('pasantia_id', $pasantia_id);
		$this->db->update('pasantias');
	}

	public function reprobarPasantia($pasantia_id)	{
		$this->db->set(array('estado' => 'Reprobada'));
		$this->db->where('pasantia_id', $pasantia_id);
		$this->db->update('pasantias');
	}
}
?>