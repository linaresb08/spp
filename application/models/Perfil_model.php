<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function obtenerPerfiles()	{
		$query = $this->db->get('perfiles'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerPerfil($id)	{
		$this->db->where('perfil_id',$id);
		$query = $this->db->get('perfiles'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerPerfil_usuario_id($id)	{
		$this->db->where('usuario_id',$id);
		$query = $this->db->get('perfiles'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function buscarUsuario($ci)	{
		$this->db->where('ci', $ci); 
		$query = $this->db->get('perfiles'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function crearPerfil($datos) {
		#	$this->db->insert('NombreTabla',array('NombreVariableEnLaTabla' => $datos['NombreVariableQueSeTraeDelControlador']
		$this->db->insert('perfiles', $datos);

		$perfil = $this->obtenerPerfil_usuario_id($datos['usuario_id']);
		
		if ($perfil) {
			return $perfil->perfil_id;
		}
		else {
			return false;
		}
	}

	public function actualizarPerfil($datos) {
		$this->db->set($datos);
		$this->db->where('usuario_id', $datos['usuario_id']);
		$this->db->update('perfiles');

		$perfil = $this->obtenerPerfil_usuario_id($datos['usuario_id']);

		if ($perfil) {
			return $perfil->perfil_id;
		}
		else {
			return false;
		}
	}

}
?>