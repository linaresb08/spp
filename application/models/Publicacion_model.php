<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publicacion_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function obtenerPublicaciones()	{
		$this->db->order_by('fecha', 'DESC');
		$query = $this->db->get('publicaciones');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerPublicacion($id) {
		$this->db->where('publicacion_id',$id);
		$query = $this->db->get('publicaciones');
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerPublicacion_por_titulo($titulo)	{
		$this->db->where('titulo',$titulo);
		$query = $this->db->get('publicaciones'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function crearPublicacion($datos) {
		$this->db->insert('publicaciones', $datos);

		$publicacion = $this->obtenerPublicacion_por_titulo($datos['titulo']);
		
		if ($publicacion) {
			return $publicacion->publicacion_id;
		}
		else {
			return false;
		}
	}

	public function actualizarPublicacion($id, $datos) {
		$this->db->set($datos);
		$this->db->where('publicacion_id', $id);
		$this->db->update('publicaciones');

		$publicacion = $this->obtenerPublicacion($id);

		if ($publicacion) {
			return $publicacion->publicacion_id;
		}
		else {
			return false;
		}
	}

	public function eliminarPublicacion($id) {
		if ($this->db->delete('publicaciones', array('publicacion_id' => $id))) {
			return true;
		}
		else {
			return false;
		}
	}

	public function obtener_paginacion($numero_por_pagina='')	{
		$this->db->order_by('fecha', 'DESC');
		return $this->db->get('publicaciones', $numero_por_pagina,$this->uri->segment(3));
	}
}
?>