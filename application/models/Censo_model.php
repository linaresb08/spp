<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Censo_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function obtenerCensos()	{
		$query = $this->db->get('censos');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerCenso($id) {
		$this->db->where('censo_id',$id);
		$query = $this->db->get('censos');
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerCenso_por_nombre($nombre)	{
		$this->db->where('nombre',$nombre);
		$query = $this->db->get('censos'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function crearCenso($datos) {
		$this->db->insert('censos', $datos);

		$censo = $this->obtenerCenso_por_nombre($datos['nombre']);
		
		if ($censo) {
			return $censo->censo_id;
		}
		else {
			return false;
		}
	}

	public function actualizarCenso($id, $datos) {
		$this->db->set($datos);
		$this->db->where('censo_id', $id);
		$this->db->update('censos');

		$censo = $this->obtenerCenso($id);

		if ($censo) {
			return $censo->censo_id;
		}
		else {
			return false;
		}
	}

	public function eliminarCenso($id) {
		if ($this->db->delete('censos', array('censo_id' => $id))) {
			return true;
		}
		else {
			return false;
		}
	}

	public function cerrarCenso($id) {
		$this->db->set(array('estatus' => 'Cerrado'));
		$this->db->where('censo_id', $id);
		if ($this->db->update('censos')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function abrirCenso($id) {
		$this->db->set(array('estatus' => 'Abierto'));
		$this->db->where('censo_id', $id);
		if ($this->db->update('censos')) {
			return true;
		}
		else {
			return false;
		}
	}
}
?>