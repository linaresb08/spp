<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supervision_model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}

	public function obtenerSupervisiones()	{
		$query = $this->db->get('supervisiones'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerSupervision($id)	{
		$this->db->where('supervision_id',$id);
		$query = $this->db->get('supervisiones'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerSupervision_por_pasantia($pasantia_id)	{
		$this->db->where('pasantia_id',$pasantia_id);
		$query = $this->db->get('supervisiones'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerSupervisiones_por_pasantia($pasantia_id)	{
		$this->db->where('pasantia_id',$pasantia_id);
		$query = $this->db->get('supervisiones'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function crearSupervision($datos) {
		if ($this->db->insert('supervisiones', $datos)) {
			return true;
		}
		else {
			return false;
		}
	}

	public function actualizarSupervision($id, $datos) {
		$this->db->set($datos);
		$this->db->where('supervision_id', $id);
		$this->db->update('supervisiones');

		$supervision = $this->obtenerSupervision($id);

		if ($supervision) {
			return $supervision->supervision_id;
		}
		else {
			return false;
		}
	}

	public function actualizarSupervision_por_Pasantia($pasantia_id, $datos) {
		$this->db->set($datos);
		$this->db->where('pasantia_id', $pasantia_id);
		$this->db->update('supervisiones');

		$supervision = $this->obtenerSupervision_por_pasantia($pasantia_id);

		if ($supervision) {
			return $supervision->supervision_id;
		}
		else {
			return false;
		}
	}

	public function eliminarSupervision($id)	{
		$this->db->where('supervision_id',$id);
		if($this->db->delete('supervisiones')) {	
			return true;
		}
		else {
			return false;
		}
		
	}

	public function eliminarSupervision_por_pasantia($pasantia_id)	{
		$this->db->where('pasantia_id',$pasantia_id);
		$this->db->delete('supervisiones');
	}

}
?>