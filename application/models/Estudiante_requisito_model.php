<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estudiante_requisito_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function crearEstudianteRequisito($datos) {
		$this->db->insert('estudiante_requisitos', $datos);		
	}

	public function obtenerRequisito_Estudiante($id)	{
		$this->db->where('estudiante_requisito_id',$id);
		$query = $this->db->get('estudiante_requisitos');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerRequisitos_por_Estudiante($estudiante_id)	{
		$this->db->where('estudiante_id',$estudiante_id);
		$query = $this->db->get('estudiante_requisitos');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function actualizarRequisito_Estudiante($datos)	{
		$this->db->set($datos);
		$this->db->where('estudiante_requisito_id', $datos['estudiante_requisito_id']);
		
		if ($this->db->update('estudiante_requisitos')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function verificarRequisito_Estudiante($datos)	{
		$this->db->set($datos);
		$this->db->where('estudiante_requisito_id', $datos['estudiante_requisito_id']);
		
		if ($this->db->update('estudiante_requisitos')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function agregarObservacion_Requisito($datos)	{
		$this->db->set($datos);
		$this->db->where('estudiante_requisito_id', $datos['estudiante_requisito_id']);
		
		if ($this->db->update('estudiante_requisitos')) {
			return true;
		}
		else {
			return false;
		}
	}


}
?>