<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Censo_usuario_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function inscripcion($datos)	{
		if ($this->db->insert('censo_usuarios', $datos)) {
			return true;
		}
		else {
			return false;
		}
	}

	public function retiro($datos)	{
		if ($this->db->delete('censo_usuarios', $datos)) {
			return true;
		}
		else {
			return false;
		}
	}

	public function consultarInscripcion($datos)	{
		$this->db->from('censo_usuarios');
		$this->db->where('estudiante_id', $datos['estudiante_id']);
		$this->db->where('censo_id', $datos['censo_id']);

		if ($this->db->count_all_results() > 0)
			return true;
		else 
			return false;
	}

	public function inscritos($id)	{
		$this->db->where('censo_id', $id);
		$query = $this->db->get('censo_usuarios');
		return $query;
	}

	public function obtener_usuarios_de_censo($censo_id) {
    $this->db->select('*');
    $this->db->where('censo_id',$censo_id);
    $this->db->from('censo_usuarios');
    $this->db->join('usuarios', 'usuarios.usuario_id = censo_usuarios.estudiante_id');
    $this->db->join('perfiles', 'perfiles.usuario_id = usuarios.usuario_id');
    $this->db->order_by('ci', 'ASC');
    $query = $this->db->get();

    if ($query->num_rows() > 0) {
      return $query->result();
    }
    else return false;
  }
}
?>	