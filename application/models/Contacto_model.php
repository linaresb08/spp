
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto_model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}

	public function obtenerMensajes()	{
		$query = $this->db->get('contactos'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerMensaje($id)	{
		$this->db->where('contacto_id',$id);
		$query = $this->db->get('contactos'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerUltimos5()	{
		$this->db->order_by('contacto_id','DESC');
		$this->db->where('estado','No leido');
		$this->db->limit(10); 
		$query = $this->db->get('contactos');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function contarMensajes() { 	
		$this->db->where('estado','No leido');	
		$query = $this->db->get('contactos');
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		else return 0;
	}

	public function crearMensaje($mensaje) {		
		if ($this->db->insert('contactos', $mensaje)) {
			return true;
		}
		else {
			return false;
		}
	}

	public function eliminarMensaje($id)	{
		$this->db->where('contacto_id',$id);
		if ($this->db->delete('contactos')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function estadoLeido($id)	{
		$this->db->set(array('estado' => 'Leido'));
		$this->db->where('contacto_id', $id);
		$this->db->update('contactos');
	}

	public function estadoPendiente($id)	{
		$this->db->set(array('estado' => 'No leido'));
		$this->db->where('contacto_id', $id);
		$this->db->update('contactos');
	}
}
?>