<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imagen_model extends CI_Model {

	public function subir($usuario_id, $foto) {
		$datos = array(
			'foto' => $foto, 
		);
		$this->db->set($datos);
		$this->db->where('usuario_id',$usuario_id);
		return $this->db->update('perfiles');
	}

	
}
?>