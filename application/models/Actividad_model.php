<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actividad_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function obtenerActividades()	{
		$this->db->order_by('actividad_id', 'DESC');
		$query = $this->db->get('actividades'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerActividad($id)	{
		$this->db->where('actividad_id',$id);
		$query = $this->db->get('actividades'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerActividades_por_pasantia($pasantia_id)	{
		$this->db->order_by('actividad_id', 'DESC');
		$this->db->where('pasantia_id',$pasantia_id);
		$query = $this->db->get('actividades'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function crearActividad($datos, $tipo) {
		if ($this->db->insert('actividades', $datos)) {
			$pasantia = $this->Pasantia_model->obtenerPasantia($datos['pasantia_id']);

			if ($pasantia->tipo_reporte_actividad == 'Ninguna') {
				$this->Pasantia_model->actualizarPasantia($datos['pasantia_id'], array('tipo_reporte_actividad' => $tipo));
			}
			return true;
		}
		else {
			return false;
		}
	}

	public function actualizarActividad($id, $datos) {
		$this->db->set($datos);
		$this->db->where('actividad_id', $id);
		if ($this->db->update('actividades')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function actualizarEmpresa_por_Pasantia($pasantia_id, $datos) {
		$this->db->set($datos);
		$this->db->where('pasantia_id', $pasantia_id);
		$this->db->update('empresas');

		$empresa = $this->obtenerEmpresa_por_pasantia($pasantia_id);

		if ($empresa) {
			return $empresa->empresa_id;
		}
		else {
			return false;
		}
	}

	public function eliminarActividad($id)	{
		$this->db->where('actividad_id',$id);
		$this->db->delete('actividades');
	}
}
?>