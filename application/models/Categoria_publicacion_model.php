<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria_publicacion_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function crearCategoriaPublicacion($datos) {
		$this->db->insert('categoria_publicaciones', $datos);		
	}

	public function obtenerCategorias_por_Publicacion($publicacion_id)	{
		$this->db->where('publicacion_id',$publicacion_id);
		$query = $this->db->get('categoria_publicaciones');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerPublicaciones_por_Categoria($categoria_id)	{
		$this->db->where('categoria_id',$categoria_id);
		$query = $this->db->get('categoria_publicaciones');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function eliminarCategorias_Publicacion($publicacion_id)	{
		$this->db->where('publicacion_id',$publicacion_id);
		$this->db->delete('categoria_publicaciones');
	}

	public function obtener_paginacion($numero_por_pagina='')	{
		$this->db->order_by('fecha', 'DESC');
		return $this->db->get('publicaciones', $numero_por_pagina,$this->uri->segment(4));
	}

	public function cantidad_publicaciones($tipo_categoria='')	{
		$this->db->from('categoria_publicaciones');
		$this->db->where('categoria_id',$tipo_categoria);
		$num_results = $this->db->count_all_results();

		return $num_results;
	}

	public function obtener_por_categoria_paginacion($categoria_id, $numero_por_pagina='')	{
		$this->db->select('*');
		$this->db->where('categoria_id',$categoria_id);
		$this->db->from('categoria_publicaciones');
		$this->db->join('publicaciones', 'publicaciones.publicacion_id = categoria_publicaciones.publicacion_id');
		$this->db->order_by('fecha', 'DESC');
		$this->db->limit($numero_por_pagina, $this->uri->segment(4));
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}
}
?>