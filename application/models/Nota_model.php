<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nota_model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}

	public function obtenerNotas()	{
		$query = $this->db->get('notas'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerNota($id)	{
		$this->db->where('nota_id',$id);
		$query = $this->db->get('notas'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerNota_por_pasantia($pasantia_id)	{
		$this->db->where('pasantia_id',$pasantia_id);
		$query = $this->db->get('notas'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function registarNotas($datos) {
		$this->db->insert('notas', $datos);

		$nota = $this->obtenerNota_por_pasantia($datos['pasantia_id']);
		
		if ($nota) {
			if ($datos['definitiva'] >= 10 && $datos['definitiva'] <= 20) {
				$this->Pasantia_model->aprobarPasantia($datos['pasantia_id']);
			}
			else {
				$this->Pasantia_model->reprobarPasantia($datos['pasantia_id']);
			}
			return $nota->nota_id;
		}
		else {
			return false;
		}
	}

	public function actualizarNotas($pasantia_id, $datos) {
		$this->db->set($datos);
		$this->db->where('pasantia_id', $pasantia_id);
		$this->db->update('notas');

		$nota = $this->obtenerNota_por_pasantia($pasantia_id);

		if ($nota) {
			if ($datos['definitiva'] >= 10 && $datos['definitiva'] <= 20) {
				$this->Pasantia_model->aprobarPasantia($datos['pasantia_id']);
			}
			else {
				$this->Pasantia_model->reprobarPasantia($datos['pasantia_id']);
			}
			return $nota->nota_id;
		}
		else {
			return false;
		}
	}

	public function actualizarNotas_por_Pasantia($pasantia_id, $datos) {
		$this->db->set($datos);
		$this->db->where('pasantia_id', $pasantia_id);
		$this->db->update('notas');

		$nota = $this->obtenerNota_por_pasantia($pasantia_id);

		if ($nota) {
			return $nota->nota_id;
		}
		else {
			return false;
		}
	}

	public function eliminarNotas($pasantia_id)	{
		$this->db->where('pasantia_id',$pasantia_id);
		if($this->db->delete('notas')){
			return true;
		}
		else {
			return false;
		}
	}
}
?>