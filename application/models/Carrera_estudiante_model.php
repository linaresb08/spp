<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrera_estudiante_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function crearCarreraEstudiante($datos) {
		$this->db->insert('carrera_estudiantes', $datos);		
	}

	public function obtenerCarreras_por_Estudiante($estudiante_id)	{
		$this->db->where('estudiante_id',$estudiante_id);
		$query = $this->db->get('carrera_estudiantes');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function eliminarCarreras_Estudiante($estudiante_id)	{
		$this->db->where('estudiante_id',$estudiante_id);
		$this->db->delete('carrera_estudiantes');
	}

	public function asignarPasantia($datos)	{
		$this->db->set('pasantia_id',$datos['pasantia_id']);
		$this->db->where('estudiante_id',$datos['estudiante_id']);
		$this->db->where('carrera_id',$datos['carrera_id']);
		$this->db->update('carrera_estudiantes');
	}

		public function cambioCarreraPasantia($datos)	{
		$this->db->set('pasantia_id',$datos['pasantia_id']);
		$this->db->where('estudiante_id',$datos['estudiante_id']);
		$this->db->where('carrera_id',$datos['carrera_id']);
		$this->db->update('carrera_estudiantes');
	}

	public function buscarCarreras_por_pasantia($estudiante_id,$pasantia_id) {
		$this->db->where('estudiante_id',$estudiante_id);
		$this->db->where('pasantia_id',$pasantia_id);
		$query = $this->db->get('carrera_estudiantes');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}
}
?>