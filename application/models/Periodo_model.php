<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periodo_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function obtenerPeriodos()	{
		$query = $this->db->get('periodo_academicos');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerPeriodo($id) {
		$this->db->where('periodo_academico_id',$id);
		$query = $this->db->get('periodo_academicos');
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerPeriodo_por_nombre($nombre)	{
		$this->db->where('nombre',$nombre);
		$query = $this->db->get('periodo_academicos'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function crearPeriodoAcademico($datos) {
		$this->db->insert('periodo_academicos', $datos);

		$periodo = $this->obtenerPeriodo_por_nombre($datos['nombre']);
		
		if ($periodo) {
			return $periodo->periodo_academico_id;
		}
		else {
			return false;
		}
	}

	public function actualizarPeriodo($id, $datos) {
		$this->db->set($datos);
		$this->db->where('periodo_academico_id', $id);
		$this->db->update('periodo_academicos');

		$periodo = $this->obtenerPeriodo($id);

		if ($periodo) {
			return $periodo->periodo_academico_id;
		}
		else {
			return false;
		}
	}

	public function eliminarPeriodo($id) {
		if ($this->db->delete('periodo_academicos', array('periodo_academico_id' => $id))) {
			return true;
		}
		else {
			return false;
		}
	}
}
?>