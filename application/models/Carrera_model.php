<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrera_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function obtenerCarreras()	{
		$query = $this->db->get('carreras');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerCarrera($id) {
		$this->db->where('carrera_id',$id);
		$query = $this->db->get('carreras');
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerCarrera_por_nombre($nombre)	{
		$this->db->where('nombre',$nombre);
		$query = $this->db->get('carreras'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function crearCarrera($datos) {
		$this->db->insert('carreras', $datos);

		$carrera = $this->obtenerCarrera_por_nombre($datos['nombre']);
		
		if ($carrera) {
			return $carrera->carrera_id;
		}
		else {
			return false;
		}
	}

	public function actualizarCarrera($id, $datos) {
		$this->db->set($datos);
		$this->db->where('carrera_id', $id);
		$this->db->update('carreras');

		$carrera = $this->obtenerCarrera($id);

		if ($carrera) {
			return $carrera->carrera_id;
		}
		else {
			return false;
		}
	}

	public function eliminarCarrera($id) {
		if ($this->db->delete('carreras', array('carrera_id' => $id))) {
			return true;
		}
		else {
			return false;
		}
	}
}
?>