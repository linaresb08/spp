<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function obtenerUsuarios()	{
		$query = $this->db->get('usuarios'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerUsuario($id)	{
		$this->db->where('usuario_id',$id);
		$query = $this->db->get('usuarios'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerUsuario_por_nombre($nombre)	{
		$this->db->where('nombre_usuario',$nombre);
		$query = $this->db->get('usuarios'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerUsuarios_por_rol($rol)	{
		$this->db->where('role',$rol);
		$query = $this->db->get('usuarios'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function crearUsuario($datos) {
		$this->db->insert('usuarios', $datos);

		$usuario = $this->obtenerUsuario_por_nombre($datos['nombre_usuario']);
		
		if ($usuario) {
			return $usuario->usuario_id;
		}
		else {
			return false;
		}
	}

	public function actualizarUsuario($id, $datos) {
		$this->db->set($datos);
		$this->db->where('usuario_id', $id);
		$this->db->update('usuarios');

		$usuario = $this->obtenerUsuario($id);

		if ($usuario) {
			return $usuario->usuario_id;
		}
		else {
			return false;
		}
	}

	public function eliminarUsuario($id)	{
		$this->db->where('usuario_id',$id);
		if ($this->db->delete('usuarios')) {
			return TRUE;
		}
		else {
			return false;
		}
	}


	//OPERACIONES DE HABLITAR / INHABILITAR USUARIOS

	public function inhabilitarUsuario($id) {
		$this->db->set(array('estado' => 'Inhabilitado'));
		$this->db->where('usuario_id', $id);
		if ($this->db->update('usuarios')) {
			return TRUE;
		}
		else {
			return false;
		}
	}

	public function habilitarUsuario($id) {
		$this->db->set(array('estado' => 'Habilitado'));
		$this->db->where('usuario_id', $id);
		if ($this->db->update('usuarios')) {
			return TRUE;
		}
		else {
			return false;
		}
	}


	//OPERACIONES CON LAS CLAVES DEL USUARIO

	public function restablecerClave($id)	{
		//Busco el usuario
		$usuario = $this->obtenerUsuario($id);

		$password = $this->encrypt->encode($usuario->nombre_usuario);

		$this->db->set(array('clave' => $password));		
		$this->db->where('usuario_id', $id);
		if ($this->db->update('usuarios')) {
			return TRUE;
		}
		else {
			return false;
		}
	}

	public function cambiarClave($id, $nueva_clave)	{
		$this->db->set(array('clave' => $nueva_clave));
		$this->db->where('usuario_id', $id);
		$this->db->update('usuarios');

		return TRUE;
	}
}
?>