<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function obtenerEmpresas()	{
		$query = $this->db->get('empresas'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else return false;
	}

	public function obtenerEmpresa($id)	{
		$this->db->where('empresa_id',$id);
		$query = $this->db->get('empresas'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerEmpresa_por_nombre($nombre)	{
		$this->db->where('nombre',$nombre);
		$query = $this->db->get('empresas'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerEmpresa_por_rif($rif)	{
		$this->db->where('rif',$rif);
		$query = $this->db->get('empresas'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function obtenerEmpresa_por_pasantia($pasantia_id)	{
		$this->db->where('pasantia_id',$pasantia_id);
		$query = $this->db->get('empresas'); //nombre de la tabla
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else return false;
	}

	public function crearEmpresa($datos) {
		$this->db->insert('empresas', $datos);

		$empresa = $this->obtenerEmpresa_por_rif($datos['rif']);
		
		if ($empresa) {
			return $empresa->empresa_id;
		}
		else {
			return false;
		}
	}

	public function actualizarEmpresa($id, $datos) {
		$this->db->set($datos);
		$this->db->where('empresa_id', $id);
		$this->db->update('empresas');

		$empresa = $this->obtenerEmpresa($id);

		if ($empresa) {
			return $empresa->empresa_id;
		}
		else {
			return false;
		}
	}

	public function actualizarEmpresa_por_Pasantia($pasantia_id, $datos) {
		$this->db->set($datos);
		$this->db->where('pasantia_id', $pasantia_id);
		$this->db->update('empresas');

		$empresa = $this->obtenerEmpresa_por_pasantia($pasantia_id);

		if ($empresa) {
			return $empresa->empresa_id;
		}
		else {
			return false;
		}
	}

	public function eliminarEmpresa($id)	{
		$this->db->where('empresa_id',$id);
		$this->db->delete('empresas');
	}
}
?>