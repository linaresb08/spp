<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notas extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login')) 
		{
			header('location: ' . base_url() . 'dashboard/login');
		}
	}

	public function ver($pasantia_id) {
		$informacion = array(
			'seccion'     => 'Reporte de notas definitivas', 
			'controlador' => 'pasantias'
		);

		$pasantia = $this->Pasantia_model->obtenerPasantia($pasantia_id);

		$datos['tipo'] = 'nota';
		$datos['nota'] = $this->Nota_model->obtenerNota_por_pasantia($pasantia_id);		
		$datos['pasantia'] = $this->Pasantia_model->obtenerPasantia($pasantia_id);
		$datos['estudiante'] = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->estudiante_id);
		$datos['tutor_academico'] = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->tutor_academico_id);

//		print_r($datos);

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/pasantias/ver', $datos);
		$this->load->view('backend/footer');
	}

	public function registrar($pasantia_id)	{

		$informacion = array(
			'seccion'     => 'Registro de notas definitivas', 
			'controlador' => 'pasantias'
		);

		$datos['pasantia_id'] = $pasantia_id;
		$pasantia = $this->Pasantia_model->obtenerPasantia($pasantia_id);
		$datos['estudiante'] = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->estudiante_id);

		/* Reglas de validación */
		$this->form_validation->set_rules('eval_desemp_tutor_l', 'Evaluación del desempeño (Tutor Laboral)', 'required');
		$this->form_validation->set_rules('eval_informe_tutor_l', 'Evaluación del informe (Tutor Laboral)', 'required');
		$this->form_validation->set_rules('eval_desemp_tutor_a', 'Evaluación del desempeño (Tutor Académico)', 'required');
		$this->form_validation->set_rules('eval_informe_tutor_a', 'Evaluación del informe (Tutor Académico)', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/notas/registro',$datos);
			$this->load->view('backend/footer');
		}
		else {

			$ev_de_tl = $this->input->post('eval_desemp_tutor_l');
			$ev_in_tl = $this->input->post('eval_informe_tutor_l');
			$ev_de_ta = $this->input->post('eval_desemp_tutor_a');
			$ev_in_ta = $this->input->post('eval_informe_tutor_a');

			$nota_final_tutor_l = ($ev_de_tl * 0.7) + ($ev_in_tl * 0.3);
			$nota_final_tutor_a = ($ev_de_ta * 0.7) + ($ev_in_ta * 0.3);
			$definitiva = ($nota_final_tutor_l + $nota_final_tutor_a) / 2;

			$nota = array(
				'eval_desemp_tutor_l'  => $ev_de_tl,
				'eval_informe_tutor_l' => $ev_in_tl,
				'eval_desemp_tutor_a'  => $ev_de_ta,
				'eval_informe_tutor_a' => $ev_in_ta,
				'nota_final_tutor_l'   => $nota_final_tutor_l,
				'nota_final_tutor_a'   => $nota_final_tutor_a,
				'definitiva'           => $definitiva,
				'pasantia_id'          => $this->input->post('pasantia_id')
			);

//			print_r($nota);

			$nota_id = $this->Nota_model->registarNotas($nota);

			if ($nota_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Notas registradas');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al registrar notas');
				$this->session->set_flashdata('color','alert-danger');			
			}	
			header('Location: ' . base_url() . 'pasantias'); 
		}
	}


	public function editar($pasantia_id = false)	{
		
		$informacion = array(
			'seccion'     => 'Actualización de notas',
			'controlador' => 'pasantias'
		);

		$pasantia = $this->Pasantia_model->obtenerPasantia($pasantia_id);

		$datos['pasantia_id'] = $pasantia_id;
		$datos['nota'] = $this->Nota_model->obtenerNota_por_pasantia($pasantia_id);		
		$datos['estudiante'] = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->estudiante_id);

		/* Reglas de validación */
		$this->form_validation->set_rules('eval_desemp_tutor_l', 'Evaluación del desempeño (Tutor Laboral)', 'required');
		$this->form_validation->set_rules('eval_informe_tutor_l', 'Evaluación del informe (Tutor Laboral)', 'required');
		$this->form_validation->set_rules('eval_desemp_tutor_a', 'Evaluación del desempeño (Tutor Académico)', 'required');
		$this->form_validation->set_rules('eval_informe_tutor_a', 'Evaluación del informe (Tutor Académico)', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/notas/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$ev_de_tl = $this->input->post('eval_desemp_tutor_l');
			$ev_in_tl = $this->input->post('eval_informe_tutor_l');
			$ev_de_ta = $this->input->post('eval_desemp_tutor_a');
			$ev_in_ta = $this->input->post('eval_informe_tutor_a');

			$nota_final_tutor_l = ($ev_de_tl * 0.7) + ($ev_in_tl * 0.3);
			$nota_final_tutor_a = ($ev_de_ta * 0.7) + ($ev_in_ta * 0.3);
			$definitiva = ($nota_final_tutor_l + $nota_final_tutor_a) / 2;

			$nota = array(
				'eval_desemp_tutor_l'  => $ev_de_tl,
				'eval_informe_tutor_l' => $ev_in_tl,
				'eval_desemp_tutor_a'  => $ev_de_ta,
				'eval_informe_tutor_a' => $ev_in_ta,
				'nota_final_tutor_l'   => $nota_final_tutor_l,
				'nota_final_tutor_a'   => $nota_final_tutor_a,
				'definitiva'           => $definitiva,
				'pasantia_id'          => $this->input->post('pasantia_id')
			);

//			print_r($nota);

			$nota_id = $this->Nota_model->actualizarNotas($pasantia_id, $nota);

			if ($nota_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Notas actualizadas');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar notas');
				$this->session->set_flashdata('color','alert-danger');			
			}	
			header('Location: ' . base_url() . 'pasantias'); 
		}
	}

	public function eliminar($pasantia_id)	{
		if($this->Nota_model->eliminarNotas($pasantia_id)) {
			$this->Pasantia_model->habilitarPasantia($pasantia_id);
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Notas eliminadas');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar notas');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'pasantias');
	}
}
?>