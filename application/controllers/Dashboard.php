<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()	{
		parent::__construct();

		if (($this->uri->segment(2) != 'registrar' && $this->uri->segment(2) != 'login' && $this->uri->segment(2) !=  'iniciar_sesion') && !$this->session->userdata('login')) 
		{
			header('location: ' . base_url() . 'dashboard/login');
		}
	}

	public function index() {

		$informacion = array(
			'seccion'     => 'Panel de control', 
			'controlador' => 'dashboard'
		);
		
		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/content');
		$this->load->view('backend/footer');
	}

	public function login() {
		if(!$this->session->userdata('login')) {
			$this->load->view('sistema/login');
		}
		else {
			header('Location: ' . base_url() . 'dashboard');
		}
		
	}

	public function iniciar_sesion()	{
		$usuario = $this->Usuario_model->obtenerUsuario_por_nombre($this->input->post('nombre_usuario'));
		if ($this->encrypt->decode($usuario->clave) === $this->input->post('clave') && $usuario->estado == 'Habilitado') {
			$perfil = $this->Perfil_model->obtenerPerfil_usuario_id($usuario->usuario_id);
			$datos = array(
				'id' => $perfil->perfil_id, 
				'rol' => $usuario->rol,
				'login' => true
				);

			$this->session->set_userdata($datos);

			header('Location: ' . base_url() . 'dashboard');
		}
		else {
			header('Location: ' . base_url() . 'dashboard/login');
		}
	}

	public function registrar() {
		
		/* Reglas de validación */
		$this->form_validation->set_rules('primer_nombre', 'Primer nombre', 'required');
		$this->form_validation->set_rules('primer_apellido', 'Primer apellido', 'required');
		$this->form_validation->set_rules('ci', 'Cédula de Identidad', 'required|callback_confirmar_existencia');
		$this->form_validation->set_rules('clave', 'Clave', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('sistema/registro-estudiantes');
		}
		else {
			$password = $this->encrypt->encode($this->input->post('clave'));

			//Información para la tabla de usuarios
			$usuario = array(
				'nombre_usuario'	=> $this->input->post('ci'),
				'clave'						=> $password,
				'rol'							=> 'Estudiante',
				'estado'					=> 'Habilitado'
			);

			$usuario_id = $this->Usuario_model->crearUsuario($usuario);

			//Información para la tabla de perfiles
			$perfil = array(
				'usuario_id'       => $usuario_id,
				'nacionalidad'     => $this->input->post('nacionalidad'),
				'ci'               => $this->input->post('ci'),
				'primer_nombre'    => ucwords($this->input->post('primer_nombre')),
				'segundo_nombre'   => ucwords($this->input->post('segundo_nombre')),				
				'primer_apellido'  => ucwords($this->input->post('primer_apellido')),
				'segundo_apellido' => ucwords($this->input->post('segundo_apellido'))
			);

			$perfil_id = $this->Perfil_model->crearPerfil($perfil);

			if ($perfil_id) {
				foreach ($this->input->post('carreras') as $carrera_id) {
					$datos = array(
						'estudiante_id' => $usuario_id,
						'carrera_id'    => $carrera_id
					);
					$this->Carrera_estudiante_model->crearCarreraEstudiante($datos);
				}
			}

			/* 
				EN ESTUDIANTE SE CREA LA RELACIÓN:
				ESTUDIANTE_REQUISITOS
			*/
			$requisitos = $this->Requisito_model->obtenerRequisitos();
			foreach ($requisitos as $requisito) {
				$datos = array(
					'requisito_id'  => $requisito->requisito_id,
					'estudiante_id' => $usuario_id,
					'estatus'       => 'Sin adjuntar'
				);
				$this->Estudiante_requisito_model->crearEstudianteRequisito($datos);
			}
			
			/* 
				INICIAR SESIÓN
			*/

			$datos = array(
				'id'    => $perfil_id, 
				'rol'   => 'Estudiante',
				'login' => true
				);

			$this->session->set_userdata($datos);

			/* Mensaje de bienvenido */
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Registro exitoso');
			$this->session->set_flashdata('color','alert-success');

			header('Location: ' . base_url() . 'dashboard');
		}
	}

	public function salir()
	{
		$array_sesiones = array(
			'id'    => '', 
			'rol'   => '',
			'login' => ''
		);
		$this->session->unset_userdata($array_sesiones);
		$this->session->sess_destroy();
		header("location: " . base_url() . 'dashboard/login');
	}

	public function confirmar_existencia($ci='') {
		$usuario = $this->Usuario_model->obtenerUsuario_por_nombre($ci);

		if ($usuario) {
			$this->form_validation->set_message('confirmar_existencia', 'Ya existe un usuario con esa cedula');
      return FALSE;
		}else {
			return TRUE;
		}
	}
}
?>