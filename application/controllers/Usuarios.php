<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login')) 
		{
			header('location: ' . base_url() . 'dashboard/login');
		}

		$this->comprobarRol(array("Administrador", "Coordinador"));
	}

	public function index($id = false) {

		$informacion = array(
			'seccion'     => 'Gestor de usuarios', 
			'controlador' => 'usuarios'
		);

		$datos['segmento'] = $id;
		
		if (!$id) {
			$datos['usuarios'] = $this->Usuario_model->obtenerUsuarios();
		}
		else {
			$datos['usuario'] = $this->Usuario_model->obtenerUsuario($id);
			$datos['perfil'] = $this->Perfil_model->obtenerPerfil_usuario_id($id);
		}

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/usuarios/listar', $datos);
		$this->load->view('backend/footer');
	}

	public function nuevo()	{

		$informacion = array(
			'seccion'     => 'Nuevo usuario', 
			'controlador' => 'usuarios'
		);

		/* Reglas de validación */
		$this->form_validation->set_rules('primer_nombre', 'Primer nombre', 'required');
		$this->form_validation->set_rules('primer_apellido', 'Primer apellido', 'required');
		$this->form_validation->set_rules('ci', 'Cédula de Identidad', 'required|callback_confirmar_existencia');
		$this->form_validation->set_rules('clave', 'Clave', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/usuarios/registro');
			$this->load->view('backend/footer');
		}
		else {
			$password = $this->encrypt->encode($this->input->post('clave'));

			$usuario = array(
				# 'NombreQueEnvíaAlModelo' => $this->input->post('NombreVariableQueSeTraeDeRegistro'),
				'nombre_usuario'	=> $this->input->post('ci'),
				'clave'						=> $password,
				'rol'							=> $this->input->post('rol'),
				'estado'					=> 'Habilitado'
			);

			$usuario_id = $this->Usuario_model->crearUsuario($usuario);

			$perfil = array(
				'usuario_id'			=> $usuario_id,
				'nacionalidad'		=> $this->input->post('nacionalidad'),
				'ci'							=> $this->input->post('ci'),
				'primer_nombre'		=> ucwords($this->input->post('primer_nombre')),
				'primer_apellido'	=> ucwords($this->input->post('primer_apellido'))
			);

			$perfil_id = $this->Perfil_model->crearPerfil($perfil);

			if ($perfil_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Usuario registrado');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al registrar usuario');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'usuarios');
		}
	}

	public function nuevoEstudiante()	{

		$informacion = array(
			'seccion'     => 'Nuevo estudiante', 
			'controlador' => 'usuarios'
		);

		/* Reglas de validación */
		$this->form_validation->set_rules('primer_nombre', 'Primer nombre', 'required');
		$this->form_validation->set_rules('primer_apellido', 'Primer apellido', 'required');
		$this->form_validation->set_rules('ci', 'Cédula de Identidad', 'required|callback_confirmar_existencia');
		$this->form_validation->set_rules('clave', 'Clave', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/usuarios/registro-estudiante');
			$this->load->view('backend/footer');
		}
		else {
			$password = $this->encrypt->encode($this->input->post('clave'));

			$usuario = array(
				'nombre_usuario'	=> $this->input->post('ci'),
				'clave'						=> $password,
				'rol'							=> 'Estudiante',
				'estado'					=> 'Habilitado'
			);

			$usuario_id = $this->Usuario_model->crearUsuario($usuario);

			$perfil = array(
				'usuario_id'			=> $usuario_id,
				'nacionalidad'		=> $this->input->post('nacionalidad'),
				'ci'							=> $this->input->post('ci'),
				'primer_nombre'		=> ucwords($this->input->post('primer_nombre')),
				'primer_apellido'	=> ucwords($this->input->post('primer_apellido'))
			);

			$perfil_id = $this->Perfil_model->crearPerfil($perfil);

			/* 
				EN ESTUDIANTE SE CREA LA RELACIÓN:
				ESTUDIANTE_REQUISITOS
			*/

			if ($usuario['rol'] == 'Estudiante') {
				$requisitos = $this->Requisito_model->obtenerRequisitos();
				foreach ($requisitos as $requisito) {
					$datos = array(
						'requisito_id'  => $requisito->requisito_id,
						'estudiante_id' => $usuario_id,
						'estatus'       => 'Sin adjuntar'
					);
					$this->Estudiante_requisito_model->crearEstudianteRequisito($datos);
				}
			}

			if ($perfil_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Estudiante registrado');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al registrar estudiante');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'usuarios');
		}
	}

	public function editar($id = false)	{
		
		$informacion = array(
			'seccion'     => 'Actualización de usuario', 
			'controlador' => 'usuarios'
		);

		$datos['id'] = $id;
		$datos['usuario'] = $this->Usuario_model->obtenerUsuario($id);
		$datos['perfil'] = $this->Perfil_model->obtenerPerfil_usuario_id($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('primer_nombre', 'Primer nombre', 'required');
		$this->form_validation->set_rules('primer_apellido', 'Primer apellido', 'required');
		$this->form_validation->set_rules('ci', 'Cédula de Identidad', 'required');
		$this->form_validation->set_rules('clave', 'Clave', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/usuarios/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$password = $this->encrypt->encode($this->input->post('clave'));

			$usuario = array(
				'nombre_usuario'	=> $this->input->post('ci'),
				'clave'						=> $password,
				'rol'							=> $this->input->post('rol'),
				'estado'					=> $this->input->post('estado')
			);

			$usuario_id = $this->Usuario_model->actualizarUsuario($id, $usuario);

			$perfil = array(
				'usuario_id'			=> $usuario_id,
				'nacionalidad'		=> $this->input->post('nacionalidad'),
				'ci'							=> $this->input->post('ci'),
				'primer_nombre'		=> ucwords($this->input->post('primer_nombre')),
				'primer_apellido'	=> ucwords($this->input->post('primer_apellido'))
			);

			$perfil_id = $this->Perfil_model->actualizarPerfil($perfil);

			if ($perfil_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Usuario actualizado');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar usuario');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'usuarios');
		}
	}

	public function editarEstudiante($id = false)	{
		
		$informacion = array('seccion' => 'Actualización de estudiante', 'controlador' => 'usuarios');

		$datos['id'] = $id;
		$datos['usuario'] = $this->Usuario_model->obtenerUsuario($id);
		$datos['perfil'] = $this->Perfil_model->obtenerPerfil_usuario_id($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('primer_nombre', 'Primer nombre', 'required');
		$this->form_validation->set_rules('primer_apellido', 'Primer apellido', 'required');
		$this->form_validation->set_rules('ci', 'Cédula de Identidad', 'required');
		$this->form_validation->set_rules('clave', 'Clave', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/usuarios/editar-estudiante', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$password = $this->encrypt->encode($this->input->post('clave'));

			$usuario = array(
				'nombre_usuario'	=> $this->input->post('ci'),
				'clave'						=> $password,
				'rol'							=> $this->input->post('rol'),
				'estado'					=> $this->input->post('estado')
			);

			$usuario_id = $this->Usuario_model->actualizarUsuario($id, $usuario);

			$perfil = array(
				'usuario_id'			=> $usuario_id,
				'nacionalidad'		=> $this->input->post('nacionalidad'),
				'ci'							=> $this->input->post('ci'),
				'primer_nombre'		=> ucwords($this->input->post('primer_nombre')),
				'primer_apellido'	=> ucwords($this->input->post('primer_apellido'))
			);

			$perfil_id = $this->Perfil_model->actualizarPerfil($perfil);

			if ($perfil_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Estudiante actualizado');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar estudiante');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'usuarios');
		}
	}

	public function eliminar($id)	{
		if ($this->Usuario_model->eliminarUsuario($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Usuario eliminado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar usuario');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'usuarios');
	}

	public function inhabilitar($id)	{
		if ($this->Usuario_model->inhabilitarUsuario($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Usuario inhabilitado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al inhabilitar usuario');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'usuarios');
	}

	public function habilitar($id)	{
		if ($this->Usuario_model->habilitarUsuario($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Usuario habilitado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al habilitar usuario');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'usuarios');
	}

	public function restablecer($id)	{
		if ($this->Usuario_model->restablecerClave($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Clave restablecida');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al restablecer clave');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'usuarios');
	}

	public function cambioClave()	{

		/*	TIPOS DE MENSAJES DE ALERTA

			0 - No hay mensaje
			1 - Nueva clave es diferente de la clave de confirmación
			2 - Clave cambiada exitosamente

		*/

		$informacion = array('seccion' => 'Cambio de clave', 'controlador' => 'usuarios/cambioClave');

		/* Reglas de validación */
		$this->form_validation->set_rules('clave', 'Clave actual', 'required');
		$this->form_validation->set_rules('nueva_clave', 'Nueva clave', 'required');
		$this->form_validation->set_rules('confirm_nueva_clave', 'Confirmar nueva clave', 'required');


		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerida');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar', $informacion);
			$this->load->view('sistema/usuarios/cambio-clave');
			$this->load->view('backend/footer');
		}
		else {
			/* Encriptación de la clave */
		//	$password = $this->encrypt->encode($this->input->post('clave'));

			// $id almacena la ID del usuario a modificar la clave
			$id = $this->perfil_session->usuario_id;
			$usuario = $this->Usuario_model->obtenerUsuario($id);

			//Verifica que la clave del usuario almacenada sea diferente a la clave actual ingresada en el formulario
			if ( password_verify( $this->input->post('clave'), $usuario->clave ) /*$password*/ ) {
				/* Mensaje de error */
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> <strong>ERROR:</strong> ¡Clave actual inválida!');
				$this->session->set_flashdata('color','alert-danger');

				$this->load->view('backend/head');
				$this->load->view('backend/sidebar');
				$this->load->view('backend/navbar', $informacion);
				$this->load->view('sistema/usuarios/cambio-clave');
				$this->load->view('backend/footer');
			}
			else {
				//Verifica que la nueva clave sea diferente a la confirmación de la nueva clave
				if ( $this->input->post('nueva_clave') != $this->input->post('confirm_nueva_clave') ) {

					/* Mensaje de error */
					$this->session->set_flashdata('status','<i class="fa fa-times"></i> <strong>ERROR:</strong> ¡La clave de confirmación es diferente a la nueva clave ingresada!');
					$this->session->set_flashdata('color','alert-danger');

					$this->load->view('backend/head');
					$this->load->view('backend/sidebar');
					$this->load->view('backend/navbar', $informacion);
					$this->load->view('sistema/usuarios/cambio-clave');
					$this->load->view('backend/footer');
				}
				else{
					$nueva_clave = $this->encrypt->encode($this->input->post('nueva_clave'));

					if ( $this->Usuario_model->cambiarClave($id, $nueva_clave) ) {
						/* Mensaje */
						$this->session->set_flashdata('status','<i class="fa fa-check"></i> ¡La clave fue cambiada exitosamente!');
						$this->session->set_flashdata('color','alert-success');
						$this->load->view('backend/head');
						$this->load->view('backend/sidebar');
						$this->load->view('backend/navbar', $informacion);
						$this->load->view('sistema/usuarios/cambio-clave');
						$this->load->view('backend/footer');
					}
				}
			}
		}
	}

	public function confirmar_existencia($ci='') {
		$usuario = $this->Usuario_model->obtenerUsuario_por_nombre($ci);

		if ($usuario) {
			$this->form_validation->set_message('confirmar_existencia', 'Ya existe un usuario con esa cedula');
      return FALSE;
		}else {
			return TRUE;
		}
	}

}
?>