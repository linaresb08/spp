<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresas extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login')) 
		{
			header('location: ' . base_url() . 'dashboard/login');
		}

		$this->comprobarRol(array("Administrador", "Coordinador"));
	}

	public function index($id = false) {

		$informacion = array(
			'seccion'     => 'Empresas registradas', 
			'controlador' => 'empresas'
		);

		$datos['segmento'] = $id;
		
		if (!$id) {
			$datos['empresas'] = $this->Empresa_model->obtenerEmpresas();
		}
		else {
			$datos['empresa'] = $this->Empresa_model->obtenerEmpresa($id);
		}

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/empresas/listar', $datos);
		$this->load->view('backend/footer');
	}

	public function nueva()	{

		$informacion = array( 
			'seccion'     => 'Nueva empresa', 
			'controlador' => 'empresas'
		);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre de la empresa', 'required');
		$this->form_validation->set_rules('rif', 'RIF', 'required');
		$this->form_validation->set_rules('direccion', 'Direccion', 'required');
		$this->form_validation->set_rules('telefono1', 'Teléfono 1', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/empresas/registro');
			$this->load->view('backend/footer');
		}
		else {
			$empresa = array(
				'nombre'       => $this->input->post('nombre'),
				'rif'          => $this->input->post('rif'),
				'direccion'    => $this->input->post('direccion'),
				'telefono1'    => $this->input->post('telefono1'),
				'telefono2'    => $this->input->post('telefono2'),
				'jr_apellidos' => ucwords($this->input->post('jr_apellidos')),
				'jr_nombres'   => ucwords($this->input->post('jr_nombres')),
				'jr_correo'    => strtolower($this->input->post('jr_correo')),
				'jr_telefono'  => $this->input->post('jr_telefono')
			);

			$empresa_id = $this->Empresa_model->crearEmpresa($empresa);

			header('Location: ' . base_url() . 'empresas');
		}
	}


	public function editar($id = false)	{
		
		$informacion = array(
			'seccion'     => 'Actualización de empresa',
			'controlador' => 'empresas'
		);

		$datos['id'] = $id;
		$datos['empresa'] = $this->Empresa_model->obtenerEmpresa($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre de la empresa', 'required');
		$this->form_validation->set_rules('rif', 'RIF', 'required');
		$this->form_validation->set_rules('direccion', 'Direccion', 'required');
		$this->form_validation->set_rules('telefono1', 'Teléfono 1', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/empresas/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$empresa = array(
				'nombre'       => $this->input->post('nombre'),
				'rif'          => $this->input->post('rif'),
				'direccion'    => $this->input->post('direccion'),
				'telefono1'    => $this->input->post('telefono1'),
				'telefono2'    => $this->input->post('telefono2'),
				'jr_apellidos' => ucwords($this->input->post('jr_apellidos')),
				'jr_nombres'   => ucwords($this->input->post('jr_nombres')),
				'jr_correo'    => strtolower($this->input->post('jr_correo')),
				'jr_telefono'  => $this->input->post('jr_telefono')
			);

			$empresa_id = $this->Empresa_model->actualizarEmpresa($id, $empresa);

			header('Location: ' . base_url() . 'empresas');
		}
	}

	public function eliminar($id)	{
		$this->Empresa_model->eliminarEmpresa($id);
		header('Location: ' . base_url() . 'empresas');
	}
}
?>