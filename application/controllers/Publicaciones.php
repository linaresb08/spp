<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publicaciones extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login')) {
			header('location: ' . base_url() . 'dashboard/login');
		}

		$this->comprobarRol(array("Administrador", "Coordinador"));
	}

	public function index($id = false) {

		$informacion = array('seccion' => 'Gestor de publicaciones', 'controlador' => 'publicaciones');

		$datos['segmento'] = $id;
		
		if (!$id) {
			$datos['publicaciones'] = $this->Publicacion_model->obtenerPublicaciones();
		}
		else{
			$datos['publicacion'] = $this->Publicacion_model->obtenerPublicacion($id);
		}

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/publicaciones/listar', $datos);
		$this->load->view('backend/footer');
	}

	public function nueva()	{

		$informacion = array('seccion' => 'Nueva publicación', 'controlador' => 'publicaciones');

		/* Reglas de validación */
		$this->form_validation->set_rules('titulo', 'Título', 'required|is_unique[publicaciones.titulo]');
		$this->form_validation->set_rules('descripcion', 'Descripción', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');
		$this->form_validation->set_message('is_unique', '%s ya está en uso');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/publicaciones/registro');
			$this->load->view('backend/footer');
		}
		else {
			$publicacion = array(
				'foto'        => $this->input->post('foto'),
				'fecha'       => date("Y-m-d H:i:s"),
				'titulo'      => ucfirst($this->input->post('titulo')),
				'descripcion' => ucfirst($this->input->post('descripcion')),
				'usuario_id'  => $this->perfil_session->usuario_id
			);
			$publicacion_id = $this->Publicacion_model->crearPublicacion($publicacion);
			
			if ($publicacion_id) {
				foreach ($this->input->post('categorias') as $categoria_id) {
					$datos = array(
						'publicacion_id' => $publicacion_id,
						'categoria_id' => $categoria_id
					);
					$this->Categoria_publicacion_model->crearCategoriaPublicacion($datos);
				}

				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Publicado correctamente');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al publicar');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'publicaciones/index/' . $publicacion_id);
		}
	}

	public function nuevoDocumento()	{

		$informacion = array('seccion' => 'Nuevo documento', 'controlador' => 'publicaciones');

		/* Reglas de validación */
		$this->form_validation->set_rules('titulo', 'Título', 'required|is_unique[publicaciones.titulo]');
		$this->form_validation->set_rules('archivo', 'Archivo', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');
		$this->form_validation->set_message('is_unique', '%s del documento ya está en uso');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/publicaciones/registro-doc');
			$this->load->view('backend/footer');
		}
		else {
			$publicacion = array(
				'fecha'      => date("Y-m-d H:i:s"),
				'titulo'     => ucfirst($this->input->post('titulo')),
				'archivo'    => $this->input->post('archivo'),
				'usuario_id' => $this->perfil_session->usuario_id,
				'tipo'       => $this->input->post('tipo')
			);
			$publicacion_id = $this->Publicacion_model->crearPublicacion($publicacion);
			
			if ($publicacion_id) {
				$datos = array(
					'publicacion_id' => $publicacion_id,
					'categoria_id'   => 3
				);

				$this->Categoria_publicacion_model->crearCategoriaPublicacion($datos);
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Registrado correctamente');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al registrar');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'publicaciones');
		}
	}

	public function editar($id)	{

		$informacion = array('seccion' => 'Actualización de publicación', 'controlador' => 'publicaciones');

		/* Búsqueda de los datos */
		$datos['id'] = $id;
		$datos['publicacion'] = $this->Publicacion_model->obtenerPublicacion($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('titulo', 'Título', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/publicaciones/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$publicacion = array(
				'foto'        => $this->input->post('foto'),
				'fecha'       => $this->input->post('fecha'),
				'titulo'      => ucfirst($this->input->post('titulo')),
				'descripcion' => ucfirst($this->input->post('descripcion')),
				'archivo'     => $this->input->post('archivo'),
				'usuario_id'  => $this->perfil_session->usuario_id,
				'tipo'        => $this->input->post('tipo')
			);

			$publicacion_id = $this->Publicacion_model->actualizarPublicacion($id,$publicacion);

			$this->Categoria_publicacion_model->eliminarCategorias_Publicacion($publicacion_id);

			if ($publicacion_id) {
				foreach ($this->input->post('categorias') as $categoria_id) {
					$datos = array(
						'publicacion_id' => $publicacion_id,
						'categoria_id' => $categoria_id
					);
					$this->Categoria_publicacion_model->crearCategoriaPublicacion($datos);
				}
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Editado correctamente');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al editar');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'publicaciones/index/' . $publicacion_id);
		}
	}

	public function editarDocumento($id)	{

		$informacion = array('seccion' => 'Actualización de documento', 'controlador' => 'publicaciones');

		/* Búsqueda de los datos */
		$datos['id'] = $id;
		$datos['publicacion'] = $this->Publicacion_model->obtenerPublicacion($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('titulo', 'Título', 'required');
		$this->form_validation->set_rules('archivo', 'Archivo', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/publicaciones/editar-doc', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$publicacion = array(
				'fecha'      => $this->input->post('fecha'),
				'titulo'     => ucfirst($this->input->post('titulo')),
				'archivo'    => $this->input->post('archivo'),
				'usuario_id' => $this->perfil_session->usuario_id,
				'tipo'       => $this->input->post('tipo')
			);

			$publicacion_id = $this->Publicacion_model->actualizarPublicacion($id,$publicacion);

			$this->Categoria_publicacion_model->eliminarCategorias_Publicacion($publicacion_id);

			if ($publicacion_id) {
				$datos = array(
					'publicacion_id' => $publicacion_id,
					'categoria_id'   => 3
				);
				$this->Categoria_publicacion_model->crearCategoriaPublicacion($datos);
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Editado correctamente');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al editar');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'publicaciones/index/' . $publicacion_id);
		}
	}

	public function eliminar($id)	{
		if ($this->Publicacion_model->eliminarPublicacion($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Eliminado correctamente');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'publicaciones');
	}
}
?>