<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informaciones extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login')) 
		{
			header('location: ' . base_url() . 'dashboard/login');
		}
		$this->comprobarRol(array("Administrador", "Coordinador"));
	}

	public function editar($id) {
		
		$informacion = array(
			'seccion'     => 'Información de contacto', 
			'controlador' => 'informaciones/editar/1'
		);
		$datos['id'] = $id;
		$datos['informacion'] = $this->Informacion_model->obtenerInformacion($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('descripcion_breve', 'Descripción', 'required');
/*		$this->form_validation->set_rules('mision', 'Misión', 'required');
		$this->form_validation->set_rules('vision', 'Visión', 'required');
*/		$this->form_validation->set_rules('numeros_telefonicos', 'Números telefónicos', 'required');
		$this->form_validation->set_rules('redes_sociales', 'Redes sociales', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/informacion/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$informacion = array(
				'descripcion_breve' => ucfirst($this->input->post('descripcion_breve')), 
				'descripcion_general' => ucfirst($this->input->post('descripcion_general')),
				'mision' => ucfirst($this->input->post('mision')),
				'vision' => ucfirst($this->input->post('vision')),
				'objetivos' => ucfirst($this->input->post('objetivos')),				
				'numeros_telefonicos' => $this->input->post('numeros_telefonicos'),
				'ubicacion' => ucfirst($this->input->post('ubicacion')),
				'redes_sociales' => $this->input->post('redes_sociales'),
				'horario' => ucfirst($this->input->post('horario'))
			);


			$informacion_id = $this->Informacion_model->actualizarInformacion($id, $informacion);

			if($informacion_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Información actualizada');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar información');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'informaciones/editar/' . $informacion_id);
		}
	}
}
?>