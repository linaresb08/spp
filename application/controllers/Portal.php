<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {

	public function index() {
		$datos['informacion'] = $this->Informacion_model->obtenerInformacion(1);
		
		$this->load->view('portal/layout/head');
		$this->load->view('portal/layout/navigation');
		$this->load->view('portal/content',$datos);
		$this->load->view('portal/layout/footer');
	}

	public function contacto()	{
		$informacion = array('seccion' => 'Información de contacto');

		$datos['informacion'] = $this->Informacion_model->obtenerInformacion(1);

		$this->load->view('portal/layout/head');
		$this->load->view('portal/layout/navigation');
		$this->load->view('portal/layout/section-title', $informacion);
		$this->load->view('portal/contactos/contacto',$datos);
		$this->load->view('portal/layout/footer');
	}

	public function enviarMensaje() {
		$mensaje = array(
			'correo'      => $this->input->post('correo'),
			'asunto'      => ucfirst($this->input->post('asunto')),
			'descripcion' => ucfirst($this->input->post('descripcion')),
			'estado'      => 'No leido'
		);

		//	print_r($mensaje);

		if ($this->Contacto_model->crearMensaje($mensaje)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Mensaje se enviado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al enviar mensaje');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'portal/contacto');
	}

	public function documentos()	{
		$informacion = array('seccion' => 'Documentos');

		//Busco la categoría 'Documentos'
		$categoria = $this->Categoria_model->obtenerCategoria_por_nombre('Documentos');

		//Almaceno el ID de la categoría, para pasarla a la vista
		$datos['id'] = $categoria->categoria_id;

		$this->load->view('portal/layout/head');
		$this->load->view('portal/layout/navigation');
		$this->load->view('portal/layout/section-title', $informacion);
		$this->load->view('portal/documentos/listar', $datos);
		$this->load->view('portal/layout/footer');
	}

	public function publicaciones() {
		$this->load->library('pagination');
		
		$config['base_url'] = base_url() . 'portal/publicaciones/';
		$config['total_rows'] = $this->db->count_all('publicaciones');
		$config['per_page'] = 8;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<ul id="pagination" class="pagination justify-content-center mb-4">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = false;
		$config['last_link'] = false;
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
				
		$this->pagination->initialize($config);

		$publicaciones = $this->Publicacion_model->obtener_paginacion($config['per_page']);

		$paginacion = $this->pagination->create_links();

		$informacion = array('seccion' => 'Publicaciones');

		//Busco todas las publicaciones
		$datos['publicaciones'] = $publicaciones;
		$datos['paginacion'] = $paginacion;
		//$datos['publicaciones'] = $this->Publicacion_model->obtenerPublicaciones();

		$this->load->view('portal/layout/head');
		$this->load->view('portal/layout/navigation');
		$this->load->view('portal/layout/section-title', $informacion);
		$this->load->view('portal/publicaciones/listar', $datos);
		$this->load->view('portal/layout/footer');
	}

	public function publicacion($id) {
		//Busco la publicación
		$datos['publicacion'] = $this->Publicacion_model->obtenerPublicacion($id);

		//Envío el título de la entrada
		$informacion = array('seccion' => $this->Publicacion_model->obtenerPublicacion($id)->titulo);

		$this->load->view('portal/layout/head');
		$this->load->view('portal/layout/navigation');
		$this->load->view('portal/layout/section-title', $informacion);
		$this->load->view('portal/publicaciones/detalle', $datos);
		$this->load->view('portal/layout/footer');
	}

	public function categoria($id)	{
		$this->load->library('pagination');
		
		$config['base_url'] = base_url() . 'portal/categoria/' . $id . '/';
		$config['total_rows'] = $this->Categoria_publicacion_model->cantidad_publicaciones($id);
		$config['per_page'] = 8;
		$config['uri_segment'] = 4;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<ul id="pagination" class="pagination justify-content-center mb-4">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = false;
		$config['last_link'] = false;
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
				
		$this->pagination->initialize($config);

		//$datos['publicaciones_categoria'] = $this->Publicacion_model->obtener_paginacion($config['per_page']);
		$datos['publicaciones_categoria'] = $this->Categoria_publicacion_model->obtener_por_categoria_paginacion($id, $config['per_page']);
		$datos['paginacion'] = $this->pagination->create_links();

		$informacion = array('seccion' => $this->Categoria_model->obtenerCategoria($id)->nombre);


		// Busca todas las publicaciones de esa categoría
		//$datos['publicaciones_categoria'] = $this->Categoria_publicacion_model->obtenerPublicaciones_por_Categoria($id);
		
		if ($id != 3) {
			$this->load->view('portal/layout/head');
			$this->load->view('portal/layout/navigation');
			$this->load->view('portal/layout/section-title', $informacion);
			$this->load->view('portal/categorias/listar', $datos);
			$this->load->view('portal/layout/footer');
		}
		else {
			header('Location: ' . base_url() . 'portal/documentos');
		}
	}

}
?>