
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactos extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login'))	{
			header('location: ' . base_url() . 'dashboard/login');
		}

		$this->comprobarRol(array("Administrador", "Coordinador"));
	}

	public function index()	{

		$informacion = array(
			'seccion'     => 'Bandeja de mensajes', 
			'controlador' => 'contactos'
		);

		$datos['mensajes'] = $this->Contacto_model->obtenerMensajes();

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/contactos/mensajes', $datos);
		$this->load->view('backend/footer');
	}

	public function obtener()	{
		$id      = $this->input->post('id');
		$mensaje = $this->Contacto_model->obtenerMensaje($id);

		echo json_encode($mensaje);
	}

	public function eliminar($id)	{
		if ($this->Contacto_model->eliminarMensaje($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> El mensaje se eliminó correctamente');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar mensaje');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'contactos');
	}

	public function leido($id) {
		$this->Contacto_model->estadoLeido($id);
		header('Location: ' . base_url() . 'contactos');
	}

	public function pendiente($id) {
		$this->Contacto_model->estadoPendiente($id);
		header('Location: ' . base_url() . 'contactos');
	}
}
?>