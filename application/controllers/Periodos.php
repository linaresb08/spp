<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periodos extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login')) 
		{
			header('location: ' . base_url() . 'dashboard/login');
		}

		$this->comprobarRol(array("Administrador", "Coordinador"));
	}

	public function index($id = false)  {

		$informacion = array(
			'seccion'     => 'Períodos académicos', 
			'controlador' => 'periodos'
		);

		$datos['segmento'] = $id;

		if (!$id) {
			$datos['periodos'] = $this->Periodo_model->obtenerPeriodos();
		}
		else{
			$datos['periodo'] = $this->Periodo_model->obtenerPeriodo($id);
		}

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/periodos-academicos/listar', $datos);
		$this->load->view('backend/footer');
	}

	public function nuevo() {

		$informacion = array(
			'seccion'     => 'Nuevo período académico', 
			'controlador' => 'periodos'
		);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/periodos-academicos/registro');
			$this->load->view('backend/footer');
		}
		else {
			$periodo = array(
				'nombre'             => strtoupper($this->input->post('nombre')),
				'fecha_inicio'       => $this->input->post('fecha_inicio'),
				'fecha_finalizacion' => $this->input->post('fecha_finalizacion')
			);
			$periodo_academico_id = $this->Periodo_model->crearPeriodoAcademico($periodo);

			if ($periodo_academico_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Período creado');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al crear período');
				$this->session->set_flashdata('color','alert-danger');      
			}
			header('Location: ' . base_url() . 'periodos');
		}
	}

	public function editar($id) {

		$informacion = array(
			'seccion'     => 'Actualización período académico', 
			'controlador' => 'periodos'
		);

		/* Búsqueda de los datos */
		$datos['id'] = $id;
		$datos['periodo'] = $this->Periodo_model->obtenerPeriodo($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/periodos-academicos/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$periodo = array(
				'nombre'             => strtoupper($this->input->post('nombre')),
				'fecha_inicio'       => $this->input->post('fecha_inicio'),
				'fecha_finalizacion' => $this->input->post('fecha_finalizacion')
			);

			$periodo_academico_id = $this->Periodo_model->actualizarPeriodo($id,$periodo);

			if ($periodo_academico_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Período actualizado');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar período');
				$this->session->set_flashdata('color','alert-danger');      
			}
			header('Location: ' . base_url() . 'periodos');
		}
	}

	public function eliminar($id) {
		if ($this->Periodo_model->eliminarPeriodo($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Período eliminado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar período');
			$this->session->set_flashdata('color','alert-danger');      
		}
		header('Location: ' . base_url() . 'periodos');
	}
}
?>