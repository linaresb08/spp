<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imagenes extends CI_Controller {
	
	function __construct() { 
		parent::__construct();
		if (!$this->session->userdata('login')) {
			header('location: ' . base_url() . 'dashboard/login');
		}
	}

	public function seleccionar() {

		$informacion = array(
			'seccion'     => 'Subir imagen de perfil', 
			'controlador' => 'imagenes/seleccionar'
		);

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/perfiles/subir-foto');
		$this->load->view('backend/footer');
	}

	public function subirImagen() {

		//Variable de configuración
		$config['upload_path'] = './profile/images/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '10240'; //Tamaño máximo 10MB
		$config['max_width'] = '4608'; //Ancho máximo
		$config['max_height'] = '4608'; //altura máxima

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload("foto")) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/perfiles/subir-foto');
			echo "No se pudo subir la imagen";
			$this->load->view('backend/footer');
		}
		else {
			$file_info = $this->upload->data();

			//Para crear miniatura
	//		$this->crearMiniatura($file_info['file_name']);

			$usuario_id = $this->perfil_session->usuario_id;
			$foto = $file_info['file_name'];
			$subir = $this->Imagen_model->subir($usuario_id, $foto);

			header('Location: ' . base_url() . 'perfiles/index/');
		}
	}

	public function crearMiniatura($file_name)	{
		$config['image_library'] = 'gd2';
		$config['source_image'] = 'profile-images' . $file_name;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['new_image'] = 'profile-images/miniaturas/';
		$config['thumb_marker'] = '';
		$config['width'] = '34';
		$config['height'] = '34';

		$this->load->library('image_lib', $config);
		$this->image_lib->resize();
	}

}