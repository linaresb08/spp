<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Censos extends CI_Controller {

	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login'))	{
			header('location: ' . base_url() . 'dashboard/login');
		}
	}

	public function index($id = false)	{

		$informacion = array(
			'seccion'     => 'Gestor de censos', 
			'controlador' => 'censos'
		);

		$datos['segmento'] = $id;

		if (!$id) {
			$datos['censos'] = $this->Censo_model->obtenerCensos();
		}
		else{
			$datos['censo'] = $this->Censo_model->obtenerCenso($id);
		}

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/censos/listar', $datos);
		$this->load->view('backend/footer');
	}

	public function nuevo()	{

		$informacion = array(
			'seccion'     => 'Nuevo censo',
			'controlador' => 'censos'
		);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre', 'required|is_unique[censos.nombre]');
		$this->form_validation->set_rules('fecha_taller', 'Fecha', 'required');
		$this->form_validation->set_rules('lugar', 'Lugar', 'required');
		$this->form_validation->set_rules('limite', 'Limite', 'required');


		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');
		$this->form_validation->set_message('is_unique', '%s del taller ya está en uso');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/censos/registro');
			$this->load->view('backend/footer');
		}
		else {
			$censo = array(
				'fecha_taller'   => $this->input->post('fecha_taller'),
				'nombre'         => ucfirst($this->input->post('nombre')),
				'descripcion'    => ucfirst($this->input->post('descripcion')),
				'lugar'          => $this->input->post('lugar'),
				'duracion'       => $this->input->post('duracion'),
				'limite'         => $this->input->post('limite'),
				'estatus'        => 'Abierto',
				'coordinador_id' => $this->perfil_session->usuario_id
			);

			$censo_id = $this->Censo_model->crearCenso($censo);

			if ($censo_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Censo creado correctamente');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al crear censo');
				$this->session->set_flashdata('color','alert-danger');			
			}
			header('Location: ' . base_url() . 'censos/index/' . $censo_id);
		}
	}

	public function editar($id)	{

		$informacion = array(
			'seccion'     => 'Actualización del censo',
			'controlador' => 'censos'
		);

		/* Búsqueda de los datos */
		$datos['id'] = $id;
		$datos['censo'] = $this->Censo_model->obtenerCenso($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('fecha_taller', 'Fecha', 'required');
		$this->form_validation->set_rules('lugar', 'Lugar', 'required');
		$this->form_validation->set_rules('limite', 'Limite', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/censos/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$censo = array(
				'fecha_taller'   => $this->input->post('fecha_taller'),
				'nombre'         => ucfirst($this->input->post('nombre')),
				'descripcion'    => ucfirst($this->input->post('descripcion')),
				'lugar'          => $this->input->post('lugar'),
				'duracion'       => $this->input->post('duracion'),
				'limite'         => $this->input->post('limite'),
				'estatus'        => $this->input->post('estatus'),
				'coordinador_id' => $this->perfil_session->usuario_id
			);

			$censo_id = $this->Censo_model->actualizarCenso($id,$censo);

			if ($censo_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Censo editado');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al editar censo');
				$this->session->set_flashdata('color','alert-danger');	
			}
			header('Location: ' . base_url() . 'censos/index/' . $censo_id);
		}
	}

	public function eliminar($id)	{
		if ($this->Censo_model->eliminarCenso($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Censo eliminado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar censo');
			$this->session->set_flashdata('color','alert-danger');
		}
		header('Location: ' . base_url() . 'censos');
	}

	public function cerrar($id = false)	{
		if ($this->Censo_model->cerrarCenso($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Censo cerrado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al cerrar censo');
			$this->session->set_flashdata('color','alert-danger');
		}
		header('Location: ' . base_url() . 'censos');
	}

	public function abrir($id = false)	{
		if($this->Censo_model->abrirCenso($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Censo abierto');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al abrir censo');
			$this->session->set_flashdata('color','alert-danger');
		}
		header('Location: ' . base_url() . 'censos');
	}

	public function registrarse($id)	{
		$datos = array(
			'censo_id'      => $id, 
			'estudiante_id' => $this->perfil_session->usuario_id
		);
		
		$inscritos = $this->Censo_usuario_model->inscritos($id)->num_rows();
		$censo = $this->Censo_model->obtenerCenso($id);

		if ($inscritos < $censo->limite) {
			if ($this->Censo_usuario_model->inscripcion($datos)) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Has sido registrado');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al registrar');
				$this->session->set_flashdata('color','alert-danger');
			}
		}
		header('Location: ' . base_url() . 'censos');
	}

	public function retirarse($id)	{
		$datos = array(
			'censo_id'      => $id, 
			'estudiante_id' => $this->perfil_session->usuario_id
		);

		if ($this->Censo_usuario_model->retiro($datos)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Has sido retirado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al retirar');
			$this->session->set_flashdata('color','alert-danger');
		}

		header('Location: ' . base_url() . 'censos');
	}
}
?>	