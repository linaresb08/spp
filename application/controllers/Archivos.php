<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Archivos extends CI_Controller {
	
	function __construct() { 
		parent::__construct();
		if (!$this->session->userdata('login')) {
			header('location: ' . base_url() . 'dashboard/login');
		}
	}

	public function descargarConstanciasTaller() {
		$censo_id = $this->input->post('censo_id');
		$coordinador_id = $this->input->post('coordinador_id');
		$estudiante_id = $this->input->post('estudiante_id');

		//Busco la información del censo
		$datos['censo'] = $this->Censo_model->obtenerCenso($censo_id);

		//Busco la información del facilitador
		$datos['facilitador'] = $this->Perfil_model->obtenerPerfil_usuario_id($coordinador_id);

		//Busco la información del estudiante
		$datos['estudiante'] = $this->Perfil_model->obtenerPerfil_usuario_id($estudiante_id);

//		$this->load->view('sistema/archivos/constancia-taller', $datos);

		$estudiante = $this->Perfil_model->obtenerPerfil_usuario_id($estudiante_id);

		//load teh view and save it into $html variable
		$html = $this->load->view('sistema/archivos/constancia-taller',$datos,true);

		//Nombre del archivo pdf
		$pdfFilePath = $estudiante->ci . " " . $estudiante->primer_nombre . " " . $estudiante->primer_apellido . ".pdf";

		//load mPDF library
		$this->load->library('M_pdf');

		//generate the PDF from the given html
		$this->m_pdf->pdf->writeHTML($html);

		//Download it
		$this->m_pdf->pdf->Output($pdfFilePath, "D");

	}

	public function exportarPDFlistadoEstudiantes() {
		$censo_id = $this->input->post('censo_id');

		//Busco la información del censo
		$datos['censo_datos'] = $this->Censo_model->obtenerCenso($censo_id);

		//Busco la información de los estudiantes del censo
		$datos['censos'] = $this->Censo_usuario_model->obtener_usuarios_de_censo($censo_id);
//		print_r($datos);
//		$this->load->view('sistema/archivos/listado-estudiantes-censos2', $datos);

		$censo = $this->Censo_model->obtenerCenso($censo_id);
		$fecha = new DateTime($censo->fecha_taller);

		//load teh view and save it into $html variable
		$html = $this->load->view('sistema/archivos/listado-estudiantes-censos', $datos, true);

		//Nombre del archivo pdf
		$pdfFilePath = "Censo " . $censo->nombre  . " " . $fecha->format('d-m-Y') .".pdf";

		//load mPDF library
		$this->load->library('M_pdf');

		//generate the PDF from the given html
		$this->m_pdf->pdf->writeHTML($html);

		//Download it
		$this->m_pdf->pdf->Output($pdfFilePath, "D");
	}

	public function cartaPostulacion() {
		$estudiante_ci = $this->input->post('estudiante_ci');
		$datos['modalidad'] = $this->input->post('modalidad');

		$estudiante = $this->Perfil_model->buscarUsuario($estudiante_ci);
		$pasantia = $this->Pasantia_model->obtenerPasantia_por_estudiante($estudiante->usuario_id);

		$datos['pasantia_id'] = $pasantia->pasantia_id;

		$datos['estudiante'] = $this->Perfil_model->buscarUsuario($estudiante_ci);
		
		$datos['empresa'] = $this->Empresa_model->obtenerEmpresa_por_pasantia($pasantia->pasantia_id);

	//	$this->load->view('sistema/archivos/carta-postulacion2', $datos);

		//load teh view and save it into $html variable
		$html = $this->load->view('sistema/archivos/carta-postulacion2', $datos, true);

		//Nombre del archivo pdf
		$pdfFilePath = $estudiante->ci . " " . $estudiante->primer_nombre . " " . $estudiante->primer_apellido .".pdf";

		//load mPDF library
		$this->load->library('M_pdf');

		//generate the PDF from the given html
		$this->m_pdf->pdf->writeHTML($html);

		//Download it
		$this->m_pdf->pdf->Output($pdfFilePath, "D");

	}

	public function reportePeriodoEstudiantes() {
		$periodo_id = $this->input->post('periodo_id');
		$periodo = $this->Periodo_model->obtenerPeriodo($periodo_id);
		$seccion    = ucfirst($this->input->post('seccion'));
		
		$datos['periodo'] = $this->Periodo_model->obtenerPeriodo($periodo_id);
		$datos['seccion'] = $seccion;
		$datos['datos']   = $this->Pasantia_model->obtenerPasantias_por_periodo($periodo_id,$seccion);
//		print_r($datos);
	

//		$this->load->view('sistema/archivos/reporte-periodo-estudiantes', $datos);

		//load teh view and save it into $html variable
		$html = $this->load->view('sistema/archivos/reporte-periodo-estudiantes', $datos, true);

		//Nombre del archivo pdf
		$pdfFilePath = $periodo->nombre . " " . $seccion .".pdf";

		//load mPDF library
		$this->load->library('M_pdf');

		//generate the PDF from the given html
		$this->m_pdf->pdf->writeHTML($html);

		//Download it
		$this->m_pdf->pdf->Output($pdfFilePath, "D");

	}

}