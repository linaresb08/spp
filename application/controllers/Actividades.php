<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actividades extends CI_Controller {

	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login'))	{
			header('location: ' . base_url() . 'dashboard/login');
		}
	}

	public function index($id = false) {

		$informacion = array(
			'seccion'     => 'Actividades registradas', 
			'controlador' => 'actividades'
		);

		$datos['segmento'] = $id;

		if ($id) {
			$datos['actividades'] = $this->Actividad_model->obtenerActividades_por_pasantia($id);
		}else {
			$datos['pasantias'] = $this->Pasantia_model->obtenerPasantias_por_estudiante($this->perfil_session->usuario_id);
		}

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/actividades/listar', $datos);
		$this->load->view('backend/footer');
	}

	public function obtener($id='')	{
		$id        = $this->input->post('id');
		$actividad = $this->Actividad_model->obtenerActividad($id);

		echo json_encode($actividad);
	}

	public function nueva()	{

		$informacion = array(
			'seccion'     => 'Nueva actividad', 
			'controlador' => 'actividades'
		);

		/* Reglas de validación */
		$this->form_validation->set_rules('pasantia_id', 'Pasantía', 'required|callback_comprobar_estudiante');
		$this->form_validation->set_rules('fecha', 'Fecha', 'required');
		$this->form_validation->set_rules('tipo', 'Tipo de reporte', 'in_list[Diaria,Semanal]');
		$this->form_validation->set_rules('descripcion', 'Descripción', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '<strong>%s</strong> es requerido');
		$this->form_validation->set_message('in_list', '<strong>%s</strong> no tiene un valor valido');

		if ($this->form_validation->run() == FALSE){
			$datos['pasantias'] = $this->Pasantia_model->obtenerPasantias_habilitadas_por_estudiante($this->perfil_session->usuario_id);

			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/actividades/registro', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$pasantia             = $this->Pasantia_model->obtenerPasantia($this->input->post('pasantia_id'));
			$fecha                = $this->input->post('fecha');
			$dias_semana          = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
			$dia_semana_actividad = '';
			$dia_semana           = date('w', strtotime($fecha)) - 1;

			if ($pasantia->tipo_reporte_actividad == 'Ninguna') {
				$tipo = $this->input->post('tipo');
			}else {
				$tipo = $pasantia->tipo_reporte_actividad;
			}

			if ($tipo == 'Diaria') {
				$fecha_inicio         = $fecha;
				$fecha_fin            = $fecha;
				$dia_semana_actividad = $dias_semana[$dia_semana];
			}else {
				$fecha_inicio = date( "Y-m-d", strtotime( $fecha . ' -' . $dia_semana. ' day' ) );
				$fecha_fin    = date( "Y-m-d", strtotime( $fecha_inicio . ' +4 day' ) );
			}

			$actividad = array(
				'pasantia_id'  => $pasantia->pasantia_id,
				'dia'          => $dia_semana_actividad,
				'fecha_inicio' => $fecha_inicio,
				'fecha_fin'    => $fecha_fin,
				'descripcion'  => ucfirst($this->input->post('descripcion')),
				'observacion'  => ucfirst($this->input->post('observacion'))
			);

			$actividad_id = $this->Actividad_model->crearActividad($actividad, $tipo);

			if ($actividad_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Actividad creada');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al crear actividad');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'actividades/index/' . $pasantia->pasantia_id);
		}
	}

	public function editar($id = false)	{
		$this->comprobar_usuario($id);

		$actividad = $this->Actividad_model->obtenerActividad($id);
		$pasantia  = $this->Pasantia_model->obtenerPasantia($actividad->pasantia_id);

		$informacion = array(
			'seccion'     => 'Actualización de actividad',
			'controlador' => 'empresas'
		);
		
		$datos['actividad'] = $actividad;
		$datos['id']        = $id;
		$datos['pasantia']  = $pasantia;

		/* Reglas de validación */
		$this->form_validation->set_rules('fecha', 'Fecha', 'required');
		$this->form_validation->set_rules('descripcion', 'Descripción', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/actividades/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$fecha                = $this->input->post('fecha');
			$dias_semana          = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
			$dia_semana_actividad = '';
			$dia_semana           = date('w', strtotime($fecha)) - 1;

			$tipo = $pasantia->tipo_reporte_actividad;

			if ($tipo == 'Diaria') {
				$fecha_inicio         = $fecha;
				$fecha_fin            = $fecha;
				$dia_semana_actividad = $dias_semana[$dia_semana];
			}else {
				$fecha_inicio = date( "Y-m-d", strtotime( $fecha . ' -' . $dia_semana. ' day' ) );
				$fecha_fin    = date( "Y-m-d", strtotime( $fecha_inicio . ' +4 day' ) );
			}

			$actividad = array(
				'pasantia_id'  => $pasantia->pasantia_id,
				'dia'          => $dia_semana_actividad,
				'fecha_inicio' => $fecha_inicio,
				'fecha_fin'    => $fecha_fin,
				'descripcion'  => ucfirst($this->input->post('descripcion')),
				'observacion'  => ucfirst($this->input->post('observacion'))
			);

			$actividad_id = $this->Actividad_model->actualizarActividad($id, $actividad);

			if ($actividad_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Actividad actualizada');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar actividad');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'actividades/index/' . $pasantia->pasantia_id);
		}
	}

	public function eliminar($id)	{
		$this->comprobar_usuario($id);

		$this->Actividad_model->eliminarActividad($id);

		$this->session->set_flashdata('status','<i class="fa fa-check"></i> La actividad se eliminó correctamente');
		$this->session->set_flashdata('color','alert-success');

		header('Location: ' . base_url() . 'actividades');
	}

	public function comprobar_estudiante($pasantia_id='') {
		$pasantia = $this->Pasantia_model->obtenerPasantia($pasantia_id);

		if ($pasantia->estudiante_id != $this->perfil_session->usuario_id) {
			$this->form_validation->set_message('comprobar_estudiante', '<strong>Pasantía</strong> no corresponde a este usuario');
      return FALSE;
		}else {
			return TRUE;
		}
	}

	public function comprobar_usuario($id='')	{
		$actividad = $this->Actividad_model->obtenerActividad($id);
		$pasantia  = $this->Pasantia_model->obtenerPasantia($actividad->pasantia_id);

		if ($pasantia->estudiante_id != $this->perfil_session->usuario_id) {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error no posee permisos para ejecutar la acción');
			$this->session->set_flashdata('color','alert-danger');
      header('Location: ' . base_url() . 'actividades');		
    }
	}
}
?>