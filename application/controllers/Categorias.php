<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login')) 
		{
			header('location: ' . base_url() . 'dashboard/login');
		}

		$this->comprobarRol(array("Administrador", "Coordinador"));
	}

	public function index($id = false)	{

		$informacion = array(
			'seccion'     => 'Configuración de las categorías', 
			'controlador' => 'categorias'
		);

		$datos['segmento'] = $id;

		if (!$id) {
			$datos['categorias'] = $this->Categoria_model->obtenerCategorias();
		}
		else{
			$datos['categoria'] = $this->Categoria_model->obtenerCategoria($id);
		}

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/categorias/listar', $datos);
		$this->load->view('backend/footer');
	}

	public function nueva()	{

		$informacion = array(
			'seccion'     => 'Nueva categoría', 
			'controlador' => 'categorias'
		);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/categorias/registro');
			$this->load->view('backend/footer');
		}
		else {
			$categoria = array(
				'color'  => $this->input->post('color'),
				'nombre' => ucfirst($this->input->post('nombre'))
			);

			$categoria_id = $this->Categoria_model->crearCategoria($categoria);
			
			if($categoria_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Categoría creada correctamente');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al crear categoría');
				$this->session->set_flashdata('color','alert-danger');			
			}
			header('Location: ' . base_url() . 'categorias');
		}
	}

	public function editar($id)	{

		$informacion = array(
			'seccion'     => 'Actualización de categoría', 
			'controlador' => 'categorias'
		);

		/* Búsqueda de los datos */
		$datos['id'] = $id;
		$datos['categoria'] = $this->Categoria_model->obtenerCategoria($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/categorias/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$categoria = array(
				'color'  => $this->input->post('color'),
				'nombre' => ucfirst($this->input->post('nombre'))
			);

			$categoria_id = $this->Categoria_model->actualizarCategoria($id,$categoria);

			if ($categoria_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Categoría actualizada');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar categoría');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'categorias');
		}
	}

	public function eliminar($id)	{
		if ($this->Categoria_model->eliminarCategoria($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Categoría eliminada');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar categoría');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'categorias');
	}
}
?>	