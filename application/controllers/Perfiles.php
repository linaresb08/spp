<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfiles extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login')) {
			header('location: ' . base_url() . 'dashboard/login');
		}
	}

	public function index() {  
		/* Función para ver el perfil del usuario */

		$informacion = array(
			'seccion'     => 'Mi perfil',
			'controlador' => 'perfiles'
		);

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/perfiles/perfil');
		$this->load->view('backend/footer');
	}

	public function editar($id = '')	{
		
		$informacion = array(
			'seccion'     => 'Actualización del perfil', 
			'controlador' => 'perfiles'
		);

		$datos['id']     = $this->perfil_session->perfil_id;
		$datos['perfil'] = $this->Perfil_model->obtenerPerfil_usuario_id($this->perfil_session->perfil_id);

		/* Reglas de validación */
		$this->form_validation->set_rules('segundo_nombre', 'Segundo nombre', 'required');
		$this->form_validation->set_rules('segundo_apellido', 'Segundo apellido', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/perfiles/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$perfil = array(
				'usuario_id' => $this->perfil_session->usuario_id,
				'primer_nombre' => ucwords($this->input->post('primer_nombre')),
				'segundo_nombre' => ucwords($this->input->post('segundo_nombre')),
				'primer_apellido' => ucwords($this->input->post('primer_apellido')),
				'segundo_apellido' => ucwords($this->input->post('segundo_apellido')),
				'cumpleaños' => $this->input->post('cumpleaños'),
				'direccion_residencial' => ucfirst($this->input->post('direccion_residencial')),
				'correo_institucional' => strtolower($this->input->post('correo_institucional')),
				'correo_personal' => strtolower($this->input->post('correo_personal')),
				'telefono_movil' => $this->input->post('telefono_movil'),
				'telefono_residencial' => $this->input->post('telefono_residencial'),
				'descripcion' => ucfirst($this->input->post('descripcion'))
			);

			$perfil_id = $this->Perfil_model->actualizarPerfil($perfil);
			$this->Carrera_estudiante_model->eliminarCarreras_Estudiante($this->perfil_session->usuario_id);

			if ($perfil_id) {
				foreach ($this->input->post('carreras') as $carrera_id) {
					$datos = array(
						'estudiante_id' => $this->perfil_session->usuario_id,
						'carrera_id'    => $carrera_id
					);
					$this->Carrera_estudiante_model->crearCarreraEstudiante($datos);
				}
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Perfil editado');
				$this->session->set_flashdata('color','alert-success');
			}
			header('Location: ' . base_url() . 'perfiles/index/');
		}
	}

	public function busqueda()	{
		$ci = $this->input->post('ci');
		$usuario = $this->Perfil_model->buscarUsuario($ci);

		if ( $usuario ) {
			header('Location: ' . base_url() . 'perfiles/usuario/' . $usuario->perfil_id);
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> El usuario no está registrado en el sistema');
			$this->session->set_flashdata('color','alert-danger');
			header('Location: ' . base_url() . 'dashboard');
		}
	}

	public function usuario($id)	{
		$datos['usuario'] = $this->Perfil_model->obtenerPerfil($id);
		$usuario = $this->Perfil_model->obtenerPerfil($id);
		$controlador = 'perfiles/usuario/' . $usuario->perfil_id;

		$informacion = array(
			'seccion'     => 'Resultado de búsqueda', 
			'controlador' => $controlador
		);

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/perfiles/usuario',$datos);
		$this->load->view('backend/footer');		
	}
}
?>