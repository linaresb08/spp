<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requisitos extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login'))	{
			header('location: ' . base_url() . 'dashboard/login');
		}
	}

	public function index($id = false)	{

		$informacion = array(
			'seccion'     => 'Gestor de requisitos', 
			'controlador' => 'requisitos'
		);

		$datos['segmento'] = $id;

		if (!$id) {
			$datos['requisitos'] = $this->Requisito_model->obtenerRequisitos();
		}
		else{
			$datos['requisito'] = $this->Requisito_model->obtenerRequisito($id);
		}

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/requisitos/listar', $datos);
		$this->load->view('backend/footer');
	}

	public function nuevo()	{

		$informacion = array(
			'seccion'     => 'Nuevo requisito', 
			'controlador' => 'requisitos'
		);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/requisitos/registro');
			$this->load->view('backend/footer');
		}
		else {
			$requisito = array(
				'nombre'      => ucfirst($this->input->post('nombre')),
				'descripcion' => ucfirst($this->input->post('descripcion'))
			);

			$requisito_id = $this->Requisito_model->crearRequisito($requisito);

			if ($requisito_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Requisito creado');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al crear requisito');
				$this->session->set_flashdata('color','alert-danger');			
			}
			header('Location: ' . base_url() . 'requisitos');
		}
	}

	public function editar($id)	{

		$informacion = array(
			'seccion'     => 'Actualización del censo', 
			'controlador' => 'requisitos'
		);

		/* Búsqueda de los datos */
		$datos['id'] = $id;
		$datos['requisito'] = $this->Requisito_model->obtenerRequisito($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/requisitos/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$requisito = array(
				'nombre'      => ucfirst($this->input->post('nombre')),
				'descripcion' => ucfirst($this->input->post('descripcion'))
			);

			$requisito_id = $this->Requisito_model->actualizarRequisito($id,$requisito);
			if ($requisito_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Requisito actualizado');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar requisito');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'requisitos');
		}
	}

	public function eliminar($id)	{
		if ($this->Requisito_model->eliminarRequisito($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Requisito eliminado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar requisito');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'requisitos');
	}

	/* 
		FUNCIONES CON LA TABLA ESTUDIANTE_REQUISITOS
	*/

	public function estudiante()	{

		$informacion = array(
			'seccion'     => 'Gestor de requisitos', 
			'controlador' => 'requisitos/estudiante' 
		);

		$datos['requisitos_estudiante'] = $this->Estudiante_requisito_model->obtenerRequisitos_por_Estudiante($this->perfil_session->usuario_id);

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/requisitos/listar-estudiante', $datos);
		$this->load->view('backend/footer');
	}

	public function actualizarRequisito() {
		$data = array(
			'estudiante_requisito_id' => $this->input->post('id'), 
			'archivo'                 => $this->input->post('archivo'),
			'estatus'                 => 'Adjuntado',
			'observacion'             => ''
		);

		if ($this->Estudiante_requisito_model->actualizarRequisito_Estudiante($data) ) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> El requisito se actualizó correctamente');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar requisito');
			$this->session->set_flashdata('color','alert-danger');
		}
		header('Location: ' . base_url() . 'requisitos/estudiante');		
	}

	/*	BOTONES DE ACCIONES PARA USO DEL ADMIN/COORD	*/

	public function verificarRequisito() {
		$data = array(
			'estudiante_requisito_id' => $this->input->post('id'),
			'estatus'                 => 'Verificado',
			'observacion'             => ''
		);

		if ($this->Estudiante_requisito_model->agregarObservacion_Requisito($data) ) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Se ha verificado el requisito correctamente');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al verificar requisito');
			$this->session->set_flashdata('color','alert-danger');
		}
		header('Location: ' . base_url() . 'perfiles/usuario/' . $this->input->post('estudiante_id') );
	}

	public function agregarObservacion() {
		$data = array(
			'estudiante_requisito_id' => $this->input->post('id'), 
			'observacion'             => $this->input->post('observacion'),
			'estatus'                 => 'Corregir'
		);

		if ($this->Estudiante_requisito_model->agregarObservacion_Requisito($data) ) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Se actualizó la observación correctamente');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar la observación');
			$this->session->set_flashdata('color','alert-danger');
		}
		header('Location: ' . base_url() . 'perfiles/usuario/' . $this->input->post('estudiante_id') );
	}

}
?>	