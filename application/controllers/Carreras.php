<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carreras extends CI_Controller {
	
	function __construct() { 
		parent::__construct();
		
		if (!$this->session->userdata('login')) {
			header('location: ' . base_url() . 'dashboard/login');
		}

		$this->comprobarRol(array("Administrador", "Coordinador"));
	}

	public function index($id = false)	{

		$informacion = array(
			'seccion'     => 'Carreras ofertadas', 
			'controlador' => 'carreras'
		);

		$datos['segmento'] = $id;

		if (!$id) {
			$datos['carreras'] = $this->Carrera_model->obtenerCarreras();
		}
		else{
			$datos['carrera'] = $this->Carrera_model->obtenerCarrera($id);
		}

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/carreras/listar', $datos);
		$this->load->view('backend/footer');
	}

	public function nueva()	{

		$informacion = array(
			'seccion'     => 'Nueva carrera', 
			'controlador' => 'carreras'
		);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/carreras/registro');
			$this->load->view('backend/footer');
		}
		else {
			$carrera = array(
				'nombre'      => ucwords($this->input->post('nombre')),
				'descripcion' => ucfirst($this->input->post('descripcion'))
			);
			$carrera_id = $this->Carrera_model->crearCarrera($carrera);

			if ($carrera_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Carrera creada');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al crear carrera');
				$this->session->set_flashdata('color','alert-danger');			
			}

			header('Location: ' . base_url() . 'carreras');
		}
	}

	public function editar($id)	{

		$informacion = array(
			'seccion'     => 'Actualización de carrera', 
			'controlador' => 'carreras'
		);

		/* Búsqueda de los datos */
		$datos['id'] = $id;
		$datos['carrera'] = $this->Carrera_model->obtenerCarrera($id);

		/* Reglas de validación */
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/carreras/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$carrera = array(
				'nombre'      => ucwords($this->input->post('nombre')),
				'descripcion' => ucfirst($this->input->post('descripcion'))
			);
			$carrera_id = $this->Carrera_model->actualizarCarrera($id,$carrera);

			if ($carrera_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Carrera actualizada');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar carrera');
				$this->session->set_flashdata('color','alert-danger');			
			}
	//		print_r($carrera);
			header('Location: ' . base_url() . 'carreras');
		}
	}

	public function eliminar($id)	{
		if ($this->Carrera_model->eliminarCarrera($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Carrera eliminada');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar carrera');
			$this->session->set_flashdata('color','alert-danger');			
		}

		header('Location: ' . base_url() . 'carreras');
	}
}
?>
