<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasantias extends CI_Controller {
	
	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login')) 
		{
			header('location: ' . base_url() . 'dashboard/login');
		}
	}

	public function index() {

		$informacion = array(
			'seccion'     => 'Pasantías', 
			'controlador' => 'pasantias'
		);
	
		if ($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador') {
			$datos['pasantias'] = $this->Pasantia_model->obtenerPasantias();
		}
		elseif ($this->session->userdata('rol') == 'Estudiante') {
			$datos['pasantias'] = $this->Pasantia_model->obtenerPasantias_por_estudiante($this->perfil_session->usuario_id);
		}
		else {
		$datos['pasantias'] = $this->Pasantia_model->obtenerPasantias_por_TutorAcademico($this->perfil_session->usuario_id);
		}

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/pasantias/listar', $datos);
		$this->load->view('backend/footer');

	}

	public function ver($pasantia_id) {

		$informacion = array(
			'seccion'     => 'Registro del pasante', 
			'controlador' => 'pasantias'
		);

		$datos['tipo'] = 'pasantia';
		$datos['pasantia'] = $this->Pasantia_model->obtenerPasantia($pasantia_id);
		
		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/pasantias/ver', $datos);
		$this->load->view('backend/footer');

	}

	public function obtener()	{
		$id       = $this->input->post('id');
		$pasantia = $this->Pasantia_model->obtenerPasantia($id);

		echo json_encode($pasantia);
	}

	public function nueva()	{

		$informacion = array(
			'seccion'     => 'Registro de pasantías', 
			'controlador' => 'pasantias'
		);

		/* Reglas de validación de las pasantías */
		$this->form_validation->set_rules('departamento_estudiante', 'Departamento asignado', 'required');
		$this->form_validation->set_rules('fecha_inicio', 'Fecha de inicio', 'required');
		$this->form_validation->set_rules('fecha_fin', 'Fecha de finalización', 'required');
		$this->form_validation->set_rules('seccion', 'Sección', 'required');
		$this->form_validation->set_rules('inscripcion_periodo_id', 'Periodo de inscripción', 'required');
		$this->form_validation->set_rules('realizacion_periodo_id', 'Periodo de realización', 'required');
		$this->form_validation->set_rules('tl_apellidos', 'Apellidos del tutor laboral', 'required');
		$this->form_validation->set_rules('tl_nombres', 'Nombres del tutor laboral', 'required');
		$this->form_validation->set_rules('tl_ci', 'Cédula de identidad del tutor laboral', 'required');
		$this->form_validation->set_rules('tl_cargo', 'Cargo del tutor laboral', 'required');
		$this->form_validation->set_rules('tl_correo', 'Correo del tutor laboral', 'required');
		$this->form_validation->set_rules('tl_telefono', 'Teléfono del tutor laboral', 'required');

		/* Reglas de validación de la empresa */
		$this->form_validation->set_rules('nombre', 'Nombre de la empresa', 'required');
		$this->form_validation->set_rules('rif', 'RIF', 'required');
		$this->form_validation->set_rules('direccion', 'Direccion', 'required');
		$this->form_validation->set_rules('telefono1', 'Teléfono 1', 'required');
		$this->form_validation->set_rules('jr_apellidos', 'Apellidos del jefe de recursos humanos', 'required');
		$this->form_validation->set_rules('jr_nombres', 'Nombres del jefe de recursos humanos', 'required');
		$this->form_validation->set_rules('jr_correo', 'Correo del jefe de recursos humanos', 'required');
		$this->form_validation->set_rules('jr_telefono', 'Teléfono del jefe de recursos humanos', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/pasantias/registro');
			$this->load->view('backend/footer');
		}
		else {
			$pasantia = array(
				'estudiante_id' => $this->perfil_session->usuario_id,
				'inscripcion_periodo_id' => $this->input->post('inscripcion_periodo_id'),
				'seccion' => ucfirst($this->input->post('seccion')),
				'realizacion_periodo_id' => $this->input->post('realizacion_periodo_id'),
				'fecha_inicio' => $this->input->post('fecha_inicio'),
				'fecha_fin' => $this->input->post('fecha_fin'),
				'departamento_estudiante' => ucwords($this->input->post('departamento_estudiante')),
				'tl_apellidos' => ucwords($this->input->post('tl_apellidos')),
				'tl_nombres' => ucwords($this->input->post('tl_nombres')),
				'tl_ci' => ucfirst($this->input->post('tl_ci')),
				'tl_cargo' => ucwords($this->input->post('tl_cargo')),
				'tl_correo' => strtolower($this->input->post('tl_correo')),
				'tl_telefono' => $this->input->post('tl_telefono'),
				'estado' => 'Habilitada',
				'tipo_reporte_actividad' => 'Ninguna'
			);

			$pasantia_id = $this->Pasantia_model->crearPasantia($pasantia);

			if ($pasantia_id) {

				//Asignar el pasantia_id a la carrera a aprobar
				foreach ($this->input->post('carreras') as $carrera_id) {
					$datos = array(
						'estudiante_id' => $this->perfil_session->usuario_id,
						'carrera_id'    => $carrera_id,
						'pasantia_id'   => $pasantia_id						
					);
					$this->Carrera_estudiante_model->asignarPasantia($datos);
				}

				//Datos de la empresa
				$empresa = array(
					'nombre'       => ucwords($this->input->post('nombre')),
					'rif'          => ucfirst($this->input->post('rif')),
					'direccion'    => ucfirst($this->input->post('direccion')),
					'telefono1'    => $this->input->post('telefono1'),
					'telefono2'    => $this->input->post('telefono2'),
					'jr_apellidos' => ucwords($this->input->post('jr_apellidos')),
					'jr_nombres'   => ucwords($this->input->post('jr_nombres')),
					'jr_correo'    => strtolower($this->input->post('jr_correo')),
					'jr_telefono'  => $this->input->post('jr_telefono'),
					'pasantia_id'  => $pasantia_id
				);

				$empresa_id = $this->Empresa_model->crearEmpresa($empresa);

			
				if ($empresa_id) {
					$this->session->set_flashdata('status','<i class="fa fa-check"></i> Pasantía creada');
					$this->session->set_flashdata('color','alert-success');
				}				
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al crear pasantía');
				$this->session->set_flashdata('color','alert-danger');			
			}
			header('Location: ' . base_url() . 'pasantias');
		}
	}


	public function editar($id = false)	{
		
		$informacion = array(
			'seccion'     => 'Actualización de pasantía',
			'controlador' => 'pasantias'
		);

		$datos['id'] = $id;
		$datos['pasantia'] = $this->Pasantia_model->obtenerPasantia($id);
		$datos['empresa'] = $this->Empresa_model->obtenerEmpresa_por_pasantia($id);

		/* Reglas de validación de las pasantías */
		$this->form_validation->set_rules('departamento_estudiante', 'Departamento asignado', 'required');
		$this->form_validation->set_rules('fecha_inicio', 'Fecha de inicio', 'required');
		$this->form_validation->set_rules('fecha_fin', 'Fecha de finalización', 'required');
		$this->form_validation->set_rules('seccion', 'Sección', 'required');
		$this->form_validation->set_rules('inscripcion_periodo_id', 'Periodo de inscripción', 'required');
		$this->form_validation->set_rules('realizacion_periodo_id', 'Periodo de realización', 'required');
		$this->form_validation->set_rules('tl_apellidos', 'Apellidos del tutor laboral', 'required');
		$this->form_validation->set_rules('tl_nombres', 'Nombres del tutor laboral', 'required');
		$this->form_validation->set_rules('tl_ci', 'Cédula de identidad del tutor laboral', 'required');
		$this->form_validation->set_rules('tl_cargo', 'Cargo del tutor laboral', 'required');
		$this->form_validation->set_rules('tl_correo', 'Correo del tutor laboral', 'required');
		$this->form_validation->set_rules('tl_telefono', 'Teléfono del tutor laboral', 'required');

		/* Reglas de validación de la empresa */
		$this->form_validation->set_rules('nombre', 'Nombre de la empresa', 'required');
		$this->form_validation->set_rules('rif', 'RIF', 'required');
		$this->form_validation->set_rules('direccion', 'Direccion', 'required');
		$this->form_validation->set_rules('telefono1', 'Teléfono 1', 'required');
		$this->form_validation->set_rules('jr_apellidos', 'Apellidos del jefe de recursos humanos', 'required');
		$this->form_validation->set_rules('jr_nombres', 'Nombres del jefe de recursos humanos', 'required');
		$this->form_validation->set_rules('jr_correo', 'Correo del jefe de recursos humanos', 'required');
		$this->form_validation->set_rules('jr_telefono', 'Teléfono del jefe de recursos humanos', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/pasantias/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$pasantia = array(
				'inscripcion_periodo_id' => $this->input->post('inscripcion_periodo_id'),
				'seccion' => ucfirst($this->input->post('seccion')),
				'realizacion_periodo_id' => $this->input->post('realizacion_periodo_id'),
				'fecha_inicio' => $this->input->post('fecha_inicio'),
				'fecha_fin' => $this->input->post('fecha_fin'),
				'departamento_estudiante' => ucwords($this->input->post('departamento_estudiante')),
				'tl_apellidos' => ucwords($this->input->post('tl_apellidos')),
				'tl_nombres' => ucwords($this->input->post('tl_nombres')),
				'tl_ci' => ucfirst($this->input->post('tl_ci')),
				'tl_cargo' => ucwords($this->input->post('tl_cargo')),
				'tl_correo' => strtolower($this->input->post('tl_correo')),
				'tl_telefono' => $this->input->post('tl_telefono')
			);

			$pasantia_id = $this->Pasantia_model->actualizarPasantia($id,$pasantia);

			if ($pasantia_id) {
				$empresa = array(
					'nombre'       => ucwords($this->input->post('nombre')),
					'rif'          => ucfirst($this->input->post('rif')),
					'direccion'    => ucfirst($this->input->post('direccion')),
					'telefono1'    => $this->input->post('telefono1'),
					'telefono2'    => $this->input->post('telefono2'),
					'jr_apellidos' => ucwords($this->input->post('jr_apellidos')),
					'jr_nombres'   => ucwords($this->input->post('jr_nombres')),
					'jr_correo'    => strtolower($this->input->post('jr_correo')),
					'jr_telefono'  => $this->input->post('jr_telefono')
				);

				$empresa_id = $this->Empresa_model->actualizarEmpresa_por_Pasantia($pasantia_id, $empresa);

				if ($empresa_id) {
					$this->session->set_flashdata('status','<i class="fa fa-check"></i> Pasantía actualizada');
					$this->session->set_flashdata('color','alert-success');
				}
				else {
					$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar pasantía');
					$this->session->set_flashdata('color','alert-danger');			
				}
			}
			header('Location: ' . base_url() . 'pasantias');
		}
	}

	public function eliminar($id)	{
		if ($this->Pasantia_model->eliminarPasantia($id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Pasantía eliminada');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar pasantía');
			$this->session->set_flashdata('color','alert-danger');			
		}
		header('Location: ' . base_url() . 'pasantias');
	}

	public function asignarTutorA($pasantia_id) {
		$ta_ci = $this->input->post('ta_ci');
		$tutor_academico_id = $this->Perfil_model->buscarUsuario($ta_ci)->usuario_id;
		if ($tutor_academico_id) {
	//		echo $tutor_academico_id;
		if ($this->Pasantia_model->asignarTutorAcademico($pasantia_id, $tutor_academico_id)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Tutor académico asignado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al asignar tutor académico');
			$this->session->set_flashdata('color','alert-danger');
		}
			header('Location: ' . base_url() . 'pasantias');
		}		
	}

	public function adjuntarInforme($pasantia_id) {
		$informe = $this->input->post('informe');
		if ($this->Pasantia_model->adjuntarInforme($pasantia_id, $informe)) {
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Informe adjuntado');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al adjuntar informe');
			$this->session->set_flashdata('color','alert-danger');
		}
		header('Location: ' . base_url() . 'pasantias');
	}

	public function select_pasantias() {
		$id        = $this->input->post('id');
		$pasantia  = $this->Pasantia_model->obtenerPasantia($id);
		$respuesta = NULL;

		if ($pasantia) {
			$respuesta = $pasantia->tipo_reporte_actividad;
		}
		echo json_encode($respuesta);
	}
}
?>