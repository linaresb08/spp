<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supervisiones extends CI_Controller {

	function __construct() { 
		parent::__construct();

		if (!$this->session->userdata('login')) {
			header('location: ' . base_url() . 'dashboard/login');
		}
	}

	public function ver($pasantia_id) {

		$informacion = array(
			'seccion'     => 'Supervisiones registradas', 
			'controlador' => 'pasantias'
		);

		$pasantia = $this->Pasantia_model->obtenerPasantia($pasantia_id);

		$datos['tipo'] = 'supervisiones';
		$datos['estudiante'] = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->estudiante_id);
		$datos['tutor_academico'] = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->tutor_academico_id);
		$datos['empresa'] = $this->Empresa_model->obtenerEmpresa_por_pasantia($pasantia_id);
		$datos['pasantia'] = $this->Pasantia_model->obtenerPasantia($pasantia_id);
		$datos['supervisiones'] = $this->Supervision_model->obtenerSupervisiones_por_pasantia($pasantia_id);

//		print_r($datos);

		$this->load->view('backend/head');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/navbar',$informacion);
		$this->load->view('sistema/pasantias/ver', $datos);		
		$this->load->view('backend/footer');
	}

	public function nueva($pasantia_id)	{

		$informacion = array(
			'seccion'     => 'Registro de supervisión', 
			'controlador' => 'pasantias'
		);

		/* Reglas de validación */
		$this->form_validation->set_rules('fecha', 'Fecha de supervisión', 'required');
		$this->form_validation->set_rules('hora', 'Fecha de supervisión', 'required');
		$this->form_validation->set_rules('actividades_realizadas', 'Actividades realizadas', 'required');
		$this->form_validation->set_rules('personales_aspectos_positivos', 'Rasgos personales aspectos positivos', 'required');
		$this->form_validation->set_rules('habilidades_aspectos_positivos', 'Rasgos habilidades aspectos positivos', 'required');
		$this->form_validation->set_rules('calidad_aspectos_positivos', 'Rasgos calidad aspectos positivos', 'required');
		$this->form_validation->set_rules('rasgos_aspectos_positivos', 'Rasgos aspectos positivos', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE){
			$pasantia = $this->Pasantia_model->obtenerPasantia($pasantia_id);

			$datos['pasantia_id'] = $pasantia_id;
			$datos['estudiante'] = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->estudiante_id);

			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/supervisiones/registro',$datos);
			$this->load->view('backend/footer');
		}
		else {

			$hora = date('g:i A', strtotime( $this->input->post('hora') ) ); ;

			$fecha_inicial = new DateTime($this->input->post('fecha'));
			$fecha_sup = $fecha_inicial->format('d-m-Y');

			$fecha_supervision = $fecha_sup . " " . $hora; 

			$supervision = array(
				'pasantia_id'                    => $this->input->post('pasantia_id'),
				'fecha_supervision'              => $fecha_supervision,
				'actividades_realizadas'         => ucfirst($this->input->post('actividades_realizadas')),
				'personales_aspectos_positivos'  => ucfirst($this->input->post('personales_aspectos_positivos')),
				'personales_puntos_mejoras'      => ucfirst($this->input->post('personales_puntos_mejoras')),
				'habilidades_aspectos_positivos' => ucfirst($this->input->post('habilidades_aspectos_positivos')),
				'habilidades_puntos_mejoras'     => ucfirst($this->input->post('habilidades_puntos_mejoras')),
				'calidad_aspectos_positivos'     => ucfirst($this->input->post('calidad_aspectos_positivos')),
				'calidad_puntos_mejoras'         => ucfirst($this->input->post('calidad_puntos_mejoras')),
				'rasgos_aspectos_positivos'      => ucfirst($this->input->post('rasgos_aspectos_positivos')),
				'rasgos_puntos_mejoras'          => ucfirst($this->input->post('rasgos_puntos_mejoras')),
				'observaciones'                  => ucfirst($this->input->post('observaciones'))
			);

//		print_r($supervision);

			if ($this->Supervision_model->crearSupervision($supervision)) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Supervisión registrada');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al registrar supervisión');
				$this->session->set_flashdata('color','alert-danger');			
			}
			header('Location: ' . base_url() . 'pasantias');
		} 
	}


	public function editar($id)	{
		
		$informacion = array(
			'seccion'     => 'Actualización de supervisión',
			'controlador' => 'pasantias'
		);

		$supervision= $this->Supervision_model->obtenerSupervision($id);
		$pasantia = $this->Pasantia_model->obtenerPasantia($supervision->pasantia_id);

		$datos['supervision_id'] = $id;
		$datos['supervision'] = $this->Supervision_model->obtenerSupervision($id);
		$datos['estudiante'] = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->estudiante_id);

		/* Reglas de validación */
	/*	$this->form_validation->set_rules('fecha_supervision', 'Fecha de supervisión', 'required');*/
		$this->form_validation->set_rules('actividades_realizadas', 'Actividades realizadas', 'required');
		$this->form_validation->set_rules('personales_aspectos_positivos', 'Rasgos personales aspectos positivos', 'required');
		$this->form_validation->set_rules('habilidades_aspectos_positivos', 'Rasgos habilidades aspectos positivos', 'required');
		$this->form_validation->set_rules('calidad_aspectos_positivos', 'Rasgos calidad aspectos positivos', 'required');
		$this->form_validation->set_rules('rasgos_aspectos_positivos', 'Rasgos aspectos positivos', 'required');

		/* Mensajes que emite las validaciones */
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('backend/head');
			$this->load->view('backend/sidebar');
			$this->load->view('backend/navbar',$informacion);
			$this->load->view('sistema/supervisiones/editar', $datos);
			$this->load->view('backend/footer');
		}
		else {
			$supervision = array(
			/*	'fecha_supervision'              => $this->input->post('fecha_supervision'),*/
				'actividades_realizadas'         => ucfirst($this->input->post('actividades_realizadas')),
				'personales_aspectos_positivos'  => ucfirst($this->input->post('personales_aspectos_positivos')),
				'personales_puntos_mejoras'      => ucfirst($this->input->post('personales_puntos_mejoras')),
				'habilidades_aspectos_positivos' => ucfirst($this->input->post('habilidades_aspectos_positivos')),
				'habilidades_puntos_mejoras'     => ucfirst($this->input->post('habilidades_puntos_mejoras')),
				'calidad_aspectos_positivos'     => ucfirst($this->input->post('calidad_aspectos_positivos')),
				'calidad_puntos_mejoras'         => ucfirst($this->input->post('calidad_puntos_mejoras')),
				'rasgos_aspectos_positivos'      => ucfirst($this->input->post('rasgos_aspectos_positivos')),
				'rasgos_puntos_mejoras'          => ucfirst($this->input->post('rasgos_puntos_mejoras')),
				'observaciones'                  => ucfirst($this->input->post('observaciones'))
			);

			$supervision_id = $this->Supervision_model->actualizarSupervision($id, $supervision);

			if ($supervision_id) {
				$this->session->set_flashdata('status','<i class="fa fa-check"></i> Supervisión actualizada');
				$this->session->set_flashdata('color','alert-success');
			}
			else {
				$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al actualizar supervisión');
				$this->session->set_flashdata('color','alert-danger');			
			}
			header('Location: ' . base_url() . 'pasantias');
		}
	}

	public function eliminar($id)	{
		if($this->Supervision_model->eliminarSupervision($id)){
			$this->session->set_flashdata('status','<i class="fa fa-check"></i> Supervisión eliminada');
			$this->session->set_flashdata('color','alert-success');
		}
		else {
			$this->session->set_flashdata('status','<i class="fa fa-times"></i> Error al eliminar supervisión');
			$this->session->set_flashdata('color','alert-danger');			
		}
	//	print $flag;
		header('Location: ' . base_url() . 'pasantias');
	}
}
?>