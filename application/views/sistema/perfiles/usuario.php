		<!-- VERSIÓN MÓVIL (sm ONLY) -->
		<div class="row d-md-none" id="informacion-perfil-movil">
			
			<!-- Para mostrar la imagen de perfil -->
			<?php if ($usuario->foto): ?>
				<div class="col-12 text-center">
					<img src="<?= base_url() ?>profile/images/<?= $usuario->foto ?>" alt="Foto de perfil de <?= $usuario->primer_nombre ?> <?= $usuario->primer_apellido ?>" width="200" height="200" class="border-radius-6">			
				</div>
			<?php endif; ?>
			
			<!-- Muestra el nombre y rol -->
			<div class="col-12 text-center">
				<!-- Para mostrar el nombre del estudiante -->
				<span class="semi-bold text-size-15">
					<?=	$usuario->primer_nombre ?>

					<?php if ($usuario->segundo_nombre): ?>
						<?= $usuario->segundo_nombre ?>
						<br>
					<?php endif; ?>

					<?= $usuario->primer_apellido ?>

					<?php if ($usuario->segundo_apellido): ?>
						<?= $usuario->segundo_apellido ?>
					<?php endif; ?>					
				</span>
				<br>

				<!-- Para mostrar el rol-->
				<?php
					$usuario_info = $this->Usuario_model->obtenerUsuario($usuario->usuario_id);
				?>
				<span id="rol" class="extra-light">
					<?= $usuario_info->rol ?>
				</span>
			</div>

			<!-- Para mostrar una breve descripción -->	
			<div class="col-12 margin_sm_b-t sangria-sm">
				<?php if ($usuario->descripcion): ?>
					<span class="text-italic">
						<?= $usuario->descripcion ?>
					</span>
				<?php endif; ?>
			</div>

			<!-- TÍTULO DE INFORMACIÓN -->
			<div class="col-12 text-center margin_sm_b-t">
				<span class="semi-bold">
					Información
				</span>	
			</div>

			<!-- NAV-TABS MÓVIL PARA OTROS USUARIOS -->
			<?php if($usuario_info->rol != 'Estudiante'): ?>
				<div class="col-12">
					<nav>
					  <div class="nav nav-tabs text-size-11" id="nav-tab" role="tablist">
					    <a class="nav-item nav-link active" id="nav-personal-tab-movil" data-toggle="tab" href="#nav-personal-movil" role="tab" aria-controls="nav-personal-movil" aria-selected="true">
					    	Personal
					    </a>

					    <?php if (
								$usuario->correo_institucional || 
								$usuario->correo_personal || 
								$usuario->telefono_movil || 
								$usuario->telefono_residencial 
								): ?>
								<a class="nav-item nav-link" id="nav-contacto-tab-movil" data-toggle="tab" href="#nav-contacto-movil" role="tab" aria-controls="nav-contacto-movil" aria-selected="false">
									Contacto
								</a>
							<?php endif; ?>					    
					  </div>
					</nav>

					<div class="tab-content" id="nav-tabContent">

						<!-- Contenido de la información personal -->
					  <div class="tab-pane fade show active" id="nav-personal-movil" role="tabpanel" aria-labelledby="nav-personal-tab-movil">

					  	<div class="row">
					  		<!-- Para mostrar la cédula de identidad -->
								<div class="col-12">
									<span>Cédula de Identidad: <?= $usuario->nacionalidad ?>-<?= $usuario->ci ?></span>
								</div>
								<br>

								<!-- Para mostrar la fecha de nacimiento -->
								<?php if ($usuario->cumpleaños != '0000-00-00'): ?>				
									<div class="col-12">
										<span>
											<?php $fecha = new DateTime($usuario->cumpleaños); ?>
											<?= "Fecha de nacimiento: " . $fecha->format('d-m-Y'); ?>
											<!-- Imprime fecha y hora actual -->
											<? date("Y-m-d H:i:s") ?>
										</span>
									</div>
									<br>
								<?php endif; ?>
								
								<!-- Para mostrar la dirección residencial -->
								<?php if ($usuario->direccion_residencial): ?>
									<div class="col-12">
										<span>			
											<?= "Dirección residencial: " . $usuario->direccion_residencial ?>
										</span>
									</div>
									<br>	
								<?php endif; ?>									
					  	</div>				  	
					  </div>

					  <!-- Contenido de la información de contacto -->
					  <div class="tab-pane fade" id="nav-contacto-movil" role="tabpanel" aria-labelledby="nav-contacto-tab-movil">
					  	<div class="row">

					  		<!-- Para mostrar el correo institucional -->	
								<?php if ($usuario->correo_institucional): ?>
									<div class="col-12">
										<span>			
											<?= "Correo institucional: " . $usuario->correo_institucional ?>
										</span>
									</div>
									<br>
								<?php endif; ?>

								<!-- Para mostrar el correo personal -->	
								<?php if ($usuario->correo_personal): ?>
									<div class="col-12">
										<span>		
											<?= "Correo personal: " . $usuario->correo_personal ?>
										</span>
									</div>
									<br>
								<?php endif; ?>				
								
								<!-- Para mostrar los números telefónicos -->
								<?php if ($usuario->telefono_movil): ?>		
									<div class="col-12">
										<span>		
											<?= "Teléfono móvil: " . $usuario->telefono_movil ?>
										</span>
									</div>
									<br>
								<?php endif; ?>

								<?php if ($usuario->telefono_residencial): ?>
									<div class="col-12">
										<span>
											<?= "Teléfono residencial: " . $usuario->telefono_residencial ?>
										</span>
									</div>
									<br>
								<?php endif; ?>								
					  	</div>
					  </div>

					</div>
				</div>
			<?php endif; ?>

			<!-- NAV-TABS MÓVIL PARA USUARIOS ESTUDIANTES -->
			<?php if($usuario_info->rol == 'Estudiante'): ?>
				<div class="col-12">
					<nav>
					  <div class="nav nav-tabs text-size-11" id="nav-tab" role="tablist">
					    <a class="nav-item nav-link active" id="nav-personal-tab-est-movil" data-toggle="tab" href="#nav-personal-est-movil" role="tab" aria-controls="nav-personal-est-movil" aria-selected="true">Personal</a>

					    <a class="nav-item nav-link" id="nav-requisitos-tab-movil" data-toggle="tab" href="#nav-requisitos-movil" role="tab" aria-controls="nav-requisitos-movil" aria-selected="false">Requisitos</a>

					    <a class="nav-item nav-link" id="nav-pasantia-tab-movil" data-toggle="tab" href="#nav-pasantia-movil" role="tab" aria-controls="nav-pasantia-movil" aria-selected="false">Pasantía</a>
					  </div>
					</nav>
					<div class="tab-content" id="nav-tabContent">

						<!-- Contenido de la información personal -->
					  <div class="tab-pane fade show active" id="nav-personal-est-movil" role="tabpanel" aria-labelledby="nav-personal-tab-est-movil">
					  	<div class="row">

					  		<!-- Para mostrar la(s) carrera(s) del estudiante -->
								<?php if ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($usuario->usuario_id)): ?>
									<div class="col-12">
										<span>Carrera(s): </span>
										<?php $cantidad_carrera = 0; ?>
										<?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($usuario->usuario_id) as $carrera_estudiante): ?>
											<?php $cantidad_carrera = $cantidad_carrera + 1; ?>
											<!-- Condicional para separar las carreras con comas -->
											<?php if ($cantidad_carrera > 1): ?> 
												<?php echo ", "; ?>
											<?php endif;?>
											<!-- Fin del condicional para separar las carreras con comas -->
											<?= $this->Carrera_model->obtenerCarrera($carrera_estudiante->carrera_id)->nombre ?>
										<?php endforeach;  ?>
									</div>
								<?php endif; ?>


					  		<!-- Para mostrar la cédula de identidad -->
								<div class="col-12">
									<span>
										Cédula de Identidad: 
										<?= $usuario->nacionalidad ?>-<?= $usuario->ci ?>
									</span>
								</div>
								<br>

								<!-- Para mostrar la fecha de nacimiento -->
								<?php if ($usuario->cumpleaños != '0000-00-00'): ?>				
									<div class="col-12">
										<span>
											<?php $fecha = new DateTime($usuario->cumpleaños); ?>
											<?= "Fecha de nacimiento: " . $fecha->format('d-m-Y'); ?>
											<!-- Imprime fecha y hora actual -->
											<? date("Y-m-d H:i:s") ?>
										</span>
									</div>
									<br>
								<?php endif; ?>
								
								<!-- Para mostrar la dirección residencial -->
								<?php if ($usuario->direccion_residencial): ?>
									<div class="col-12">
										<span>			
											<?= "Dirección residencial: " . $usuario->direccion_residencial ?>
										</span>
									</div>
									<br>	
								<?php endif; ?>
								
								<!-- Para mostrar la información de contacto -->
								<?php if (
									$usuario->correo_institucional || 
									$usuario->correo_personal || 
									$usuario->telefono_movil || 
									$usuario->telefono_residencial 
									): ?>

									<!-- Para mostrar el correo institucional -->
									<?php if ($usuario->correo_institucional): ?>
										<div class="col-12">
											<span>			
												<?= "Correo institucional: " . $usuario->correo_institucional ?>
											</span>
										</div>
										<br>
									<?php endif; ?>

									<!-- Para mostrar el correo personal -->	
									<?php if ($usuario->correo_personal): ?>
										<div class="col-12">
											<span>		
												<?= "Correo personal: " . $usuario->correo_personal ?>
											</span>
										</div>
										<br>
									<?php endif; ?>				
									
									<!-- Para mostrar los números telefónicos -->
									<?php if ($usuario->telefono_movil): ?>		
										<div class="col-12">
											<span>		
												<?= "Teléfono móvil: " . $usuario->telefono_movil ?>
											</span>
										</div>
										<br>
									<?php endif; ?>

									<?php if ($usuario->telefono_residencial): ?>
										<div class="col-12">
											<span>
												<?= "Teléfono residencial: " . $usuario->telefono_residencial ?>
											</span>
										</div>
										<br>
									<?php endif; ?>	
								<?php endif; ?>									
					  	</div>
					  </div>

					  <!-- Contenido de la información de los requisitos -->
					  <div class="tab-pane fade" id="nav-requisitos-movil" role="tabpanel" aria-labelledby="nav-requisitos-tab-movil">

							<div class="row">

								<!-- Inicio de mensaje flash -->
								<?php if($this->session->flashdata('status')): ?>
									<div class="col-12">
										<div class="alert <?= $this->session->flashdata('color') ?>  border-radius-10" role="alert">
										 <?= $this->session->flashdata('status') ?>
										</div>
									</div>
								<?php endif; ?>								
								<!-- Fin de mensaje flash -->

						  	<!-- Listado de los requisitos del estudiante -->
								<div class="col-12">
									<div id="accordion-movil">

										<?php 
											$requisitos_estudiante = $this->Estudiante_requisito_model->obtenerRequisitos_por_Estudiante($usuario->usuario_id);
										?>

										<?php if ($requisitos_estudiante): ?>
											<?php foreach ($requisitos_estudiante as $requisito_estudiante): ?>
												<?php $requisito = $this->Requisito_model->obtenerRequisito($requisito_estudiante->requisito_id); ?>

												<!-- Estatus -->
								      	<?php if($requisito_estudiante->estatus == 'Adjuntado'): ?>
													<?php $color = 'bg-gris' ?>
												<?php elseif ($requisito_estudiante->estatus == 'Corregir'): ?>
													<?php $color = 'bg-amarillo-l' ?>
												<?php elseif ($requisito_estudiante->estatus == 'Verificado'): ?>
													<?php $color = 'bg-verde-l' ?>
												<?php else: ?>
													<?php $color = 'bg-light' ?>
												<?php endif; ?>

											  <div class="card no-margin">
											    <div class="card-header cursor-pointer <?= $color ?>" id="requisito-<?= $requisito_estudiante->requisito_id ?>-movil"  data-toggle="collapse" data-target="#collapse-<?= $requisito_estudiante->requisito_id ?>-movil" aria-controls="collapse-<?= $requisito_estudiante->requisito_id ?>-movil">
											      <h5 class="mb-0 text-size-11">
											      	<!-- Requisito -->
											      	<?= $requisito->nombre ?>
											      	
											        <p class="float-right text-size-10"><?= $requisito_estudiante->estatus ?></p>
											      </h5>
											    </div>

											    <div id="collapse-<?= $requisito_estudiante->requisito_id ?>-movil" class="collapse" aria-labelledby="requisito-<?= $requisito_estudiante->requisito_id ?>-movil" data-parent="#accordion-movil">
											      <div class="card-body">
											      	<!-- Descripción -->
											      	<p class="text-size-11">
											      		<?= $requisito->descripcion ?>
											      	</p>
														
															<div class="row justify-content-center margin-bottom-sm">
																<?php if ($requisito_estudiante->archivo): ?>
																	<div class="col-6 col-sm-3 col-md-2 text-center">
																		<a href="<?= $requisito_estudiante->archivo ?>" target="_blank" class="btn btn-info" role="button">Ver archivo</a>
																	</div>

																	<?php if( ($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador') && ($requisito_estudiante->estatus != 'Verificado')): ?>
																		
																		<form action="<?= base_url() ?>requisitos/verificarRequisito" method="POST">
																			<div class="col-6 col-sm-3 col-md-2 text-center no-padding">
																				<input type="submit" value="Verificar" class="btn btn-success">												
																			</div>

																			<input type="hidden" name="id" value="<?= $requisito_estudiante->estudiante_requisito_id ?>">
																			<input type="hidden" name="estudiante_id" value="<?= $usuario->usuario_id ?>">	

																		</form>

																	<?php endif; ?>

																<?php endif; ?>	
															</div>

															<?php if($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador'): ?>
																<form action="<?= base_url() ?>requisitos/agregarObservacion" method="POST">
																	<div class="row justify-content-center">
																		<div class="col-12 col-sm-6 col-md-8 margin-bottom-sm">
																			<input type="hidden" name="id" value="<?= $requisito_estudiante->estudiante_requisito_id ?>">
																			<input type="hidden" name="estudiante_id" value="<?= $usuario->usuario_id ?>">
																			<input type="text" name="observacion" value="<?= $requisito_estudiante->observacion ?>" placeholder="Escribe una observación" class="form-control">
																		</div>

																		<?php if ($requisito_estudiante->observacion): ?>
																			<div class="col-5 col-sm-3 col-md-2 text-center margin-bottom-sm">
																				<input type="submit" value="Actualizar" class="btn btn-success">												
																			</div>
																		<?php else: ?>
																			<div class="col-5 col-sm-3 col-md-3 text-center margin-bottom-sm">
																				<input type="submit" value="Agregar Observación" class="btn btn-success">												
																			</div>
																		<?php endif; ?>													
																	</div>											
																</form>
															<?php endif; ?>

											      </div>
											    </div>
											  </div>
											<?php endforeach; ?>
									  <?php else: ?>
											<div class="alert alert-warning border-radius-10" role="alert">
												<h4 class="semi-bold text-center"> 
													<i class="fa fa-warning"></i> <br>
													¡No hay requisitos registrados!
												</h4>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
					  </div>

					  <!-- Contenido de la información de las pasantías -->
					  <div class="tab-pane fade" id="nav-pasantia-movil" role="tabpanel" aria-labelledby="nav-pasantia-tab-movil">
					  	<?php foreach ($this->Pasantia_model->obtenerPasantias_habilitadas_por_estudiante($usuario->usuario_id) as $pasantia): ?>

					  		<!-- Pasantía -->
					  		<div class="row">
					  			<div class="col-12">

					  				<!-- Título -->
					  				<div class="row">
					  					<div class="col text-center">
					  						<span class="semi-bold">Pasantía</span>
					  					</div>
					  				</div>

					  				<!-- Departamento asignado -->
					  				<div class="row">
					  					<div class="col">
					  						<span>Departamento asignado: </span>
					  						<?= $pasantia->departamento_estudiante ?>
					  					</div>
					  				</div>					  				
										
										<!-- Fechas -->
					  				<div class="row">
					  					<div class="col">
					  						<span>Fecha de inicio:</span>
					  						<?php $fecha_inicio = new DateTime($pasantia->fecha_inicio); ?>
												<?= $fecha_inicio->format('d-m-Y'); ?>	
					  					</div>
					  				</div>
					  				<div class="row">
					  					<div class="col">
					  						<span>Fecha de finalización: </span>
					  						<?php $fecha_fin = new DateTime($pasantia->fecha_fin); ?>
												<?= $fecha_fin->format('d-m-Y'); ?>	
					  					</div>
					  				</div>
										
										<!-- Periodos -->
					  				<div class="row">
					  					<div class="col">
					  						<span>Inscripción: </span>
					  						<?php 
					  							$periodo_incripcion = $this->Periodo_model->obtenerPeriodo($pasantia->inscripcion_periodo_id);
					  						?>
					  						<?= $periodo_incripcion->nombre ?>
					  					</div>
					  				</div>
					  				<div class="row">
					  					<div class="col">
					  						<span>Sección: </span>
					  						<?= $pasantia->seccion ?>
					  					</div>
					  				</div>
					  				<div class="row">
					  					<div class="col">
					  						<span>Realización: </span>
					  						<?php 
					  							$periodo_realizacion = $this->Periodo_model->obtenerPeriodo($pasantia->realizacion_periodo_id);
					  						?>
					  						<?= $periodo_realizacion->nombre ?>
					  					</div>
					  				</div>
					  			</div>
					  		</div>

					  		<?php
					  			$empresa = $this->Empresa_model->obtenerEmpresa_por_pasantia($pasantia->pasantia_id);
					  		?>
								<!-- Empresa -->
						  	<div class="row margin_b-t">
						  		<div class="col-12">
						  			<div class="row">
						  				<div class="col text-center">
						  					<span class="semi-bold">Empresa</span>
						  				</div>
						  			</div>
						  			<div class="row">
						  				<div class="col">
						  					<?= $empresa->nombre ?>
						  				</div>
						  			</div>
						  			<div class="row">
						  				<div class="col">
						  					<span>RIF: </span> 
						  					<?= $empresa->rif ?>
						  				</div>
						  			</div>
						  			<div class="row">
						  				<div class="col">
						  					<span>Teléfonos: </span>
						  					<?php echo $empresa->telefono1; 
						  						if ($empresa->telefono2) {
						  							echo " / " . $empresa->telefono2;
						  						}
						  					?>
						  				</div>
						  			</div>

						  			<div class="row">
						  				<div class="col">
						  					<span>Dirección: </span>
						  					<?= $empresa->direccion ?>
						  				</div>
						  			</div>
						  				
						  		</div> 		
						  	</div>

					  		<!-- Tutor laboral -->
					  		<div class="row margin_b-t">
					  			<div class="col-12">					  				
					  				<div class="row">
					  					<div class="col text-center">
					  						<span class="semi-bold">Tutor laboral </span>
					  					</div>
					  				</div>

					  				<div class="row justify-content-around">
					  					<div class="col">
					  						<span>Apellidos y nombres: </span>
					  						<?= $pasantia->tl_apellidos . " " . $pasantia->tl_nombres ?>
					  					</div>
					  				</div>
					  				<div class="row">
					  					<div class="col">
					  						<span>C.I: </span>
					  						<?= $pasantia->tl_ci ?>
					  					</div>
					  				</div>

					  				<div class="row justify-content-around">
					  					<div class="col">
					  						<span>Cargo: </span>
					  						<?= $pasantia->tl_cargo ?>
					  					</div>
					  				</div>
					  				<div class="row">
					  					<div class="col">
					  						<span>Teléfono: </span>
					  						<?= $pasantia->tl_telefono ?>
					  					</div>
					  				</div>

					  				<div class="row">
					  					<div class="col">
					  						<span>Correo: </span>
					  						<?= $pasantia->tl_correo ?>
					  					</div>
					  				</div>
					  			</div>
					  		</div>

					  		<!-- Tutor académico -->
						  	<?php if ($pasantia->tutor_academico_id): ?>
						  		<div class="row margin_b-t">
						  			<div class="col-12">					  				
						  				<div class="row">
						  					<div class="col text-center">
						  						<span class="semi-bold">Tutor académico </span>
						  					</div>
						  				</div>

						  				<?php
						  					$tutorA = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->tutor_academico_id);
						  				?>

						  				<div class="row">
						  					<div class="col">
						  						<span>Apellidos y nombres: </span>
						  						<?= $tutorA->primer_apellido ?>
						  						<?php if ($tutorA->segundo_apellido): ?>
						  							<?= $tutorA->segundo_apellido ?>
						  						<?php endif; ?>
						  						<?= $tutorA->primer_nombre ?>
						  						<?php if($tutorA->segundo_nombre): ?>
						  							<?= $tutorA->segundo_nombre ?>
						  						<?php endif; ?>
						  					</div>
						  					
						  				</div>

						  				<div class="row">
						  					<div class="col">
						  						<span>C.I: </span>
						  						<?= $tutorA->nacionalidad . "-" . $tutorA->ci ?>
						  					</div>
						  				</div>
						  				<div class="row">
						  					<div class="col">
						  						<span>Correo: </span>
						  						<?= $tutorA->correo_institucional ?>
						  					</div>
						  				</div>
						  			</div>
						  		</div>
						  	<?php endif; ?>					  		
					  	<?php endforeach; ?>
					  </div>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<!-- Cierre de la Versión Móvil -->

		<!-- VERSIÓN TABLET Y DESKTOP -->
		<div class="d-none d-md-block" id="informacion-perfil">
			
			<!-- Para el nombre del usuario, carrera y rol -->
			<div class="row" style="border-bottom: 1px solid #dddddd;">

				<!-- Nombres y Apellidos -->
				<div class="col-9 margin-l-200">
					<span class="text-size-20">
						<?=	$usuario->primer_nombre ?>
						<?php if ($usuario->segundo_nombre): ?>
							<?= $usuario->segundo_nombre ?>
						<?php endif; ?>
						<?= $usuario->primer_apellido ?>
						<?php if ($usuario->segundo_apellido): ?>
							<?= $usuario->segundo_apellido ?>
						<?php endif; ?>
					</span>
				</div>

				<!-- Para identificar el Rol del usuario -->
				<?php
				$usuario_info = $this->Usuario_model->obtenerUsuario($usuario->usuario_id);
				?>
				
				<!-- Carrera(s) y Rol  -->
				<?php if($usuario_info->rol == 'Estudiante'): ?>
					<div class="col-9 margin-l-200">
						<!-- Para mostrar la(s) carrera(s) del estudiante -->
						<?php if ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($usuario->usuario_id)): ?>
							<?php $cantidad_carrera = 0; ?>
							<?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($usuario->usuario_id) as $carrera_estudiante): ?>
								<?php $cantidad_carrera = $cantidad_carrera + 1; ?>
								<!-- Condicional para separar las carreras con comas -->
								<?php if ($cantidad_carrera > 1): ?> 
									<?php echo ", "; ?>
								<?php endif;?>
								<!-- Fin del condicional para separar las carreras con comas -->
								<?= $this->Carrera_model->obtenerCarrera($carrera_estudiante->carrera_id)->nombre ?>
							<?php endforeach;  ?>
						<?php endif; ?>					
					</div>
				<?php else: ?>
					<div class="col-9 margin-l-200">
						<span id="rol" class="extra-light">
							<?= $usuario_info->rol ?>
						</span>
					</div>
				<?php endif; ?>					
			</div>

			<div class="row">

				<!-- Foto de perfil y los vertical pills -->
				<div class="col-md-4 col-xl-3">					
					<?php if ($usuario->foto): ?> 
						<div class="row padding_l margin_t-55">
							<img src="<?= base_url() ?>profile/images/<?= $usuario->foto ?>" alt="Foto de perfil de <?= $usuario->primer_nombre ?> <?= $usuario->primer_apellido ?>" width="180" height="180" class="border-radius-6">
						</div>
					<?php else: ?>
						<div class="row padding_l margin_t-55 text-size-100">
							<i class="fa fa-user-circle-o"></i>
						</div>
					<?php endif; ?>

					<div class="row padding_l margin_t">
						<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

						  <!-- Información personal -->
						  <a class="nav-link active" id="v-pills-personal-tab" data-toggle="pill" href="#v-pills-personal" role="tab" aria-controls="v-pills-personal" aria-selected="true">
						  	Personal
						  </a>

						  <!-- Información de contacto -->
							<?php if (
								$usuario->correo_institucional || 
								$usuario->correo_personal || 
								$usuario->telefono_movil || 
								$usuario->telefono_residencial 
								): ?>
								<a class="nav-link" id="v-pills-contacto-tab" data-toggle="pill" href="#v-pills-contacto" role="tab" aria-controls="v-pills-contacto" aria-selected="false">
							  	Contacto
							  </a>
							<?php endif; ?>

						  <?php if ($usuario_info->rol == 'Estudiante'): ?>
						  	<a class="nav-link" id="v-pills-requisitos-tab" data-toggle="pill" href="#v-pills-requisitos" role="tab" aria-controls="v-pills-requisitos" aria-selected="false">
							  	Requisitos
							  </a>

							  <!-- Si el estudiante tiene alguna pasantia habilitada registrada -->
							  <?php if($this->Pasantia_model->obtenerPasantias_habilitadas_por_estudiante($usuario->usuario_id)): ?>
							  	<a class="nav-link" id="v-pills-pasantias-tab" data-toggle="pill" href="#v-pills-pasantias" role="tab" aria-controls="v-pills-pasantias" aria-selected="false">
								  	Pasantías
								  </a>
									
									<!-- Verificar si una pasantía tiene actividades -->
								  <?php foreach ($this->Pasantia_model->obtenerPasantias_habilitadas_por_estudiante($usuario->usuario_id) as $pasantia): ?>

							  		<?php if ($this->Actividad_model->obtenerActividades_por_pasantia($pasantia->pasantia_id)): ?>
							  			<a class="nav-link" id="v-pills-actividades-tab" data-toggle="pill" href="#v-pills-actividades" role="tab" aria-controls="v-pills-actividades" aria-selected="false">
										  	Actividades
										  </a>
							  		<?php endif; ?>
								  <?php endforeach; ?>
							  <?php endif; ?>								  
						  <?php endif; ?>							  
						</div>
					</div>
				</div>
				
				<!-- Área del contenido -->
				<div class="col-md-8 margin_l-44">
					<div class="tab-content" id="v-pills-tabContent">

						<!-- Información personal -->
					  <div class="tab-pane fade show active margin-r-44" id="v-pills-personal" role="tabpanel" aria-labelledby="v-pills-personal-tab">

					  	<div class="row">
					  		<!-- Para mostrar una breve descripción -->	
								<?php if ($usuario->descripcion): ?>
									<div class="col-12 margin_b">
										<span class="text-italic">
											<?= $usuario->descripcion ?>
										</span>
									</div>
								<?php endif; ?>

								<!-- Para mostrar la cédula de identidad -->
								<div class="col-md-6">
									<span>
										Cédula de Identidad: <?= $usuario->nacionalidad ?>-<?= $usuario->ci ?>			
									</span>
								</div>
								<br>

								<!-- Para mostrar la fecha de nacimiento -->
								<?php if ($usuario->cumpleaños != '0000-00-00'): ?>				
									<div class="col-md-6">
										<span>
											<?php $fecha = new DateTime($usuario->cumpleaños); ?>
											<?= "Fecha de nacimiento: " . $fecha->format('d-m-Y'); ?>
											<!-- Imprime fecha y hora actual -->
											<? date("Y-m-d H:i:s") ?>
										</span>
									</div>
									<br>
								<?php endif; ?>
								
								<!-- Para mostrar la dirección residencial -->
								<?php if ($usuario->direccion_residencial): ?>
									<div class="col-md-6">
										<span>			
											<?= "Dirección residencial: " . $usuario->direccion_residencial ?>
										</span>
									</div>
									<br>
								<?php endif; ?>
					  	</div>		  	
					  </div>

					  <!-- Información de contacto -->
					  <div class="tab-pane fade margin-r-44" id="v-pills-contacto" role="tabpanel" aria-labelledby="v-pills-contacto-tab">
					  	
					  	<div class="row">

					  		<!-- Para mostrar el correo institucional -->
					  		<?php if ($usuario->correo_institucional): ?>	
									<div class="col-md-6">
										<span>			
											<?= "Correo institucional: " . $usuario->correo_institucional ?>
										</span>
									</div>
									<br>
								<?php endif; ?>

								<!-- Para mostrar el correo personal -->	
								<?php if ($usuario->correo_personal): ?>
									<div class="col-md-6">
										<span>		
											<?= "Correo personal: " . $usuario->correo_personal ?>
										</span>
									</div>
									<br>
								<?php endif; ?>				
								
								<!-- Para mostrar los números telefónicos -->
								<?php if ($usuario->telefono_movil): ?>		
									<div class="col-md-6">
										<span>		
											<?= "Teléfono móvil: " . $usuario->telefono_movil ?>
										</span>
									</div>
									<br>
								<?php endif; ?>

								<?php if ($usuario->telefono_residencial): ?>
									<div class="col-md-6">
										<span>
											<?= "Teléfono residencial: " . $usuario->telefono_residencial ?>
										</span>
									</div>
									<br>
								<?php endif; ?>
					  	</div>
					  </div>

					  <!-- Información de los requisitos -->
					  <div class="tab-pane fade margin-r-44" id="v-pills-requisitos" role="tabpanel" aria-labelledby="v-pills-requisitos-tab">
					  	<div class="row">

					  		<!-- Listado de los requisitos del estudiante -->
								<div class="col-12">
									<div id="accordion">

										<?php 
											$requisitos_estudiante = $this->Estudiante_requisito_model->obtenerRequisitos_por_Estudiante($usuario->usuario_id);
										?>

										<?php if ($requisitos_estudiante): ?>
											<?php foreach ($requisitos_estudiante as $requisito_estudiante): ?>
												<?php $requisito = $this->Requisito_model->obtenerRequisito($requisito_estudiante->requisito_id); ?>

												<!-- Estatus -->
								      	<?php if($requisito_estudiante->estatus == 'Adjuntado'): ?>
													<?php $color = 'bg-gris' ?>
												<?php elseif ($requisito_estudiante->estatus == 'Corregir'): ?>
													<?php $color = 'bg-amarillo-l' ?>
												<?php elseif ($requisito_estudiante->estatus == 'Verificado'): ?>
													<?php $color = 'bg-verde-l' ?>
												<?php else: ?>
													<?php $color = 'bg-light' ?>
												<?php endif; ?>

											  <div class="card no-margin">
											    <div class="card-header cursor-pointer <?= $color ?>" id="requisito-<?= $requisito_estudiante->requisito_id ?>"  data-toggle="collapse" data-target="#collapse-<?= $requisito_estudiante->requisito_id ?>" aria-controls="collapse-<?= $requisito_estudiante->requisito_id ?>">
											      <h5 class="mb-0 text-size-12">
											      	<!-- Requisito -->
											      	<?= $requisito->nombre ?>
											      	
											        <p class="float-right text-size-10"><?= $requisito_estudiante->estatus ?></p>
											      </h5>
											    </div>

											    <div id="collapse-<?= $requisito_estudiante->requisito_id ?>" class="collapse" aria-labelledby="requisito-<?= $requisito_estudiante->requisito_id ?>" data-parent="#accordion">
											      <div class="card-body">
											      	<!-- Descripción -->
											      	<p>
											      		<?= $requisito->descripcion ?>
											      	</p>
														
															<div class="row justify-content-center margin-bottom-sm">
																<?php if ($requisito_estudiante->archivo): ?>
																	<div class="col-6 col-sm-3 col-md-2 text-center">
																		<a href="<?= $requisito_estudiante->archivo ?>" target="_blank" class="btn btn-info" role="button">Ver archivo</a>
																	</div>

																	<?php if( ($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador') && ($requisito_estudiante->estatus != 'Verificado')): ?>
																		
																		<form action="<?= base_url() ?>requisitos/verificarRequisito" method="POST">
																			<div class="col-6 col-sm-3 col-md-2 text-center no-padding">
																				<input type="submit" value="Verificar" class="btn btn-success">												
																			</div>

																			<input type="hidden" name="id" value="<?= $requisito_estudiante->estudiante_requisito_id ?>">
																			<input type="hidden" name="estudiante_id" value="<?= $usuario->usuario_id ?>">	

																		</form>

																	<?php endif; ?>

																<?php endif; ?>	
															</div>

															<?php if($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador'): ?>
																<form action="<?= base_url() ?>requisitos/agregarObservacion" method="POST">
																	<div class="row justify-content-center">
																		<div class="col-12 col-sm-6 margin-bottom-sm">
																			<input type="hidden" name="id" value="<?= $requisito_estudiante->estudiante_requisito_id ?>">
																			<input type="hidden" name="estudiante_id" value="<?= $usuario->usuario_id ?>">
																			<input type="text" name="observacion" value="<?= $requisito_estudiante->observacion ?>" placeholder="Escribe una observación" class="form-control">
																		</div>

																		<?php if ($requisito_estudiante->observacion): ?>
																			<div class="col-5 col-sm-3 col-md-2 text-center margin-bottom-sm">
																				<input type="submit" value="Actualizar" class="btn btn-success">												
																			</div>
																		<?php else: ?>
																			<div class="col-5 col-sm-3 col-md-3 text-center margin-bottom-sm">
																				<input type="submit" value="Agregar Observación" class="btn btn-success">												
																			</div>
																		<?php endif; ?>													
																	</div>											
																</form>
															<?php endif; ?>

											      </div>
											    </div>
											  </div>
											<?php endforeach; ?>
									  <?php else: ?>
											<div class="alert alert-warning border-radius-10" role="alert">
												<h5 class="semi-bold text-center no-margin"> 
													<i class="fa fa-warning"></i> <br>
													¡No hay requisitos registrados!
												</h5>
											</div>
										<?php endif; ?>
									</div>
								</div>
					  	</div>
					  </div>

					  <!-- Información de las pasantías -->
					  <div class="tab-pane fade margin-r-44" id="v-pills-pasantias" role="tabpanel" aria-labelledby="v-pills-pasantias-tab">

					  	<?php foreach ($this->Pasantia_model->obtenerPasantias_habilitadas_por_estudiante($usuario->usuario_id) as $pasantia): ?>

					  		<!-- Pasantía -->
					  		<div class="row">
					  			<div class="col-12">

					  				<!-- Título -->
					  				<div class="row">
					  					<div class="col text-center">
					  						<span class="semi-bold">Pasantía</span>
					  					</div>
					  				</div>

					  				<!-- Departamento asignado -->
					  				<div class="row">
					  					<div class="col">
					  						<span>Departamento asignado: </span>
					  						<?= $pasantia->departamento_estudiante ?>
					  					</div>
					  				</div>					  				
										
										<!-- Fechas -->
					  				<div class="row">
					  					<div class="col">
					  						<span>Fecha de inicio:</span>
					  						<?php $fecha_inicio = new DateTime($pasantia->fecha_inicio); ?>
												<?= $fecha_inicio->format('d-m-Y'); ?>	
					  					</div>
					  					<div class="col">
					  						<span>Fecha de finalización: </span>
					  						<?php $fecha_fin = new DateTime($pasantia->fecha_fin); ?>
												<?= $fecha_fin->format('d-m-Y'); ?>	
					  					</div>
					  				</div>
										
										<!-- Periodos -->
					  				<div class="row">
					  					<div class="col">
					  						<span>Inscripción: </span>
					  						<?php 
					  							$periodo_incripcion = $this->Periodo_model->obtenerPeriodo($pasantia->inscripcion_periodo_id);
					  						?>
					  						<?= $periodo_incripcion->nombre ?>
					  					</div>
					  					<div class="col">
					  						<span>Sección: </span>
					  						<?= $pasantia->seccion ?>
					  					</div>
					  					<div class="col">
					  						<span>Realización: </span>
					  						<?php 
					  							$periodo_realizacion = $this->Periodo_model->obtenerPeriodo($pasantia->realizacion_periodo_id);
					  						?>
					  						<?= $periodo_realizacion->nombre ?>
					  					</div>
					  				</div>
					  			</div>
					  		</div>

					  		<?php
					  			$empresa = $this->Empresa_model->obtenerEmpresa_por_pasantia($pasantia->pasantia_id);
					  		?>
								<!-- Empresa -->
						  	<div class="row margin_b-t">
						  		<div class="col-12">
						  			<div class="row">
						  				<div class="col text-center">
						  					<span class="semi-bold">Empresa</span>
						  				</div>
						  			</div>
						  			<div class="row">
						  				<div class="col">
						  					<?= $empresa->nombre ?>
						  				</div>
						  			</div>
						  			<div class="row">
						  				<div class="col-4">
						  					<span>RIF: </span> 
						  					<?= $empresa->rif ?>
						  				</div>
						  				<div class="col-8">
						  					<span>Teléfonos: </span>
						  					<?php echo $empresa->telefono1; 
						  						if ($empresa->telefono2) {
						  							echo " / " . $empresa->telefono2;
						  						}
						  					?>
						  				</div>
						  			</div>

						  			<div class="row">
						  				<div class="col">
						  					<span>Dirección: </span>
						  					<?= $empresa->direccion ?>
						  				</div>
						  			</div>
						  				
						  		</div> 		
						  	</div>

					  		<!-- Tutor laboral -->
					  		<div class="row margin_b-t">
					  			<div class="col-12">					  				
					  				<div class="row">
					  					<div class="col text-center">
					  						<span class="semi-bold">Tutor laboral </span>
					  					</div>
					  				</div>

					  				<div class="row justify-content-around">
					  					<div class="col">
					  						<span>Apellidos y nombres: </span>
					  						<?= $pasantia->tl_apellidos . " " . $pasantia->tl_nombres ?>
					  					</div>
					  					<div class="col-4">
					  						<span>C.I: </span>
					  						<?= $pasantia->tl_ci ?>
					  					</div>
					  				</div>

					  				<div class="row justify-content-around">
					  					<div class="col">
					  						<span>Cargo: </span>
					  						<?= $pasantia->tl_cargo ?>
					  					</div>
					  					<div class="col-5">
					  						<span>Teléfono: </span>
					  						<?= $pasantia->tl_telefono ?>
					  					</div>
					  				</div>

					  				<div class="row">
					  					<div class="col">
					  						<span>Correo: </span>
					  						<?= $pasantia->tl_correo ?>
					  					</div>
					  				</div>
					  			</div>
					  		</div>

					  		<!-- Tutor académico -->
						  	<?php if ($pasantia->tutor_academico_id): ?>
						  		<div class="row margin_b-t">
						  			<div class="col-12">					  				
						  				<div class="row">
						  					<div class="col text-center">
						  						<span class="semi-bold">Tutor académico </span>
						  					</div>
						  				</div>

						  				<?php
						  					$tutorA = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->tutor_academico_id);
						  				?>

						  				<div class="row">
						  					<div class="col">
						  						<span>Apellidos y nombres: </span>
						  						<?= $tutorA->primer_apellido ?>
						  						<?php if ($tutorA->segundo_apellido): ?>
						  							<?= $tutorA->segundo_apellido ?>
						  						<?php endif; ?>
						  						<?= $tutorA->primer_nombre ?>
						  						<?php if($tutorA->segundo_nombre): ?>
						  							<?= $tutorA->segundo_nombre ?>
						  						<?php endif; ?>
						  					</div>
						  					
						  				</div>

						  				<div class="row">
						  					<div class="col-4">
						  						<span>C.I: </span>
						  						<?= $tutorA->nacionalidad . "-" . $tutorA->ci ?>
						  					</div>
						  					<div class="col">
						  						<span>Correo: </span>
						  						<?= $tutorA->correo_institucional ?>
						  					</div>
						  				</div>
						  			</div>
						  		</div>
						  	<?php endif; ?>					  		
					  	<?php endforeach; ?>
					  </div>

					  <!-- Información de las Actividades -->
					  <div class="tab-pane fade margin-r-44" id="v-pills-actividades" role="tabpanel" aria-labelledby="v-pills-actividades-tab">
					  	<?php
					  		$actividades = $this->Actividad_model->obtenerActividades_por_pasantia($pasantia->pasantia_id);
					  	?>
					  	<!-- Tabla de las actividades -->
							<div class="row justify-content-center">
								<div class="col">
									<?php if ($actividades): ?>
										<div class="card data-tables">
											<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
												<div class="fresh-datatables">
													<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

														<thead>
															<tr>
																<th>#</th>
																<th>Fecha de Inicio</th>		
																<th>Fecha de Culminación</th>	
																<th class="disabled-sorting"></th>
															</tr>
														</thead>

														<tfoot>
															<tr>
																<th>#</th>
																<th>Fecha de Inicio</th>		
																<th>Fecha de Culminación</th>
																<th class="disabled-sorting"></th>
															</tr>
														</tfoot>

														<tbody>
															<?php $cant_actividad = 1; ?>
															<?php foreach ($actividades as $actividad): ?>
																<tr>
																	<!-- # DE ACTIVIDAD -->
																	<td>
																		<?= $cant_actividad ?>
																	</td>
																	
																	<!-- FECHA DE INICIO -->
																	<td>
																		<?php $fecha_inicio = new DateTime($actividad->fecha_inicio); ?>
																		<?= $fecha_inicio->format('d-m-Y'); ?>
																	</td>

																	<!--  FECHA DE FIN -->
																	<td>
																		<?php $fecha_fin = new DateTime($actividad->fecha_fin); ?>
																		<?= $fecha_fin->format('d-m-Y'); ?>
																	</td>

																	<!-- Acciones -->
																	<td class="text-right">
																		<a class="btn-modal" href="#modal-actividad" data-toggle="modal" data-target="#modal-actividad" data-actividad="<?= $actividad->actividad_id ?>">
																    	<i class="fa fa-eye"></i> Ver				 
																    </a>
																	</td>
																</tr>
																<?php $cant_actividad = $cant_actividad + 1; ?>
															<?php endforeach; ?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									<?php else: ?>
										<div class="row justify-content-center">						
											<div class="alert alert-warning border-radius-10 text-center text-size-12" role="alert">
												<span class="semi-bold">
													<i class="fa fa-warning"></i><br>
													¡No hay actividades registradas!
												</span>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Cierre de la Versión Tablet y Desktop -->

<!-- Modal -->
<div class="modal fade" id="modal-actividad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">												     
        <h5 class="modal-title semi-bold" id="exampleModalLabel">								        	
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fa fa-times"></i>
        </button>											        
      </div>
      <div class="modal-body">
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-12 semi-bold">
        			Descripción
        		</div>
        	</div>
        	<div class="row">
        		<div id="descripcion" class="col-12"></div>
        	</div>
        	<div class="row">
        		<div class="col-12 semi-bold">
        			Observación
        		</div>
        	</div>
        	<div class="row">
        		<div id="observacion" class="col-12"></div>
        	</div>
			  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script>

	function formato_fecha(fecha) {
		nueva_fecha = new Date(fecha);

		nueva_fecha = nueva_fecha.getDate() + '/' + (nueva_fecha.getMonth() + 1) + '/' + nueva_fecha.getFullYear();

		return nueva_fecha;
	}

	$('.btn-modal').click(function () {
		var id     = $(this).data('actividad');
		var tipo   = 'Diaria';
		var titulo = 'Reporte de Actividad <br>';

		$('#modal-actividad .modal-title, #modal-actividad #descripcion, #modal-actividad #observacion').empty();

		$.ajax({
            type:"post",
            url: "<?php echo base_url(); ?>actividades/obtener",
            data:{id: id},
            success:function(response)
            {
            	data = JSON.parse(response);
            	if (data) {
            		if (data.dia == '') {
            			tipo = 'Semanal';
            		}

            		if (tipo == 'Diaria') {
            			titulo += data.dia + ': ' + formato_fecha(data.fecha_inicio);
            		}else{
            			titulo += formato_fecha(data.fecha_inicio) + ' - ' + formato_fecha(data.fecha_fin);
            		}
            		$('#modal-actividad .modal-title').html(titulo);
            		$('#modal-actividad #descripcion').html(data.descripcion);
            		$('#modal-actividad #observacion').html(data.observacion);
            		console.log(data);
            	}            	
            },
            error: function() 
            {
              console.log('Error');
            }
        	});
	});
</script>