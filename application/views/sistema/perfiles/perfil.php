		<!-- VERSIÓN MÓVIL (sm ONLY) -->
		<div class="row d-md-none" id="informacion-perfil-movil">

			<!-- Para mostrar la imagen de perfil -->
			<?php if ($this->perfil_session->foto): ?>
				<div class="col-12 text-center">
					<img src="<?= base_url() ?>profile/images/<?= $this->perfil_session->foto; ?>" alt="Foto de perfil de <?= $this->perfil_session->primer_nombre ?> <?= $this->perfil_session->primer_apellido ?>" width="200" height="200" class="border-radius-6">				
				</div>
			<?php endif; ?>

			<!-- Muestra el nombre y rol -->
			<div class="col-12 text-center">
				<!-- Para mostrar el nombre del estudiante -->
				<span class="semi-bold text-size-15">
					<?=	$this->perfil_session->primer_nombre ?>

					<?php if ($this->perfil_session->segundo_nombre): ?>
						<?= $this->perfil_session->segundo_nombre ?>
						<br>
					<?php endif; ?>

					<?= $this->perfil_session->primer_apellido ?>

					<?php if ($this->perfil_session->segundo_apellido): ?>
						<?= $this->perfil_session->segundo_apellido ?>
					<?php endif; ?>					
				</span>
				<br>

				<!-- Para mostrar el rol-->
				<?php
					$usuario_info = $this->Usuario_model->obtenerUsuario($this->perfil_session->usuario_id);
				?>
				<span id="rol" class="extra-light">
					<?= $usuario_info->rol ?>
				</span>
			</div>

			<!-- Para mostrar una breve descripción -->	
			<div class="col-12 margin_sm_b-t sangria-sm">
				<?php if ($this->perfil_session->descripcion): ?>
					<span class="text-italic">
						<?= $this->perfil_session->descripcion ?>
					</span>
				<?php endif; ?>
			</div>

			<!-- TÍTULO DE INFORMACIÓN -->
			<div class="col-12 text-center margin_sm_b-t">
				<span class="semi-bold text-size-15">
					Información
				</span>	
			</div>

			<div class="col-12">
				<!-- NAV-TABS DEL CONTENIDO MOVIL -->
				<div class="row margin_sm_b-t">
					<nav class="col-12">
					  <div class="nav nav-tabs" id="nav-tab" role="tablist">
					  	<!-- Información personal -->
					    <a class="nav-item nav-link active" id="nav-personal-tab" data-toggle="tab" href="#nav-personal" role="tab" aria-controls="nav-personal" aria-selected="true">Personal</a>
							
							<!-- Información de contacto -->
							<?php if (
								$this->perfil_session->correo_institucional || 
								$this->perfil_session->correo_personal || 
								$this->perfil_session->telefono_movil || 
								$this->perfil_session->telefono_residencial 
								): ?>
								<a class="nav-item nav-link" id="nav-contacto-tab" data-toggle="tab" href="#nav-contacto" role="tab" aria-controls="nav-contacto" aria-selected="false">
									Contacto
								</a>
							<?php endif; ?>
					  </div>
					</nav>
					
					<div class="tab-content col-12" id="nav-tabContent">

						<!-- Contenido de la información personal -->
					  <div class="tab-pane fade show active" id="nav-personal" role="tabpanel" aria-labelledby="nav-personal-tab">

						  <div class="row">

						  	<!-- Para mostrar la cédula de identidad -->
								<div class="col-12">
									<span>
										Cédula de Identidad: 
										<?= $this->perfil_session->nacionalidad ?>-<?= $this->perfil_session->ci ?>
									</span>
								</div>
								<br>

						  	<!-- Para mostrar la(s) carrera(s) del estudiante -->
								<?php if ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($this->perfil_session->usuario_id)): ?>
									<div class="col-12">
										<span>Carrera(s): </span>
										<?php $cantidad_carrera = 0; ?>
										<?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($this->perfil_session->usuario_id) as $carrera_estudiante): ?>
											<?php $cantidad_carrera = $cantidad_carrera + 1; ?>

											<!-- Condicional para separar las carreras con comas -->
											<?php if ($cantidad_carrera > 1): ?> 
												<?php echo ", "; ?>
											<?php endif;?>
											<!-- Fin del condicional para separar las carreras con comas -->
											
											<?= $this->Carrera_model->obtenerCarrera($carrera_estudiante->carrera_id)->nombre ?>
										<?php endforeach;  ?>
									</div>
									<br>
								<?php endif; ?>

								<!-- Para mostrar la fecha de nacimiento -->
								<?php if ($this->perfil_session->cumpleaños != '0000-00-00'): ?>				
									<div class="col-12">
										<span>
											<?php $fecha = new DateTime($this->perfil_session->cumpleaños); ?>
											<?= "Fecha de nacimiento: " . $fecha->format('d-m-Y'); ?>
										</span>
									</div>
									<br>
								<?php endif; ?>

								<!-- Para mostrar la dirección residencial -->
								<?php if ($this->perfil_session->direccion_residencial): ?>	
									<div class="col-12">
										<span>			
											<?= "Dirección residencial: " . $this->perfil_session->direccion_residencial ?>
										</span>	
									</div>									
									<br>
								<?php endif; ?>	
						  </div>
					  </div>

					  <!-- Contenido de la información de contacto -->
					  <div class="tab-pane fade" id="nav-contacto" role="tabpanel" aria-labelledby="nav-contacto-tab">

						  <div class="row">
						  	<!-- Para mostrar el correo institucional -->
								<?php if ($this->perfil_session->correo_institucional): ?>	
									<div class="col-12">
										<span>			
											<?= "Correo institucional: " . $this->perfil_session->correo_institucional ?>
										</span>
									</div>
									<br>
								<?php endif; ?>

								<!-- Para mostrar el correo personal -->	
								<?php if ($this->perfil_session->correo_personal): ?>
									<div class="col-12">
										<span>		
											<?= "Correo personal: " . $this->perfil_session->correo_personal ?>
										</span>
									</div>
									<br>
								<?php endif; ?>				
								
								<!-- Para mostrar el número telefónico móvil -->
								<?php if ($this->perfil_session->telefono_movil): ?>		
									<div class="col-12">
										<span>		
											<?= "Teléfono móvil: " . $this->perfil_session->telefono_movil ?>
										</span>
									</div>
									<br>
								<?php endif; ?>

								<!-- Para mostrar el número telefónico residencial -->
								<?php if ($this->perfil_session->telefono_residencial): ?>
									<div class="col-12">
										<span>
											<?= "Teléfono residencial: " . $this->perfil_session->telefono_residencial ?>
										</span>
									</div>
									<br>
								<?php endif; ?>
						  </div>
					  </div>
					</div>
				</div>
				<!-- Cierre de los NAV-TABS -->
			</div>
		</div> 
		<!-- Cierre de VERSIÓN MÓVIL -->

		<!-- VERSIÓN TABLET Y DESKTOP -->
		<div class="d-none d-md-block" id="informacion-perfil">
			<div class="row">
				<div class="col-md-4 col-xl-3">
					<div class="row">
						<!-- Para mostrar la imagen de perfil -->
						<?php if ($this->perfil_session->foto): ?>
							<img class="float-left border-radius-6" src="<?= base_url() ?>profile/images/<?= $this->perfil_session->foto; ?>" alt="Foto de perfil de <?= $this->perfil_session->primer_nombre ?> <?= $this->perfil_session->primer_apellido ?>" width="200" height="200">						
						<?php endif; ?>
					</div>

					<!-- Para mostrar Nombre, rol y descripción personal -->
					<div class="row justify-content-center">
						<!-- Para mostrar el nombre del estudiante -->
						<div class="col-10 text-center margin-l-43">
							<span class="semi-bold text-center text-size-15">
								<?=	$this->perfil_session->primer_nombre ?>

								<?php if ($this->perfil_session->segundo_nombre): ?>
									<?= $this->perfil_session->segundo_nombre ?> <br>
								<?php endif; ?>

								<?= $this->perfil_session->primer_apellido ?>

								<?php if ($this->perfil_session->segundo_apellido): ?>
									<?= $this->perfil_session->segundo_apellido ?>
								<?php endif; ?>					
							</span>
						</div>
					</div>

					<!-- Para mostrar el rol--> <!--
					<div class="row justify-content-center">
						<?php
							$usuario_info = $this->Usuario_model->obtenerUsuario($this->perfil_session->usuario_id);
						?>
						<div class="col text-center margin-l-43">
							<span id="rol" class="extra-light">
								<?= $usuario_info->rol ?>
							</span>
						</div>
					</div> -->

					<!-- Para mostrar la(s) carrera(s) del estudiante -->
					<?php if ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($this->perfil_session->usuario_id)): ?>
						<div class="col-md-11 text-center margin-l-16">
							<?php $cantidad_carrera = 0; ?>
							<?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($this->perfil_session->usuario_id) as $carrera_estudiante): ?>
								<?php $cantidad_carrera = $cantidad_carrera + 1; ?>

								<!-- Condicional para separar las carreras con comas -->
								<?php if ($cantidad_carrera > 1): ?> 
									<?php echo "<br>"; ?>
								<?php endif;?>
								<!-- Fin del condicional para separar las carreras con comas -->
								<span class="extra-light"><?= $this->Carrera_model->obtenerCarrera($carrera_estudiante->carrera_id)->nombre ?></span>								
							<?php endforeach;  ?>
						</div>
					<?php endif; ?>

					<!-- Para mostrar una breve descripción -->	
					<?php if ($this->perfil_session->descripcion): ?>
						<div class="row justify-content-center">
							<div class="col-10 text-center margin-l-43">
								<span class="text-italic">
									<?= $this->perfil_session->descripcion ?>
								</span>
							</div>
						</div>
					<?php endif; ?>
				</div>
				
				<!-- NAV-TABS DEL CONTENIDO TABLET Y DESKTOP -->
				<div class="col-md-8 margin_l-44">
					<nav>
					  <div class="nav nav-tabs" id="nav-tab" role="tablist">
					  	<!-- Información personal -->
					    <a class="nav-item nav-link active" id="nav-personal-tab" data-toggle="tab" href="#nav-personal-desktop" role="tab" aria-controls="nav-personal-desktop" aria-selected="true">
					    	Información personal
					    </a>
							
							<!-- Información de contacto -->
							<?php if (
								$this->perfil_session->correo_institucional || 
								$this->perfil_session->correo_personal || 
								$this->perfil_session->telefono_movil || 
								$this->perfil_session->telefono_residencial 
								): ?>
								<a class="nav-item nav-link" id="nav-contacto-tab" data-toggle="tab" href="#nav-contacto-desktop" role="tab" aria-controls="nav-contacto-desktop" aria-selected="false">
									Información de contacto
								</a>
							<?php endif; ?>
					  </div>
					</nav>
					
					<div class="tab-content col-12" id="nav-tabContent">

						<!-- Contenido de la información personal -->
					  <div class="tab-pane fade show active margin-r-44" id="nav-personal-desktop" role="tabpanel" aria-labelledby="nav-personal-tab">

						  <div class="row">

						  	<!-- Para mostrar la cédula de identidad -->
								<div class="col-md-6">
									<span>
										Cédula de Identidad: 
										<?= $this->perfil_session->nacionalidad ?>-<?= $this->perfil_session->ci ?>
									</span>
								</div>
								<br>

								<!-- Para mostrar la fecha de nacimiento -->
								<?php if ($this->perfil_session->cumpleaños != '0000-00-00'): ?>				
									<div class="col-md-6">
										<span>
											<?php $fecha = new DateTime($this->perfil_session->cumpleaños); ?>
											<?= "Fecha de nacimiento: " . $fecha->format('d-m-Y'); ?>
										</span>
									</div>
									<br>
								<?php endif; ?>

						  	<!-- Para mostrar la(s) carrera(s) del estudiante -->
								<?php if ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($this->perfil_session->usuario_id)): ?>
									<div class="col-md-12">
										<span>Carrera(s): </span>
										<?php $cantidad_carrera = 0; ?>
										<?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($this->perfil_session->usuario_id) as $carrera_estudiante): ?>
											<?php $cantidad_carrera = $cantidad_carrera + 1; ?>

											<!-- Condicional para separar las carreras con comas -->
											<?php if ($cantidad_carrera > 1): ?> 
												<?php echo ", "; ?>
											<?php endif;?>
											<!-- Fin del condicional para separar las carreras con comas -->
											
											<?= $this->Carrera_model->obtenerCarrera($carrera_estudiante->carrera_id)->nombre ?>
										<?php endforeach;  ?>
									</div>
									<br>
								<?php endif; ?>

								

								<!-- Para mostrar la dirección residencial -->
								<?php if ($this->perfil_session->direccion_residencial): ?>	
									<div class="col-md-12">
										<span>			
											<?= "Dirección residencial: " . $this->perfil_session->direccion_residencial ?>
										</span>	
									</div>									
									<br>
								<?php endif; ?>	
						  </div>
					  </div>

					  <!-- Contenido de la información de contacto -->
					  <div class="tab-pane fade margin-r-44" id="nav-contacto-desktop" role="tabpanel" aria-labelledby="nav-contacto-tab">

						  <div class="row">
						  	<!-- Para mostrar el correo institucional -->
								<?php if ($this->perfil_session->correo_institucional): ?>	
									<div class="col-md-6">
										<span>			
											<?= "Correo institucional: " . $this->perfil_session->correo_institucional ?>
										</span>
									</div>
									<br>
								<?php endif; ?>

								<!-- Para mostrar el correo personal -->	
								<?php if ($this->perfil_session->correo_personal): ?>
									<div class="col-md-6">
										<span>		
											<?= "Correo personal: " . $this->perfil_session->correo_personal ?>
										</span>
									</div>
									<br>
								<?php endif; ?>				
								
								<!-- Para mostrar el número telefónico móvil -->
								<?php if ($this->perfil_session->telefono_movil): ?>		
									<div class="col-md-6">
										<span>		
											<?= "Teléfono móvil: " . $this->perfil_session->telefono_movil ?>
										</span>
									</div>
									<br>
								<?php endif; ?>

								<!-- Para mostrar el número telefónico residencial -->
								<?php if ($this->perfil_session->telefono_residencial): ?>
									<div class="col-md-6">
										<span>
											<?= "Teléfono residencial: " . $this->perfil_session->telefono_residencial ?>
										</span>
									</div>
									<br>
								<?php endif; ?>
						  </div>
					  </div>
					</div>
				</div>
				<!-- Cierre de los NAV-TABS -->
			</div>
		</div> 
		<!-- Cierre de VERSIÓN TABLET Y DESKTOP -->

		<!-- AVISOS DE LA INFORMACIÓN FALTANTE EN EL PERFIL -->

		<!-- Condicional que sirve para listar los datos faltantes en el perfil del usuario -->
		<?php if (
			!$this->perfil_session->segundo_nombre || 
			!$this->perfil_session->segundo_apellido ||
			!$this->perfil_session->cumpleaños && $usuario_info->rol == 'Estudiante' || 
			$this->perfil_session->cumpleaños == '0000-00-00' && $usuario_info->rol == 'Estudiante' ||
			!$this->perfil_session->direccion_residencial && $usuario_info->rol == 'Estudiante'  ||
			!$this->perfil_session->correo_institucional ||
			!$this->perfil_session->correo_personal  && $usuario_info->rol == 'Estudiante'  ||
			!$this->perfil_session->telefono_movil ||
			!$this->perfil_session->telefono_residencial  && $usuario_info->rol == 'Estudiante'  ||
			!$this->perfil_session->foto
			): ?>
			<div class="row">
				<div class="col">
					<div id="mensaje" class="alert alert-warning section-description border-radius-15 margin_sm_t" role="alert">
						<p class="text-size-20 semi-bold text-center">
							<i class="fa fa-warning"></i> <br>
							¡Aviso!
						</p>

						<span class="text-size-12 margin_sm_l">
							Se le notifica que usted debe llenar los siguientes campos:
						</span>

						<ul class="type-none text-size-12 margin_sm_l">
							<div class="row">
								<?php if (!$this->perfil_session->segundo_nombre): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3">
										<li>- Segundo nombre</li>
									</div>
								<?php endif; ?>
								
								<?php if (!$this->perfil_session->segundo_apellido): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3">
										<li>- Segundo apellido</li>
									</div>
								<?php endif; ?>
								
								<?php if ($this->perfil_session->cumpleaños == '0000-00-00' && $usuario_info->rol == 'Estudiante'): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3">
										<li>- Fecha de nacimiento </li>
									</div>
								<?php endif; ?>
								
								<?php if (!$this->perfil_session->direccion_residencial && $usuario_info->rol == 'Estudiante' ): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3">
										<li>- Dirección residencial</li>
									</div>
								<?php endif; ?>

								<?php if (!$this->perfil_session->foto): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3">
										<li>- Foto del perfil</li>
									</div>
								<?php endif; ?>
								
								<?php if (!$this->perfil_session->correo_institucional): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3">
										<li>- Correo institucional</li>
									</div>
								<?php endif; ?>
								
								<?php if (!$this->perfil_session->correo_personal && $usuario_info->rol == 'Estudiante' ): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3">
										<li>- Correo personal</li>
									</div>
								<?php endif; ?>
								
								<?php if (!$this->perfil_session->telefono_movil): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3">
										<li>- Teléfono móvil</li>
									</div>
								<?php endif; ?>

								<?php if (!$this->perfil_session->telefono_residencial && $usuario_info->rol == 'Estudiante' ): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3">
										<li>- Teléfono residencial</li>
									</div>
								<?php endif; ?>
							</div>
						</ul>
					</div>
				</div>
			</div>			
		<?php endif; ?>