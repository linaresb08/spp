		<?= validation_errors() ?>
		
		<?= form_open("/perfiles/editar/$id") ?>
			<?php
				$primer_nombre = array(
					'name'     => 'primer_nombre', 
					'value'    => $perfil->primer_nombre,
					'class'    => 'form-control',
					'id'       => 'inputNombre1',
					'required' => 'true'
				);
				$segundo_nombre = array(
					'name'     => 'segundo_nombre', 
					'id'       => 'inputNombre2',
					'value'    => $perfil->segundo_nombre,
					'class'    => 'form-control',
					'required' => 'true'
				);

				$primer_apellido = array(
					'name'     => 'primer_apellido', 
					'value'    => $perfil->primer_apellido,
					'class'    => 'form-control',
					'id'       => 'inputApellido1',
					'required' => 'true'
				);
				$segundo_apellido = array(
					'name'     => 'segundo_apellido', 
					'value'    => $perfil->segundo_apellido,
					'class'    => 'form-control',
					'id'       => 'inputApellido2',
					'required' => 'true'
				);
				
				$cumpleaños = array(
					'name'  => 'cumpleaños', 
					'value' => $perfil->cumpleaños,
					'type'  => 'date',
					'class' => 'form-control datepicker',
					'id'    => 'inputCumpleaño'
				);

				$direccion_residencial = array(
					'name'        => 'direccion_residencial',
					'placeholder' => 'Escribe la dirección residencial',
					'value'       => $perfil->direccion_residencial,
					'class'       => 'form-control',
					'id'          => 'inputDirResidencial',
					'rows'        => '4'
				);

				$correo_institucional = array(
					'name'        => 'correo_institucional',
					'placeholder' => 'example@uvm.edu.ve',
					'value'       => $perfil->correo_institucional,
					'type'        => 'email',
					'class'       => 'form-control',
					'id'          => 'inputCorreoInstitucional'
				);

				$correo_personal = array(
					'name'        => 'correo_personal',
					'placeholder' => 'example@gmail.com',
					'value'       => $perfil->correo_personal,
					'type'        => 'email',
					'class'       => 'form-control',
					'id'          => 'inputCorreoPersonal'
				);

				$telefono_movil = array(
					'name'        => 'telefono_movil', 
					'value'       => $perfil->telefono_movil,
					'placeholder' => '0424-5528724',
					'type'        => 'tel',
					'class'       => 'form-control',
					'id'          => 'inputTelMovil'
				);

				$telefono_residencial = array(
					'name'        => 'telefono_residencial', 
					'value'       => $perfil->telefono_residencial,
					'placeholder' => '0271-5528724',
					'type'        => 'tel',
					'class'       => 'form-control',
					'id'          => 'inputTelResidencial'
				);

				$descripcion = array(
					'name'        => 'descripcion', 
					'placeholder' => 'Cuéntanos un poco más sobre ti...',
					'value'       => $perfil->descripcion,
					'class'       => 'form-control',
					'id'          => 'inputDescPersonal',
					'rows'        => '3'
				);
			?>

			<div class="row justify-content-center">

				<div class="col-12 col-sm-6 text-center">
					<span class="section-title semi-bold text-center" title="Esta información solo puede ser modificada por el Coordinador de prácticas profesionales o el Administrador del sistema">
						<?= $perfil->nacionalidad ?> - <?= $perfil->ci ?>
					</span>
					<br>
					<!-- Para mostrar el rol-->
					<?php
						$usuario_info = $this->Usuario_model->obtenerUsuario($perfil->usuario_id);
					?>
					<span id="rol" class="extra-light">
						<?= $usuario_info->rol ?>
					</span>
				</div>

			</div>

			<div class="form-row justify-content-center">
				<div class="text-center col-12">
					<span class="semi-bold text-size-15">
						Información personal
					</span>					
				</div>

				<div class="form-group col-12 col-md-6 col-lg-3">
					<label for="inputNombre1">Primer nombre</label>
					<?= form_input($primer_nombre) ?>
				</div>
				
				<div class="form-group col-12 col-md-6 col-lg-3">
					<label for="inputNombre2">Segundo nombre</label>
					<?= form_input($segundo_nombre) ?>
				</div>

				<div class="form-group col-12 col-md-6 col-lg-3">
					<label for="inputApellido1">Primer apellido</label>
					<?= form_input($primer_apellido) ?>
				</div>
				
				<div class="form-group col-12 col-md-6 col-lg-3">
					<label for="inputApellido2">Segundo apellido</label>
					<?= form_input($segundo_apellido) ?>
				</div>

				<div class="form-group col-12 col-md-6 col-lg-3">
					<label for="inputCumpleaño">Fecha de nacimiento</label>
					<?= form_input($cumpleaños) ?>
				</div>

				<div class="form-group col-12 col-md-6 col-lg-9">
					<label for="inputDirResidencial">Dirección residencial</label>
					<?= form_textarea($direccion_residencial) ?>
				</div>

			</div>
			<hr>

		<?php if ($this->session->userdata('rol') == 'Estudiante'): ?>

			<div class="row">

				<div class="text-center col-12">
					<span class="semi-bold text-size-15">
						Información académica
					</span>					
				</div>

				<div class="col-12">
					<label>Selecciona tu(s) carrera(s) </label>
				</div>

				<?php $carreras_estudiante = array();  ?>

				<?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($perfil->usuario_id) as $carrera_estudiante): ?>
					<?php array_push($carreras_estudiante, $carrera_estudiante->carrera_id); ?>
				<?php endforeach;  ?>

				<?php foreach ($this->Carrera_model->obtenerCarreras() as $carrera): ?>
					<div class="col-12 col-md-6 col-lg-4 checkbox-radios">					
						<div class="form-check">

							<label class="form-check-label" for="carrera_<?= $carrera->carrera_id ?>">
								<input class="form-check-input" type="checkbox" name="carreras[]" value="<?= $carrera->carrera_id ?>" id="carrera_<?= $carrera->carrera_id ?>" 
								<?php if (in_array($carrera->carrera_id, $carreras_estudiante)) {
								echo "checked";	} ?>>
								<span class="form-check-sign"></span>
								<?= $carrera->nombre ?>
							</label>
							
						</div>					
					</div>	
				<?php endforeach; ?>
				
			</div>
			<hr>
		<?php endif ?>

			<div class="row">

				<div class="text-center col-12">
					<span class="semi-bold text-size-15">
						Información de contacto
					</span>					
				</div>

				<div class="form-group col-12 col-md-6 col-lg-3">
					<label for="inputCorreoInstitucional">Correo institucional</label>
					<?= form_input($correo_institucional) ?>
				</div>

				<div class="form-group col-12 col-md-6 col-lg-3">
					<label for="inputCorreoPersonal">Correo personal</label>
					<?= form_input($correo_personal) ?>
				</div>

				<div class="form-group col-12 col-md-6 col-lg-3">
					<label for="inputTelMovil">Teléfono móvil</label>
					<?= form_input($telefono_movil) ?>
				</div>

				<div class="form-group col-12 col-md-6 col-lg-3">
					<label for="inputTelResidencial">Teléfono residencial</label>
					<?= form_input($telefono_residencial) ?>
				</div>

			</div>
			<hr>

			<div class="row">

				<div class="text-center col-12">
					<span class="semi-bold text-size-15">
						Por último
					</span>					
				</div>

				<div class="form-group col-12">
					<label for="inputDescPersonal">Breve descripción personal</label>
					<?= form_textarea($descripcion) ?>
				</div>
				
			</div>

			<div class="row justify-content-center">
				<?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?>	
			</div>
			
		<?= form_close() ?>
