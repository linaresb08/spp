		<?= validation_errors() ?>

		<?= form_open("/empresas/nueva") ?>
			<?php 
				$nombre = array(
					'name' => 'nombre',
					'placeholder' => 'Nombre de la empresa',
					'class' => 'form-control'
				);

				$rif = array(
					'name' => 'rif', 
					'placeholder' => 'J-12345678-9',
					'class' => 'form-control'
				);

				$direccion = array(
					'name' 				=> 'direccion', 
					'placeholder' => 'Escribe la dirección',
					'class' 			=> 'form-control',
					'rows'        => '5'
				);

				$telefono1 = array(
					'name' 				=> 'telefono1', 
					'placeholder' => '0271-1234444',
					'type'        => 'tel',
					'class'       => 'form-control',
				);

				$telefono2 = array(
					'name' 				=> 'telefono2', 
					'placeholder' => '0271-1112222',
					'type'        => 'tel',
					'class'       => 'form-control',
				);

				$jr_apellidos = array(
					'name'        => 'jr_apellidos',
					'placeholder' => 'Escribe los apellidos',
					'class'       => 'form-control'
				);

				$jr_nombres = array(
					'name'        => 'jr_nombres',
					'placeholder' => 'Escribe los nombres',
					'class'       => 'form-control'
				);

				$jr_correo = array(
					'name'        => 'jr_correo',
					'placeholder' => 'example@gmail.com',
					'type'        => 'email',
					'class'       => 'form-control'
				);

				$jr_telefono = array(
					'name'        => 'jr_telefono',
					'placeholder' => '0414-1112222',
					'type'        => 'tel',
					'class'       => 'form-control'
				);
			?>

			<!-- Campos con los datos de la empresa -->
			<div class="row justify-content-center">
				<div class="col-12 text-center margin_sm_b-t">
					<span class="semi-bold">
						Datos de la empresa
					</span>	
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Nombre</span>
					<?= form_input($nombre) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>RIF</span>
					<?= form_input($rif) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Dirección</span>
					<?= form_textarea($direccion) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Teléfono 1</span>
					<?= form_input($telefono1) ?>

					<div class="margin_sm_t">
						<span>Teléfono 2</span>
						<?= form_input($telefono2) ?>
					</div>
				</div>
			</div>

			<!-- Campos con los datos del jefe de recursos humanos -->
			<div class="row justify-content-center">
				<div class="col-12 text-center margin_md_t margin_sm_b">
					<span class="semi-bold">
						Datos del jefe de recursos humanos
					</span>
					<br>
					<small class="text-muted text-center">
						(O persona que cumple esta función)
					</small>	
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Apellidos</span>
					<?= form_input($jr_apellidos) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Nombres</span>
					<?= form_input($jr_nombres) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Correo electrónico</span>
					<?= form_input($jr_correo) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Teléfono</span>
					<?= form_input($jr_telefono) ?>
				</div>
			</div>

			<div class="row margin_t">
				<div class="col-5 col-md-6 text-right"><?= form_submit('','Crear',['class'=>'btn btn-success']) ?></div>
				<div class="col-5 col-md-6 text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>					
			</div>
			
		<?= form_close() ?>
