		<!-- TODAS LAS EMPRESAS -->
		<?php if (!$segmento): ?>
			<div class="row">

				<!-- Título -->
				<div class="col-12 semi-bold section-title text-center">
					Empresas
				</div>
				
				<!-- Botón para añadir empresas -->
				<div class="col-12">
					<a class="btn btn-primary float-right" role="button" href="<?= base_url()?>empresas/nueva">
						<i class="fa fa-plus"></i> Nueva empresa
					</a>
				</div>
			</div>

			<!-- Tabla de las empresas -->
			<div class="row justify-content-center">
				<div class="col-md-12 col-lg-11 col-xl-10">
					<?php if ($empresas): ?>
						<div class="card data-tables">
							<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
								<div class="fresh-datatables">
									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

										<thead>
											<tr>
												<th>Nombre</th>					
												<th>RIF</th>	
												<th class="disabled-sorting">Acciones</th>
											</tr>
										</thead>

										<tfoot>
											<tr>
												<th>Nombre</th>				
												<th>RIF</th>		
												<th class="disabled-sorting">Acciones</th>
											</tr>
										</tfoot>

										<tbody>
											<?php foreach ($empresas as $empresa): ?>
												<tr>
													<!-- Nombre -->
													<td>
														<?= $empresa->nombre ?>
													</td>

													<!-- RIF -->
													<td>
														<?= $empresa->rif ?>
													</td>

													<!-- Acciones -->
													<td>
														<div class="dropdown">
														  <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink-<?= $empresa->empresa_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														    <i class="fa fa-gears"></i>
														  </a>

														  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?= $empresa->empresa_id ?>">
														  	<h6 class="dropdown-header">Acciones</h6>
														    <a class="dropdown-item width_130 border-radius-10" href="#modal-<?= $empresa->empresa_id ?>" data-toggle="modal" data-target="#modal-<?= $empresa->empresa_id ?>">
														    	<i class="fa fa-eye"></i> Ver														 
														    </a>
														    <a class="dropdown-item width_130 border-radius-10" href="<?= base_url(); ?>empresas/editar/<?= $empresa->empresa_id ?>">
														    	<i class="fa fa-edit"></i> Modificar
														    </a>
														    <a class="dropdown-item width_130 border-radius-10" href="<?= base_url(); ?>empresas/eliminar/<?= $empresa->empresa_id ?>">
														    	<i class="fa fa-trash"></i> Eliminar
														    </a>
														  </div>
														</div>
													</td>
												</tr>

												<!-- Modal -->
												<div class="modal fade" id="modal-<?= $empresa->empresa_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												  <div class="modal-dialog" role="document">
												    <div class="modal-content">
												      <div class="modal-header">												     
												        <h5 class="modal-title semi-bold" id="exampleModalLabel">
												        	<?= $empresa->nombre ?>
												        	<br>
												        	<small class="text-muted"><?= $empresa->rif ?></small>											        	
												        </h5>
												        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
												          <i class="fa fa-times"></i>
												        </button>											        
												      </div>
												      <div class="modal-body">
												        <div class="container-fluid">
												        	<!-- Dirección de la empresa -->
															    <div class="row">
															      <p>
															      	<?= $empresa->direccion ?>
															      </p>
															    </div>

															    <!-- Números telefónicos -->
															    <div class="row">
															      <div class="col-6">
															      	<span class="semi-bold">Teléfono 1</span>
															      	<br>
															      	<?= $empresa->telefono1 ?>
															      </div>
															      <?php if($empresa->telefono2): ?>
															      	<div class="col-6">
																      	<span class="semi-bold">Teléfono 2</span>
																      	<br>
																      	<?= $empresa->telefono2 ?>
																      </div>
															      <?php endif; ?>																      
															    </div>

															    <!-- Datos del Jefe de RRHH -->
															    <?php if($empresa->jr_nombres && $empresa->jr_apellidos): ?>
															    	<div class="row">
																    	<div class="col-12 text-center margin_md_t margin_sm_b">
																				<span class="semi-bold">
																					Jefe de recursos humanos 
																				</span>
																				<small class="text-muted text-center float-right margin-top_4">
																					(O persona que cumple esta función)
																				</small>	
																			</div>

																    	<div class="col-12 text-center">
																    		<span class="semi-bold">
																    			Nombres y Apellidos
																    		</span>
																    		<br>
																    		<?= $empresa->jr_nombres ?> <?= $empresa->jr_apellidos ?>
																    	</div>

																    	<?php if($empresa->jr_correo): ?>
																    		<div class="col-6 text-center">
																	    		<span class="semi-bold">
																	    			Correo electrónico
																	    		</span>
																	    		<br>
																	    		<?= $empresa->jr_correo ?>
																	    	</div>
																			<?php endif; ?>

																    	<?php if($empresa->jr_telefono): ?>
																    		<div class="col-6 text-center">
																	    		<span class="semi-bold">
																	    			Teléfono
																	    		</span>
																	    		<br>
																	    		<?= $empresa->jr_telefono ?>
																	    	</div>
																    	<?php endif; ?>															
																    </div>	
															    <?php endif; ?>							  
															  </div>
												      </div>
												      <div class="modal-footer">
												        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												      </div>
												    </div>
												  </div>
												</div>
											<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					<?php else: ?>
						<div class="row justify-content-center">						
							<div class="alert alert-warning border-radius-10 text-center text-size-12" role="alert">
								<span class="semi-bold">
									<i class="fa fa-warning"></i><br>
									¡No hay empresas registradas!
								</span>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>