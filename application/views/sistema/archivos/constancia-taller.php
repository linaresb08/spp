<!DOCTYPE html>
<html lang="es">
<head>

	<title>CONSTANCIA</title>

 <!-- Meta etiquetas -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' />

<!-- CSS Files -->
  <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?= base_url() ?>assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/mystyles.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/containers.css">

  <style type="text/css" media="screen">
		p { font-family: "Arial", sans;	}
	</style>
  <style>
    p { /* PARA AÑADIR SANGRÍA AL PÁRRAFO */
        text-indent: 50px;
    }
  </style>

</head>
<body>

	<div class="container-fluid">
		<div class="row justify-content-center">

			<!-- ENCABEZADO -->
      <div class="text-center">
        <!-- LOGO UVM -->
        <img style="float: left; margin-right: -55px;" src="<?= base_url() ?>assets/img/portal/logo.png" width="60" height="60">

        <!-- MEMBRETE -->
        <span class="text-size-10 text-center"> 
          UNIVERSIDAD VALLE DEL MOMBOY <br>
          VICERRECTORADO <br>
          COORDINACIÓN DE PRÁCTICAS PROFESIONALES <br> 
        </span>
      </div>

			<!-- TÍTULO -->
			<div class="col-12 text-center text-size-12 margin_md_b-t">
        <br>
        <span style="font-weight: bold; margin-bottom: 25px; margin-top:25px;">
          CONSTANCIA
        </span>
        <br><br>
			</div>

			<!-- CUERPO -->
			<div class="col-12 text-justify text-size-10">
				<p>          
					Quien suscribe Prof.
          <?= ucwords($facilitador->primer_nombre) ?>
          <?= ucwords($facilitador->segundo_nombre) ?>
          <?= ucwords($facilitador->primer_apellido) ?>
          <?= ucwords($facilitador->segundo_apellido) ?>, 
          titular de la Cédula de Identidad N° 
          <?= $facilitador->nacionalidad ?>-<?= $facilitador->ci ?>; 
          Facilitador(a) del <?= ucwords($censo->nombre); ?>
          de la Universidad Valle del Momboy, hace constar que el (la) estudiante 
          <span style="border-bottom: 1px solid black; font-weight: bold;">
            <?= mb_strtoupper($estudiante->primer_apellido, 'UTF-8'); ?> 
            <?= mb_strtoupper($estudiante->segundo_apellido, 'UTF-8'); ?> 
            <?= mb_strtoupper($estudiante->primer_nombre, 'UTF-8'); ?> 
            <?= mb_strtoupper($estudiante->segundo_nombre, 'UTF-8'); ?></span>, titular de la Cédula de Identidad N° 
          <span style="border-bottom: 1px solid black; font-weight: bold;">
            <?= $estudiante->nacionalidad ?>-<?= $estudiante->ci ?></span>, 
          cursante de la Carrera 
          <span style="border-bottom: 1px solid black; font-weight: bold;">
            <?php if ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($estudiante->usuario_id)): ?>
              <?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($estudiante->usuario_id) as $carrera_estudiante): ?>
                <?= mb_strtoupper($this->Carrera_model->obtenerCarrera($carrera_estudiante->carrera_id)->nombre,'UTF-8'); ?>
              <?php endforeach; ?>
            <?php endif; ?>
          </span> asistió al 
          <span style="font-weight: bold;"><?= mb_strtoupper($censo->nombre, 'UTF-8'); ?></span>, con una duración de <?= $censo->duracion ?>, realizado en <?= ucwords($censo->lugar); ?>.
					<br><br>
        </p>
        <p>
          <?php 
            $fecha = new DateTime($censo->fecha_taller);             
            $fe_actual = $fecha->format('d-m-Y');
            $dia       = substr($fe_actual, 0, 2);
            $mes_n     = substr($fe_actual, 3,-5);
            $año       = substr($fe_actual, 6, 9);
            switch ($mes_n) {
              case '01':
                $mes = "Enero";
                break;

              case '02':
                $mes = "Febrero";
                break;

              case '03':
                $mes = "Marzo";
                break;

              case '04':
                $mes = "Abril";
                break;

              case '05':
                $mes = "Mayo";
                break;

              case '06':
                $mes = "Junio";
                break;

              case '07':
                $mes = "Julio";
                break;

              case '08':
                $mes = "Agosto";
                break;

              case '09':
                $mes = "Septiembre";
                break;

              case '10':
                $mes = "Octubre";
                break;

              case '11':
                $mes = "Noviembre";
                break;

              case '12':
                $mes = "Diciembre";
                break;
              
              default:
                # code...
                break;
            }
          ?>
					Constancia que se expide a solicitud de parte interesada en Valera el <?= $dia . " de " . $mes . " de " . $año ?>.
				</p>
			</div>

		</div>

    <br><br>

		<div class="row justify-content-center margin_lg_t">
			<div class="col-12 text-center">
				<span style="border-top: 1px solid black; font-weight: bold;">					
          <?= ucwords($facilitador->primer_nombre) ?>
          <?= ucwords($facilitador->primer_apellido) ?>  
        </span>
				<br>
				Facilitador
				<br>
				C.I. N° <?= $facilitador->nacionalidad ?>-<?= $facilitador->ci ?>
			</div>
		</div>
	</div>
	
</body>
<!--   Core JS Files   -->
<script src="<?= base_url() ?>assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
<!--  Chartist Plugin  -->
<script src="<?= base_url() ?>assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-notify.js"></script>
<!--  Share Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.sharrre.js"></script>
<!--  jVector Map  -->
<script src="<?= base_url() ?>assets/js/plugins/jquery-jvectormap.js" type="text/javascript"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?= base_url() ?>assets/js/plugins/moment.min.js"></script>
<!--  DatetimePicker   -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-datetimepicker.js"></script>
<!--  Sweet Alert  -->
<script src="<?= base_url() ?>assets/js/plugins/sweetalert2.min.js" type="text/javascript"></script>
<!--  Tags Input  -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-tagsinput.js" type="text/javascript"></script>
<!--  Sliders  -->
<script src="<?= base_url() ?>assets/js/plugins/nouislider.js" type="text/javascript"></script>
<!--  Bootstrap Select  -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  jQueryValidate  -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.validate.min.js" type="text/javascript"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--  Bootstrap Table Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-table.js"></script>
<!--  DataTable Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<!--  Full Calendar   -->
<script src="<?= base_url() ?>assets/js/plugins/fullcalendar.min.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!-- Light Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= base_url() ?>assets/js/demo.js"></script>

<!-- ACTIVA LOS TOOLTIPS -->

<script type="text/javascript">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
</script>

<!-- SCRIPT NECESARIO PARA PODER USAR LAS TABLAS -->

<script type="text/javascript">
  $(document).ready(function() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [5,10, 25, 50, -1],
        [5,10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Buscar registros",
      }
    });
      
    var table = $('#datatables').DataTable();
      // Edit record
    table.on('click', '.edit', function() {
      $tr = $(this).closest('tr');

      if ($tr.hasClass('child')) {
          $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
    });

    // Delete a record
    table.on('click', '.remove', function(e) {
      $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });
  });
</script>

<!-- CIERRE DEL SCRIPT NECESARIO PARA PODER USAR LAS TABLAS -->

<!-- Facebook Pixel Code Don't Delete -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window,
        document, 'script', '//connect.facebook.net/en_US/fbevents.js');

    try {
        fbq('init', '111649226022273');
        fbq('track', "PageView");

    } catch (err) {
        console.log('Facebook Track Error:', err);
    }
</script>
<noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=111649226022273&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->

</html>