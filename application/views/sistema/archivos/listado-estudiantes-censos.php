<?php

//LISTADO DE LOS ESTUDIANTES INSCRITOS EN UN CENSO DETERMINADO

?>
<!DOCTYPE html>
<html lang="es">
<head>

	<title>LISTADO DE ESTUDIANTES INSCRITOS</title>

	<!-- Meta etiquetas -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' />

<!-- CSS Files -->
  <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?= base_url() ?>assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/mystyles.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/containers.css">
</head>
<body>
	
	<div class="row justify-content-center">
		<!-- ENCABEZADO -->

		<!-- LOGO UVM -->
		<div class="text-center">
			<img style="float: left; margin-right: 10px;" src="<?= base_url() ?>assets/img/portal/logo.png" width="40" height="40">
			<span style="font-weight: bold; font-size: 16pt; text-align: center;"> <?= $censo_datos->nombre ?>	</span>
		</div>

		<!-- Sub-encabezado -->

		<div class="col-12 text-center">
			<?php $fecha = new DateTime($censo_datos->fecha_taller); ?>
			<span style="font-weight: bold;">Fecha: </span>
			<?= $fecha->format('d-m-Y'); ?> |

			<span style="font-weight: bold;">Inscritos: </span>
				<?= $this->Censo_usuario_model->inscritos($censo_datos->censo_id)->num_rows(); ?> |

			<span style="font-weight: bold;">Límite: </span>
			<?= $censo_datos->limite ?> |

			<?php
			$coordinador = $this->Perfil_model->obtenerPerfil_usuario_id($censo_datos->coordinador_id);
			?>
			<span style="font-weight: bold;">Creado por: </span>
			<?= $coordinador->primer_nombre ?> <?= $coordinador->primer_apellido ?> 
			<br>
			<span style="font-weight: bold;">Estudiantes Inscritos</span>
		</div>
	</div>				

	<!-- TABLA DE ESTUDIANTES INSCRITOS EN EL CENSO -->

	<div class="row justify-content-center">
		<?php if($this->Censo_usuario_model->inscritos($censo_datos->censo_id)->num_rows() > 0): ?>
			<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

				<thead>
					<tr>
						<th style="text-align:center;">Cédula de I.</th>
						<th style="text-align:center;">Nombres</th>
						<th style="text-align:center;">Apellidos</th>
						<th style="text-align:center;">Carrera</th>
						<th>Firma</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach($censos as $censo): ?>
						<tr>
							
							<!-- Cédula -->
							<td><?= $censo->nacionalidad . '-' . $censo->ci ?></td>

							<!-- Nombres -->
							<td><?= $censo->primer_nombre . ' ' . $censo->segundo_nombre ?></td>

							<!-- Apellidos -->
							<td><?= $censo->primer_apellido . ' ' . $censo->segundo_apellido ?></td>

							<!-- Carrera -->													
							<td><?php $cantidad_carrera = 0; ?>
								<?php if ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($censo->estudiante_id)): ?>											
									<?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($censo->estudiante_id) as $carrera_estudiante): ?>
										<?php $cantidad_carrera = $cantidad_carrera + 1; ?>
										<?php if ($cantidad_carrera > 1): ?>
											<?php echo ", "; ?>
										<?php endif;?>
										<?= $this->Carrera_model->obtenerCarrera($carrera_estudiante->carrera_id)->nombre ?>
									<?php endforeach;  ?>
								<?php endif; ?>
							</td>

							<td>
								<!-- Espacio para firmar -->
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>									
		<?php else: ?>
			<div class="alert alert-warning border-radius-10" role="alert">
				<strong>¡Aún no hay estudiantes registrados!</strong>
			</div>
		<?php endif; ?>	
	</div> 

</body>

<!--   Core JS Files   -->
<script src="<?= base_url() ?>assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
<!--  Chartist Plugin  -->
<script src="<?= base_url() ?>assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-notify.js"></script>
<!--  Share Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.sharrre.js"></script>
<!--  jVector Map  -->
<script src="<?= base_url() ?>assets/js/plugins/jquery-jvectormap.js" type="text/javascript"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?= base_url() ?>assets/js/plugins/moment.min.js"></script>
<!--  DatetimePicker   -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-datetimepicker.js"></script>
<!--  Sweet Alert  -->
<script src="<?= base_url() ?>assets/js/plugins/sweetalert2.min.js" type="text/javascript"></script>
<!--  Tags Input  -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-tagsinput.js" type="text/javascript"></script>
<!--  Sliders  -->
<script src="<?= base_url() ?>assets/js/plugins/nouislider.js" type="text/javascript"></script>
<!--  Bootstrap Select  -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  jQueryValidate  -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.validate.min.js" type="text/javascript"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--  Bootstrap Table Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-table.js"></script>
<!--  DataTable Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<!--  Full Calendar   -->
<script src="<?= base_url() ?>assets/js/plugins/fullcalendar.min.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!-- Light Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= base_url() ?>assets/js/demo.js"></script>

<!-- ACTIVA LOS TOOLTIPS -->

<script type="text/javascript">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
</script>

<!-- SCRIPT NECESARIO PARA PODER USAR LAS TABLAS -->

<script type="text/javascript">
  $(document).ready(function() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [5,10, 25, 50, -1],
        [5,10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Buscar registros",
      }
    });
      
    var table = $('#datatables').DataTable();
      // Edit record
    table.on('click', '.edit', function() {
      $tr = $(this).closest('tr');

      if ($tr.hasClass('child')) {
          $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
    });

    // Delete a record
    table.on('click', '.remove', function(e) {
      $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });
  });
</script>

<!-- CIERRE DEL SCRIPT NECESARIO PARA PODER USAR LAS TABLAS -->

<!-- Facebook Pixel Code Don't Delete -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window,
        document, 'script', '//connect.facebook.net/en_US/fbevents.js');

    try {
        fbq('init', '111649226022273');
        fbq('track', "PageView");

    } catch (err) {
        console.log('Facebook Track Error:', err);
    }
</script>
<noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=111649226022273&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->

</html>