<!DOCTYPE html>
<html lang="es">
<head>

	<title>CARTA DE POSTULACIÓN</title>

 <!-- Meta etiquetas -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' />

<!-- CSS Files -->
  <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?= base_url() ?>assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/mystyles.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/containers.css">

  <style type="text/css" media="screen">
		p { font-family: "Arial", sans;	}
	</style>
  <style>
    p { /* PARA AÑADIR SANGRÍA AL PÁRRAFO */
        text-indent: 50px;
    }
  </style>

</head>
<body>

	<div class="container-fluid">
		<div class="row justify-content-center">

			<!-- ENCABEZADO -->
      <div class="text-center">
        <!-- LOGO UVM -->
        <img style="float: left; margin-right: -55px;" src="<?= base_url() ?>assets/img/portal/logo.png" width="90" height="90">

        <!-- MEMBRETE -->
        <span class="text-size-10 text-center" style="font-weight: bold;"> 
          UNIVERSIDAD VALLE DEL MOMBOY <br>
          VICERRECTORADO <br>
          COORDINACIÓN DE PRÁCTICAS PROFESIONALES <br> 
        </span>
        <small>www.uvm.edu.ve</small>
      </div>

      <!-- FECHA -->
      <div id="fecha" style="margin-top: 70px;">
        <div class="text-right">
          <?php 
            $fe_actual = date("d-m-Y");
            $dia       = substr($fe_actual, 0, 2);
            $mes_n     = substr($fe_actual, 3,-5);
            $año       = substr($fe_actual, 6, 9);
            switch ($mes_n) {
              case '01':
                $mes = "Enero";
                break;

              case '02':
                $mes = "Febrero";
                break;

              case '03':
                $mes = "Marzo";
                break;

              case '04':
                $mes = "Abril";
                break;

              case '05':
                $mes = "Mayo";
                break;

              case '06':
                $mes = "Junio";
                break;

              case '07':
                $mes = "Julio";
                break;

              case '08':
                $mes = "Agosto";
                break;

              case '09':
                $mes = "Septiembre";
                break;

              case '10':
                $mes = "Octubre";
                break;

              case '11':
                $mes = "Noviembre";
                break;

              case '12':
                $mes = "Diciembre";
                break;
              
              default:
                # code...
                break;
            }
          ?>
          Valera, <?= $dia . " de " . $mes . " de " . $año ?>.
        </div>
      </div>

      <div id="empresa">
        <div class="text-left">
          Sres. <br>
          <span style="font-weight: bold;">
            <?= mb_strtoupper($empresa->nombre) ?> <br>
            DPTO. DE RECURSOS HUMANOS <br>
          </span>
          Su despacho.
        </div>
      </div>
      
      <?php if ($empresa->jr_nombres && $empresa->jr_apellidos): ?>
        <div id="jefe-rrhh">
          <div class="text-center" style="font-weight: bold; margin-top: 25px; margin-bottom: 50px;">
            Atención: <?= $empresa->jr_apellidos . " " . $empresa->jr_nombres ?>
          </div>
        </div>
      <?php endif; ?>
			

			<!-- CUERPO -->
			<div class="col-12 text-justify text-size-10">
				<p>          
					En nombre de la Universidad Valle del Momboy, reciba un cordial saludo en la oportunidad solicitar, muy gentilmente, su buena pro para que el(la) ciudaddano(a), 
          <span style="font-weight: bold;"><?= mb_strtoupper($estudiante->primer_apellido) . " " . mb_strtoupper($estudiante->segundo_apellido) . " " . mb_strtoupper($estudiante->primer_nombre) . " " . mb_strtoupper($estudiante->segundo_nombre) ?></span>, 
          titular de la Cédula de Identidad N° 
          <span style="font-weight: bold;"><?= $estudiante->nacionalidad ?>-<?= $estudiante->ci ?></span>; estudiante de la carrera de 
          <?php
            $carreras_id = $this->Carrera_estudiante_model->buscarCarreras_por_pasantia($estudiante->usuario_id,$pasantia_id);
          //  print_r($carreras_id);

            //Variable de conteo de carreras
            $cant = 1;
          ?>

          <?php foreach ($carreras_id as $carrera_id): ?>
            <?php $carrera = $this->Carrera_model->obtenerCarrera($carrera_id->carrera_id); ?>
            <?php if ($cant > 1): ?>
              , <span style="font-weight: bold;"><?= mb_strtoupper($carrera->nombre) ?></span>
            <?php else: ?>
              <span style="font-weight: bold;"><?= mb_strtoupper($carrera->nombre) ?></span>
            <?php endif; ?>
          <?php endforeach;?>

          realice sus Prácticas Profesionales en esa Organización donde deberá permanecer por un lapso mínimo de
          <?php if ($modalidad == 'completo'): ?>
            <?php echo " 10 "; ?>
          <?php else: ?>
            <?php echo " 20 "; ?>
          <?php endif; ?>
          semanas, contados a partir de su incorporación a la misma. El(la) pasante estará disponible a partir de la presente fecha.
        </p>
        <br>
        <p>
					En este sentido, <span style="font-weight: bold;">agradecemos nos envíe la carta de aceptación antes de que el estudiante comience en esa organización</span>, los datos del Tutor Laboral asignado así como Plan de trabajo que deberá desarrollar. (Se anexa formato)
				</p>
        <br>
        <p>
          Sin otro particular y agradeciendo su amable atención, quedo de Ud.
        </p>
			</div>

      <div class="text-center">Atentamente,</div>
      <br><br><br><br>

      <div id="Prof" class="text-center">
        <?php 
          $coordinador = $this->Perfil_model->obtenerPerfil_usuario_id($this->perfil_session->usuario_id);
        ?>
        <span style="border-top: 1px solid black;">Prof. <?= $coordinador->primer_nombre . " " . $coordinador->segundo_nombre . " " . $coordinador->primer_apellido . " " . $coordinador->segundo_apellido ?></span>
        <br>
        Coord. de Prácticas Profesionales     
      </div>
      <br><br>
      <div id="footer" class="footer" style="position: absolute; bottom: 0;width: 100%; height: 42px;">
        <small>
          <span style="font-weight: bold;">NOTA:</span> Esta correspondencia  no constituye un compromiso para la Universidad y  debe tener firma y sello originales de la Coord de Prácticas Profesionales. La postulación se hará efectiva si el (la) bachiller cumple con los requisitos académicos para iniciar las Prácticas Profesionales en la fecha antes mencionada. Cualquier eventualidad favor comunicarla a la siguiente dirección: <span style="font-weight: bold;">practicasprofesionales@uvm.edu.ve</span>  o Tlf. <span style="font-weight: bold;">0271-4144664</span>
        </small>
      </div>

		</div>

    <br><br>

	
	</div>
	
</body>
<!--   Core JS Files   -->
<script src="<?= base_url() ?>assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
<!--  Chartist Plugin  -->
<script src="<?= base_url() ?>assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-notify.js"></script>
<!--  Share Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.sharrre.js"></script>
<!--  jVector Map  -->
<script src="<?= base_url() ?>assets/js/plugins/jquery-jvectormap.js" type="text/javascript"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?= base_url() ?>assets/js/plugins/moment.min.js"></script>
<!--  DatetimePicker   -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-datetimepicker.js"></script>
<!--  Sweet Alert  -->
<script src="<?= base_url() ?>assets/js/plugins/sweetalert2.min.js" type="text/javascript"></script>
<!--  Tags Input  -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-tagsinput.js" type="text/javascript"></script>
<!--  Sliders  -->
<script src="<?= base_url() ?>assets/js/plugins/nouislider.js" type="text/javascript"></script>
<!--  Bootstrap Select  -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  jQueryValidate  -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.validate.min.js" type="text/javascript"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--  Bootstrap Table Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-table.js"></script>
<!--  DataTable Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<!--  Full Calendar   -->
<script src="<?= base_url() ?>assets/js/plugins/fullcalendar.min.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!-- Light Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= base_url() ?>assets/js/demo.js"></script>

<!-- ACTIVA LOS TOOLTIPS -->

<script type="text/javascript">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
</script>

<!-- SCRIPT NECESARIO PARA PODER USAR LAS TABLAS -->

<script type="text/javascript">
  $(document).ready(function() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [5,10, 25, 50, -1],
        [5,10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Buscar registros",
      }
    });
      
    var table = $('#datatables').DataTable();
      // Edit record
    table.on('click', '.edit', function() {
      $tr = $(this).closest('tr');

      if ($tr.hasClass('child')) {
          $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
    });

    // Delete a record
    table.on('click', '.remove', function(e) {
      $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });
  });
</script>

<!-- CIERRE DEL SCRIPT NECESARIO PARA PODER USAR LAS TABLAS -->

<!-- Facebook Pixel Code Don't Delete -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window,
        document, 'script', '//connect.facebook.net/en_US/fbevents.js');

    try {
        fbq('init', '111649226022273');
        fbq('track', "PageView");

    } catch (err) {
        console.log('Facebook Track Error:', err);
    }
</script>
<noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=111649226022273&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->

</html>