<!-- Todas las categorías -->

		<div class="row">
			<div class="col-12 semi-bold section-title text-center">
				Categorías
			</div>
			<div class="col-12">
				<a class="btn btn-primary float-right" role="button" href="<?= base_url()?>categorias/nueva">
					<i class="fa fa-plus"></i> Nueva categoría
				</a>
			</div>
		</div>

<!-- TABLA DE CATEGORÍAS -->

		<div class="row">
			<?php if (!$segmento): ?>

				<div class="col-md-12">
					<div class="card data-tables">
						<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
							<div class="fresh-datatables">

								<?php if ($categorias): ?>

									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

										<thead>
											<tr>
												<th>#</th>
												<th>Nombre</th>
												<th>Color</th>
												<th class="disabled-sorting text-right">Acciones</th>
											</tr>
										</thead>

										<tfoot>
											<tr>
												<th>#</th>
												<th>Nombre</th>
												<th>Color</th>
												<th class="text-right">Acciones</th>
											</tr>
										</tfoot>

										<tbody>
											<?php foreach ($categorias as $categoria): ?>
												<tr>
													<!-- # ID -->
													<td><?= $categoria->categoria_id ?></td>

													<!-- Nombre de la categoría -->
													<td>
														<?php if(strlen($categoria->nombre) > 30): ?>
									        		<?= substr($categoria->nombre, 0, 30); ?>...
									        	<?php else: ?>
									        		<?= $categoria->nombre ?>
									        	<?php endif;?>
													</td>

													<!-- Color -->
													<td>
														<input type="color" name="color" value="<?= $categoria->color ?>" disabled>
													</td>

													<!-- Acciones -->
													<td class="text-right">	
														<div class="dropdown">
														  <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink-<?= $categoria->categoria_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														    <i class="fa fa-gears"></i>
														  </a>

														  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?= $categoria->categoria_id ?>">
														  	<h6 class="dropdown-header">Acciones</h6>

							                	<!-- Ver -->
							                <!--
																<a href="<?= base_url()?>categorias/index/<?=$categoria->categoria_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-tag"></i> Ver
							                	</a>
							                -->

							                	<!-- Modificar -->
							                	<a href="<?= base_url()?>categorias/editar/<?=$categoria->categoria_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-edit"></i> Modificar
																</a>

																<!-- Eliminar -->
																<?php if ($categoria->categoria_id != 3): ?>
																	<a href="<?= base_url()?>categorias/eliminar/<?=$categoria->categoria_id ?>" class="dropdown-item width_130 border-radius-10">
																		<i class="fa fa-trash"></i> Eliminar
																	</a>
																<?php endif; ?>
							                </div>
							              </div>
													</td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
								<?php else: ?>
									<div class="alert alert-warning border-radius-10" role="alert">
										¡No hay categorías registradas!
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				
			<!-- Una sola categoría -->
			<?php else: ?>
				<div class="col-12 text-center">
					<h3>Datos de la categoría</h3>
					<p>
						#: <?= $categoria->categoria_id ?> <br>
						Nombre: <?= $categoria->nombre ?> <br>
						Color: <input type="color" name="color" value="<?= $categoria->color ?>" disabled>
					</p>
				</div>
			<?php endif; ?>	
		</div>
		