
		<?= validation_errors() ?>

		<?= form_open("/categorias/editar/$id") ?>
			<?php
				$color = array(
					'name' => 'color', 
					'type' => 'color',
					'value' => $categoria->color,
					'class' => 'form-control color-input'
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">
				<div class="col-md-6 col-xl-5">
					<span>Nombre: </span>
					<input type="text" name="nombre" value="<?= $categoria->nombre ?>" <?php if ($categoria->nombre == 'Documentos'){ echo "disabled"; } ?> class="form-control" >
				</div>
				
				<div class="col-md-6 col-xl-5">
					<span>Color: </span>
					<?= form_input($color) ?>	
				</div>
				
			</div>

			<div class="row">

				<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>
				
			</div>
		<?= form_close() ?>