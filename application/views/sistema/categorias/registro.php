
		<?= validation_errors() ?>

		<?= form_open("/categorias/nueva") ?>
			<?php
				$color = array(
					'name' => 'color', 
					'type' => 'color',
					'class' => 'form-control color-input'
				);

				$nombre = array(
					'name' => 'nombre', 
					'placeholder' => 'Nombre de la categoría. (Máximo 50 caracteres)',
					'class' => 'form-control'
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">
				<div class="col-md-6 col-xl-5">
					<span>Nombre: </span>
					<?= form_input($nombre) ?>
				</div>
				
				<div class="col-md-6 col-xl-5">
					<span>Color: </span>
					<?= form_input($color) ?>	
				</div>
				
			</div>

			<div class="row">

				<div class="col-5 col-md-6 col-xl-5 text-right"><?= form_submit('','Registrar',['class'=>'btn btn-success']) ?></div>
				<div class="col-5  col-md-6 col-xl-5 text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-danger']) ?></div>
									
			</div>

		<?= form_close() ?>