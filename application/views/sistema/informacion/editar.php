		<?= validation_errors() ?>

		<?= form_open("/informaciones/editar/$id") ?>
			<?php
				$descripcion_breve = array(
					'name' => 'descripcion_breve', 
					'value' => $informacion->descripcion_breve,
					'class' => 'form-control',
					'rows' => '5'
				);

				$descripcion_general = array(
					'name' => 'descripcion_general', 
					'value' => $informacion->descripcion_general,
					'class' => 'form-control'
				);

				$objetivos = array(
					'name' => 'objetivos',
					'value' => $informacion->objetivos,
					'class' => 'form-control'
				);
				
				$mision = array(
					'name' => 'mision', 
					'value' => $informacion->mision,
					'class' => 'form-control'
				);

				$vision = array(
					'name' => 'vision', 
					'value' => $informacion->vision,
					'class' => 'form-control'
				);

				$numeros_telefonicos = array(
					'name' => 'numeros_telefonicos', 
					'value' => $informacion->numeros_telefonicos,
					'class' => 'form-control',
					'rows' => '3'
				);

				$ubicacion = array(
					'name' => 'ubicacion', 
					'value' => $informacion->ubicacion,
					'class' => 'form-control',
					'rows' => '3'
				);

				$redes_sociales = array(
					'name' => 'redes_sociales', 
					'value' => $informacion->redes_sociales,
					'class' => 'form-control',
					'rows' => '3'
				);

				$horario = array(
					'name' => 'horario',
					'value' => $informacion->horario,
					'class' => 'form-control',
					'rows' => '3'
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">
				<div class="col-12 col-md-12 col-xl-10 margin-bottom-sm">
					<span>Breve descripción</span>
					<?= form_textarea($descripcion_breve) ?>	
				</div>

<!-- OCULTO	
				<div class="col-12 col-md-6 col-xl-5 margin-bottom-sm">
					<span>Descripción general</span>
					<?= form_textarea($descripcion_general) ?>
				</div>

				<div class="col-12 col-md-6 col-xl-5 margin-bottom-sm">
					<span>Objetivos</span>
					<?= form_textarea($objetivos) ?>
				</div>

				<div class="col-12 col-md-6 col-xl-5 margin-bottom-sm">
					<span>Misión</span>
					<?= form_textarea($mision) ?>
				</div>

				<div class="col-12 col-md-6 col-xl-5 margin-bottom-sm">
					<span>Visión</span>
					<?= form_textarea($vision) ?>
				</div>
-->
				<div class="col-12 col-md-6 col-xl-5 margin-bottom-sm">
					<span>Números telefónicos</span>
					<?= form_textarea($numeros_telefonicos) ?>
				</div>

				<div class="col-12 col-md-6 col-xl-5 margin-bottom-sm">
					<span>Redes sociales</span>
					<?= form_textarea($redes_sociales) ?>
				</div>

				<div class="col-12 col-md-6 col-xl-5 margin-bottom-sm">
					<span>Horario de atención</span>
					<?= form_textarea($horario) ?>
				</div>

				<div class="col-12 col-md-6 col-xl-5 margin-bottom-sm">
					<span>Ubicación de la oficina</span>
					<?= form_textarea($ubicacion) ?>
				</div>

			</div>

			<div class="row">

				<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>
				
			</div>

		<?= form_close() ?>

