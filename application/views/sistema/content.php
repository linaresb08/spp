		<?php if(in_array($this->session->userdata('rol'), array("Administrador", "Coordinador"))): ?>

			<!-- Tarjeta de reportes de pasantías -->
			<div class="row justify-content-center">
				<div class="col-12 col-md-10 col-xl-8">
					<div class="card stacked-form">
						<div class="card-header">
								<h6 class="card-title semi-bold">Pasantías</h6>
						</div>
						<div class="card-body">
							<div class="row justify-content-center">
								<?php foreach ($this->Funciones_model->obtenerUltimos('periodo_academico_id', 2, 'periodo_academicos') as $periodo): ?>
									<div class="col-12 col-md-6 border-card-r">
										<div class="row justify-content-center">
											<div class="col-4">
												<h6 class="card-title semi-bold"><?= $periodo->nombre ?></h6>
											</div>
										</div>
										<?php
											$pasantias = $this->Funciones_model->obtenerPasantias_por_periodo($periodo->periodo_academico_id);

											$total       = 0;
											$habilitadas = 0;
											$aprobadas   = 0;
											$reprobadas  = 0;

											if ($pasantias) {
												foreach ($pasantias as $pasantia) {
													$total += 1;

													if ($pasantia->estado == 'Habilitada') {
														$habilitadas += 1;
													}
													elseif ('Aprobada') {
														$aprobadas += 1;
													}
													else {
														$reprobadas += 1;
													}												
												}
											}
											
										?>
										<div class="row justify-content-center margin_b-t">
											<div class="col-5 no-padding_r">
												<h2 class="no-margin semi-bold text-right">
													<?= $total ?>
												</h2>
											</div>
											<div class="col-5 align-self-center">
												<h6>Total registradas</h6>
											</div>
										</div>
										<?php if ($total > 0): ?>
											<div class="row justify-content-around">
												<div class="col-5">
													<div class="row">
														<div class="col-12 text-center">
															<h5 class="no-margin semi-bold"><?= $habilitadas ?></h5>
														</div>
													</div>
													<div class="row">
														<h6 class="col-12 text-center">Habilitadas
														</h6>
													</div>
												</div>
												<div class="col-5">
													<div class="row">
														<div class="col-12 text-center">
															<h5 class="no-margin semi-bold"><?= $aprobadas ?></h5>
														</div>
													</div>
													<div class="row">
														<h6 class="col-12 text-center">Aprobadas
														</h6>
													</div>
												</div>
												<?php if ($reprobadas > 0): ?>
													<div class="col-5">
														<div class="row">
															<div class="col-12 text-center">
																<h5 class="no-margin semi-bold">10</h5>
															</div>
														</div>
														<div class="row">
															<h6 class="col-12 text-center">Reprobadas
															</h6>
														</div>
													</div>
												<?php endif;?>
											</div>
										<?php endif; ?>									
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					
						<div class="card-footer no-padding_t">
							<small class="text-muted text-uppercase">
								<?php $fe_actual = date("d-m-Y"); ?>
								<i class="fa fa-refresh"></i>
								Información actualizada al <?= $fe_actual ?> 
							</small>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Tarjetas de Usuarios y Censos -->
			<div class="row justify-content-center">
				<!-- Tarjeta de usuarios -->
				<div class="col-12 col-md-5 col-xl-4">
					<div class="card stacked-form">
						<div class="card-header">
							<h6 class="card-title semi-bold">USUARIOS</h6>
						</div>
						<div class="card-body">
							<?php 
								$usuarios = $this->Funciones_model->obtenerDatosTabla('usuarios');

								$estudiantes   = 0;
								$coordinadores = 0;
								$tutores_a     = 0;
								$invitados     = 0;
								$total         = 0;

								foreach ($usuarios as $usuario) {
									if ($usuario->rol != 'Administrador') {
										$total += 1;

										if ($usuario->rol == 'Coordinador') {
											$coordinadores += 1;
										}
										elseif ($usuario->rol == 'Estudiante') {
											$estudiantes += 1;
										}
										elseif ($usuario->rol == 'Invitado') {
											$invitados += 1;
										}
										else {
											$tutores_a += 1;
										}
									}										
								}
							?>
							<div class="row justify-content-center margin_b">
								<div class="col-5 no-padding_r">
									<h2 class="no-margin semi-bold text-right"><?= $total ?></h2>
								</div>
								<div class="col-5 align-self-center">
									<h6>Usuarios registrados</h6>
								</div>
							</div>

							<div class="row justify-content-around">
								<div class="col-5 margin_b">
									<div class="row">
										<div class="col-12 text-center">
											<h5 class="no-margin semi-bold">
												<?= $estudiantes ?>
											</h5>
										</div>
									</div>
									<div class="row">
										<h6 class="col-12 text-center">
											Estudiantes
										</h6>
									</div>
								</div>
								<div class="col-5 margin_b">
									<div class="row">
										<div class="col-12 text-center">
											<h5 class="no-margin semi-bold">
												<?= $tutores_a ?>
											</h5>
										</div>
									</div>
									<div class="row">
										<h6 class="col-12 text-center">
											Tutores A.
										</h6>
									</div>
								</div>
								<div class="col-6 margin_b">
									<div class="row">
										<div class="col-12 text-center">
											<h5 class="no-margin semi-bold">
												<?= $coordinadores ?>
											</h5>
										</div>
									</div>
									<div class="row">
										<h6 class="col-12 text-center">
											Coordinadores
										</h6>
									</div>
								</div>
								<div class="col-5 margin_b">
									<div class="row">
										<div class="col-12 text-center">
											<h5 class="no-margin semi-bold">
												<?= $invitados ?>
											</h5>
										</div>
									</div>
									<div class="row">
										<h6 class="col-12 text-center">
											Invitados
										</h6>
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer no-padding_t">
							<small class="text-muted text-uppercase">
								<?php $fe_actual = date("d-m-Y"); ?>
								<i class="fa fa-refresh"></i>
								Última actualización el <?= $fe_actual ?>
							</small>
						</div>
					</div>
				</div>

				<!-- Tajeta de censo -->
				<?php 
					$censo = $this->Funciones_model->obtenerUltimos('censo_id', 1, 'censos');

					$fecha = new DateTime($censo->fecha_taller);
				?>
				<div class="col-12 col-md-5 col-xl-4">
					<div class="card stacked-form">
						<div class="card-header">
							<h6 class="card-title semi-bold">Último censo <span class="float-right semi-bold">Fecha: <?= $fecha->format('d-m-Y'); ?></span></h6>
						</div>
						<div class="card-body">							
							<div class="row">
								<div class="col-12 text-center semi-bold">
									<a href="<?= base_url()?>censos/index/<?= $censo->censo_id ?>" class="font-color-black"> <?= mb_strtoupper($censo->nombre) ?>	</a>
									
								</div>
							</div>
							<div class="row margin_sm_b-t">
								<div class="col-12">
									<strong>Lugar:</strong> <?= $censo->lugar ?>
								</div>
							</div>
							<div class="row justify-content-around">
								<div class="col-5">
									<div class="row">
										<div class="col-12 text-center">
											<h5 class="no-margin semi-bold">
												<?= $this->Censo_usuario_model->inscritos($censo->censo_id)->num_rows() . " / " . $censo->limite ?> 
											</h5>
										</div>
									</div>
									<div class="row">
										<h6 class="col-12 text-center">
											Inscritos
										</h6>
									</div>
								</div>

								<div class="col-5">
									<div class="row">
										<div class="col-12 text-center">
											<h5 class="no-margin semi-bold">
												<?= $censo->estatus ?> 
											</h5>
										</div>
									</div>
									<div class="row">
										<h6 class="col-12 text-center">
											Estatus
										</h6>
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer no-padding_t">
							<small class="text-muted text-uppercase">
								<?php $fe_actual = date("d-m-Y"); ?>
								<i class="fa fa-refresh"></i>
								Última actualización el <?= $fe_actual ?>
							</small>
						</div>
					</div>
				</div>
			</div>

			<div class="row justify-content-center">
				<!-- TARJETA PARA GENERAR CARTA DE POSTULACIÓN -->
				<div class="col-12 col-md-5 col-xl-5">
					<div class="card stacked-form">
						<form action="<?= base_url() ?>archivos/cartaPostulacion" method="POST">
							<div class="card-header">
								<h6 class="card-title semi-bold">Carta de postulación</h6>
							</div>
							<div class="card-body">
								<div class="row justify-content-center">
									<div class="form-group col-12 col-lg-11">
										<label for="estudiante_ci">
											Cédula del estudiante 
											<br>
											<small class="font-color-red semi-bold">(Sin letras,guiones ni puntos)</small>
										</label>
										<input type="number" class="form-control" name="estudiante_ci" placeholder="Ej: 11222333" min="1" required>
									</div>
									<h6 class="card-title text-center semi-bold col-12">
										Modalidad
									</h6>
									<div class="col-12 col-md-6 margin_sm_t">
										<div class="form-check form-check-radio">
											<label class="form-check-label" for="medio">
												<input type="radio" name="modalidad" id="medio" value="medio" class="form-check-input">
												<span class="form-check-sign"></span>
												<span style="padding-left: 15px;"></span>Medio tiempo
											</label>							
										</div>
									</div>

									<div class="col-12 col-md-6 margin_sm_t">
										<div class="form-check form-check-radio">
											<label class="form-check-label" for="completo">
												<input type="radio" name="modalidad" id="completo" value="completo" class="form-check-input">
												<span class="form-check-sign"></span>
												<span style="padding-left: 15px;"></span>Tiempo completo
											</label>							
										</div>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<div class="row justify-content-around">
									<div class="col text-center">
										<input type="reset" class="btn btn-warning" value="Limpiar">
									</div>
									<div class="col text-center">
										<input type="submit" class="btn btn-success" value="Generar">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>

				<!-- TARJETA PARA EMITIR REPORTE DE NOTAS Y ESTUDIANTES POR PERIODO -->
				<div class="col-12 col-md-5 col-xl-3">
					<div class="card stacked-form">
						<div class="card-header">
							<h6 class="card-title semi-bold">Reporte de notas por período</h6>
						</div>
						<div class="card-body">
							<div class="row justify-content-center">
								<form action="<?= base_url() ?>archivos/reportePeriodoEstudiantes" method="POST">
									<div class="form-group col-12 col-lg-11">
										<label for="periodo_id">Seleccione período</label>
										<select name="periodo_id" class="form-control">
											<?php foreach ($this->Periodo_model->obtenerPeriodos() as $periodo): ?>										
												<option value="<?= $periodo->periodo_academico_id ?>"> <?= $periodo->nombre ?> </option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="form-group">
										<label for="seccion">Indique sección</label>
										<input type="text" class="form-control" name="seccion" pattern="[A-Za-z]{1}" placeholder="Ej: K">
									</div>
									<button type="submit" class="btn btn-success">Descargar reporte</button>
								</form>
							</div>
						</div>
						<div class="card-footer padding_xs_b-t">
							<small class="text-muted text-uppercase">
								<?php $fe_actual = date("d-m-Y"); ?>
								<i class="fa fa-refresh"></i>
								Listados actualizados al <?= $fe_actual ?>
							</small>
						</div>
					</div>
				</div>		
			</div>

		<?php else: ?>
			<div class="row justify-content-center">
				<div class="col section-title text-center">
						¡Bienvenido al sistema!
					</div>
			</div>
		<?php endif; ?>