		<?= validation_errors() ?>

		<?= form_open("/censos/nuevo") ?>
			<?php
				$fecha_taller = array(
					'name' => 'fecha_taller',
					'type' => 'date',
					'class' => 'form-control'
				);

				$nombre = array(
					'name' => 'nombre', 
					'placeholder' => 'Ingrese el nombre del taller',
					'class' => 'form-control'
				);

				$descripcion = array(
					'name' => 'descripcion', 
					'placeholder' => 'Escribe la descripción del taller',
					'class' => 'form-control'
				);

				$lugar = array(
					'name' => 'lugar', 
					'placeholder' => 'Ingrese el lugar donde se realizará el taller',
					'class' => 'form-control'
				);

				$duracion = array(
					'name' => 'duracion',
					'placeholder' => 'Ejemplo: 4 horas',
					'class' => 'form-control'
				);

				$limite = array(
					'name' => 'limite', 
					'type' => 'number',
					'placeholder' => 'Ejemplo: 10',
					'min' => '1',
					'class' => 'form-control',
					'number' => 'true',
					'aria-invalid' => 'false'
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">	
				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Nombre del taller: </span>
					<?= form_input($nombre) ?>									
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Fecha en que se realizará: </span>
					<?= form_input($fecha_taller) ?>					
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Descripción: </span>
					<?= form_textarea($descripcion) ?>					
				</div>	

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Lugar: </span>
					<?= form_textarea($lugar) ?>					
				</div>	

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Duración del taller: </span>
					<?= form_input($duracion) ?>					
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Límite de inscritos: </span>
					<?= form_input($limite) ?>					
				</div>		

			</div>	
			<div class="row">
				<div class="col-5 col-md-6 text-right"><?= form_submit('','Crear',['class'=>'btn btn-success']) ?></div>
				<div class="col-5 col-md-6 text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>					
			</div>
			
		<?= form_close() ?>