		<?= validation_errors() ?>

		<?= form_open("/censos/editar/$id") ?>
			<?php
				$fecha_taller = array(
					'name' => 'fecha_taller', 
					'type' => 'date',
					'value' => $censo->fecha_taller,
					'class' => 'form-control'
				);

				$nombre = array(
					'name' => 'nombre', 
					'placeholder' => 'Ingrese el nombre del taller',
					'value' => $censo->nombre,
					'class' => 'form-control'
				);

				$descripcion = array(
					'name' => 'descripcion', 
					'placeholder' => 'Escribe la descripción del taller',
					'value' => $censo->descripcion,
					'class' => 'form-control'
				);

				$lugar = array(
					'name' => 'lugar', 
					'placeholder' => 'Ingrese el lugar donde se realizará el taller',
					'value' => $censo->lugar,
					'class' => 'form-control'
				);

				$duracion = array(
					'name' => 'duracion',
					'placeholder' => 'Escribe la duración del taller. Ej: 4 horas',
					'value' => $censo->duracion,
					'class' => 'form-control'
				);

				$limite = array(
					'name' => 'limite', 
					'type' => 'number',
					'value' => $censo->limite,
					'class' => 'form-control'
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">	

				<div class="col-md-6 col-xl-5">
					<span>Nombre del taller: </span>
					<?= form_input($nombre) ?>										
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Fecha en que se realizará: </span>
					<?= form_input($fecha_taller) ?>					
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Descripción: </span>
					<?= form_textarea($descripcion) ?>						
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Lugar: </span>
					<?= form_textarea($lugar) ?>						
				</div>

				<div class="col-md-4 margin_sm_t">
					<span>Duración del taller: </span>
					<?= form_input($duracion) ?>					
				</div>

				<div class="col-md-4 col-xl-2 margin_sm_t">
					<span>Límite de inscritos: </span>
					<?= form_input($limite) ?>					
				</div>				

				<div class="col-md-4 margin_sm_t">
					<span>Estatus del censo: </span>
					<select name="estatus" id="" class="form-control">
						<option value="Abierto" <?php if ($censo->estatus == 'Abierto') { echo "selected"; } ?> >Abierto</option>
						<option value="Cerrado" <?php if ($censo->estatus == 'Cerrado') { echo "selected"; } ?> >Cerrado</option>
					</select>
				</div>

			</div>	

			<div class="row">

				<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>
				
			</div>
			
		<?= form_close() ?>



<!-- 
			<div>
				<span>Nombre del taller: </span>
				<?= form_input($nombre) ?>
				<br>

				<span>Descripción: </span>
				<?= form_textarea($descripcion) ?>
				<br>

				<span>Fecha en que se realizará: </span>
				<?= form_input($fecha_taller) ?>
				<br>

				<span>Lugar: </span>
				<?= form_textarea($lugar) ?>
				<br>

				<span>Límite de inscritos: </span>
				<?= form_input($limite) ?>
				<br>

				<span>Estatus del censo: </span>
				<select name="estatus" id="">
					<option value="Abierto" <?php if ($censo->estatus == 'Abierto') { echo "selected"; } ?> >Abierto</option>
					<option value="Cerrado" <?php if ($censo->estatus == 'Cerrado') { echo "selected"; } ?> >Cerrado</option>
				</select>
			</div>

			<?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?>
		<?= form_close() ?> 
-->