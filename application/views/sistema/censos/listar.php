<!-- Todos los censos -->


<!-- TABLA DE CENSOS -->

		<?php if (!$segmento): ?>
			<div class="row">
				<div class="col-12 semi-bold section-title text-center">
					Censos
				</div>

				<?php if($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador'): ?>
					<div class="col-12">
						<a class="btn btn-primary float-right" role="button" href="<?= base_url()?>censos/nuevo">
							<i class="fa fa-plus"></i> Nuevo censo
						</a>
					</div>
				<?php endif; ?>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="card data-tables">
						<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
							<div class="fresh-datatables">

								<?php if ($censos): ?>

									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

										<thead>
											<tr>
												<th>Fecha</th>
												<th>Nombre</th>												
												<th>Inscritos</th>
												<th>Límite</th>
												<th>Estatus</th>											
												<th class="disabled-sorting text-right">Acciones</th>
											</tr>
										</thead>

										<tfoot>
											<tr>												
												<th>Fecha</th>
												<th>Nombre</th>												
												<th>Inscritos</th>
												<th>Límite</th>
												<th>Estatus</th>											
												<th class="disabled-sorting text-right">Acciones</th>
											</tr>
										</tfoot>

										<tbody>
											<?php foreach ($censos as $censo): ?>
												<tr>

													<!-- Censo Fecha -->
													<td>
														<?php $fecha = new DateTime($censo->fecha_taller); ?>
														<?= $fecha->format('d-m-Y'); ?>
													</td>
													
													<!-- Censo Nombre -->
													<td class="parrafo-card overflow-auto"> <?= $censo->nombre ?> </td>

													<!-- Censo Inscritos -->
													<td>
														<?= $this->Censo_usuario_model->inscritos($censo->censo_id)->num_rows(); ?>
													</td>

													<!-- Censo Límite -->
													<td> <?= $censo->limite ?> </td>

													<!-- Censo Estatus -->
													<td> <?= $censo->estatus ?> </td>								

													<!-- Acciones -->

													<td class="text-right">

														<div class="dropdown">
														  <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink-<?=$censo->censo_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														    <i class="fa fa-gears"></i>
														  </a>

														  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?=$censo->censo_id ?>">
														  	<h6 class="dropdown-header">Acciones</h6>

																<a href="<?= base_url()?>censos/index/<?=$censo->censo_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-list-alt"></i> Ver censo
							                	</a>

							                	<!-- OPERACIONES ESPECIALES ENTRE COORD Y ESTUDIANTES -->

																<?php if($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador'): ?>
																	<a href="<?= base_url()?>censos/editar/<?=$censo->censo_id ?>" class="dropdown-item width_130 border-radius-10">
																		<i class="fa fa-edit"></i> Modificar censo
																	</a>

																	<a href="<?= base_url()?>censos/eliminar/<?=$censo->censo_id ?>" class="dropdown-item width_130 border-radius-10">
																		<i class="fa fa-trash"></i> Eliminar censo
																	</a>
																	<!-- Condicional para la apertura y cierre del censo -->
																	<?php if ($censo->estatus == 'Abierto'): ?>	<!-- Si Censo == 'Abierto' -->							
																		<?php if ($this->Censo_usuario_model->inscritos($censo->censo_id)->num_rows() == $censo->limite): ?>					
																			<!-- Si inscritos == limite -->
																			<?php  
																					$this->Censo_model->cerrarCenso($censo->censo_id);
																					header('Location: ' . base_url() . 'censos');
																				?>
																		<?php else: ?>
																			<!-- Si Censo == 'Abierto' -->
																			<a href="<?= base_url()?>censos/cerrar/<?=$censo->censo_id ?>" class="dropdown-item width_130 border-radius-10">
																				<i class="fa fa-folder"></i> Cerrar censo
										                	</a>
										                <?php endif; ?>
									                <?php elseif ($this->Censo_usuario_model->inscritos($censo->censo_id)->num_rows() < $censo->limite): ?>
									                	<a href="<?= base_url()?>censos/abrir/<?=$censo->censo_id ?>" class="dropdown-item width_130 border-radius-10">
																			<i class="fa fa-folder-open"></i> Abrir censo
									                	</a>
																	<?php endif; ?>
																	<!-- Fin del condicional para la apertura y cierre del censo -->

																<!-- Condicional para el registro y retiro del estudiante -->
																<?php elseif($this->session->userdata('rol') == 'Estudiante'): ?>
																	<?php	if ( $this->Censo_usuario_model->inscritos($censo->censo_id)->num_rows() == $censo->limite): ?>

																		<?php	if ($censo->estatus == 'Abierto'): ?>
																			<?php  
																				$this->Censo_model->cerrarCenso($censo->censo_id);
																				header('Location: ' . base_url() . 'censos');
																			?>
																		<?php	elseif ($this->Censo_usuario_model->consultarInscripcion(array('estudiante_id' => $this->perfil_session->usuario_id, 'censo_id' => $censo->censo_id))): ?>
																			<a href="<?= base_url()?>censos/retirarse/<?=$censo->censo_id ?>" class="dropdown-item width_130 border-radius-10">
																				<i class="fa fa-times"></i> Retirarme
																			</a>
																		<?php endif; ?>

																	<?php else: ?>
																		<?php if ($censo->estatus == 'Cerrado'): ?>
																			<?php  
																				$this->Censo_model->abrirCenso($censo->censo_id);
																				header('Location: ' . base_url() . 'censos');
																			?>
																		<?php elseif($this->Censo_usuario_model->consultarInscripcion(array('estudiante_id' => $this->perfil_session->usuario_id, 'censo_id' => $censo->censo_id))): ?>	
																				<a href="<?= base_url()?>censos/retirarse/<?=$censo->censo_id ?>" class="dropdown-item width_130 border-radius-10">
																				<i class="fa fa-times"></i> Retirarme
																			</a>
																		<?php else: ?>
																			<a href="<?= base_url()?>censos/registrarse/<?=$censo->censo_id ?>" class="dropdown-item width_130 border-radius-10">
																				<i class="fa fa-plus"></i> Registrarme
																			</a>
																		<?php endif; ?>

																	<?php endif; ?>												
																	<!-- Fin del condicional para el registro y retiro del estudiante -->

								                <?php endif; ?>
								              </div>
								            </div>
													</td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
								<?php else: ?>
									<div class="alert alert-warning border-radius-10" role="alert">
										¡No hay censos registrados!
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Un solo censo -->
		<?php else: ?>
			<div class="row justify-content-center">

				<div>
					<p class="col-12 semi-bold section-title text-center">
						<?= $censo->nombre ?>		
					</p>
				</div>

				<div class="col-12 text-right">
					<?php $fecha = new DateTime($censo->fecha_taller); ?>
					Fecha de realización: 
					<?= $fecha->format('d-m-Y'); ?>
				</div>

				<div class="col-12">
					<p class="section-description text-justify"> 
						<?= ucfirst($censo->descripcion); ?> 
					</p>
				</div>				

				<div class="col-12">
					<p class="text-justify">
						<strong>Lugar:</strong> 
						<?= ucwords($censo->lugar); ?> 
					</p>
				</div>

				<div class="col-6 col-md-3">
					<p class="text-justify"> 
						<strong>Duración: </strong>
						<?= $censo->duracion ?> 
					</p>
				</div>

				<div class="col-6 col-md-3">
					<p>
						<strong>Estatus: </strong>
						<?= $censo->estatus ?> 
					</p>
				</div>

				<div class="col-6 col-md-3">
					<p>
						<strong>Inscritos: </strong>
						<?= $this->Censo_usuario_model->inscritos($censo->censo_id)->num_rows(); ?> 
					</p>
				</div>

				<div class="col-6 col-md-3">
					<p> 
						<strong>Límite: </strong>
						<?= $censo->limite ?> 
					</p>
				</div>

				<div class="col-12 col-md-6">
					<p> 
						<?php
						$coordinador = $this->Perfil_model->obtenerPerfil_usuario_id($censo->coordinador_id);
						?>
						<strong>Creado por: </strong>
						<?= ucwords($coordinador->primer_nombre); ?> <?= ucwords($coordinador->primer_apellido); ?> 
					</p>
				</div>

				<div class="col-12 col-md-6">
					<form action="<?= base_url(); ?>archivos/exportarPDFlistadoEstudiantes" method="POST">
						<input type="hidden" name="censo_id" value="<?= $censo->censo_id ?>">
						<input type="submit" class="btn btn-success float-right" value="Exportar listado">
					</form>
				</div>			
			</div>


			<!-- TABLA DE ESTUDIANTES INSCRITOS EN EL CENSO -->

			<div class="row justify-content-center">
				<?php if($this->Censo_usuario_model->inscritos($censo->censo_id)->num_rows() > 0): ?>
					<div class="col-md-12">
						<div class="card data-tables">
							<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
								<div class="fresh-datatables">

									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

										<thead>
											<tr>
												<th>Cédula de identidad</th>
												<th>Nombres</th>
												<th>Apellidos</th>												
												<th>Carrera</th>
												<th class="disabled-sorting">Constancia</th>
											</tr>
										</thead>

										<tfoot>
											<tr>
												<th>Cédula de identidad</th>
												<th>Nombres</th>
												<th>Apellidos</th>
												<th>Carrera</th>
												<th>Constancia</th>
											</tr>
										</tfoot>

										<tbody>
											<?php foreach($this->Censo_usuario_model->inscritos($censo->censo_id)->result() as $censo_usuario): ?>
												<tr>
													<?php $estudiante = $this->Perfil_model->obtenerPerfil_usuario_id($censo_usuario->estudiante_id); ?>

													<!-- Cédula -->
													<td><?= $estudiante->nacionalidad . '-' . $estudiante->ci ?></td>

													<!-- Nombres -->
													<td><?= $estudiante->primer_nombre . ' ' . $estudiante->segundo_nombre ?></td>

													<!-- Apellidos -->
													<td><?= $estudiante->primer_apellido . ' ' . $estudiante->segundo_apellido ?></td>

													<!-- Carrera -->													
													<td><?php $cantidad_carrera = 0; ?>
														<?php if ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($censo_usuario->estudiante_id)): ?>											
															<?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($censo_usuario->estudiante_id) as $carrera_estudiante): ?>
																<?php $cantidad_carrera = $cantidad_carrera + 1; ?>
																<?php if ($cantidad_carrera > 1): ?>
																	<?php echo ", "; ?>
																<?php endif;?>
																<?= $this->Carrera_model->obtenerCarrera($carrera_estudiante->carrera_id)->nombre ?>
															<?php endforeach;  ?>
														<?php endif; ?>
													</td>

													<!-- Descargar Constancia -->
													<td>
														<form action="<?= base_url() ?>archivos/descargarConstanciasTaller" method="POST">

															<!-- ID del Censo -->
															<input type="hidden" name="censo_id" value="<?= $censo->censo_id ?>">

															<!-- ID del Coordinador -->
															<input type="hidden" name="coordinador_id" value="<?= $censo->coordinador_id ?>">

															<!-- ID del Estudiante -->
															<input type="hidden" name="estudiante_id" value="<?= $estudiante->usuario_id ?>">

															<input type="submit" class="btn btn-info" value="Descargar">
																				
														</form>
													</td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>									
								</div>
							</div>
						</div>
					</div>
				<?php else: ?>
					<div class="alert alert-warning border-radius-10" role="alert">
						<strong>¡Aún no hay estudiantes registrados!</strong>
					</div>
				<?php endif; ?>	
			</div>
		<?php endif; ?>