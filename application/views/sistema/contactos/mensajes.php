	<div class="row justify-content-center">
		<?php if ($mensajes): ?>
			<div class="col-md-12">
				<div class="card data-tables">
					<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
						<div class="fresh-datatables">

						

							<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

								<thead>
									<tr>
										<th class="semi-bold">#</th>
										<th class="semi-bold">Remitente</th>
										<th class="semi-bold">Asunto</th>
										<th class="semi-bold">Estado</th>
										<th class="semi-bold disabled-sorting text-right">Acciones</th>
									</tr>
								</thead>

								<tfoot>
									<tr>
										<th class="semi-bold">#</th>
										<th class="semi-bold">Remitente</th>
										<th class="semi-bold">Asunto</th>
										<th class="semi-bold">Estado</th>
										<th class="semi-bold disabled-sorting text-right">Acciones</th>
									</tr>
								</tfoot>

								<tbody>
									<?php $cant_mensajes = 1; ?>
									<?php foreach ($mensajes as $mensaje): ?>
										<tr>
											<td <?php if ($mensaje->estado != 'Leido') { echo 'class="semi-bold"'; } ?> >
												<?= $cant_mensajes ?>
											</td>
											<td class="parrafo-card <?php if ($mensaje->estado != 'Leido') { echo 'semi-bold"'; } ?>">
												<?php if(strlen($mensaje->correo) > 50):  ?>
													<?= substr($mensaje->correo, 0, 50); ?>...
												<?php else: ?>
													<?= $mensaje->correo ?>
												<?php endif; ?>										
											</td>
											<td <?php if ($mensaje->estado != 'Leido') { echo 'class="semi-bold"'; } ?> >
												<?php if(strlen($mensaje->asunto) > 50):  ?>
													<?= substr($mensaje->asunto, 0, 50); ?>...
												<?php else: ?>
													<?= $mensaje->asunto ?>
												<?php endif; ?>										
											</td>
											<td <?php if ($mensaje->estado != 'Leido') { echo 'class="semi-bold"'; } ?> >
												<?php 
													if ($mensaje->estado != 'Leido') {
														echo '<i class="fa fa-envelope-o"></i>';
													}
													else {
														echo '<i class="fa fa-envelope-open-o"></i>';
													}
												?>
												<?= $mensaje->estado ?>
											</td>
											
											<!-- Acciones -->
											<td class="text-right">
												<div class="dropdown">
												  <a class="btn btn-link dropdown-toggle" href="#" id="dropdownMenuLink-<?= $mensaje->contacto_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-gears"></i>
												  </a>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?= $mensaje->contacto_id ?>">
												  	<h6 class="dropdown-header">Acciones</h6>

												  	<a href="#verMensajeModal" class="dropdown-item width_130 border-radius-10 btn-modal"  data-toggle="modal" data-mensaje="<?= $mensaje->contacto_id ?>" data-target="#verMensajeModal">
															<i class="fa fa-edit"></i> Ver mensaje
														</a>

														<?php if ($mensaje->estado != 'Leido'): ?>
															<a href="<?= base_url()?>contactos/leido/<?=$mensaje->contacto_id ?>" class="dropdown-item width_130 border-radius-10">
																<i class="fa fa-envelope-open-o"></i> Marcar leido
															</a>
														<?php else: ?>
															<a href="<?= base_url()?>contactos/pendiente/<?=$mensaje->contacto_id ?>" class="dropdown-item width_130 border-radius-10">
																<i class="fa fa-envelope-o"></i> Marcar no leido
															</a>
														<?php endif; ?>

														<a href="<?= base_url()?>contactos/eliminar/<?=$mensaje->contacto_id ?>" class="dropdown-item width_130 border-radius-10">
															<i class="fa fa-trash"></i> Eliminar mensaje
														</a>
					                </div>
					              </div>
											</td>
										</tr>
										<?php $cant_mensajes += 1; ?>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		<?php else: ?>
			<div class="col-4">
				<div class="alert alert-success border-radius-10" role="alert">
					<i class="fa fa-check-square-o"></i> <strong>¡No hay mensajes registrados!</strong>
				</div>
			</div>				
		<?php endif; ?>
	</div>

<!-- Modal para ver mensaje -->
<div class="modal fade" id="verMensajeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title semi-bold" id="exampleModalLabel">
        </h5>
        <a href="" role="button" class="close" aria-label="Close"><i class="fa fa-times"></i></a>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
      	<a href="" role="button" class="close btn btn-secondary">Cerrar</a>
      </div>
    </div>
  </div>
</div>


<script>
	$('.btn-modal').click(function () {
		var id = $(this).data('mensaje');
		var ruta = "<?= base_url() ?>contactos/leido/" + id;

		$('#verMensajeModal .modal-title, #verMensajeModal .modal-body').empty();

		$.ajax({
            type:"post",
            url: "<?php echo base_url(); ?>contactos/obtener",
            data:{id: id},
            success:function(response)
            {
            	data = JSON.parse(response);
            	if (data) {
            		var titulo = data.asunto + "<br> <small>" + data.correo + "</small>";

            		$('#verMensajeModal .modal-title').html(titulo);
            		$('#verMensajeModal .modal-body').html(data.descripcion);
            		$('#verMensajeModal .close').attr('href', ruta);
            		console.log(data);
            	}            	
            },
            error: function() 
            {
              console.log('Error');
            }
        	});
	});
</script>