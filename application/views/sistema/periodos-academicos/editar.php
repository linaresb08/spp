<?= validation_errors() ?>

		<?= form_open("/periodos/editar/$id") ?>

		<?php

				$nombre = array(
					'name' => 'nombre', 
					'placeholder' => 'Escribe el nuevo período académico',
					'class' => 'form-control',
					'value' => $periodo->nombre,
					'required' => 'true'
				);

				$fecha_inicio = array(
					'name' => 'fecha_inicio',
					'placeholder' => 'Selecciona la fecha inicial del período académico',
					'type' => 'date',
					'value' => $periodo->fecha_inicio,
					'class' => 'form-control'
				);

				$fecha_finalizacion = array(
					'name' => 'fecha_finalizacion',
					'placeholder' => 'Selecciona la fecha final del período académico',
					'type' => 'date',
					'value' => $periodo->fecha_finalizacion,
					'class' => 'form-control'
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-md-12 col-xl-4 margin-bottom-sm">
					<span>Período académico: </span>
					<?= form_input($nombre) ?>
				</div>
				
				<div class="col-md-6 col-xl-4 margin-bottom-sm">
					<span>Fecha de inicio: </span>
					<?= form_input($fecha_inicio) ?>	
				</div>

				<div class="col-md-6 col-xl-4 margin-bottom-sm">
					<span>Fecha de finalización: </span>
					<?= form_input($fecha_finalizacion) ?>	
				</div>
				
			</div>

			<div class="row">

				<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>
				
			</div>

		<?= form_close() ?>