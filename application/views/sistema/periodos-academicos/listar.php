<!-- Todas los períodos académicos -->

		<div class="row">

			<div class="col-12 semi-bold section-title text-center">
				Períodos Académicos
			</div>

			<div class="col-12">
				<a class="btn btn-primary float-right" role="button" href="<?= base_url()?>periodos/nuevo">
					<i class="fa fa-plus"></i> Nuevo período
				</a>
			</div>
		</div>

<!-- TABLA DE PERÍODOS ACADÉMICOS -->

		<div class="row">
			<?php if (!$segmento): ?>

				<div class="col-md-12">
					<div class="card data-tables">
						<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
							<div class="fresh-datatables">

								<?php if ($periodos): ?>

									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

										<thead>
											<tr>												
												<th>Período</th>
												<th>Fecha de inicio</th>
												<th>Fecha de finalización</th>
												<th class="disabled-sorting text-right">Acciones</th>
											</tr>
										</thead>

										<tfoot>
											<tr>												
												<th>Período</th>
												<th>Fecha de inicio</th>
												<th>Fecha de finalización</th>
												<th class="text-right">Acciones</th>
											</tr>
										</tfoot>

										<tbody>
											<?php foreach ($periodos as $periodo): ?>
												<tr>
													<!-- Nombre del período -->
													<td><?= $periodo->nombre ?></td>

													<!-- Fecha de inicio -->
													<td>
														<?php $fecha_inicio = new DateTime($periodo->fecha_inicio); ?>
														<?= $fecha_inicio->format('d-m-Y'); ?>
													</td>

													<!-- Fecha de finalización -->
													<td>
														<?php $fecha_finalizacion = new DateTime($periodo->fecha_finalizacion); ?>
														<?= $fecha_finalizacion->format('d-m-Y'); ?>
													</td>

													<!-- Acciones -->
													<td class="text-right">	
														<div class="dropdown">
															<a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink-<?= $periodo->periodo_academico_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="fa fa-gears"></i>
															</a>

															<div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?=$periodo->periodo_academico_id ?>">
																<h6 class="dropdown-header">Acciones</h6>
																
															<!--
																<a href="<?= base_url()?>periodos/index/<?= $periodo->periodo_academico_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-tag"></i> Ver
																</a>
															-->

																<a href="<?= base_url()?>periodos/editar/<?=$periodo->periodo_academico_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-edit"></i> Modificar
																</a>

																<a href="<?= base_url()?>periodos/eliminar/<?=$periodo->periodo_academico_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-times"></i> Eliminar
																</a>
															</div>
														</div>	
													</td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
								<?php else: ?>
									<div class="alert alert-warning border-radius-10 section-message semi-bold text-center" role="alert">
										<i class="fa fa-warning"></i>
										¡No hay períodos académicos registrados!
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				
			<!-- Un solo período académico -->
			<?php else: ?>
				<div class="col-12 text-center">
					<h3>Datos del período académico</h3>
					<p>
						#: <?= $periodo->periodo_academico_id ?> <br>
						Nombre: <?= $periodo->nombre ?> <br>

						<?php $fecha_inicio = new DateTime($periodo->fecha_inicio); ?>
						Fecha de incio: <?= $fecha_inicio->format('d-m-Y'); ?> <br>
						
						<?php $fecha_finalizacion = new DateTime($periodo->fecha_finalizacion); ?>
						Fecha de finalzación:	<?= $fecha_finalizacion->format('d-m-Y'); ?>
					</p>
				</div>
			<?php endif; ?>	
		</div>