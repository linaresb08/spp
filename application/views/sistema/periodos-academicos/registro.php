
		<?= validation_errors() ?>

		<?= form_open("/periodos/nuevo") ?>
			<?php

				$nombre = array(
					'name' => 'nombre', 
					'placeholder' => 'Escribe el nuevo período académico',
					'class' => 'form-control',
					'required' => 'true'
				);

				$fecha_inicio = array(
					'name' => 'fecha_inicio',
					'placeholder' => 'Selecciona la fecha inicial del período académico',
					'type' => 'date',
					'class' => 'form-control'
				);

				$fecha_finalizacion = array(
					'name' => 'fecha_finalizacion',
					'placeholder' => 'Selecciona la fecha final del período académico',
					'type' => 'date',
					'class' => 'form-control'
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-md-12 col-xl-4 margin-bottom-sm">
					<span>Período académico: </span>
					<?= form_input($nombre) ?>
				</div>
				
				<div class="col-md-6 col-xl-4 margin-bottom-sm">
					<span>Fecha de inicio: </span>
					<?= form_input($fecha_inicio) ?>	
				</div>

				<div class="col-md-6 col-xl-4 margin-bottom-sm">
					<span>Fecha de finalización: </span>
					<?= form_input($fecha_finalizacion) ?>	
				</div>
				
			</div>

			<div class="row">

				<div class="col-5 col-md-6 col-xl-5 text-right"><?= form_submit('','Registrar',['class'=>'btn btn-success']) ?></div>
				<div class="col-5  col-md-6 col-xl-5 text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-danger']) ?></div>
									
			</div>

		<?= form_close() ?>