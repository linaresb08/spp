			<!-- Listado de los requisitos del estudiante -->

			<div id="accordion">
				<h3 class="semi-bold text-center">
					Requisitos de <?= $this->perfil_session->primer_nombre ?> <?= $this->perfil_session->primer_apellido?>
				</h3>

				<?php if ($requisitos_estudiante): ?>
					<?php foreach ($requisitos_estudiante as $requisito_estudiante): ?>
						<?php $requisito = $this->Requisito_model->obtenerRequisito($requisito_estudiante->requisito_id); ?>

						<!-- Estatus -->
		      	<?php if($requisito_estudiante->estatus == 'Adjuntado'): ?>
							<?php $color = 'bg-gris' ?>
						<?php elseif ($requisito_estudiante->estatus == 'Corregir'): ?>
							<?php $color = 'bg-amarillo-l' ?>
						<?php elseif ($requisito_estudiante->estatus == 'Verificado'): ?>
							<?php $color = 'bg-verde-l' ?>
						<?php else: ?>
							<?php $color = 'bg-light' ?>
						<?php endif; ?>

					  <div class="card no-margin">
					    <div class="card-header cursor-pointer <?= $color ?>" id="requisito-<?= $requisito_estudiante->requisito_id ?>"  data-toggle="collapse" data-target="#collapse-<?= $requisito_estudiante->requisito_id ?>" aria-controls="collapse-<?= $requisito_estudiante->requisito_id ?>">
					      <h5 class="mb-0">
					       	<!-- Requisito -->
					        <?= $requisito->nombre ?>
					      
					        <p class="float-right"><?= $requisito_estudiante->estatus ?></p>
					      </h5>
					    </div>

					    <div id="collapse-<?= $requisito_estudiante->requisito_id ?>" class="collapse" aria-labelledby="requisito-<?= $requisito_estudiante->requisito_id ?>" data-parent="#accordion">
					      <div class="card-body">
					      	
					      	<!-- Descripción -->
					      	<p>
					      		<?= $requisito->descripcion ?>
					      	</p>

									<form action="<?= base_url() ?>requisitos/actualizarRequisito" method="POST">
										<div class="row justify-content-center">
											<div class="col-12 col-sm-6 col-md-4 margin-bottom-sm">
												<input type="hidden" name="id" value="<?= $requisito_estudiante->estudiante_requisito_id ?>">
												<input type="url" name="archivo" value="<?= $requisito_estudiante->archivo ?>" placeholder="Introduce el enlace del archivo" class="form-control">
											</div>

											<div class="col-5 col-sm-3 col-md-2 text-center margin-bottom-sm">
												<input type="submit" value="Actualizar" class="btn btn-success">												
											</div>

											<?php if ($requisito_estudiante->archivo): ?>
												<div class="col-5 col-sm-3 col-md-2 text-center margin-bottom-sm">
													<a href="<?= $requisito_estudiante->archivo ?>" target="_blank" class="btn btn-info" role="button">Ver archivo</a>
												</div>
											<?php endif; ?>	

										</div>
									</form>									

									<?php if ($requisito_estudiante->observacion): ?>
										<!-- Observación -->
										<div class="alert alert-warning border-radius-10 text-center" role="alert">
										  <p> 
										  	<i class="fa fa-warning"></i>
										  	<?= $requisito_estudiante->observacion ?>
										  </p>
										</div>										
									<?php endif; ?>

					      </div>
					    </div>
					  </div>
					<?php endforeach; ?>
			  <?php else: ?>
			  	<div id="mensaje" class="alert alert-warning border-radius-10 text-center section-description" role="alert">
			  		<i class="fa fa-warning"></i> <br>
						No hay requisitos registrados!
					</div>
				<?php endif; ?>
			</div>