<!-- Todos los requisitos -->

		<div class="row">
			<div class="col-12 semi-bold section-title text-center">
				Requisitos
			</div>
			<div class="col-12">
				<a class="btn btn-primary float-right" role="button" href="<?= base_url()?>requisitos/nuevo">
					<i class="fa fa-plus"></i> Nuevo requisito
				</a>
			</div>
		</div>

<!-- TABLA DE REQUISITOS -->

		<div class="row">
			<?php if (!$segmento): ?>

				<div class="col-md-12">
					<div class="card data-tables">
						<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
							<div class="fresh-datatables">

								<?php if ($requisitos): ?>

									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

										<thead>
											<tr>
												<th>Requisito</th>
												<th>Descripción</th>
												<th class="disabled-sorting text-right">Acciones</th>
											</tr>
										</thead>

										<tfoot>
											<tr>
												<th>Requisito</th>
												<th>Descripción</th>
												<th class="text-right">Acciones</th>
											</tr>
										</tfoot>

										<tbody>
											<?php foreach ($requisitos as $requisito): ?>
												<tr>
													<td class="parrafo-card">
														<?php if(strlen($requisito->nombre) > 28):  ?>
															<?= substr($requisito->nombre, 0, 28); ?>...
														<?php else: ?>
															<?= $requisito->nombre ?>
														<?php endif; ?>										
													</td>
													<td class="parrafo-card extra-light">
														<?php if(strlen($requisito->descripcion) > 50):  ?>
															<?= substr($requisito->descripcion, 0, 50); ?>...
														<?php else: ?>
															<?= $requisito->descripcion ?>
														<?php endif; ?>
													</td>

													<!-- Acciones -->
													<td class="text-right">
														<div class="dropdown">
														  <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink-<?= $requisito->requisito_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														    <i class="fa fa-gears"></i>
														  </a>

														  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?= $requisito->requisito_id ?>">
														  	<h6 class="dropdown-header">Acciones</h6>

														  	<a href="<?= base_url()?>requisitos/editar/<?= $requisito->requisito_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-edit"></i> Modificar requisito
																</a>

																<a href="<?= base_url()?>requisitos/eliminar/<?=$requisito->requisito_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-trash"></i> Eliminar requisito
																</a>
							                </div>
							              </div>
													</td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
								<?php else: ?>
									<div class="alert alert-warning border-radius-10" role="alert">
										¡No hay requisitos registrados!
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>	
		</div>
		