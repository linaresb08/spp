		<?= validation_errors() ?>

		<?= form_open("/requisitos/nuevo") ?>
			<?php 
				$nombre = array(
					'name'        => 'nombre', 
					'placeholder' => 'Escribe el nombre del requisito',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$descripcion = array(
					'name'        => 'descripcion',
					'placeholder' => 'Escribe una breve descripcion del requisito',
					'class'       => 'form-control',
					'rows'        => 3
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-12 col-md-11 col-xl-10 margin-bottom-sm">
					<span>Nombre: </span>
					<?= form_input($nombre) ?>
				</div>
				<div class="col-12 col-md-11 col-xl-10 margin-bottom-sm">
					<span>Descripción: </span>
					<?= form_textarea($descripcion) ?>
				</div>
	
			</div>

			<div class="row">

				<div class="col text-right"><?= form_submit('','Crear',['class'=>'btn btn-success']) ?></div>
				<div class="col text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>
				
			</div>
		<?= form_close() ?>