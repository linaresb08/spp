		<?= validation_errors() ?>

		<?= form_open("/requisitos/editar/$id") ?>
			<?php  
				$nombre = array(
					'name'  => 'nombre', 
					'value' => $requisito->nombre,
					'class' => 'form-control',
				);

				$descripcion = array(
					'name'  => 'descripcion', 
					'value' => $requisito->descripcion,
					'class' => 'form-control',
					'rows'  => 3
				);
			?>
			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-12 col-md-11 col-xl-10 margin-bottom-sm">
					<span>Nombre: </span>
					<?= form_input($nombre) ?>
				</div>
				<div class="col-12 col-md-11 col-xl-10 margin-bottom-sm">
					<span>Descripción: </span>
					<?= form_textarea($descripcion) ?>
				</div>

			</div>

			<div class="row">

				<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>
				
			</div>

		<?= form_close() ?>