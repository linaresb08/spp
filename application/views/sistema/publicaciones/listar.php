<!-- Todas las publicaciones -->
		<?php if (!$segmento): ?>
			<div class="row justify-content-center">
				<div class="col-12 semi-bold section-title text-center">
					Publicaciones
				</div>

				<div class="dropdown col-12 col-lg-12 col-xl-10 margin_sm">
				  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuNuevo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-plus"></i> Nuevo
				  </button>
				  <div class="dropdown-menu" aria-labelledby="dropdownMenuNuevo">
				    <a class="dropdown-item" href="<?= base_url()?>publicaciones/nueva">
				    	<i class="fa fa-newspaper-o"></i> Nueva publicación
				    </a>
				    <a class="dropdown-item" href="<?= base_url()?>publicaciones/nuevoDocumento">
				    	<i class="fa fa-file-word-o"></i> Nuevo documento
				    </a>
				  </div>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-md-12 col-lg-12 col-xl-10">
					<div class="card data-tables">
						<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
							<div class="fresh-datatables">

								<?php if ($publicaciones): ?>

									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

										<thead>
											<tr>
												<th>Fecha</th>
												<th>Título</th>
												<th>Categoría</th>
												<th>Autor</th>												
												<th class="disabled-sorting text-right">Acciones</th>
											</tr>
										</thead>

										<tfoot>
											<tr>												
												<th>Fecha</th>
												<th>Título</th>
												<th>Categoría</th>
												<th>Autor</th>
												<th class="text-right">Acciones</th>
											</tr>
										</tfoot>

										<tbody>
											<?php foreach ($publicaciones as $publicacion): ?>
												<tr>
													<td class="parrafo-card">
														<?php $fecha = new DateTime($publicacion->fecha); ?>
														<?= $fecha->format('d-m-Y'); ?>	
													</td>
													<td class="parrafo-card">
														<?php if(strlen($publicacion->titulo) > 20):  ?>
															<?= substr($publicacion->titulo, 0, 20); ?>...
														<?php else: ?>
															<?= $publicacion->titulo ?>
														<?php endif; ?>		
													</td>
													<td class="parrafo-card">
														<?php foreach($this->Categoria_publicacion_model->obtenerCategorias_por_Publicacion($publicacion->publicacion_id) as $categoria): ?>
																<?php 
																	$categoria_nombre = $this->Categoria_model->obtenerCategoria($categoria->categoria_id)->nombre;
																	$categoria_id = $categoria->categoria_id; 
																?>
																<?= $categoria_nombre ?>
															<?php endforeach; ?>
													</td>
													<td class="parrafo-card">
														<?php 
															$autor = $this->Perfil_model->obtenerPerfil_usuario_id($publicacion->usuario_id);
														?>
														<?= $autor->primer_nombre ?> <?= $autor->primer_apellido?>						
													</td>
													<td class="text-right">
														<div class="dropdown">
														  <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink-<?=$publicacion->publicacion_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														    <i class="fa fa-gears"></i>
														  </a>

														  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?=$publicacion->publicacion_id ?>">
														  	<h6 class="dropdown-header">Acciones</h6>
																
																<!-- Si la categoría es diferente de 3 (Documentos) -->
							                	<?php if ($categoria_id != 3): ?>
							                		<a href="<?= base_url()?>publicaciones/index/<?=$publicacion->publicacion_id ?>" class="dropdown-item width_130 border-radius-10">
																		<i class="fa fa-newspaper-o"></i> Ver publicación
								                	</a>

							                		<a href="<?= base_url()?>publicaciones/editar/<?=$publicacion->publicacion_id ?>" class="dropdown-item width_130 border-radius-10">
																		<i class="fa fa-edit"></i> Editar publicación
																	</a>

																<?php else: ?>
																	<a href="<?= base_url()?>publicaciones/index/<?=$publicacion->publicacion_id ?>" class="dropdown-item width_130 border-radius-10">
																		<i class="fa fa-file-word-o"></i> Ver documento
								                	</a>

																	<a href="<?= base_url()?>publicaciones/editarDocumento/<?=$publicacion->publicacion_id ?>" class="dropdown-item width_130 border-radius-10">
																		<i class="fa fa-edit"></i> Editar documento
																	</a>
							                	<?php endif; ?>
							                	

																<a href="<?= base_url()?>publicaciones/eliminar/<?=$publicacion->publicacion_id ?>"  class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-trash"></i> Eliminar
																</a>
															</div>
														</div>
													</td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
								<?php else: ?>
									<div class="alert alert-warning border-radius-10" role="alert">
										¡No hay publicaciones registradas!
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Una sola publicación -->
		<?php else: ?>
			<div class="row">
				<div class="col-12 text-center">
					<?php if ($publicacion->foto): ?>
						<img src="<?= $publicacion->foto ?>" class="img-fluid rounded" alt="Imagen alusiva a la publicación N° <?= $publicacion->publicacion_id ?> - <?= $publicacion->titulo ?>" width="90%" height="10px">
					<?php endif; ?>
				</div>

				<div class="col-12 semi-bold section-title text-center">
					<?= $publicacion->titulo ?>
				</div>
				<div class="col-12 text-center">
					<?php $fecha = new DateTime($publicacion->fecha); ?>
					Publicado el <?= $fecha->format('d-m-Y'); ?>, 
					<?php $autor = $this->Perfil_model->obtenerPerfil_usuario_id($publicacion->usuario_id); ?>					
					por <?= $autor->primer_nombre ?> <?= $autor->primer_apellido ?>
				</div>
				<div class="col-12">
					<p class="section-description text-justify"> <?= $publicacion->descripcion ?> </p>
				</div>

				<div class="col-12">

					<?php if($publicacion->archivo): ?>
						Archivo: 
						<a href="<?= $publicacion->archivo ?>" target="_blank"><?= $publicacion->archivo ?></a>
						 <br>
					<?php endif; ?>

					<?php if($publicacion->tipo): ?>
						Tipo: 
						<?php 
							if ($publicacion->tipo == 'Alumno') {	
								echo "Carpeta del " . $publicacion->tipo;	
							}
							else {
								echo "Sobre del " . $publicacion->tipo;	
							}
						?>
						<br>
					<?php endif; ?>

				</div>

				<div class="col-12">
					<strong>Categoria: </strong> 
					<?php foreach($this->Categoria_publicacion_model->obtenerCategorias_por_Publicacion($publicacion->publicacion_id) as $categoria): ?>
						<a class="btn" style="color: white; background-color: <?= $this->Categoria_model->obtenerCategoria($categoria->categoria_id)->color ?>" href="<?= base_url() ?>portal/categoria/<?= $this->Categoria_model->obtenerCategoria($categoria->categoria_id)->categoria_id ?>" target="_blank" role="button">
							<i class="fa fa-tag"></i> <?= $this->Categoria_model->obtenerCategoria($categoria->categoria_id)->nombre ?>
						</a>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>