
		<?= validation_errors() ?>

		<?= form_open("/publicaciones/nueva") ?>
			<?php
				$titulo = array(
					'name' => 'titulo',
					'placeholder' => 'Título de la publicación',
					'class' => 'form-control',
					'required' => 'true'
				);

				$descripcion = array(
					'name' => 'descripcion',
					'placeholder' => 'Escribe la descripción de la publicación',
					'class' => 'form-control',
					'required' => 'true'
				);

				$foto = array(
					'name' => 'foto',
					'placeholder' => 'Inserte el link de la imagen',
					'class' => 'form-control'
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-12 col-md-6 col-xl-5 margin_sm_t">
					<span>Título: </span>
					<?= form_input($titulo) ?>					
				</div>

				<div class="col-12 col-md-6 col-xl-5 margin_sm_t">
					<span>Foto: </span>
					<?= form_input($foto) ?>					
				</div>

				<div class="col-12 col-md-12 col-xl-10 margin_sm_t">
					<span>Descripción: </span>
					<?= form_textarea($descripcion) ?>					
				</div>

				<div class="col-12 col-xl-11 margin_sm_t text-center">
					<span>Seleccione la categoría de la publicación:</span>
				</div>	

				
				<?php foreach ($this->Categoria_model->obtenerCategorias() as $categoria): ?>
					<?php if($categoria->categoria_id != 3): ?>
						<div class="col-6 col-md-2 margin_sm_t">
							<div class="form-check form-check-radio">
								<label class="form-check-label" for="categoria_<?= $categoria->categoria_id ?>">
									<input type="radio" name="categorias[]" value="<?= $categoria->categoria_id ?>" id="categoria_<?= $categoria->categoria_id ?>" class="form-check-input">
									<span class="form-check-sign"></span>
									<?= $categoria->nombre ?>
								</label>							
							</div>
						</div>
					<?php endif;?>				
				<?php endforeach; ?>
				

			</div>

			<div class="row">

				<div class="col text-right"><?= form_submit('','Publicar',['class'=>'btn btn-success']) ?></div>
				<div class="col text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>
				
			</div>

		<?= form_close() ?>