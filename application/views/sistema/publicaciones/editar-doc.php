		<?= validation_errors() ?>

		<?= form_open("/publicaciones/editarDocumento/$id") ?>
		<?php
				$titulo = array(
					'name' => 'titulo', 
					'value' => $publicacion->titulo,
					'class' => 'form-control'
				);

				$archivo = array(
					'name' => 'archivo',
					'value' => $publicacion->archivo,
					'type' => 'url',
					'class' => 'form-control'
				);

				$fecha = array(
					'name' => 'fecha',
					'type' => 'date',
					'value' => $publicacion->fecha,
					'class' => 'form-control datepicker'
				);
			?>


			<div class="row justify-content-center">

				<div class="col-12 col-md-6 col-xl-5">
					<span>Título: </span>
					<?= form_input($titulo) ?>
					<br>
				</div>

				<div class="col-12 col-md-6 col-xl-5">
					<span>Archivo: </span>
					<?= form_input($archivo) ?>
					<br>
				</div>

			</div>

			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-12 col-md-6 col-xl-5">
					<span>Fecha de publicación: </span>
					<div class="form-group">
						<?= form_input($fecha) ?>
					</div>
				</div>

				<div class="col-12 col-md-6 col-xl-5">
					<span>Tipo de archivo:</span>
					<select name="tipo" id="" class="form-control">
						<option value="Alumno" <?php if ($publicacion->tipo == 'Alumno') { echo "selected"; } ?> >Carpeta del alumno</option>
						<option value="Tutor académico" <?php if ($publicacion->tipo == 'Tutor académico') { echo "selected"; } ?> >Sobre del tutor académico</option>
						<option value="Tutor laboral" <?php if ($publicacion->tipo == 'Tutor laboral') { echo "selected"; } ?> >Sobre del tutor laboral</option>
					</select>
				</div>
			</div>

			<div class="row">

				<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>
				
			</div>

		<?= form_close() ?>