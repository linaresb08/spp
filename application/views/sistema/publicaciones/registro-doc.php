
		<?= validation_errors() ?>

		<?= form_open("/publicaciones/nuevoDocumento") ?>
			<?php
				$titulo = array(
					'name' => 'titulo', 
					'placeholder' => 'Título de la publicación',
					'class' => 'form-control',
					'required' => 'true'
				);

				$archivo = array(
					'name' => 'archivo',
					'placeholder' => 'Inserte el link / url del archivo',
					'type' => 'url',
					'class' => 'form-control',
					'required' => 'true'
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-12 col-md-6 col-xl-5 margin_sm_t">
					<span>Título: </span>
					<?= form_input($titulo) ?>					
				</div>

				<div class="col-12 col-md-6 col-xl-5 margin_sm_t">
					<span>Archivo: </span>
					<?= form_input($archivo) ?>					
				</div>
			</div>

			<div class="row justify-content-center margin-bottom-sm">
				<div class="col-12 col-md-3 col-xl-2">
					<span>Tipo de archivo:</span>
				</div>

				<div class="col-12 col-md-9 col-xl-3 margin-bottom-sm">
					<select name="tipo" id="" class="form-control" required="true">
						<option value="">Elige un tipo de carpeta</option>
						<option value="Alumno">Carpeta del alumno</option>
						<option value="Tutor académico">Sobre del tutor académico</option>
						<option value="Tutor laboral">Sobre del tutor laboral</option>
					</select>
				</div>

			</div>

			<div class="row">

				<div class="col text-right"><?= form_submit('','Registrar',['class'=>'btn btn-success']) ?></div>
				<div class="col text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>
				
			</div>

		<?= form_close() ?>