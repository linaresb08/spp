<!DOCTYPE html>
<html lang="es">
	<head>

		<!-- Título de la ventana -->
	<title>Inicio de sesión | Sistema de Prácticas Profesionales</title>
	
	<!-- Favicon -->
	<link href='<?= base_url() ?>assets/img/portal/logo.png' rel='shortcut icon' type='image/png'/>

		<!-- Meta etiquetas -->
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

		<!-- Programadora -->
	<meta name="author" content="Betzabeth Linares">

		<!-- Descripción -->
	<meta name="description" content="Sistema web para la Coordinación de Prácticas Profesionales de la Universidad Valle del Momboy">

		<!-- Palabras claves -->
	<meta name="keywords" content="uvm, practicas profesionales, universidad valle del momboy, pasantias, sistema de practicas profesionales">


	<!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="Sistema Web de Prácticas Profesionales UVM">  
	<meta itemprop="image" content="http://uvm.edu.ve/img/logo.png">

	<!-- Twitter Card data 
		<meta name="twitter:card" content="product">
		<meta name="twitter:site" content="@creativetim">
		<meta name="twitter:title" content="Light Bootstrap Dashboard PRO by Creative Tim">
		<meta name="twitter:description" content="Forget about boring dashboards, get a bootstrap 4 admin template designed to be simple and beautiful.">
		<meta name="twitter:creator" content="@creativetim">
		<meta name="twitter:image" content="http://s3.amazonaws.com/creativetim_bucket/products/34/original/opt_lbd_pro_thumbnail.jpg">
		<meta name="twitter:data1" content="Light Bootstrap Dashboard PRO by Creative Tim">
		<meta name="twitter:label1" content="Product Type">
		<meta name="twitter:data2" content="$39">
		<meta name="twitter:label2" content="Price">
	-->

	<!-- Open Graph data 
		<meta property="og:title" content="Light Bootstrap Dashboard PRO by Creative Tim" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="http://demos.creative-tim.com/light-bootstrap-dashboard-pro/examples/dashboard.html" />
		<meta property="og:image" content="http://s3.amazonaws.com/creativetim_bucket/products/34/original/opt_lbd_pro_thumbnail.jpg" />
		<meta property="og:description" content="Forget about boring dashboards, get a bootstrap 4 admin template designed to be simple and beautiful." />
		<meta property="og:site_name" content="Creative Tim" />
	-->

	<!-- Links -->

	<!-- Canonical SEO -->
	<link rel="canonical" href="<?= base_url() ?>" />

	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>assets/img/apple-icon.png">
	
	<link rel="icon" type="image/png" href="<?= base_url() ?>assets/img/favicon.png">   


	<!--     Fonts and icons     -->
	<link href="https://fonts.googleapis.com/css?family=Dosis:200,400,600" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
	<link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?= base_url() ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/mystyles.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/containers.css">
	</head>

	<body>
		<div class="wrapper wrapper-full-page">
		 
		  <div class="full-page  section-image" data-color="black" data-image="<?= base_url() ?>assets/img/full-screen-image-2.jpg">
			<!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
				<div class="content">
				  <div class="container">
						<div class="col-lg-6 col-md-6 col-sm-6 ml-auto mr-auto">
						  <form class="form" method="POST" action="<?= base_url() ?>dashboard/iniciar_sesion">
								<div class="card card-login card-hidden">
								  <div class="card-header ">
								  	<div class="text-center">
											<img class="margin-auto" src="http://localhost/spp/assets/img/portal/logo.png" width="100" height="100">
										</div>
									<!--
										<h3 class="header text-center text-title">Login</h3>
									-->
								  </div>
								  <div class="card-body">
										<div class="card-body">
										  <div class="form-group">
												<label>Usuario</label>
												<input type="text" placeholder="Usuario" class="form-control" name="nombre_usuario">
										  </div>
										  <div class="form-group">
												<label>Clave</label>
												<input type="password" placeholder="Clave" class="form-control" name="clave">
										  </div>                             
										</div>
								  </div>
								  <div class="card-footer ml-auto mr-auto text-center">
										<button type="submit" class="btn btn-success btn-wd margin-l-r-5">
										  Iniciar sesión
										</button>
										<button type="reset" class="btn btn-warning  margin-l-r-5">
											Limpiar datos
										</button>
								  </div>
								  <div class="dropdown-divider"></div>

								  <!-- FALTA CREAR VISTA DE REGISTRO DE ESTUDIANTE -->
						  	 	<div class="card-body text-center">
					 					¿Aún no te has registrado?  
					 					<a href="<?= base_url() ?>dashboard/registrar" class="btn btn-info margin-l-r-10">¡Regístrate ya!</a>
						  	 	</div>
						  	 	
								</div>
						  </form>
						</div>
					</div>
				</div>
		  </div>
		</div>
	</body>
<!--   Core JS Files   -->
<script src="<?= base_url() ?>assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
<!--  Chartist Plugin  -->
<script src="<?= base_url() ?>assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-notify.js"></script>
<!--  Share Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.sharrre.js"></script>
<!--  jVector Map  -->
<script src="<?= base_url() ?>assets/js/plugins/jquery-jvectormap.js" type="text/javascript"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?= base_url() ?>assets/js/plugins/moment.min.js"></script>
<!--  DatetimePicker   -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-datetimepicker.js"></script>
<!--  Sweet Alert  -->
<script src="<?= base_url() ?>assets/js/plugins/sweetalert2.min.js" type="text/javascript"></script>
<!--  Tags Input  -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-tagsinput.js" type="text/javascript"></script>
<!--  Sliders  -->
<script src="<?= base_url() ?>assets/js/plugins/nouislider.js" type="text/javascript"></script>
<!--  Bootstrap Select  -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  jQueryValidate  -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.validate.min.js" type="text/javascript"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--  Bootstrap Table Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-table.js"></script>
<!--  DataTable Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<!--  Full Calendar   -->
<script src="<?= base_url() ?>assets/js/plugins/fullcalendar.min.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!-- Light Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= base_url() ?>assets/js/demo.js"></script>
<script>
	$(document).ready(function() {
		demo.checkFullPageBackgroundImage();

		setTimeout(function() {
			// after 1000 ms we add the class animated to the login/register card
			$('.card').removeClass('card-hidden');
		}, 700)
	});
</script>
<!-- Facebook Pixel Code Don't Delete -->
<script>
	! function(f, b, e, v, n, t, s) {
		if (f.fbq) return;
		n = f.fbq = function() {
			n.callMethod ?
				n.callMethod.apply(n, arguments) : n.queue.push(arguments)
		};
		if (!f._fbq) f._fbq = n;
		n.push = n;
		n.loaded = !0;
		n.version = '2.0';
		n.queue = [];
		t = b.createElement(e);
		t.async = !0;
		t.src = v;
		s = b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t, s)
	}(window,
		document, 'script', '//connect.facebook.net/en_US/fbevents.js');

	try {
		fbq('init', '111649226022273');
		fbq('track', "PageView");

	} catch (err) {
		console.log('Facebook Track Error:', err);
	}
</script>
<noscript>
	<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=111649226022273&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->

</html>