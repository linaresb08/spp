		<?= validation_errors() ?>

		<?= form_open("/supervisiones/nueva/$pasantia_id") ?>
			<?php 
				$fecha = array(
					'name'     => 'fecha',
					'type'     => 'date',
					'class'    => 'form-control',
					'required' => 'true'
				);

				$hora = array(
					'name'     => 'hora',
					'type'     => 'time',
					'class'    => 'form-control',
					'required' => 'true'
				);

				$actividades_realizadas = array(
					'name'     => 'actividades_realizadas', 
					'class'    => 'form-control',
					'rows'     => '4',
					'required' => 'true'
				);

				$personales_aspectos_positivos = array(
					'name'     => 'personales_aspectos_positivos', 
					'class'    => 'form-control',
					'rows'     => '3',
					'required' => 'true'
				);

				$personales_puntos_mejoras = array(
					'name'  => 'personales_puntos_mejoras', 
					'class' => 'form-control',
					'rows'  => '3'
				);

				$habilidades_aspectos_positivos = array(
					'name'     => 'habilidades_aspectos_positivos', 
					'class'    => 'form-control',
					'rows'     => '3',
					'required' => 'true'
				);

				$habilidades_puntos_mejoras = array(
					'name'  => 'habilidades_puntos_mejoras', 
					'class' => 'form-control',
					'rows'  => '3'
				);

				$calidad_aspectos_positivos = array(
					'name'     => 'calidad_aspectos_positivos', 
					'class'    => 'form-control',
					'rows'     => '3',
					'required' => 'true'
				);

				$calidad_puntos_mejoras = array(
					'name'  => 'calidad_puntos_mejoras', 
					'class' => 'form-control',
					'rows'  => '3'
				);

				$rasgos_aspectos_positivos = array(
					'name'     => 'rasgos_aspectos_positivos', 
					'class'    => 'form-control',
					'rows'     => '3',
					'required' => 'true'
				);

				$rasgos_puntos_mejoras = array(
					'name'  => 'rasgos_puntos_mejoras', 
					'class' => 'form-control',
					'rows'  => '3'
				);

				$observaciones = array(
					'name'  => 'observaciones', 
					'class' => 'form-control',
					'rows'  => '3'
				);				
			?>

			<input type="hidden" value="<?= $pasantia_id ?>" name="pasantia_id">

			<!-- Datos del estudiante -->
			<div class="row justify-content-center">
				<div class="col semi-bold text-center text-size-11 margin_b">
					DATOS DEL ESTUDIANTE
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12 col-md-11">
					<table class="table text-center">
					  <tbody>
					  	<tr>
					      <th class="semi-bold text-center text-size-10">APELLIDOS Y NOMBRES</th>
					      <th class="semi-bold text-center text-size-10">C.I.</th>
					      <th class="semi-bold text-center text-size-10">CARRERA</th>
					    </tr>
					    <tr>
					      <td class="text-size-10">
					      	<?= $estudiante->primer_apellido . " " . $estudiante->segundo_apellido . " " . $estudiante->primer_nombre . " " . $estudiante->segundo_nombre ?>
					      </td>
					      <td class="text-size-10">
					      	<?= $estudiante->nacionalidad . "-" . $estudiante->ci ?>
					      </td>
					      <td class="text-size-10">
					      	<?php 
					      		$carreras = 1;
					      		foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($estudiante->usuario_id) as $carrera_estudiante_id) {
					      			$carrera = $this->Carrera_model->obtenerCarrera($carrera_estudiante_id->carrera_id);
					      			if ($carreras > 1) {
					      				echo ", " . $carrera->nombre;
					      			}
					      			else {
					      				echo $carrera->nombre;
					      			}
					      			$carreras = $carreras + 1;
					      		}
					      	?>
					      </td>
					    </tr>
					  </tbody>
					</table>
				</div>
			</div>
			
			<!-- Datos de la supervición académica -->
			<div class="row justify-content-around margin_b-t">
				<div class="col-12 col-md-6 semi-bold text-center text-size-11 margin_sm_b  align-self-center">
					SUPERVISIÓN ACADÉMICA
				</div>
				<div class="col text-size-11">
					<div class="row">
						<div class="col-12 text-center">
							<span class="semi-bold">FECHA Y HORA</span>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-md-6 margin_sm_t">
							<?= form_input($fecha) ?>
						</div>
						<div class="col-12 col-md-6 margin_sm_t">
							<?= form_input($hora) ?>
						</div>
					</div>
				</div>
			</div>

			<!-- Datos del estudiante -->
			<div class="row justify-content-center">
				<div class="col-12 col-md-11">
					<table class="table text-center">
					  <tbody>
					    <tr>
					      <th class="semi-bold text-center text-size-10">ACTIVIDADES REALIZADAS</th>
					    	<td class="text-size-10" colspan="2">
					    		<?= form_textarea($actividades_realizadas) ?>
					    	</td>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">RASGOS DESEMPEÑO</th>
					    	<th class="semi-bold text-center text-size-10">Aspectos positivos</th>
					    	<th class="semi-bold text-center text-size-10">Puntos de mejora</th>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">PERSONALES</th>
					    	<td><?= form_textarea($personales_aspectos_positivos) ?></td>
					    	<td><?= form_textarea($personales_puntos_mejoras) ?></td>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">HABILIDADES PROFESIONALES</th>
					    	<td><?= form_textarea($habilidades_aspectos_positivos) ?></td>
					    	<td><?= form_textarea($habilidades_puntos_mejoras) ?></td>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">CALIDAD DEL DESEMPEÑO</th>
					    	<td><?= form_textarea($calidad_aspectos_positivos) ?></td>
					    	<td><?= form_textarea($calidad_puntos_mejoras) ?></td>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">RASGOS ORGANIZACIONALES</th>
					    	<td><?= form_textarea($rasgos_aspectos_positivos) ?></td>
					    	<td><?= form_textarea($rasgos_puntos_mejoras) ?></td>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">Observaciones</th>
					    	<td colspan="2"><?= form_textarea($observaciones) ?></td>
					    </tr>
					  </tbody>
					</table>
				</div>
			</div>

			<div class="row margin_t">
				<div class="col-5 col-md-6 text-right"><?= form_submit('','Crear',['class'=>'btn btn-success']) ?></div>
				<div class="col-5 col-md-6 text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>					
			</div>

		<?= form_close() ?>
