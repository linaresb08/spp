		<?= validation_errors() ?>

		<?= form_open("/supervisiones/editar/$supervision_id") ?>
			<?php 
			/* ME DA ERROR AL COLOCAR LA FECHA EN EL VALUE 
				$fecha_supervision = array(
					'name'     => 'fecha_supervision',
					'value'    => $supervision->fecha_supervision,
					'type'     => 'datetime-local',
					'class'    => 'form-control',
					'required' => 'true'
				);
			*/

				$actividades_realizadas = array(
					'name'     => 'actividades_realizadas', 
					'value'    => $supervision->actividades_realizadas,
					'class'    => 'form-control',
					'rows'     => '4',
					'required' => 'true'
				);

				$personales_aspectos_positivos = array(
					'name'     => 'personales_aspectos_positivos', 
					'value'    => $supervision->personales_aspectos_positivos,
					'class'    => 'form-control',
					'rows'     => '3',
					'required' => 'true'
				);

				$personales_puntos_mejoras = array(
					'name'  => 'personales_puntos_mejoras', 
					'value'    => $supervision->personales_puntos_mejoras,
					'class' => 'form-control',
					'rows'  => '3'
				);

				$habilidades_aspectos_positivos = array(
					'name'     => 'habilidades_aspectos_positivos', 
					'value'    => $supervision->habilidades_aspectos_positivos,
					'class'    => 'form-control',
					'rows'     => '3',
					'required' => 'true'
				);

				$habilidades_puntos_mejoras = array(
					'name'  => 'habilidades_puntos_mejoras', 
					'value' => $supervision->habilidades_puntos_mejoras,
					'class' => 'form-control',
					'rows'  => '3'
				);

				$calidad_aspectos_positivos = array(
					'name'     => 'calidad_aspectos_positivos', 
					'value'    => $supervision->calidad_aspectos_positivos,
					'class'    => 'form-control',
					'rows'     => '3',
					'required' => 'true'
				);

				$calidad_puntos_mejoras = array(
					'name'  => 'calidad_puntos_mejoras', 
					'value' => $supervision->calidad_puntos_mejoras,
					'class' => 'form-control',
					'rows'  => '3'
				);

				$rasgos_aspectos_positivos = array(
					'name'     => 'rasgos_aspectos_positivos', 
					'value'    => $supervision->rasgos_aspectos_positivos,
					'class'    => 'form-control',
					'rows'     => '3',
					'required' => 'true'
				);

				$rasgos_puntos_mejoras = array(
					'name'  => 'rasgos_puntos_mejoras', 
					'value' => $supervision->rasgos_puntos_mejoras,
					'class' => 'form-control',
					'rows'  => '3'
				);

				$observaciones = array(
					'name'  => 'observaciones', 
					'value' => $supervision->observaciones,
					'class' => 'form-control',
					'rows'  => '3'
				);				
			?>
			
			<!-- Datos del estudiante -->
			<div class="row justify-content-center">
				<div class="col semi-bold text-center text-size-11 margin_b">
					DATOS DEL ESTUDIANTE
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12 col-md-11">
					<table class="table text-center">
					  <tbody>
					  	<tr>
					      <th class="semi-bold text-center text-size-10">APELLIDOS Y NOMBRES</th>
					      <th class="semi-bold text-center text-size-10">C.I.</th>
					      <th class="semi-bold text-center text-size-10">CARRERA</th>
					    </tr>
					    <tr>
					      <td class="text-size-10">
					      	<?= $estudiante->primer_apellido . " " . $estudiante->segundo_apellido . " " . $estudiante->primer_nombre . " " . $estudiante->segundo_nombre ?>
					      </td>
					      <td class="text-size-10">
					      	<?= $estudiante->nacionalidad . "-" . $estudiante->ci ?>
					      </td>
					      <td class="text-size-10">
					      	<?php 
					      		$carreras = 1;
					      		foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($estudiante->usuario_id) as $carrera_estudiante_id) {
					      			$carrera = $this->Carrera_model->obtenerCarrera($carrera_estudiante_id->carrera_id);
					      			if ($carreras > 1) {
					      				echo ", " . $carrera->nombre;
					      			}
					      			else {
					      				echo $carrera->nombre;
					      			}
					      			$carreras = $carreras + 1;
					      		}
					      	?>
					      </td>
					    </tr>
					  </tbody>
					</table>
				</div>
			</div>
			
			<!-- Datos de la supervición académica -->
			<div class="row justify-content-around margin_b-t">
				<div class="col-12 col-md-6 semi-bold text-center text-size-11 margin_sm_b  align-self-center">SUPERVISIÓN ACADÉMICA</div>
				<div class="col text-size-11">
					<div class="row">
						<div class="col-12 col-md-5 align-self-center">
							<span class="semi-bold text-center">FECHA Y HORA</span>
						</div>
						<div class="col-12 col-md-6 align-self-center">
							<?php $date = new DateTime($supervision->fecha_supervision) ?>
							<?= $date->format('d-m-Y g:i A') ?>
						</div>
					</div>
				</div>
			</div>

			<!-- Datos del estudiante -->
			<div class="row justify-content-center">
				<div class="col-12 col-md-11">
					<table class="table text-center">
					  <tbody>
					    <tr>
					      <th class="semi-bold text-center text-size-10">ACTIVIDADES REALIZADAS</th>
					    	<td class="text-size-10" colspan="2">
					    		<?= form_textarea($actividades_realizadas) ?>
					    	</td>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">RASGOS DESEMPEÑO</th>
					    	<th class="semi-bold text-center text-size-10">Aspectos positivos</th>
					    	<th class="semi-bold text-center text-size-10">Puntos de mejora</th>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">PERSONALES</th>
					    	<td><?= form_textarea($personales_aspectos_positivos) ?></td>
					    	<td><?= form_textarea($personales_puntos_mejoras) ?></td>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">HABILIDADES PROFESIONALES</th>
					    	<td><?= form_textarea($habilidades_aspectos_positivos) ?></td>
					    	<td><?= form_textarea($habilidades_puntos_mejoras) ?></td>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">CALIDAD DEL DESEMPEÑO</th>
					    	<td><?= form_textarea($calidad_aspectos_positivos) ?></td>
					    	<td><?= form_textarea($calidad_puntos_mejoras) ?></td>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">RASGOS ORGANIZACIONALES</th>
					    	<td><?= form_textarea($rasgos_aspectos_positivos) ?></td>
					    	<td><?= form_textarea($rasgos_puntos_mejoras) ?></td>
					    </tr>
					    <tr>
					    	<th class="semi-bold text-center text-size-10">Observaciones</th>
					    	<td colspan="2"><?= form_textarea($observaciones) ?></td>
					    </tr>
					  </tbody>
					</table>
				</div>
			</div>

			<div class="row justify-content-center margin_t">
				<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>				
			</div>

		<?= form_close() ?>
