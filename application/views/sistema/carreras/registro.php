		<?= validation_errors() ?>

		<?= form_open("/carreras/nueva") ?>
			<?php  
				$nombre = array(
					'name'        => 'nombre',
					'placeholder' => 'Escribe el nombre de la carrera',
					'class'       => 'form-control'
				);

				$descripcion = array(
					'name'        => 'descripcion',
					'placeholder' => 'Escribe una breve descripción de la carrera',
					'class'       => 'form-control',
					'rows'        => 3
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">
				<div class="col-md-11 col-xl-10">
					<span>Nombre: </span>
					<?= form_input($nombre) ?>
				</div>
				
				<div class="col-md-11 col-xl-10">
					<span>Descripción: </span>
				<?= form_textarea($descripcion) ?>
				</div>
				
			</div>

			<div class="row">

				<div class="col-5 col-md-6 col-xl-5 text-right"><?= form_submit('','Registrar',['class'=>'btn btn-success']) ?></div>
				<div class="col-5  col-md-6 col-xl-5 text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-danger']) ?></div>
									
			</div>

		<?= form_close() ?>