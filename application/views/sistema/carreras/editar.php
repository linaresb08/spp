		<?= validation_errors() ?>

		<?= form_open("/carreras/editar/$id") ?>
			<?php  
				$nombre = array(
					'name'        => 'nombre',
					'placeholder' => 'Escribe el nombre de la carrera',
					'value'       => $carrera->nombre,
					'class'       => 'form-control'
				);

				$descripcion = array(
					'name'        => 'descripcion',
					'placeholder' => 'Escribe una breve descripción de la carrera',
					'value'       => $carrera->descripcion,
					'class'       => 'form-control',
					'rows'        => 3
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">
				<div class="col-md-11 col-xl-10 margin-bottom-sm">
					<span>Nombre: </span>
					<?= form_input($nombre) ?>
				</div>
				
				<div class="col-md-11 col-xl-10 margin-bottom-sm">
					<span>Descripción: </span>
				<?= form_textarea($descripcion) ?>
				</div>
				
			</div>
			
			<div class="row">

				<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>
				
			</div>
		<?= form_close() ?>