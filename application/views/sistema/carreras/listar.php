<!-- Todas las carreras -->

		<div class="row">
			<div class="col-12 semi-bold section-title text-center">
				Carreras
			</div>
			<div class="col-12">
				<a class="btn btn-primary float-right" role="button" href="<?= base_url()?>carreras/nueva">
					<i class="fa fa-plus"></i> Nueva carrera
				</a>
			</div>
		</div>

<!-- TABLA DE CARRERAS -->

		<div class="row">
			<?php if (!$segmento): ?>

				<div class="col-md-12">
					<div class="card data-tables">
						<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
							<div class="fresh-datatables">

								<?php if ($carreras): ?>

									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

										<thead>
											<tr>
												<th>#</th>
												<th>Nombre</th>
												<th>Descripción</th>
												<th class="disabled-sorting text-right">Acciones</th>
											</tr>
										</thead>

										<tfoot>
											<tr>
												<th>#</th>
												<th>Nombre</th>
												<th>Descripción</th>
												<th class="text-right">Acciones</th>
											</tr>
										</tfoot>

										<tbody>
											<?php foreach ($carreras as $carrera): ?>
												<tr>
													<!-- # ID -->
													<td><?= $carrera->carrera_id ?></td>

													<!-- Nombre de la carrera -->
													<td>
														<?php if(strlen($carrera->nombre) > 30): ?>
									        		<?= substr($carrera->nombre, 0, 30); ?>...
									        	<?php else: ?>
									        		<?= $carrera->nombre ?>
									        	<?php endif;?>
													</td>

													<!-- Descripción -->
													<td class="parrafo-card extra-light">
														<?php if(strlen($carrera->descripcion) > 25): ?>
									        		<?= substr($carrera->descripcion, 0, 25); ?>...
									        	<?php else: ?>
									        		<?= $carrera->descripcion ?>
									        	<?php endif;?>
													</td>

													<!-- Acciones -->
													<td class="text-right">	
														<div class="dropdown">
														  <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink-<?= $carrera->carrera_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														    <i class="fa fa-gears"></i>
														  </a>

														  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?= $carrera->carrera_id ?>">
														  	<h6 class="dropdown-header">Acciones</h6>

							                	<!-- Modificar -->
																<a href="<?= base_url()?>carreras/editar/<?= $carrera->carrera_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-edit"></i> Modificar
							                	</a>

							                	<!-- Eliminar -->
							                	<a href="<?= base_url()?>carreras/eliminar/<?= $carrera->carrera_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-times"></i> Eliminar
																</a>
							                </div>
							              </div>	
													</td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
								<?php else: ?>
									<div class="alert alert-warning border-radius-10" role="alert">
										¡No hay categorías registradas!
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>	
		</div>