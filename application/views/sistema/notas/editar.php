		<?= validation_errors() ?>

		<?= form_open("/notas/editar/$pasantia_id") ?>
			<?php 
				$eval_desemp_tutor_l = array(
					'name'  => 'eval_desemp_tutor_l',
					'value' => $nota->eval_desemp_tutor_l,
					'type'  => 'number',
					'min'   => 0,
					'max'   => 20,
					'class' => 'form-control'
				);

				$eval_informe_tutor_l = array(
					'name'  => 'eval_informe_tutor_l',
					'value' => $nota->eval_informe_tutor_l,
					'type'  => 'number',
					'min'   => 0,
					'max'   => 20,
					'class' => 'form-control'
				);

				$eval_desemp_tutor_a = array(
					'name'  => 'eval_desemp_tutor_a',
					'value' => $nota->eval_desemp_tutor_a,
					'type'  => 'number',
					'min'   => 0,
					'max'   => 20,
					'class' => 'form-control'
				);

				$eval_informe_tutor_a = array(
					'name'  => 'eval_informe_tutor_a',
					'value' => $nota->eval_informe_tutor_a,
					'type'  => 'number',
					'min'   => 0,
					'max'   => 20,
					'class' => 'form-control'
				);
			?>

			<input type="hidden" value="<?= $pasantia_id ?>" name="pasantia_id">

			<!-- Datos del estudiante -->
			<div class="row justify-content-center">
				<div class="col-12 col-md-11">
					<table class="table text-center">
					  <thead>
					    <tr>
					      <th class="semi-bold text-center text-size-11" colspan="3">DATOS DEL ESTUDIANTE</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<tr>
					      <th class="semi-bold text-center text-size-10">APELLIDOS</th>
					      <th class="semi-bold text-center text-size-10">NOMBRES</th>
					      <th class="semi-bold text-center text-size-10">CÉDULA DE IDENTIDAD</th>
					    </tr>
					    <tr>
					      <td>
					      	<?= $estudiante->primer_apellido . " " . $estudiante->segundo_apellido?>
					      </td>
					      <td>
					      	<?= $estudiante->primer_nombre . " " . $estudiante->segundo_nombre ?>
					      </td>
					      <td>
					      	<?= $estudiante->nacionalidad . "-" . $estudiante->ci ?>
					      </td>
					    </tr>				      
					  </tbody>
					</table>
				</div>
			</div>

			<!-- Calificación del tutor laboral -->
			<div class="row justify-content-center">

				<div class="col-12 col-md-11">
					<table class="table">
					  <thead>
					    <tr>
					      <th class="semi-bold text-center text-size-11" colspan="2">CALIFICACIÓN DEL TUTOR LABORAL</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<tr>
					      <td class="semi-bold text-size-10">EVALUACIÓN</td>
					      <td class="semi-bold text-center text-size-10">NOTA</td>
					    </tr>
					    <tr>
					      <th>
					      	Desempeño del pasante
					      </th>
					      <td>
					      	<?= form_input($eval_desemp_tutor_l) ?>
					      </td>
					    </tr>
					    <tr>
					      <th>
					      	Informe del pasante
					      </th>
					      <td>
					      	<?= form_input($eval_informe_tutor_l) ?>					      	
					      </td>
					    </tr>					      
					  </tbody>
					</table>
				</div>
			</div>

			<!-- Calificación del tutor académico -->
			<div class="row justify-content-center">

				<div class="col-12 col-md-11">
					<table class="table">
					  <thead>
					    <tr>
					      <th class="semi-bold text-center text-size-11" colspan="2">CALIFICACIÓN DEL TUTOR ACADÉMICO</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<tr>
					      <td class="semi-bold text-size-10">EVALUACIÓN</td>
					      <td class="semi-bold text-center text-size-10">NOTA</td>
					    </tr>
					    <tr>
					      <th>
					      	Desempeño del pasante
					      </th>
					      <td>
					      	<?= form_input($eval_desemp_tutor_a) ?>
					      </td>
					    </tr>
					    <tr>
					      <th>
					      	Informe del pasante
					      </th>
					      <td>
					      	<?= form_input($eval_informe_tutor_a) ?>					      	
					      </td>
					    </tr>					      
					  </tbody>
					</table>
				</div>
			</div>

			<div class="row justify-content-center">
				<?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?>	
			</div>

		<?= form_close() ?>
