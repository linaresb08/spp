		<?= validation_errors() ?>

		<?= form_open("/tutoreslaborales/nuevo") ?>
			<?php 
			
				$nombres = array(
					'name'				=> 'nombres',
					'placeholder' => 'Nombres del tutor laboral',
					'class'				=> 'form-control'
				);

				$apellidos = array(
					'name' 				=> 'apellidos',
					'placeholder' => 'Apellidos del tutor laboral',
					'class' 			=> 'form-control'
				);

				$rif = array(
					'name' 				=> 'rif', 
					'placeholder' => 'J-12345678-9',
					'class' 			=> 'form-control'
				);

				$direccion = array(
					'name' 				=> 'direccion', 
					'placeholder' => 'Escribe la dirección de la empresa',
					'class' 			=> 'form-control',
					'rows'        => '5'
				);

				$telefono1 = array(
					'name' 				=> 'telefono1', 
					'placeholder' => '0271-1234444',
					'type'        => 'tel',
					'class'       => 'form-control',
				);

				$telefono2 = array(
					'name' 				=> 'telefono2', 
					'placeholder' => '0271-1112222',
					'type'        => 'tel',
					'class'       => 'form-control',
				);
			?>

			<div class="row justify-content-center">
				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Nombre</span>
					<?= form_input($nombre) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>RIF</span>
					<?= form_input($rif) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Dirección</span>
					<?= form_textarea($direccion) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Teléfono 1</span>
					<?= form_input($telefono1) ?>

					<div class="margin_sm_t">
						<span>Teléfono 2</span>
						<?= form_input($telefono2) ?>
					</div>
				</div>

			</div>

			<div class="row margin_t">
				<div class="col-5 col-md-6 text-right"><?= form_submit('','Crear',['class'=>'btn btn-success']) ?></div>
				<div class="col-5 col-md-6 text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>					
			</div>
			
		<?= form_close() ?>
