		
		<?php if ($tipo == 'pasantia'): ?>
			<div class="row justify-content-center">
				<!-- Título -->
				<div class="col-12 semi-bold text-size-15 text-center">
					<?php 
        		$estudiante = $this->Perfil_model->obtenerPerfil($pasantia->estudiante_id);
      		?>
        	<?= $estudiante->primer_apellido . " " . $estudiante->segundo_apellido . " " . $estudiante->primer_nombre . " " . $estudiante->segundo_nombre ?>       	
				</div>

				<!-- Carrera -->
				<div class="col-12 text-center">
	      	<?php
	      		$cantidad = 1;
	      		foreach ($this->Carrera_estudiante_model->buscarCarreras_por_pasantia($pasantia->estudiante_id,$pasantia->pasantia_id) as $carrera_ids) {
	      			$carrera = $this->Carrera_model->obtenerCarrera($carrera_ids->carrera_id);
	      			?>
	      			<small class="text-muted uppercase">
		      			<?php

			      			if ($cantidad > 1) {
		      					echo ", " . $carrera->nombre;
			      			}
			      			else {
				      			echo $carrera->nombre;
			      			}
			      			$cantidad = $cantidad + 1;
			      		?>
			      	</small>
			      	<?php
	      		}
	      	?>
	      </div>
			</div>

			<div class="row justify-content-center margin_t">
				<div class="col-12 col-md-11 col-xl-8">
					<div id="accordion">

						<!-- #1 -->
					  <div class="card no-margin">
					    <div class="card-header cursor-pointer" id="headingEstudiante" data-toggle="collapse" data-target="#collapseEstudiante" aria-expanded="true" aria-controls="collapseEstudiante">
					      <h5 class="mb-0">
					        <button class="btn btn-link semi-bold text-size-10 font-color-green">
					          Datos del estudiante
					        </button>
					      </h5>
					    </div>

					    <div id="collapseEstudiante" class="collapse show" aria-labelledby="headingEstudiante" data-parent="#accordion">
					      <div class="card-body">
					      	<div class="row justify-content-center">
					      		<!-- Cédula -->
							      <div class="col-12 col-md-4">
							      	<span class="semi-bold">Cédula de identidad </span>
							      	<br>
							      	<?= $estudiante->nacionalidad . "-". $estudiante->ci ?>
							      </div>

							      <!-- Dirección de habitación -->
							      <?php if($estudiante->direccion_residencial): ?>
							      	<div class="col-12 col-md-7">
								      	<span class="semi-bold">Dirección de habitación</span>
								      	<br>
								      	<?= $estudiante->direccion_residencial ?>
								      </div>
							      <?php endif; ?>
								      

							      <!-- Teléfono(s) -->
							      <?php if($estudiante->telefono_movil || $estudiante->telefono_residencial): ?>
							      	<div class="col-12 col-md-4">
								      	<span class="semi-bold">Teléfono movil / residencial </span>
								      	<br>
								      	<?= $estudiante->telefono_movil ?> / <?= $estudiante->telefono_residencial ?>
								      </div>
							      <?php endif; ?>
								      
							      <!-- Correo(s) -->
							      <?php if($estudiante->correo_institucional || $estudiante->correo_personal): ?>
							      	<div class="col-12 col-md-7">
						      			<span class="semi-bold">Correo institucional / personal</span>
						      			<br>
						      			<?= $estudiante->correo_institucional ?> / <?= $estudiante->correo_personal ?>
						      		</div>
							      <?php endif; ?>
								      
					      	</div>
					      </div>
					    </div>
					  </div>

					  <!-- #2 -->
					  <div class="card no-margin">
					    <div class="card-header cursor-pointer" id="headingPracticas" data-toggle="collapse" data-target="#collapsePracticas" aria-expanded="false" aria-controls="collapsePracticas">
					      <h5 class="mb-0">
					        <button class="btn btn-link collapsed semi-bold text-size-10 font-color-green">
					          Prácticas profesionales
					        </button>
					      </h5>
					    </div>
					    <div id="collapsePracticas" class="collapse" aria-labelledby="headingPracticas" data-parent="#accordion">
					      <div class="card-body">
					        <div class="row justify-content-center">
					        	<!-- Sección -->
					        	<?php if($pasantia->seccion): ?>
					        		<div class="col-12 col-md-3">
								    		<span class="semi-bold">Sección </span>
								    		<?= $pasantia->seccion ?>
								    	</div>
					        	<?php endif; ?>								    	

					        	<!-- Período de inscripción -->
					        	<?php if($pasantia->inscripcion_periodo_id): ?>
					        		<div class="col-12 col-md-3">
								    		<span class="semi-bold">Inscripción </span>
								    		<?php 
													$inscripcion = $this->Periodo_model->obtenerPeriodo($pasantia->inscripcion_periodo_id);
												?>
								    		<?= $inscripcion->nombre ?>
								    	</div>
					        	<?php endif; ?>								    	

							    	<!-- Período de realización -->
					        	<?php if($pasantia->realizacion_periodo_id): ?>
					        		<div class="col-12 col-md-3">
								    		<span class="semi-bold">Realización </span>
								    		<?php 
													$realizacion = $this->Periodo_model->obtenerPeriodo($pasantia->realizacion_periodo_id);
												?>
								    		<?= $realizacion->nombre ?>
								    	</div>
					        	<?php endif; ?>								    	

										<!-- Fecha de inicio -->
										<?php if($pasantia->fecha_inicio): ?>
											<div class="col-12 col-md-4">
								    		<span class="semi-bold">Fecha inicio </span>
								    		<?php $fecha_inicio = new DateTime($pasantia->fecha_inicio); ?>
								    		<?= $fecha_inicio->format('d-m-Y'); ?>
								    	</div>
					        	<?php endif; ?>								    	

							    	<!-- Fecha de finalización -->
							    	<?php if($pasantia->fecha_fin): ?>
							    		<div class="col-12 col-md-5">
								    		<span class="semi-bold">Fecha finalización </span>
								    		<?php $fecha_fin = new DateTime($pasantia->fecha_fin); ?>
								    		<?= $fecha_fin->format('d-m-Y'); ?>
								    	</div>
					        	<?php endif; ?>								    	
					        </div>
					      </div>
					    </div>
					  </div>

					  <!-- #3 -->
					  <div class="card no-margin">
					    <div class="card-header cursor-pointer" id="headingEmpresa" data-toggle="collapse" data-target="#collapseEmpresa" aria-expanded="false" aria-controls="collapseEmpresa">
					      <h5 class="mb-0">
					        <button class="btn btn-link collapsed semi-bold text-size-10 font-color-green">
					          Datos de la organización
					        </button>
					      </h5>
					    </div>
					    <div id="collapseEmpresa" class="collapse" aria-labelledby="headingEmpresa" data-parent="#accordion">
					      <div class="card-body">
					        <div class="row justify-content-center">
					        	<!-- Nombre de la empresa -->
							    	<div class="col-12 col-md-11">
							    		<span class="semi-bold">
							    			Nombre legal de la organización
							    		</span>
							    		<br>
							    		<?php 
							    			$empresa = $this->Empresa_model->obtenerEmpresa_por_pasantia($pasantia->pasantia_id);
							    		?>
							    		<?= $empresa->nombre ?>
							    	</div>

							    	<!-- RIF -->
							    	<div class="col-12 col-md-3">
							    		<span class="semi-bold">RIF</span>
							    		<?= $empresa->rif ?>
							    	</div>
										
										<!-- Teléfonos -->
										<?php if($empresa->telefono1): ?>
											<div class="col-12 col-md-8">
								    		<span class="semi-bold">Teléfono(s)</span>
								    		<?= $empresa->telefono1 ?>
								    		<?php if ($empresa->telefono2): ?>
								    			/ <?= $empresa->telefono2 ?>
								    		<?php endif; ?>
								    	</div>
					        	<?php endif; ?>								    	

							    	<!-- Dirección -->
							    	<?php if($empresa->direccion): ?>
							    		<div class="col-12 col-md-11">
								    		<span class="semi-bold">Dirección </span>
								    		<?= $empresa->direccion ?>
								    	</div>
					        	<?php endif; ?>								    	
							    </div>
					      </div>
					    </div>
					  </div>

					  <!-- #4 -->
					  <div class="card no-margin">
					    <div class="card-header cursor-pointer" id="headingJefe" data-toggle="collapse" data-target="#collapseJefe" aria-expanded="false" aria-controls="collapseJefe">
					      <h5 class="mb-0">
					        <button class="btn btn-link collapsed semi-bold text-size-10 font-color-green">
					          Datos del jefe de recursos humanos 
					          <small class="text-muted">(O persona que cumple esta función)</small>
					        </button>
					      </h5>
					    </div>
					    <div id="collapseJefe" class="collapse" aria-labelledby="headingJefe" data-parent="#accordion">
					      <div class="card-body">
					        <div class="row justify-content-center">
					        	<!-- Apellidos -->
							    	<div class="col-12 col-md-5">
							    		<span class="semi-bold">Apellidos </span>
							    		<?= $empresa->jr_apellidos ?>
							    	</div>

							    	<!-- Nombres -->
							    	<div class="col-12 col-md-5">
							    		<span class="semi-bold">Nombres </span>
							    		<?= $empresa->jr_nombres ?>
							    	</div>
										
										<!-- Correo -->
										<?php if($empresa->jr_correo): ?>
											<div class="col-12 col-md-5">
								    		<span class="semi-bold">Correo</span>
								    		<?= $empresa->jr_correo ?>
								    	</div>
					        	<?php endif; ?>								    	

							    	<!-- Teléfono -->
							    	<?php if($empresa->jr_telefono): ?>
							    		<div class="col-12 col-md-5">
								    		<span class="semi-bold">Teléfono</span>
								    		<?= $empresa->jr_telefono ?>
								    	</div>	
					        	<?php endif; ?>
							    </div>
					      </div>
					    </div>
					  </div>

					  <!-- #5 -->
					  <div class="card no-margin">
					    <div class="card-header cursor-pointer" id="headingLaboral" data-toggle="collapse" data-target="#collapseLaboral" aria-expanded="false" aria-controls="collapseLaboral">
					      <h5 class="mb-0">
					        <button class="btn btn-link collapsed semi-bold text-size-10 font-color-green">
					          Datos del tutor laboral
					        </button>
					      </h5>
					    </div>
					    <div id="collapseLaboral" class="collapse" aria-labelledby="headingLaboral" data-parent="#accordion">
					      <div class="card-body">
					        <div class="row justify-content-center">
					        	<!-- Apellidos -->
							    	<div class="col-12 col-md-5">
							    		<span class="semi-bold">Apellidos </span>
							    		<?= $pasantia->tl_apellidos ?>
							    	</div>

							    	<!-- Nombres -->
							    	<div class="col-12 col-md-5">
							    		<span class="semi-bold">Nombres </span>
							    		<?= $pasantia->tl_nombres ?>
							    	</div>

							    	<!-- Cédula -->
							    	<div class="col-12 col-md-5">
							    		<span class="semi-bold">Cédula de identidad </span>
							    		<?= $pasantia->tl_ci ?>
							    	</div>

							    	<!-- Cargo -->
							    	<div class="col-12 col-md-5">
							    		<span class="semi-bold">Cargo </span>
							    		<?= $pasantia->tl_cargo ?>
							    	</div>
										
										<!-- Correo -->
										<?php if($pasantia->tl_correo): ?>
											<div class="col-12 col-md-5">
							    		<span class="semi-bold">Correo</span>
							    		<?= $pasantia->tl_correo ?>
							    	</div>
					        	<?php endif; ?>							    	

							    	<!-- Teléfono -->
							    	<?php if($pasantia->tl_telefono): ?>
							    		<div class="col-12 col-md-5">
							    		<span class="semi-bold">Teléfono</span>
							    		<?= $pasantia->tl_telefono ?>
							    	</div>
					        	<?php endif; ?>							    	
					        </div>
					      </div>
					    </div>
					  </div>

					  <?php if($pasantia->tutor_academico_id): ?>
						  <!-- #6 -->
						  <div class="card no-margin">
						    <div class="card-header cursor-pointer" id="headingAcademico" data-toggle="collapse" data-target="#collapseAcademico" aria-expanded="false" aria-controls="collapseAcademico">
						      <h5 class="mb-0">
						        <button class="btn btn-link collapsed semi-bold text-size-10 font-color-green">
						          Datos del tutor académico
						        </button>
						      </h5>
						    </div>
						    <div id="collapseAcademico" class="collapse" aria-labelledby="headingAcademico" data-parent="#accordion">
						      <div class="card-body">
						        <div class="row justify-content-center">
						        	<?php 
							    			$tutorA = $this->Perfil_model->obtenerPerfil($pasantia->tutor_academico_id);
							    		?>

							    		<!-- Apellidos -->
								    	<div class="col-12 col-md-5">
								    		<span class="semi-bold">Apellidos </span>
								    		<?= $tutorA->primer_apellido . " " . $tutorA->segundo_apellido ?>
								    	</div>

								    	<!-- Nombres -->
								    	<div class="col-12 col-md-5">
								    		<span class="semi-bold">Nombres </span>
								    		<?=  $tutorA->primer_nombre . " " . $tutorA->segundo_nombre ?>
								    	</div>

							    		<!-- Cédula -->
							    		<div class="col-12 col-md-5">
							    			<span class="semi-bold">Cédula de identidad</span>
							    			<?= $tutorA->nacionalidad . "-" . $tutorA->ci ?>
							    		</div>
											
											<!-- Teléfono -->
											<?php if($tutorA->telefono_movil): ?>
												<div class="col-12 col-md-5">
								    			<span class="semi-bold">Teléfono</span>		    			
								    			<?= $tutorA->telefono_movil ?>
								    		</div>
					        		<?php endif; ?>							    		

								    	<!-- Correo -->
								    	<?php if($tutorA->correo_institucional || $tutorA->correo_personal): ?>
								    		<div class="col-12 col-md-10">
									    		<span class="semi-bold">
									    			Correos institucional / personal
									    		</span>
									    		<br>
									    		<?= $tutorA->correo_institucional . " / " . $tutorA->correo_personal ?>
									    	</div>
					        		<?php endif; ?>
						        </div>
						      </div>
						    </div>
						  </div>
						<?php endif; ?>
					</div>
				</div>
			</div>

		<?php elseif ($tipo == 'nota'): ?>

			<?php 
				$pasantia = $this->Pasantia_model->obtenerPasantia($nota->pasantia_id);
				$estudiante = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->estudiante_id);
				$tutorA = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->tutor_academico_id);
			?>

			<!-- Datos del estudiante -->
			<div class="row justify-content-center">
				<div class="col-12 col-md-11">
					<table class="table text-center">
					  <thead>
					    <tr>
					      <th class="semi-bold text-center text-size-11" colspan="3">DATOS DEL ESTUDIANTE</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<tr>
					      <th class="semi-bold text-center text-size-10">APELLIDOS</th>
					      <th class="semi-bold text-center text-size-10">NOMBRES</th>
					      <th class="semi-bold text-center text-size-10">CÉDULA DE IDENTIDAD</th>
					    </tr>
					    <tr>
					      <td>
					      	<?= $estudiante->primer_apellido . " " . $estudiante->segundo_apellido?>
					      </td>
					      <td>
					      	<?= $estudiante->primer_nombre . " " . $estudiante->segundo_nombre ?>
					      </td>
					      <td>
					      	<?= $estudiante->nacionalidad . "-" . $estudiante->ci ?>
					      </td>
					    </tr>				      
					  </tbody>
					</table>
				</div>
			</div>

			<!-- Calificación del tutor laboral -->
			<div class="row justify-content-center">

				<div class="col-12 col-md-11">
					<table class="table text-center">
					  <thead>
					    <tr>
					      <th class="semi-bold text-center text-size-11" colspan="4">CALIFICACIÓN DEL TUTOR LABORAL</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<tr>
					  		<td class="semi-bold text-size-10" colspan="2">APELLIDOS Y NOMBRES</td>
					  		<td class="semi-bold text-size-10" colspan="2">CÉDULA DE IDENTIDAD</td>
					  	</tr>
					  	<tr>
					  		<td colspan="2"> <?= $pasantia->tl_apellidos . " " . $pasantia->tl_nombres ?> </td>
					  		<td colspan="2"> <?= $pasantia->tl_ci ?> </td>
					  	</tr>
					  	<tr>
					      <td class="semi-bold text-size-10">EVALUACIÓN</td>
					      <td class="semi-bold text-size-10">NOTA</td>
					      <td class="semi-bold text-size-10">PORCENTAJE %</td>
					      <td class="semi-bold text-size-10">RESULTADO DEL %</td>
					    </tr>
					    <tr>
					      <td>Desempeño del pasante</td>
					      <td> <?= $nota->eval_desemp_tutor_l ?> </td>
					      <td>70%</td>
						     <td>
						     	<?php 
						      	$porcent_desemp_tl = $nota->eval_desemp_tutor_l * 0.7; 
						      	echo $porcent_desemp_tl;
						      ?>
						     </td>
					    </tr>
					    <tr>
					      <td>Informe del pasante</td>
					      <td> <?= $nota->eval_informe_tutor_l ?> </td>
					      <td>30%</td>
					      <td>
					      	<?php
					      		$porcent_informe_tl = $nota->eval_informe_tutor_l * 0.3;
					      		echo $porcent_informe_tl;
					      	?>
					      </td>
					    </tr>
					    <tr>
					    	<td class="semi-bold text-right text-size-10" colspan="3">NOTA FINAL <span class="text-italic">(Sumatoria de %)</span></td>
					    	<td class="semi-bold text-size-10"> <?= $nota->nota_final_tutor_l ?> </td>
					    </tr>					      
					  </tbody>
					</table>
				</div>
			</div>

			<!-- Calificación del tutor académico -->
			<div class="row justify-content-center">

				<div class="col-12 col-md-11">
					<table class="table text-center">
					  <thead>
					    <tr>
					      <th class="semi-bold text-center text-size-11" colspan="4">CALIFICACIÓN DEL TUTOR ACADÉMICO</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<?php if($pasantia->tutor_academico_id): ?>
						  	<tr>
						  		<td class="semi-bold text-size-10" colspan="2">APELLIDOS Y NOMBRES</td>
						  		<td class="semi-bold text-size-10" colspan="2">CÉDULA DE IDENTIDAD</td>
						  	</tr>
						  	<tr>
						  		<td colspan="2"> <?= $tutor_academico->primer_apellido . " " . $tutor_academico->segundo_apellido . " " . $tutor_academico->primer_nombre . " " . $tutor_academico->segundo_nombre ?> </td>
						  		<td colspan="2"> <?= $tutor_academico->nacionalidad . "-" . $tutor_academico->ci ?> </td>
						  	</tr>
						  <?php endif; ?>
					  	<tr>
					      <td class="semi-bold text-size-10">EVALUACIÓN</td>
					      <td class="semi-bold text-size-10">NOTA</td>
					      <td class="semi-bold text-size-10">PORCENTAJE %</td>
					      <td class="semi-bold text-size-10">RESULTADO DEL %</td>
					    </tr>
					    <tr>
					      <td>Desempeño del pasante</td>
					      <td> <?= $nota->eval_desemp_tutor_a ?> </td>
					      <td>70%</td>
						     <td>
						     	<?php 
						      	$porcent_desemp_ta = $nota->eval_desemp_tutor_a * 0.7; 
						      	echo $porcent_desemp_ta;
						      ?>
						     </td>
					    </tr>
					    <tr>
					      <td>Informe del pasante</td>
					      <td> <?= $nota->eval_informe_tutor_a ?> </td>
					      <td>30%</td>
					      <td>
					      	<?php
					      		$porcent_informe_ta = $nota->eval_informe_tutor_a * 0.3;
					      		echo $porcent_informe_ta;
					      	?>
					      </td>
					    </tr>	
					    <tr>
					    	<td class="semi-bold text-right text-size-10" colspan="3">NOTA FINAL <span class="text-italic">(Sumatoria de %)</span></td>
					    	<td class="semi-bold text-size-10"> <?= $nota->nota_final_tutor_a ?> </td>
					    </tr>				      
					  </tbody>
					</table>
				</div>
			</div>

			<!-- Evaluación definitiva -->
			<div class="row justify-content-center">
				<div class="col-12 col-md-11">
					<table class="table text-center">
					  <thead>
					    <tr>
					      <th class="semi-bold text-center text-size-11" colspan="2">EVALUACIÓN DEFINITIVA</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<tr>
					      <th class="semi-bold text-center text-size-10">LETRAS</th>
					      <th class="semi-bold text-center text-size-10">NÚMEROS</th>
					    </tr>
					    <tr>
					      <td class="semi-bold text-size-10">
					      	<?php 
					      		switch ($nota->definitiva) {
					      			case 0:
					      				echo "Cero ";
					      				break;

					      			case 1:
					      				echo "Un ";
					      				break;

					      			case 2:
					      				echo "Dos ";
					      				break;

					      			case 3:
					      				echo "Tres ";
					      				break;

					      			case 4:
					      				echo "Cuatro ";
					      				break;

					      			case 5:
					      				echo "Cinco ";
					      				break;

					      			case 6:
					      				echo "Seis ";
					      				break;

					      			case 7:
					      				echo "Siete ";
					      				break;

					      			case 8:
					      				echo "Ocho ";
					      				break;

					      			case 9:
					      				echo "Nueve ";
					      				break;

					      			case 10:
					      				echo "Diez ";
					      				break;

					      			case 11:
					      				echo "Once ";
					      				break;

					      			case 12:
					      				echo "Doce ";
					      				break;

					      			case 13:
					      				echo "Trece ";
					      				break;

					      			case 14:
					      				echo "Catorce ";
					      				break;

					      			case 15:
					      				echo "Quince ";
					      				break;


					      			case 16:
					      				echo "Dieciséis ";
					      				break;


					      			case 17:
					      				echo "Diecisiete ";
					      				break;


					      			case 18:
					      				echo "Dieciocho ";
					      				break;

					      			case 19:
					      				echo "Diecinueve ";
					      				break;

					      			case 20:
					      				echo "Veinte ";
					      				break;
					      		}
					      	?>
					      	Pts.
					      </td>
					      <td class="semi-bold text-size-10">	<?= $nota->definitiva ?>Pts. </td>
					    </tr>				      
					  </tbody>
					</table>
				</div>
			</div>
		<?php elseif ($tipo == 'supervisiones'): ?>

			<ul class="nav nav-tabs" id="tabSupervision" role="tablist">
				<!-- Datos del pasante -->
			  <li class="nav-item">
			    <a class="nav-link" id="pasante-tab" data-toggle="tab" href="#pasante" role="tab" aria-controls="pasante" aria-selected="true">Pasante</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="tutorA-tab" data-toggle="tab" href="#tutorA" role="tab" aria-controls="tutorA" aria-selected="false">Tutor académico</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="empresa-tab" data-toggle="tab" href="#empresa" role="tab" aria-controls="empresa" aria-selected="false">Empresa</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="tutorL-tab" data-toggle="tab" href="#tutorL" role="tab" aria-controls="tutorL" aria-selected="false">Tutor laboral</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link active" id="supervisiones-tab" data-toggle="tab" href="#supervisiones" role="tab" aria-controls="supervisiones" aria-selected="false">Supervisiones</a>
			  </li>
			</ul>
			<div class="tab-content" id="tabSupervisionContent">

				<!-- Datos del pasante -->
			  <div class="tab-pane fade" id="pasante" role="tabpanel" aria-labelledby="pasante-tab">
					<div class="row justify-content-center">
						<div class="col-12 col-md-11">
							<table class="table text-center">
							  <thead>
							    <tr>
							      <th class="semi-bold text-center text-size-11" colspan="3">DATOS DEL PASANTE</th>
							    </tr>
							  </thead>
							  <tbody>
							  	<tr>
							      <th class="semi-bold text-center text-size-10">APELLIDOS</th>
							      <th class="semi-bold text-center text-size-10">NOMBRES</th>
							      <th class="semi-bold text-center text-size-10">CÉDULA DE IDENTIDAD</th>
							    </tr>
							    <tr>
							      <td>
							      	<?= $estudiante->primer_apellido . " " . $estudiante->segundo_apellido?>
							      </td>
							      <td>
							      	<?= $estudiante->primer_nombre . " " . $estudiante->segundo_nombre ?>
							      </td>
							      <td>
							      	<?= $estudiante->nacionalidad . "-" . $estudiante->ci ?>
							      </td>
							    </tr>
							    <tr>
							    	<th class="semi-bold text-center text-size-10">CARRERA</th>
							    	<th class="semi-bold text-center text-size-10" colspan="2">CORREOS</th>
							    </tr>	
							    <tr>
							    	<td>
							    		<?php
							      		$cantidad = 1;
							      		foreach ($this->Carrera_estudiante_model->buscarCarreras_por_pasantia($pasantia->estudiante_id,$pasantia->pasantia_id) as $carrera_ids) {
							      			$carrera = $this->Carrera_model->obtenerCarrera($carrera_ids->carrera_id);
							      			if ($cantidad > 1) {
								      			echo ", " . $carrera->nombre;
									      	}
									      	else {
										      	echo $carrera->nombre;
									      	}
									      	$cantidad = $cantidad + 1;
							      		}
							      	?>
							    	</td>
							    	<td colspan="2"><?= $estudiante->correo_institucional . " / " . $estudiante->correo_personal ?></td>
							    </tr>	 
							    <tr>
							    	<th class="semi-bold text-center text-size-10">Teléfonos</th>
							    	<th class="semi-bold text-center text-size-10">FECHA DE INICIO</th>
							    	<th class="semi-bold text-center text-size-10">FECHA DE FINALIZACIÓN</th>
							    </tr>	
							    <tr>
							    	<td>
							    		<?= $estudiante->telefono_movil . " / " . $estudiante->telefono_residencial ?>					    		
							    	</td>
							    	<td>
							    		<?= $pasantia->fecha_inicio ?>
							    	</td>
							    	<td>
							    		<?= $pasantia->fecha_fin ?>					    		
							    	</td>
							    </tr>	     
							  </tbody>
							</table>
						</div>
					</div>
			  </div>

				<!-- Datos del tutor académico -->
			  <div class="tab-pane fade" id="tutorA" role="tabpanel" aria-labelledby="tutorA-tab">
			  	<div class="row justify-content-center">
						<div class="col-12 col-md-11">
							<table class="table text-center">
							  <thead>
							    <tr>
							      <th class="semi-bold text-center text-size-11" colspan="3">DATOS DEL TUTOR ACADÉMICO</th>
							    </tr>
							  </thead>
							  <tbody>
							  	<tr>
							      <th class="semi-bold text-center text-size-10">APELLIDOS</th>
							      <th class="semi-bold text-center text-size-10">NOMBRES</th>
							      <th class="semi-bold text-center text-size-10">CÉDULA DE IDENTIDAD</th>
							    </tr>
							    <tr>
							      <td>
							      	<?= $tutor_academico->primer_apellido . " " . $tutor_academico->segundo_apellido?>
							      </td>
							      <td>
							      	<?= $tutor_academico->primer_nombre . " " . $tutor_academico->segundo_nombre ?>
							      </td>
							      <td>
							      	<?= $tutor_academico->nacionalidad . "-" . $tutor_academico->ci ?>
							      </td>
							    </tr>				      
							  </tbody>
							</table>
						</div>
					</div>
			  </div>

				<!-- Datos de la organización -->
			  <div class="tab-pane fade" id="empresa" role="tabpanel" aria-labelledby="empresa-tab">
			  	<div class="row justify-content-center">
						<div class="col-12 col-md-11">
							<table class="table text-center">
							  <thead>
							    <tr>
							      <th class="semi-bold text-center text-size-11" colspan="4">DATOS DE LA ORGANIZACIÓN</th>
							    </tr>
							  </thead>
							  <tbody>
							  	<tr>
							      <th class="semi-bold text-center text-size-10" colspan="2">
							      	RAZÓN SOCIAL
							      	<small class="text-muted">(Nombre legal de la organización)</small>
							      </th>
							      <th class="semi-bold text-center text-size-10">RIF</th>
							      <th class="semi-bold text-center text-size-10">TELÉFONOS</th>
							    </tr>
							    <tr>
							      <td colspan="2">
							      	<?= $empresa->nombre ?>
							      </td>
							      <td>
							      	<?= $empresa->rif ?>
							      </td>
							      <td>
							      	<?= $empresa->telefono1 ?>
							      	<?php if ($empresa->telefono2) {
							      		echo " / " . $empresa->telefono2;
							      	} ?>
							      </td>
							    </tr>
							    <tr>
							    	<th class="semi-bold text-center text-size-10">DIRECCIÓN</th>
							    	<td colspan="3"><?= $empresa->direccion ?></td>
							    </tr>
							  </tbody>
							</table>
						</div>
					</div>
			  </div>


				<!-- Datos del tutor laboral -->
			  <div class="tab-pane fade" id="tutorL" role="tabpanel" aria-labelledby="tutorL-tab">
			  	<div class="row justify-content-center">
						<div class="col-12 col-md-11">
							<table class="table text-center">
							  <thead>
							    <tr>
							      <th class="semi-bold text-center text-size-11" colspan="3">DATOS DEL TUTOR LABORAL</th>
							    </tr>
							  </thead>
							  <tbody>
							  	<tr>
							      <th class="semi-bold text-center text-size-10">APELLIDOS</th>
							      <th class="semi-bold text-center text-size-10">NOMBRES</th>
							      <th class="semi-bold text-center text-size-10">CÉDULA DE IDENTIDAD</th>
							    </tr>
							    <tr>
							      <td>
							      	<?= $pasantia->tl_apellidos ?>
							      </td>
							      <td>
							      	<?= $pasantia->tl_nombres ?>
							      </td>
							      <td>
							      	<?= $pasantia->tl_ci ?>
							      </td>
							    </tr>
							    <tr>
							    	<th class="semi-bold text-center text-size-10">CARGO</th>
							    	<th class="semi-bold text-center text-size-10">CORREO</th>
							    	<th class="semi-bold text-center text-size-10">TELÉFONO</th>
							    </tr>
							    <tr>
							    	<td><?= $pasantia->tl_cargo ?></td>
							    	<td><?= $pasantia->tl_correo ?></td>
							    	<td><?= $pasantia->tl_telefono ?></td>
							    </tr>
							  </tbody>
							</table>
						</div>
					</div>
			  </div>

			  <!-- Datos de las supervisiones académicas -->
			  <div class="tab-pane fade show active" id="supervisiones" role="tabpanel" aria-labelledby="supervisiones-tab">
			  	
			  	<div class="row justify-content-center margin_b">
			  		<div class="col text-center semi-bold">SUPERVISIONES ACADÉMICAS</div>
			  	</div>

			  	<div class="row justify-content-center">

			  		<div class="col-md-11">
			  			<div class="row">
			  				<table class="table text-center">
			  					<tbody>
			  						<?php $cantidad = 1; ?>
							  		<?php foreach ($supervisiones as $supervision): ?>
							  			<tr>
				  							<th class="text-center semi-bold">#</th>
					  						<th class="text-center semi-bold">Fecha y hora</th>
					  						<th class="text-center semi-bold" colspan="2">Actividades realizadas</th>
				  						</tr>
							  			<tr>
							  				<td> <?= $cantidad ?>a. </td>
							  				<td> <?php
							  					$date = new DateTime($supervision->fecha_supervision);
													echo $date->format('d-m-Y g:i A'); ?>
							  				</td>
							  				<td colspan="2"> <?= $supervision->actividades_realizadas ?> </td>
							  			</tr>
							  			<tr>
							  				<th colspan="2" class="text-center semi-bold">Rasgos de desempeño</th>
							  				<th class="text-center semi-bold">Aspectos positivos</th>
							  				<th class="text-center semi-bold">Puntos de mejora</th>
							  			</tr>
							  			<tr>
							  				<th colspan="2">Personales</th>
							  				<td><?= $supervision->personales_aspectos_positivos ?></td>
							  				<td><?= $supervision->personales_puntos_mejoras ?></td>
							  			</tr>
							  			<tr>
							  				<th colspan="2">Habilidades profesionales</th>
							  				<td><?= $supervision->habilidades_aspectos_positivos ?></td>
							  				<td><?= $supervision->habilidades_puntos_mejoras ?></td>
							  			</tr>
							  			<tr>
							  				<th colspan="2">Calidad del desempeño</th>
							  				<td><?= $supervision->calidad_aspectos_positivos ?></td>
							  				<td><?= $supervision->calidad_puntos_mejoras ?></td>
							  			</tr>
							  			<tr>
							  				<th colspan="2">Rasgos organizacionales</th>
							  				<td><?= $supervision->rasgos_aspectos_positivos ?></td>
							  				<td><?= $supervision->rasgos_puntos_mejoras ?></td>
							  			</tr>
							  			<tr>
							  				<th colspan="2">Observaciones</th>
							  				<td colspan="2"><?= $supervision->observaciones ?></td>
							  			</tr>
							  			<tr>
							  				<th colspan="2">Operaciones</th>
							  				<td colspan="2">			  				
						  						<a class="btn btn-info" role="button" href="<?= base_url() ?>supervisiones/editar/<?= $supervision->supervision_id ?>">Editar</a>
			  									<a class="btn btn-danger" role="button" href="<?= base_url() ?>supervisiones/eliminar/<?= $supervision->supervision_id ?>">Eliminar</a>
							  				</td>
							  			</tr>
											<?php $cantidad = $cantidad + 1; ?>
							  		<?php endforeach; ?>
			  					</tbody>
			  				</table>
			  			</div>
			  		</div>
			  	</div>
			  </div>
			</div>
		<?php endif; ?>