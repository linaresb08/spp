		<div class="row">
			<!-- Título -->
			<div class="col-12 semi-bold section-title text-center">
				Pasantías
			</div>	

			<!-- Botón para añadir pasantías -->
			<?php if($this->session->userdata('rol') == 'Estudiante'): ?>
				<div class="col-12">
					<a class="btn btn-primary float-right" role="button" href="<?= base_url()?>pasantias/nueva">
						<i class="fa fa-plus"></i> Nueva pasantía
					</a>
				</div>
			<?php endif; ?>					
		</div>

		<!-- Tabla de las pasantías -->
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-11 col-xl-10">
				<?php if ($pasantias): ?>
					<div class="card data-tables">
						<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
							<div class="fresh-datatables">
								<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

									<thead>
										<tr>				
											<th>Inscripción</th>
											<th>CI estudiante</th>
											<th>Fecha de inicio</th>
											<th>Fecha de finalización</th>
											<th>Estado</th>
											<th class="disabled-sorting">Acciones</th>
										</tr>
									</thead>

									<tfoot>
										<tr>					
											<th>Inscripción</th>
											<th>CI estudiante</th>
											<th>Fecha de inicio</th>
											<th>Fecha de finalización</th>
											<th>Estado</th>	
											<th class="disabled-sorting">Acciones</th>
										</tr>
									</tfoot>

									<tbody>
										<?php foreach ($pasantias as $pasantia): ?>
											<tr>
												<!-- Inscripción -->
												<td>
													<?php 
														$inscripcion = $this->Periodo_model->obtenerPeriodo($pasantia->inscripcion_periodo_id);
													?>
													<?= $inscripcion->nombre ?>
												</td>

												<!-- CI estudiante -->
												<td>
													<?php 
														$estudiante = $this->Perfil_model->obtenerPerfil_usuario_id($pasantia->estudiante_id);
													?>
													<?= $estudiante->nacionalidad . "-" . $estudiante->ci ?>
												</td>

												<!-- Fecha de inicio -->
												<td>
													<?php $fecha_inicio = new DateTime($pasantia->fecha_inicio); ?>
													<?= $fecha_inicio->format('d-m-Y'); ?>
												</td>

												<!-- Fecha de finalización -->
												<td>
													<?php $fecha_fin = new DateTime($pasantia->fecha_fin); ?>
													<?= $fecha_fin->format('d-m-Y'); ?>
												</td>

												<!-- Estado -->
												<td>
													<?= $pasantia->estado ?>
												</td>

												<!-- Acciones -->
												<td>
													<div class="dropdown">
													  <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink-<?= $pasantia->pasantia_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													    <i class="fa fa-gears"></i>
													  </a>

													  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?= $pasantia->pasantia_id ?>">
													  	<h6 class="dropdown-header">Acciones</h6>

													    	<!-- Asignar / Cambiar tutor académico -->
													  	<?php if($this->session->userdata('rol') == 'Administrador' || $this->session->userdata('rol') == 'Coordinador'): ?>
													    	<a class="dropdown-item width_130 border-radius-10 btn-modal" href="#asignarTAModal" data-toggle="modal" data-funcion="asignarTutorA" data-id="<?= $pasantia->pasantia_id ?>" data-target="#asignarTAModal">
														    	<i class="fa fa-user-plus"></i>
														    	<?php if ($pasantia->tutor_academico_id): ?>
														    		Cambiar tutor
														    	<?php else: ?>
														    		Asignar tutor
														    	<?php endif; ?>
														    </a>
 															<?php endif; ?>	

													  	<!-- Ver detalles-->
													    <a class="dropdown-item width_130 border-radius-10" href="<?= base_url() ?>pasantias/ver/<?= $pasantia->pasantia_id ?>">
													    	<i class="fa fa-eye"></i> Ver detalle					 
													    </a>

													    <!-- Ver informe -->
														  <?php if($pasantia->informe != NULL): ?>
														  	<a class="dropdown-item width_130 border-radius-10" href="<?= $pasantia->informe ?>" target="_blank">
														    	<i class="fa fa-eye"></i> Ver informe
														    </a>
														  <?php endif; ?>

													    <!-- Modificar -->
													    <?php if($this->session->userdata('rol') == 'Estudiante'): ?>
														    <a class="dropdown-item width_130 border-radius-10" href="<?= base_url(); ?>pasantias/editar/<?= $pasantia->pasantia_id ?>">
														    	<i class="fa fa-edit"></i> Modificar
														    </a>

														   
													    	<a class="dropdown-item width_130 border-radius-10 btn-modal" href="#adjuntarInformeModal"  data-toggle="modal" data-funcion="adjuntarInforme" data-id="<?= $pasantia->pasantia_id ?>" data-target="#adjuntarInformeModal">
														    	<i class="fa fa-file-pdf-o"></i>
													    		<?= $pasantia->informe ? 'Actualizar informe' : 'Adjuntar informe' ?>
														    </a>														    
														  <?php endif; ?>

														  <!-- Operaciones con supervisiones -->
														  <?php if($this->session->userdata('rol') == 'Tutor académico' || 
														  $this->session->userdata('rol') == 'Administrador' ||
														  $this->session->userdata('rol') == 'Coordinador'): ?>
 																<!-- Añadir supervisiones -->
													    	<a class="dropdown-item width_130 border-radius-10" href="<?= base_url(); ?>supervisiones/nueva/<?= $pasantia->pasantia_id ?>">
														    	<i class="fa fa-plus"></i> Añadir supervisión
														    </a>

														    <?php if($this->Supervision_model->obtenerSupervisiones_por_pasantia($pasantia->pasantia_id)): ?>
														    	<a class="dropdown-item width_130 border-radius-10" href="<?= base_url() ?>supervisiones/ver/<?= $pasantia->pasantia_id ?>">
													    	<i class="fa fa-eye"></i> Ver supervisiones					 
													    </a>
														    <?php endif; ?>
 															<?php endif; ?>

													    <!-- Operaciones con notas-->
													    <?php if($this->session->userdata('rol') == 'Administrador' || $this->session->userdata('rol') == 'Coordinador'): ?>

													    	<?php if($this->Nota_model->obtenerNota_por_pasantia($pasantia->pasantia_id)): ?>

													    		<!-- Ver  nota -->
															    <a class="dropdown-item width_130 border-radius-10" href="<?= base_url() ?>notas/ver/<?= $pasantia->pasantia_id ?>">
															    	<i class="fa fa-percent"></i> Ver notas					 
															    </a>

													    		<!-- Editar calificación -->
															    <a class="dropdown-item width_130 border-radius-10" href="<?= base_url() ?>notas/editar/<?= $pasantia->pasantia_id ?>">
															    	<i class="fa fa-percent"></i> Editar notas					 
															    </a>

															    <!-- Eliminar calificación -->
													    	<a class="dropdown-item width_130 border-radius-10" href="<?= base_url(); ?>notas/eliminar/<?= $pasantia->pasantia_id ?>">
														    	<i class="fa fa-trash"></i> Eliminar notas
														    </a>

															  <?php else: ?>
															  	<!-- Registrar nota -->
															    <a class="dropdown-item width_130 border-radius-10" href="<?= base_url(); ?>notas/registrar/<?= $pasantia->pasantia_id ?>">
															    	<i class="fa fa-pencil-square-o"></i> Registrar notas
															    </a>
													    	<?php endif; ?>
 															<?php endif; ?>
													    
													    <?php if($this->session->userdata('rol') == 'Administrador' || $this->session->userdata('rol') == 'Coordinador'): ?>
																<!-- Eliminar -->
													    	<a class="dropdown-item width_130 border-radius-10" href="<?= base_url(); ?>pasantias/eliminar/<?= $pasantia->pasantia_id ?>">
														    	<i class="fa fa-trash"></i> Eliminar pasantía
														    </a>
 															<?php endif; ?>														    
													  </div>
													</div>
												</td>
											</tr>							
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<?php else: ?>
					<div class="row justify-content-center">						
						<div class="alert alert-warning border-radius-10 text-center text-size-12" role="alert">
							<span class="semi-bold">
								<i class="fa fa-warning"></i><br>
								¡No hay pasantías registradas!
							</span>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>

<!-- Modal para asignar tutor académico -->
<div class="modal fade" id="asignarTAModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
	        Asignación del tutor académico
	      </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fa fa-times"></i>
        </button>
      </div>
      <form action="" method="POST">
      	<div class="modal-body">
				  <div class="container-fluid">
				    <div class="row">
				      <div class="col ml-auto text-center">
				      	<span class="semi-bold">Ingrese número cédula de identidad del tutor laboral</span>
				      	<br>
				      	<small class="font-color-red">(Sin letras, ni puntos)</small>
				      	<br>
				      	<input type="text" class="form-control" name="ta_ci" placeholder="11222333">
				      </div>
				    </div>
				  </div>													
      	</div>
	      <div class="modal-footer" class="btn btn-primary">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	        <input type="submit" class="btn btn-success" rol="button" value="Asignar">
	      </div>
	  	</form>
    </div>
  </div>
</div>		

<!-- Modal para adjuntar informe -->
<div class="modal fade" id="adjuntarInformeModal" tabindex="-1" role="dialog" aria-labelledby="informeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="informeModalLabel">
	        Adjuntar informe de pasantías
	      </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fa fa-times"></i>
        </button>
      </div>
      <form action="<?= base_url(); ?>pasantias/adjuntarInforme/<?= $pasantia->pasantia_id ?>" method="POST">
      	<div class="modal-body">
				  <div class="container-fluid">
				    <div class="row">
				      <div class="col ml-auto text-center">
				      	<span class="semi-bold">Ingrese el enlace del informe</span>
				      	<br>
				      	<input type="url" class="form-control" name="informe">
				      </div>
				    </div>
				  </div>													
      	</div>
	      <div class="modal-footer" class="btn btn-primary">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	        <input type="submit" class="btn btn-success" rol="button" value="Adjuntar">
	      </div>
	  	</form>
    </div>
  </div>
</div>

<script>
	$('.btn-modal').click(function () {
		var id      = $(this).data('id');
		var funcion = $(this).data('funcion');
		var ruta = "<?= base_url(); ?>pasantias/" + funcion + "/" + id;

		if (funcion == 'asignarTutorA') {
			$('#asignarTAModal form').attr('action', ruta);
		}else{
			$('#adjuntarInformeModal form').attr('action', ruta);

			$.ajax({
            type:"post",
            url: "<?php echo base_url(); ?>pasantias/obtener",
            data:{id: id},
            success:function(response)
            {
            	data = JSON.parse(response);
            	if (data) {
            		$('#adjuntarInformeModal .form-control').val(data.informe);
            		console.log(data);
            	}            	
            },
            error: function() 
            {
              console.log('Error');
            }
        	});
		}
		
		console.log(id);
	});
</script>	