		<?= validation_errors() ?>

		<?= form_open("/pasantias/editar/$id") ?>
			<?php

				$seccion = array(
					'name'        => 'seccion', 
					'placeholder' => 'K',
					'class'       => 'form-control',
					'required'    => 'true',
					'value'       => $pasantia->seccion
				);

				$fecha_inicio = array(
					'name'        => 'fecha_inicio', 
					'placeholder' => 'Fecha de inicio',
					'type'        => 'date',
					'class'       => 'form-control datepicker',
					'required'    => 'true',
					'value'       => $pasantia->fecha_inicio
				);

				$fecha_fin = array(
					'name'        => 'fecha_fin', 
					'placeholder' => 'Fecha de inicio',
					'type'        => 'date',
					'class'       => 'form-control datepicker',
					'required'    => 'true',
					'value'       => $pasantia->fecha_fin
				);

				$departamento_estudiante = array(
					'name'        => 'departamento_estudiante',
					'placeholder' => 'Departamento asignado',
					'class'       => 'form-control',
					'required'    => 'true',
					'value'       => $pasantia->departamento_estudiante
				);

				//VARIABLES PARA DATOS DE LA EMPRESA

				$nombre = array(
					'name'        => 'nombre',
					'placeholder' => 'Nombre de la empresa',
					'value'       => $empresa->nombre,
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$rif = array(
					'name'        => 'rif', 
					'placeholder' => 'J-12345678-9',
					'value'       => $empresa->rif,
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$direccion = array(
					'name' 				=> 'direccion', 
					'placeholder' => 'Escribe la dirección',
					'value'       => $empresa->direccion,
					'class' 			=> 'form-control',
					'required'    => 'true',
					'rows'        => '4'
				);

				$telefono1 = array(
					'name' 				=> 'telefono1', 
					'placeholder' => '0271-1234444',
					'value'       => $empresa->telefono1,
					'type'        => 'tel',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$telefono2 = array(
					'name' 				=> 'telefono2', 
					'placeholder' => '0271-1112222',
					'value'       => $empresa->telefono2,
					'type'        => 'tel',
					'class'       => 'form-control'
				);

				//VARIABLES PARA DATOS DEL JEFE DE RECURSOS HUMANOS

				$jr_apellidos = array(
					'name'        => 'jr_apellidos',
					'placeholder' => 'Escribe los apellidos',
					'value'       => $empresa->jr_apellidos,
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$jr_nombres = array(
					'name'        => 'jr_nombres',
					'placeholder' => 'Escribe los nombres',
					'value'       => $empresa->jr_nombres,
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$jr_correo = array(
					'name'        => 'jr_correo',
					'placeholder' => 'example@gmail.com',
					'value'       => $empresa->jr_correo,
					'type'        => 'email',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$jr_telefono = array(
					'name'        => 'jr_telefono',
					'placeholder' => '0414-1112222',
					'value'       => $empresa->jr_telefono,
					'type'        => 'tel',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				//VARIABLES PARA DATOS DEL TUTOR LABORAL
				$tl_apellidos = array(
					'name'        => 'tl_apellidos', 
					'placeholder' => 'Escribe los apellidos',
					'class'       => 'form-control',
					'required'    => 'true',
					'value'       => $pasantia->tl_apellidos
				);

				$tl_nombres = array(
					'name'        => 'tl_nombres', 
					'placeholder' => 'Escribe los nombres',
					'class'       => 'form-control',
					'required'    => 'true',
					'value'       => $pasantia->tl_nombres
				);

				$tl_ci = array(
					'name'        => 'tl_ci',
					'placeholder' => 'V-11222333',
					'class'       => 'form-control',
					'required'    => 'true',
					'value'       => $pasantia->tl_ci
				);

				$tl_cargo = array(
					'name'        => 'tl_cargo',
					'placeholder' => 'Escribe el cargo',
					'class'       => 'form-control',
					'required'    => 'true',
					'value'       => $pasantia->tl_cargo
				);

				$tl_correo = array(
					'name'        => 'tl_correo',
					'placeholder' => 'Escribe el correo',
					'type'        => 'email',
					'class'       => 'form-control',
					'required'    => 'true',
					'value'       => $pasantia->tl_correo
				);

				$tl_telefono = array(
					'name'        => 'tl_telefono',
					'placeholder' => '0424-4445555',
					'type'        => 'tel',
					'class'       => 'form-control',
					'required'    => 'true',
					'value'       => $pasantia->tl_telefono
				);				
				
			?>

			<!-- Para seleccionar la(s) carreras -->
			<div class="row justify-content-center">
				<div class="text-center col-12">
					<span class="semi-bold text-size-15">
						Información académica
					</span>					
				</div>

				<div class="col-10">
					<label>Selecciona la carrera</label>
				</div>

				<?php $carreras_estudiante = array();  ?>

				<?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($this->perfil_session->usuario_id) as $carrera_estudiante): ?>
					<?php array_push($carreras_estudiante, $carrera_estudiante->carrera_id); ?>
				<?php endforeach;  ?>

				<?php foreach ($this->Carrera_model->obtenerCarreras() as $carrera): ?>

					<?php if (in_array($carrera->carrera_id, $carreras_estudiante)): ?>
						<div class="col-12 col-md-6 col-lg-4 checkbox-radios">			
							<div class="form-check">
								<label class="form-check-label" for="carrera_<?= $carrera->carrera_id ?>">
									<input class="form-check-input" type="checkbox" name="carreras[]" value="<?= $carrera->carrera_id ?>" <?php if (in_array($carrera->carrera_id, $carreras_estudiante)) {
								echo "checked";	} ?> id="carrera_<?= $carrera->carrera_id ?>">
									<span class="form-check-sign"></span>
									<?= $carrera->nombre ?>
								</label>							
							</div>					
						</div>
					<?php endif; ?>
						
				<?php endforeach; ?>				
			</div>

			<!-- Información de las prácticas profesionales -->
			<div class="row justify-content-center">

				<div class="text-center col-12">
					<span class="semi-bold text-size-15">
						Prácticas Profesionales
					</span>
				</div>

				<!-- Para seleccionar el período de inscripción -->
				<div class="col-12 col-md-4 col-xl-2 margin_sm_t">
					<span>Inscripción</span>
					<select name="inscripcion_periodo_id" class="form-control" required="true">
						<option value="">Período académico</option>
						<?php foreach ($this->Periodo_model->obtenerPeriodos() as $periodo): ?>
							<option value="<?= $periodo->periodo_academico_id ?>" <?php if ($pasantia->inscripcion_periodo_id == $periodo->periodo_academico_id) { echo "selected"; } ?> ><?= $periodo->nombre ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<!-- Para indicar la sección -->
				<div class="col-12 col-md-4 col-xl-2 margin_sm_t">
					<span>Sección </span>
					<small class="text-muted">(Solo la letra)</small>
					<?= form_input($seccion) ?>
				</div>

				<!-- Para seleccionar el período de realización -->
				<div class="col-12 col-md-4 col-xl-2 margin_sm_t">
					<span>Realización</span>
					<select name="realizacion_periodo_id" class="form-control" required="true">
						<option value="">Período académico</option>
						<?php foreach ($this->Periodo_model->obtenerPeriodos() as $periodo): ?>
							<option value="<?= $periodo->periodo_academico_id ?>" <?php if ($pasantia->realizacion_periodo_id == $periodo->periodo_academico_id) { echo "selected"; } ?> ><?= $periodo->nombre ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<!-- Para seleccionar fecha de inicio -->
				<div class="col-md-4 col-xl-2 margin_sm_t">
					<span>Fecha de inicio</span>
					<div class="form-group">
						<?= form_input($fecha_inicio) ?>
					</div>
				</div>

				<!-- Para seleccionar fecha de finalización -->
				<div class="col-md-4 col-xl-2 margin_sm_t">
					<span>Fecha de finalización</span>
					<div class="form-group">
						<?= form_input($fecha_fin) ?>
					</div>
				</div>

				<!-- Para indicar el departamento del estudiante -->
				<div class="col-12 col-md-4 col-xl-10 margin_sm_t">
					<span>Departamento asignado</span>
					<?= form_input($departamento_estudiante) ?>
				</div>
			</div>

			<!-- Datos de la organización -->
			<div class="row justify-content-center">

				<div class="text-center col-12">
					<span class="semi-bold text-size-15">
						Datos de la organización
					</span>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Nombre</span>
					<?= form_input($nombre) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>RIF</span>
					<?= form_input($rif) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Dirección</span>
					<?= form_textarea($direccion) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Teléfono 1</span>
					<?= form_input($telefono1) ?>

					<div class="margin_sm_t">
						<span>Teléfono 2</span>
						<?= form_input($telefono2) ?>
					</div>
				</div>
			</div>

			<!-- Datos del jefe de recursos humanos -->
			<div class="row justify-content-center">
				<div class="col-12 text-center margin_md_t margin_sm_b">
					<span class="semi-bold text-size-15">
						Datos del jefe de recursos humanos
					</span>
					<br>
					<small class="text-muted text-center">
						(O persona que cumple esta función)
					</small>	
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Apellidos</span>
					<?= form_input($jr_apellidos) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Nombres</span>
					<?= form_input($jr_nombres) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Correo electrónico</span>
					<?= form_input($jr_correo) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Teléfono</span>
					<?= form_input($jr_telefono) ?>
				</div>
			</div>

			<!-- Datos del tutor laboral -->
			<div class="row justify-content-center">
				<div class="col-12 text-center margin_md_t margin_sm_b">
					<span class="semi-bold text-size-15">
						Datos del tutor laboral
					</span>	
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Apellidos</span>
					<?= form_input($tl_apellidos) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Nombres</span>
					<?= form_input($tl_nombres) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Cédula de identidad</span>
					<?= form_input($tl_ci) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Cargo</span>
					<?= form_input($tl_cargo) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Correo electrónico</span>
					<?= form_input($tl_correo) ?>
				</div>

				<div class="col-md-6 col-xl-5 margin_sm_t">
					<span>Teléfono</span>
					<?= form_input($tl_telefono) ?>
				</div>
			</div>

			<div class="row justify-content-center margin_t">
				<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>				
			</div>
			
		<?= form_close() ?>