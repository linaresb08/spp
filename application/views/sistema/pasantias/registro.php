		<?= validation_errors() ?>

		<?= form_open("/pasantias/nueva") ?>
			<?php

				$seccion = array(
					'name'        => 'seccion', 
					'placeholder' => 'K',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$fecha_inicio = array(
					'name'        => 'fecha_inicio', 
					'placeholder' => 'Fecha de inicio',
					'type'        => 'date',
					'class'       => 'form-control datepicker',
					'required'    => 'true'
				);

				$fecha_fin = array(
					'name'        => 'fecha_fin', 
					'placeholder' => 'Fecha de inicio',
					'type'        => 'date',
					'class'       => 'form-control datepicker',
					'required'    => 'true'
				);

				$departamento_estudiante = array(
					'name'        => 'departamento_estudiante',
					'placeholder' => 'Departamento asignado',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				//VARIABLES PARA DATOS DE LA EMPRESA

				$nombre = array(
					'name'        => 'nombre',
					'placeholder' => 'Nombre de la empresa',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$rif = array(
					'name'        => 'rif', 
					'placeholder' => 'J-12345678-9',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$direccion = array(
					'name' 				=> 'direccion', 
					'placeholder' => 'Escribe la dirección',
					'class' 			=> 'form-control',
					'required'    => 'true',
					'rows'        => '5'
				);

				$telefono1 = array(
					'name' 				=> 'telefono1', 
					'placeholder' => '0271-1234444',
					'type'        => 'tel',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$telefono2 = array(
					'name' 				=> 'telefono2', 
					'placeholder' => '0271-1112222',
					'type'        => 'tel',
					'class'       => 'form-control'
				);

				//VARIABLES PARA DATOS DEL JEFE DE RECURSOS HUMANOS

				$jr_apellidos = array(
					'name'        => 'jr_apellidos',
					'placeholder' => 'Escribe los apellidos',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$jr_nombres = array(
					'name'        => 'jr_nombres',
					'placeholder' => 'Escribe los nombres',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$jr_correo = array(
					'name'        => 'jr_correo',
					'placeholder' => 'example@gmail.com',
					'type'        => 'email',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$jr_telefono = array(
					'name'        => 'jr_telefono',
					'placeholder' => '0414-1112222',
					'type'        => 'tel',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				//VARIABLES PARA DATOS DEL TUTOR LABORAL
				$tl_apellidos = array(
					'name'        => 'tl_apellidos', 
					'placeholder' => 'Escribe los apellidos',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$tl_nombres = array(
					'name'        => 'tl_nombres', 
					'placeholder' => 'Escribe los nombres',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$tl_ci = array(
					'name'        => 'tl_ci',
					'placeholder' => 'V-11222333',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$tl_cargo = array(
					'name'        => 'tl_cargo',
					'placeholder' => 'Escribe el cargo',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$tl_correo = array(
					'name'        => 'tl_correo',
					'placeholder' => 'Escribe el correo',
					'type'        => 'email',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$tl_telefono = array(
					'name' => 'tl_telefono',
					'placeholder' => '0424-4445555',
					'type' => 'tel',
					'class' => 'form-control',
					'required'    => 'true'
				);				
				
			?>

			<!-- Para seleccionar la(s) carreras -->
			<div class="row">
				<div class="text-center col-12">
					<span class="semi-bold text-size-15">
						Información académica
					</span>					
				</div>

				<div class="col-12">
					<label>Selecciona la carrera</label>
				</div>

				<?php $carreras_estudiante = array();  ?>

				<?php foreach ($this->Carrera_estudiante_model->obtenerCarreras_por_Estudiante($this->perfil_session->usuario_id) as $carrera_estudiante): ?>
					<?php array_push($carreras_estudiante, $carrera_estudiante->carrera_id); ?>
				<?php endforeach;  ?>

				<?php foreach ($this->Carrera_model->obtenerCarreras() as $carrera): ?>

					<?php if (in_array($carrera->carrera_id, $carreras_estudiante)): ?>
						<div class="col-12 col-md-6 col-lg-4 checkbox-radios">			
							<div class="form-check">
								<label class="form-check-label" for="carrera_<?= $carrera->carrera_id ?>">
									<input class="form-check-input" type="checkbox" name="carreras[]" value="<?= $carrera->carrera_id ?>" id="carrera_<?= $carrera->carrera_id ?>">
									<span class="form-check-sign"></span>
									<?= $carrera->nombre ?>
								</label>							
							</div>					
						</div>
					<?php endif; ?>
						
				<?php endforeach; ?>				
			</div>

			<!-- Información de las prácticas profesionales -->
			<div class="row justify-content-center">

				<div class="text-center col-12">
					<span class="semi-bold text-size-15">
						Prácticas Profesionales
					</span>
				</div>

				<!-- Para seleccionar el período de inscripción -->
				<div class="col-12 col-md-4 margin_sm_t">
					<span>Inscripción</span>
					<select name="inscripcion_periodo_id" class="form-control" required="true">
						<option value="">Período académico</option>
						<?php foreach ($this->Periodo_model->obtenerPeriodos() as $periodo): ?>
							<option value="<?= $periodo->periodo_academico_id ?>"><?= $periodo->nombre ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<!-- Para indicar la sección -->
				<div class="col-12 col-md-4 margin_sm_t">
					<span>Sección </span>
					<small class="text-muted">(Solo la letra)</small>
					<?= form_input($seccion) ?>
				</div>

				<!-- Para seleccionar el período de realización -->
				<div class="col-12 col-md-4 margin_sm_t">
					<span>Realización</span>
					<select name="realizacion_periodo_id" class="form-control" required="true">
						<option value="">Período académico</option>
						<?php foreach ($this->Periodo_model->obtenerPeriodos() as $periodo): ?>
							<option value="<?= $periodo->periodo_academico_id ?>"><?= $periodo->nombre ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<!-- Para seleccionar fecha de inicio -->
				<div class="col-md-4 margin_sm_t">
					<span>Fecha de inicio</span>
					<div class="form-group">
						<?= form_input($fecha_inicio) ?>
					</div>
				</div>

				<!-- Para seleccionar fecha de finalización -->
				<div class="col-md-4 margin_sm_t">
					<span>Fecha de finalización</span>
					<div class="form-group">
						<?= form_input($fecha_fin) ?>
					</div>
				</div>

				<!-- Para indicar el departamento del estudiante -->
				<div class="col-12 col-md-4 margin_sm_t">
					<span>Departamento asignado</span>
					<?= form_input($departamento_estudiante) ?>
				</div>
			</div>

			<!-- Datos de la organización -->
			<div class="row justify-content-center">

				<div class="text-center col-12">
					<span class="semi-bold text-size-15">
						Datos de la organización
					</span>
				</div>

				<div class="col-md-6 margin_sm_t">
					<span>Nombre</span>
					<?= form_input($nombre) ?>
				</div>

				<div class="col-md-6 margin_sm_t">
					<span>RIF</span>
					<?= form_input($rif) ?>
				</div>

				<div class="col-md-6 margin_sm_t">
					<span>Dirección</span>
					<?= form_textarea($direccion) ?>
				</div>

				<div class="col-md-6 margin_sm_t">
					<span>Teléfono 1</span>
					<?= form_input($telefono1) ?>

					<div class="margin_sm_t">
						<span>Teléfono 2</span>
						<?= form_input($telefono2) ?>
					</div>
				</div>
			</div>

			<!-- Datos del jefe de recursos humanos -->
			<div class="row justify-content-center">
				<div class="col-12 text-center margin_md_t margin_sm_b">
					<span class="semi-bold text-size-15">
						Datos del jefe de recursos humanos
					</span>
					<br>
					<small class="text-muted text-center">
						(O persona que cumple esta función)
					</small>	
				</div>

				<div class="col-md-6 col-lg-5 col-xl-3 margin_sm_t">
					<span>Apellidos</span>
					<?= form_input($jr_apellidos) ?>
				</div>

				<div class="col-md-6 col-lg-5 col-xl-3 margin_sm_t">
					<span>Nombres</span>
					<?= form_input($jr_nombres) ?>
				</div>

				<div class="col-md-6 col-lg-5 col-xl-3 margin_sm_t">
					<span>Correo electrónico</span>
					<?= form_input($jr_correo) ?>
				</div>

				<div class="col-md-6 col-lg-5 col-xl-3 margin_sm_t">
					<span>Teléfono</span>
					<?= form_input($jr_telefono) ?>
				</div>
			</div>

			<!-- Datos del tutor laboral -->
			<div class="row justify-content-center">
				<div class="col-12 text-center margin_md_t margin_sm_b">
					<span class="semi-bold text-size-15">
						Datos del tutor laboral
					</span>	
				</div>

				<div class="col-md-6 col-lg-5 col-xl-4 margin_sm_t">
					<span>Apellidos</span>
					<?= form_input($tl_apellidos) ?>
				</div>

				<div class="col-md-6 col-lg-5 col-xl-4 margin_sm_t">
					<span>Nombres</span>
					<?= form_input($tl_nombres) ?>
				</div>

				<div class="col-md-6 col-lg-5 col-xl-4 margin_sm_t">
					<span>Cédula de identidad</span>
					<?= form_input($tl_ci) ?>
				</div>

				<div class="col-md-6 col-lg-5 col-xl-4 margin_sm_t">
					<span>Cargo</span>
					<?= form_input($tl_cargo) ?>
				</div>

				<div class="col-md-6 col-lg-5 col-xl-4 margin_sm_t">
					<span>Correo electrónico</span>
					<?= form_input($tl_correo) ?>
				</div>

				<div class="col-md-6 col-lg-5 col-xl-4 margin_sm_t">
					<span>Teléfono</span>
					<?= form_input($tl_telefono) ?>
				</div>
			</div>

			<div class="row margin_t">
				<div class="col-5 col-md-6 text-right"><?= form_submit('','Crear',['class'=>'btn btn-success']) ?></div>
				<div class="col-5 col-md-6 text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>					
			</div>
			
		<?= form_close() ?>