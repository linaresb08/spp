		
		<?= validation_errors() ?>

		<?= form_open("/usuarios/nuevoEstudiante") ?>
			<?php
				$primer_nombre = array(
					'name'        => 'primer_nombre', 
					'placeholder' => 'Escribe el primer nombre',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$primer_apellido = array(
					'name'        => 'primer_apellido', 
					'placeholder' => 'Escribe el primer apellido',
					'class'       => 'form-control',
					'required'    => 'true'
				);
				
				$ci = array(
					'name'        => 'ci', 
					'placeholder' => 'Escribe la cédula',
					'type'        => 'number',
					'class'       => 'form-control',
					'required'    => 'true'
				);

				$clave = array(
					'name'        => 'clave', 
					'placeholder' => 'Escribe la clave',
					'type'        => 'password',
					'class'       => 'form-control',
					'required'    => 'true'
				);

			?>

			<div class="row justify-content-center margin-bottom-sm">
				
				<!-- Conjunto de col's para la cédula de identidad -->
				<div class="col-12 col-md-3 col-xl-2 margin-bottom-sm">
					<span>Cédula de Identidad</span>					
				</div>
				<div class="col-4 col-md-2 col-xl-1 margin-bottom-sm">
					<select name="nacionalidad" id="" class="form-control">
						<option value="V">V</option>
						<option value="E">E</option>
					</select>					
				</div>
				<div class="col-8 col-md-7 col-xl-6 margin-bottom-sm">
					<?= form_input($ci) ?>
				</div>

			</div>

			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-12 col-md-4 col-xl-3 margin-bottom-sm">
					<span>Primer nombre</span>
					<?= form_input($primer_nombre) ?>
				</div>

				<div class="col-12 col-md-4 col-xl-3 margin-bottom-sm">
					<span>Primer apellido</span>
					<?= form_input($primer_apellido) ?>
				</div>

				<div class="col-12 col-md-4 col-xl-3 margin-bottom-sm">
					<span>Clave</span>
					<?= form_input($clave) ?>
				</div>
				
			</div>

			<div class="row">

				<div class="col-12 col-xl-9">
					<label class="control-label">
					Selecciona tu(s) carrera(s):
				</label>
				</div>


				<?php foreach ($this->Carrera_model->obtenerCarreras() as $carrera): ?>
					<div class="col-12 col-md-6 col-lg-4 checkbox-radios">
						<div class="form-check">

							<label class="form-check-label" for="carrera_<?= $carrera->carrera_id ?>">

								<input class="form-check-input" type="checkbox" name="carreras[]" value="<?= $carrera->carrera_id ?>" id="carrera_<?= $carrera->carrera_id ?>">
								<span class="form-check-sign"></span>

								 <?= $carrera->nombre ?>

							</label>
						</div>						
					</div>
				<?php endforeach; ?>
			</div>

			<div class="row">

				<div class="col text-right"><?= form_submit('','Registrar',['class'=>'btn btn-success']) ?></div>
				<div class="col text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>
				
			</div>
		<?= form_close() ?>