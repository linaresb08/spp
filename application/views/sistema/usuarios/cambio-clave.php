		<?= validation_errors() ?>

		<?= form_open("/usuarios/cambioClave") ?>
			<?php
				$clave = array(
					'name' => 'clave',
					'placehorder' => 'Escriba su clave actual',
					'type' => 'password',
					'class' => 'form-control'
				);

				$nueva_clave = array(
					'name' => 'nueva_clave',
					'placehorder' => 'Escriba su clave actual',
					'type' => 'password',
					'class' => 'form-control'
				);

				$confirm_nueva_clave = array(
					'name' => 'confirm_nueva_clave',
					'placehorder' => 'Escriba su clave actual',
					'type' => 'password',
					'class' => 'form-control'
				);
			?>
			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-12 col-lg-4 margin_sm_b">
					<span>Clave actual:</span>
					<?= form_input($clave) ?>
				</div>

				<div class="col-12 col-md-6 col-lg-4 margin_sm_b">
					<span>Nueva clave:</span>
					<?= form_input($nueva_clave) ?>
				</div>

				<div class="col-12 col-md-6 col-lg-4 margin_sm_b">
					<span>Confirmar nueva clave:</span>
					<?= form_input($confirm_nueva_clave) ?>
				</div>

			</div>
			<div class="row">

				<div class="col text-right"><?= form_submit('','Registrar',['class'=>'btn btn-success']) ?></div>
				<div class="col text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>
				
			</div>
		<?= form_close() ?>