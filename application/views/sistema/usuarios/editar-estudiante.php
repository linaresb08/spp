		<?= validation_errors() ?>
		
		<?= form_open("/usuarios/editarEstudiante/$id") ?>
			<?php
				$primer_nombre = array(
					'name' => 'primer_nombre', 
					'value' => $perfil->primer_nombre,
					'class' => 'form-control'
				);

				$primer_apellido = array(
					'name' => 'primer_apellido', 
					'value' => $perfil->primer_apellido,
					'class' => 'form-control'
				);
				
				$ci = array(
					'name' => 'ci', 
					'value' => $perfil->ci,
					'type' => 'number',
					'class' => 'form-control'
				);

				$clave = array(
					'name' => 'clave', 
					'value' => $usuario->clave,
					'type' => 'password',
					'class' => 'form-control'
				);
			?>

			<div class="row justify-content-center margin-bottom-sm">

				<!-- Conjunto de col's para la cédula de identidad -->
				<div class="col-12 col-md-3 col-xl-2 margin-bottom-sm">
					<span>Cédula de Identidad</span>					
				</div>

				<div class="col-4 col-md-2 col-xl-1 margin-bottom-sm">
					<select name="nacionalidad" id="" class="form-control">
						<option value="V" <?php if ($perfil->nacionalidad == 'V') { echo "selected"; } ?> >V</option>
						<option value="E" <?php if ($perfil->nacionalidad == 'E') { echo "selected"; } ?> >E</option>
					</select>
				</div>

				<div class="col-8 col-md-7 col-xl-6 margin-bottom-sm">
					<?= form_input($ci) ?>
				</div>
				
			</div>

			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-12 col-md-4 col-xl-3 margin-bottom-sm">
					<span>Primer nombre</span>
					<?= form_input($primer_nombre) ?>
				</div>

				<div class="col-12 col-md-4 col-xl-3 margin-bottom-sm">
					<span>Primer apellido</span>
					<?= form_input($primer_apellido) ?>
				</div>

				<div class="col-12 col-md-4 col-xl-3 margin-bottom-sm">
					<span>Clave</span>
					<?= form_input($clave) ?>
				</div>

			</div>

			<div class="row justify-content-center margin-bottom-sm">

				<div class="col-12 col-md-6 col-xl-5 margin-bottom-sm">
					<span>Rol</span>
					<select name="rol" id="" class="form-control">
						<option value="Administrador" <?php if ($usuario->rol == 'Administrador') { echo "selected"; } ?> >Administrador</option>
						<option value="Coordinador" <?php if ($usuario->rol == 'Coordinador') { echo "selected"; } ?> >Coordinador</option>
						<option value="Estudiante" <?php if ($usuario->rol == 'Estudiante') { echo "selected"; } ?> >Estudiante</option>
						<option value="Invitado" <?php if ($usuario->rol == 'Invitado') { echo "selected"; } ?> >Invitado</option>
						<option value="Tutor académico" <?php if ($usuario->rol == 'Tutor académico') { echo "selected"; } ?> >Tutor académico</option>
					</select>
				</div>

				<div class="col-12 col-md-6 col-xl-4 margin-bottom-sm">
					<span>Estado</span>
					<select name="estado" id="" class="form-control">
						<option value="Habilitado" <?php if ($usuario->estado == 'Habilitado') { echo "selected"; } ?> >Habilitado</option>
						<option value="Inhabilitado" <?php if ($usuario->estado == 'Inhabilitado') { echo "selected"; } ?> >Inhabilitado</option>
					</select>
				</div>
				
				<!-- LA CARRERA DEL ESTUDIANTE DEBE MODIFICARSE A TRAVÉS DE SU PROPIO PERFIL -->
				<div class="col-12 col-xl-9 margin-top-sm">
					<div class="alert alert-warning border-radius-10" role="alert">
						<i class="fa fa-warning"></i>
						<span class="semi-bold text-underline">NOTA:</span>
					  El estudiante podrá modificar su carrera seleccionada a través de la edición de su perfil
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>
				
			</div>
		<?= form_close() ?>


<!-- 		
			<div>
				<span>Primer nombre</span>
				<?= form_input($primer_nombre) ?>
				
				<br>

				<span>Primer apellido</span>
				<?= form_input($primer_apellido) ?>

				<br>

				<span>Cédula de Identidad</span>
				<select name="nacionalidad" id="">
					<option value="V" <?php if ($perfil->nacionalidad == 'V') { echo "selected"; } ?> >V</option>
					<option value="E" <?php if ($perfil->nacionalidad == 'E') { echo "selected"; } ?> >E</option>
				</select>
				-
				<?= form_input($ci) ?>

				<br>

				<span>Clave</span>
				<?= form_input($clave) ?>

				<br>
				
				<span>Rol</span>
				<select name="rol" id="">
					<option value="Administrador" <?php if ($usuario->rol == 'Administrador') { echo "selected"; } ?> >Administrador</option>
					<option value="Coordinador" <?php if ($usuario->rol == 'Coordinador') { echo "selected"; } ?> >Coordinador</option>
					<option value="Estudiante" <?php if ($usuario->rol == 'Estudiante') { echo "selected"; } ?> >Estudiante</option>
					<option value="Invitado" <?php if ($usuario->rol == 'Invitado') { echo "selected"; } ?> >Invitado</option>
					<option value="Tutor académico" <?php if ($usuario->rol == 'Tutor académico') { echo "selected"; } ?> >Tutor académico</option>
				</select>
				
				<br>

				<span>Estado</span>
				<select name="estado" id="">
					<option value="Habilitado" <?php if ($usuario->estado == 'Habilitado') { echo "selected"; } ?> >Habilitado</option>
					<option value="Inhabilitado" <?php if ($usuario->estado == 'Inhabilitado') { echo "selected"; } ?> >Inhabilitado</option>
				</select>
			</div>

			<br><br><br>

			<?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?>
		<?= form_close() ?>
 -->