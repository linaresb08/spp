		<!-- Todos los Usuarios -->

		<div class="row">
			<div class="col-12 semi-bold section-title text-center">
				Usuarios
			</div>

			<div class="dropdown col-12 col-lg-12 col-xl-10 margin_sm">
				  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuNuevo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-plus"></i> Nuevo
				  </button>
				  <div class="dropdown-menu" aria-labelledby="dropdownMenuNuevo">
				    <a class="dropdown-item" href="<?= base_url()?>usuarios/nuevo">
				    	<i class="fa fa-id-card"></i> Usuario administrativo
				    </a>
				    <a class="dropdown-item" href="<?= base_url()?>usuarios/nuevoEstudiante">
				    	<i class="fa fa-id-badge"></i> Usuario estudiante
				    </a>
				  </div>
				</div>
			</div>

<!-- TABLA DE USUARIOS -->
		<?php if (!$segmento): ?>

			<div class="row">

				<div class="col-md-12">
					<div class="card data-tables">
						<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
							<div class="fresh-datatables">

								<?php if ($usuarios): ?>

									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

										<thead>
											<tr>												
												<th>C. I.</th>
												<th>Nombre y Apellido</th>
												<th>Rol</th>
												<th>Estado</th>
												<th class="disabled-sorting text-right">Acciones</th>
											</tr>
										</thead>

										<tfoot>
											<tr>												
												<th>Cédula de I.</th>
												<th>Nombre y Apellido</th>
												<th>Rol</th>
												<th>Estado</th>
												<th class="text-right">Acciones</th>
											</tr>
										</tfoot>

										<tbody>
											<?php foreach ($usuarios as $usuario): ?>
												<tr>
					                <td><?= $usuario->nombre_usuario ?></td>
					                <td>
					                	<?php 
					                	$perfil = $this->Perfil_model->obtenerPerfil_usuario_id($usuario->usuario_id);
					                	?>
					                	<?= $perfil->primer_nombre ." " . $perfil->primer_apellido ?>
					                </td>
					                <td><?= $usuario->rol ?></td>
					                <td><?= $usuario->estado ?></td>
													<td class="text-right">

														<div class="dropdown">
														  <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink-<?= $usuario->usuario_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														    <i class="fa fa-gears"></i>
														  </a>

														  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?= $usuario->usuario_id ?>">
														  	<h6 class="dropdown-header">Acciones</h6>

																<!-- VER -->
															<!--
																<a href="<?= base_url()?>usuarios/index/<?= $usuario->usuario_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-address-card-o"></i> Ver usuario
							                	</a>
							                -->

							                	<!-- EDITAR -->
							                	<?php if($usuario->rol != 'Estudiante'): ?>
																	<a href="<?= base_url()?>usuarios/editar/<?= $usuario->usuario_id ?>" class="dropdown-item width_130 border-radius-10">
																		<i class="fa fa-edit"></i> Modificar usuario
																	</a>
																<?php else: ?>
																	<a href="<?= base_url()?>usuarios/editarEstudiante/<?= $usuario->usuario_id ?>" class="dropdown-item width_130 border-radius-10">
																		<i class="fa fa-edit"></i> Modificar estudiante
																	</a>
																<?php endif; ?>

																<!-- RESTABLECER -->
																<a href="<?= base_url()?>usuarios/restablecer/<?= $usuario->usuario_id ?>" class="dropdown-item width_130 border-radius-10">
																	<i class="fa fa-repeat"></i> Restablecer clave
																</a>

																<!-- HABILITAR / INHABILITAR -->
																<?php if ($usuario->estado == 'Habilitado'): ?>
								                	<a href="<?= base_url()?>usuarios/inhabilitar/<?= $usuario->usuario_id ?>" class="dropdown-item width_130 border-radius-10">
								                		<i class="fa fa-user-times"></i> Inhabilitar					                		
								                	</a>
								                <?php else: ?>
								                	<a href="<?= base_url()?>usuarios/habilitar/<?= $usuario->usuario_id ?>" class="dropdown-item width_130 border-radius-10">
								                		<i class="fa fa-user-plus"></i> Habilitar
								                	</a>
								                <?php endif; ?>

								                <?php if($this->session->userdata('rol') == 'Administrador'): ?>
																	<a href="<?= base_url()?>usuarios/eliminar/<?= $usuario->usuario_id ?>" class="dropdown-item width_130 border-radius-10">
																		<i class="fa fa-trash"></i> Eliminar usuario
																	</a>
																<?php endif; ?>
															</div>
														</div>

														

							                	

													</td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
								<?php else: ?>
									<div class="alert alert-warning border-radius-10" role="alert">
										¡No hay usuarios registrados!
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			
			</div>

		<?php else: ?>
			<div class="row justify-content-center">

				<div class="col-12 col-md-4 text-center">
					<h6 class="semi-bold section-message"> Datos de Usuario	</h6>
					<p>
						<strong>Usuario: </strong> <?= $usuario->nombre_usuario ?> <br>
						<strong>Clave: </strong> <?= $usuario->clave ?> <br>
						<strong>Rol: </strong> <?= $usuario->rol ?> <br>					
						<strong>Estado: </strong> <?= $usuario->estado ?> <br>				
					</p>
				</div>
				<div class="col-12 col-md-4 text-center">
					<h6 class="semi-bold section-message"> Datos del Perfil </h6>
					<p>
						<strong>Nombre: </strong> <?= $perfil->primer_nombre ?> <br>
						<strong>Apellido: </strong> <?= $perfil->primer_apellido ?> <br>
						<strong>Cédula: </strong> <?= $perfil->nacionalidad ?> - <?= $perfil->ci ?><br>					
					</p>
				</div>

			</div>

		<?php endif; ?>