<div class="row justify-content-center">
	<div class="col-md-6 errores text-center">
		<?= validation_errors() ?>
	</div>
</div>

<?= form_open("/actividades/nueva") ?>
	<?php 
		$fecha = array(
			'name'        => 'fecha', 
			'id'          => 'fecha',
			'type'        => 'date',
			'placeholder' => '',
			'class'       => 'form-control'
		);

		$fecha_inicio = array(
			'name'        => 'fecha_inicio', 
			'id'          => 'fecha_inicio',
			'type'        => 'text',
			'placeholder' => '',
			'readonly' => 'true',
			'disabled' => 'true',
			'class'       => 'form-control'
		);

		$fecha_fin = array(
			'name' 				=> 'fecha_fin', 
			'id' 					=> 'fecha_fin',
			'type'			  => 'text',
			'placeholder' => '',
			'readonly' => 'true',
			'disabled' => 'true',
			'class' 			=> 'form-control'
		);

		$descripcion = array(
			'name' 				=> 'descripcion', 
			'id'          => 'descripcion',
			'placeholder' => 'Escribe la dirección',
			'class'       => 'form-control',
			'rows'        => '5'
		);

		$observacion = array(
			'name' 				=> 'observacion', 
			'id'					=> 'observacion',
			'placeholder' => 'Escribe la observación',
			'class'       => 'form-control',
			'rows'        => '5'
		);
	?>

	<!-- Campos con los datos de la empresa -->
	<div class="row justify-content-center">
		<div class="col-12 text-center margin_sm_b-t">
			<span class="semi-bold">
				Datos de la actividad
			</span>	
		</div>
	</div>

	<div class="row justify-content-center">
		<div class="col-md-6 col-xl-5 margin_sm_t">
			<span>Pasantía</span>
			<div class="input-group">
				<select name="pasantia_id" id="pasantia" class="form-control">
					<option value="">Seleccione</option>
					<?php foreach($pasantias as $pasantia): ?>
						<?php $empresa = $this->Empresa_model->obtenerEmpresa_por_pasantia($pasantia->pasantia_id) ?>
						<?php $periodo = $this->Periodo_model->obtenerPeriodo($pasantia->realizacion_periodo_id ) ?>
						<option value="<?= $pasantia->pasantia_id ?>"><?= $periodo->nombre . ' - ' . $empresa->nombre ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>

		<div class="col-md-6 col-xl-3 margin_sm_t display-none">
			<span>Tipo de reporte</span>
			<div class="input-group">
				<select name="tipo" id="tipo_actividad" class="form-control">
					<option value="">Seleccione</option>
					<option value="Diaria">Diario</option>
					<option value="Semanal">Semanal</option>
				</select>
			</div>
		</div>

		<div class="col-md-6 col-xl-2 margin_sm_t display-none obligatorio">
			<span>Fecha</span>
			<div class="input-group">
				<?= form_input($fecha) ?>
			</div>
		</div>
	</div>

	<div class="row justify-content-center">

		<div class="col-md-6 col-xl-5 margin_sm_t display-none">
			<span>Fecha de Inicio</span>
			<div class="input-group">
				<?= form_input($fecha_inicio) ?>
			</div>
		</div>

		<div class="col-md-6 col-xl-5 margin_sm_t display-none">
			<span>Fecha de Culminación</span>
			<div class="input-group">
				<?= form_input($fecha_fin) ?>
			</div>
		</div>
	</div>

	<div class="row justify-content-center">
		<div class="col-md-6 col-xl-5 margin_sm_t display-none obligatorio">
			<span>Descripción</span>
			<?= form_textarea($descripcion) ?>
		</div>

		<div class="col-md-6 col-xl-5 margin_sm_t display-none obligatorio">
			<span>Observación</span>
			<?= form_textarea($observacion) ?>
		</div>
	</div>

	<div class="row margin_t">
		<div class="col-5 col-md-6 text-right"><?= form_submit('','Crear',['class'=>'btn btn-success']) ?></div>
		<div class="col-5 col-md-6 text-left"><?= form_reset('','Limpiar campos',['class'=>'btn btn-warning']) ?></div>
	</div>
<?= form_close() ?>

<script>
	$('#pasantia').change(function () {
		var id = $(this).val();
		var fecha_container = $('#fecha').parents('.display-none');
		$.ajax({
            type:"post",
            url: "<?php echo base_url(); ?>pasantias/select_pasantias",
            data:{id: id},
            success:function(response)
            {
            	data = JSON.parse(response);

            	if (data) {
            		if (data == 'Ninguna') {
            			fecha_container.removeClass('col-xl-5');
            			fecha_container.addClass('col-xl-2');
            			$('#tipo_actividad').parents('.display-none').fadeIn();
            		}else{
            			fecha_container.removeClass('col-xl-2');
            			fecha_container.addClass('col-xl-5');
            			$('.obligatorio').fadeIn();
            			if (data == 'Semanal') {
            				$('#fecha_inicio, #fecha_fin').parents('.display-none').fadeIn();
            			}
            		}
            	}else {
            		$('.display-none').fadeOut();
            		$('#tipo_actividad, #fecha, #fecha_inicio, #fecha_fin, #descripcion, #observacion').val('');
            	}
            },
            error: function() 
            {
              console.log('Error');
            }
        	});
	});

	$('#tipo_actividad').change(function () {
		if ($(this).val() != '') {
			$('.obligatorio').fadeIn();
			if ($(this).val() == 'Semanal') {
				$('#fecha_inicio, #fecha_fin').parents('.display-none').fadeIn();
			}else {
				$('#fecha_inicio, #fecha_fin').parents('.display-none').fadeOut();
			}
		}else {
			$('.obligatorio').fadeOut();
			$('#fecha_inicio, #fecha_fin').parents('.display-none').fadeOut();
		}
	});

	$('#fecha').change(function () {
		var inicio = new Date($(this).val());
		var fin = new Date($(this).val());

		inicio.setDate(inicio.getDate() - inicio.getDay() + 1);
		fin.setDate(inicio.getDate() - inicio.getDay() + 5);

		fecha_inicio = inicio.getDate() + '/' + (inicio.getMonth() + 1) + '/' + inicio.getFullYear();
		fecha_fin = fin.getDate() + '/' + (fin.getMonth() + 1) + '/' + fin.getFullYear();

		$('#fecha_inicio').val(fecha_inicio);
		$('#fecha_fin').val(fecha_fin);
	});

	$('.errores p').addClass('alert alert-warning');
</script>