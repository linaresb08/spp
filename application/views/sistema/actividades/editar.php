<?= validation_errors() ?>

<?= form_open("/actividades/editar/$id") ?>
	<?php 
		$fecha = array(
			'name'        => 'fecha', 
			'id'          => 'fecha',
			'type'        => 'date',
			'placeholder' => '',
			'class'       => 'form-control',
			'value'       => $actividad->fecha_inicio
		);

		 if ($pasantia->tipo_reporte_actividad == 'Semanal'){
		 	$fecha_inicio = array(
				'id'          => 'fecha_inicio',
				'type'        => 'text',
				'placeholder' => '',
				'readonly'    => 'true',
				'disabled'    => 'true',
				'class'       => 'form-control',
				'value'       => date("d/m/Y", strtotime($actividad->fecha_inicio))
			);

			$fecha_fin = array(
				'id'          => 'fecha_fin',
				'type'        => 'text',
				'placeholder' => '',
				'readonly'    => 'true',
				'disabled'    => 'true',
				'class'       => 'form-control',
				'value'       => date("d/m/Y", strtotime($actividad->fecha_fin))
			);
		 }
			
		$descripcion = array(
			'name'        => 'descripcion', 
			'id'          => 'descripcion',
			'placeholder' => 'Escribe la dirección',
			'class'       => 'form-control',
			'rows'        => '5',
			'value'       => $actividad->descripcion
		);

		$observacion = array(
			'name' 				=> 'observacion', 
			'id'					=> 'observacion',
			'placeholder' => 'Escribe la observación',
			'class'       => 'form-control',
			'rows'        => '5',
			'value'       => $actividad->observacion
		);
	?>

	<!-- Campos con los datos de la empresa -->
	<div class="row justify-content-center">
		<div class="col-12 text-center margin_sm_b-t">
			<span class="semi-bold">
				Datos de la actividad
			</span>	
		</div>
	</div>

	<div class="row justify-content-center">
		<div class="col-md-6 col-xl-4 margin_sm_t">
			<span>Fecha</span>
			<div class="input-group">
				<?= form_input($fecha) ?>
			</div>
		</div>

		<?php if ($pasantia->tipo_reporte_actividad == 'Semanal'): ?>
			<div class="col-md-6 col-xl-3 margin_sm_t">
				<span>Fecha de Inicio</span>
				<div class="input-group">
					<?= form_input($fecha_inicio) ?>
				</div>
			</div>

			<div class="col-md-6 col-xl-3 margin_sm_t">
				<span>Fecha de Culminación</span>
				<div class="input-group">
					<?= form_input($fecha_fin) ?>
				</div>
			</div>
		<?php endif; ?>
	</div>

	<div class="row justify-content-center">
		<div class="col-md-6 col-xl-5 margin_sm_t">
			<span>Descripción</span>
			<?= form_textarea($descripcion) ?>
		</div>

		<div class="col-md-6 col-xl-5 margin_sm_t">
			<span>Observación</span>
			<?= form_textarea($observacion) ?>
		</div>
	</div>

	<div class="row margin_t">
		<div class="col text-center"><?= form_submit('','Actualizar',['class'=>'btn btn-success']) ?></div>
	</div>
<?= form_close() ?>

<?php if ($pasantia->tipo_reporte_actividad == 'Semanal'): ?>
	<script>
		$('#fecha').change(function () {
			var inicio = new Date($(this).val());
			var fin = new Date($(this).val());

			inicio.setDate(inicio.getDate() - inicio.getDay() + 1);
			fin.setDate(inicio.getDate() - inicio.getDay() + 5);

			fecha_inicio = inicio.getDate() + '/' + (inicio.getMonth() + 1) + '/' + inicio.getFullYear();
			fecha_fin = fin.getDate() + '/' + (fin.getMonth() + 1) + '/' + fin.getFullYear();

			$('#fecha_inicio').val(fecha_inicio);
			$('#fecha_fin').val(fecha_fin);
		});
	</script>
<?php endif; ?>