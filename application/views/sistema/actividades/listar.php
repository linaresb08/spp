<?php if ($segmento): ?>
	<!-- TODAS LAS actividades -->
	<div class="row">

		<!-- Título -->
		<div class="col-12 semi-bold section-title text-center">
			Actividades
		</div>
		
		<!-- Botón para añadir actividades -->
		<div class="col-12">
			<a class="btn btn-primary float-right" role="button" href="<?= base_url()?>actividades/nueva">
				<i class="fa fa-plus"></i> Nueva actividad
			</a>
		</div>
	</div>

	<!-- Tabla de las actividades -->
	<div class="row justify-content-center">
		<div class="col-md-12 col-lg-11 col-xl-10">
			<?php if ($actividades): ?>
				<div class="card data-tables">
					<div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
						<div class="fresh-datatables">
							<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">

								<thead>
									<tr>
										<th>Fecha de Inicio</th>		
										<th>Fecha de Culminación</th>		
										<th>Descripción</th>
										<th class="disabled-sorting">Acciones</th>
									</tr>
								</thead>

								<tfoot>
									<tr>
										<th>Fecha de Inicio</th>		
										<th>Fecha de Culminación</th>		
										<th>Descripción</th>	
										<th class="disabled-sorting">Acciones</th>
									</tr>
								</tfoot>

								<tbody>
									<?php foreach ($actividades as $actividad): ?>
										<tr>
											
											<!-- FECHA DE INICIO -->
											<td>
												<?php $fecha_inicio = new DateTime($actividad->fecha_inicio); ?>
												<?= $fecha_inicio->format('d-m-Y'); ?>
											</td>

											<!--  FECHA DE FIN -->
											<td>
												<?php $fecha_fin = new DateTime($actividad->fecha_fin); ?>
												<?= $fecha_fin->format('d-m-Y'); ?>
											</td>

											<!-- DESCRIPCIÓN -->
											<td>
												<?php if(strlen($actividad->descripcion) > 20):  ?>
													<?= substr($actividad->descripcion, 0, 20); ?>...
												<?php else: ?>
													<?= $actividad->descripcion ?>
												<?php endif; ?>	
											</td>

											<!-- Acciones -->
											<td>
												<div class="dropdown">
												  <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink-<?= $actividad->actividad_id ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-gears"></i>
												  </a>

												  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-<?= $actividad->actividad_id ?>">
												  	<h6 class="dropdown-header">Acciones</h6>
												    <a class="dropdown-item width_130 border-radius-10 btn-modal" href="#modal-actividad" data-toggle="modal" data-target="#modal-actividad" data-actividad="<?= $actividad->actividad_id ?>">
												    	<i class="fa fa-eye"></i> Ver														 
												    </a>
												    <a class="dropdown-item width_130 border-radius-10" href="<?= base_url(); ?>actividades/editar/<?= $actividad->actividad_id ?>">
												    	<i class="fa fa-edit"></i> Modificar
												    </a>
												    <a class="dropdown-item width_130 border-radius-10" href="<?= base_url(); ?>actividades/eliminar/<?= $actividad->actividad_id ?>">
												    	<i class="fa fa-trash"></i> Eliminar
												    </a>
												  </div>
												</div>
											</td>
										</tr>												
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			<?php else: ?>
				<div class="row justify-content-center">						
					<div class="alert alert-warning border-radius-10 text-center text-size-12" role="alert">
						<span class="semi-bold">
							<i class="fa fa-warning"></i><br>
							¡No hay actividades registradas!
						</span>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php else: ?>
	<?php foreach($pasantias as $pasantia): ?>
		<?php $empresa = $this->Empresa_model->obtenerEmpresa_por_pasantia($pasantia->pasantia_id) ?>
		<?php $periodo = $this->Periodo_model->obtenerPeriodo($pasantia->realizacion_periodo_id ) ?>
		<a href="<?= base_url('actividades') . '/index/' . $pasantia->pasantia_id ?>" class="btn btn-info" role="button"><?= $periodo->nombre . ' - ' . $empresa->nombre ?></a>
	<?php endforeach; ?>
<?php endif; ?>


<!-- Modal -->
<div class="modal fade" id="modal-actividad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">												     
        <h5 class="modal-title semi-bold" id="exampleModalLabel">								        	
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fa fa-times"></i>
        </button>											        
      </div>
      <div class="modal-body">
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-12 semi-bold">
        			Descripción
        		</div>
        	</div>
        	<div class="row">
        		<div id="descripcion" class="col-12"></div>
        	</div>
        	<div class="row">
        		<div class="col-12 semi-bold">
        			Observación
        		</div>
        	</div>
        	<div class="row">
        		<div id="observacion" class="col-12"></div>
        	</div>
			  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script>

	function formato_fecha(fecha) {
		nueva_fecha = new Date(fecha);

		nueva_fecha = nueva_fecha.getDate() + '/' + (nueva_fecha.getMonth() + 1) + '/' + nueva_fecha.getFullYear();

		return nueva_fecha;
	}

	$('.btn-modal').click(function () {
		var id     = $(this).data('actividad');
		var tipo   = 'Diaria';
		var titulo = 'Reporte de Actividad <br>';

		$('#modal-actividad .modal-title, #modal-actividad #descripcion, #modal-actividad #observacion').empty();

		$.ajax({
            type:"post",
            url: "<?php echo base_url(); ?>actividades/obtener",
            data:{id: id},
            success:function(response)
            {
            	data = JSON.parse(response);
            	if (data) {
            		if (data.dia == '') {
            			tipo = 'Semanal';
            		}

            		if (tipo == 'Diaria') {
            			titulo += data.dia + ': ' + formato_fecha(data.fecha_inicio);
            		}else{
            			titulo += formato_fecha(data.fecha_inicio) + ' - ' + formato_fecha(data.fecha_fin);
            		}
            		$('#modal-actividad .modal-title').html(titulo);
            		$('#modal-actividad #descripcion').html(data.descripcion);
            		$('#modal-actividad #observacion').html(data.observacion);
            		console.log(data);
            	}            	
            },
            error: function() 
            {
              console.log('Error');
            }
        	});
	});
</script>