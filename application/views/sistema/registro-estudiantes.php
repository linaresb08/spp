<!DOCTYPE html>
<html lang="es">
	<head>

		<!-- Título de la ventana -->
	<title>Registro Estudiantil | Sistema de Prácticas Profesionales</title>
	
	<!-- Favicon -->
	<link href='<?= base_url() ?>assets/img/portal/logo.png' rel='shortcut icon' type='image/png'/>

		<!-- Meta etiquetas -->
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

		<!-- Programadora -->
	<meta name="author" content="Betzabeth Linares">

		<!-- Descripción -->
	<meta name="description" content="Sistema web para la Coordinación de Prácticas Profesionales de la Universidad Valle del Momboy">

		<!-- Palabras claves -->
	<meta name="keywords" content="uvm, practicas profesionales, universidad valle del momboy, pasantias, sistema de practicas profesionales">


	<!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="Sistema Web de Prácticas Profesionales UVM">  
	<meta itemprop="image" content="http://uvm.edu.ve/img/logo.png">

	<!-- Links -->

	<!-- Canonical SEO -->
	<link rel="canonical" href="<?= base_url() ?>" />

	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>assets/img/apple-icon.png">
	
	<link rel="icon" type="image/png" href="<?= base_url() ?>assets/img/favicon.png">   


	<!--     Fonts and icons     -->
	<link href="https://fonts.googleapis.com/css?family=Dosis:200,400,600" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
	<link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?= base_url() ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/mystyles.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/containers.css">
	</head>

	<body>

		<div class="container">
			<div class="container-fluid">

				<div class="row">
					<div class="col text-center">
						<span class="section-title text-center">Registro Estudiantil</span>
					</div>
				</div>

				<!-- Muestra los mensajes de validación -->
				<?= validation_errors() ?>

				<div class="row justify-content-center">
					<div class="col-sm-10 col-md-8">
						<form action="<?= base_url() ?>dashboard/registrar" method="post">
							
							<div id="accordion">
								<!-- #1 Información personal -->
							  <div class="card no-margin">
							    <div class="card-header cursor-pointer bg-gris" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							      <h5 class="mb-0">
							      <!-- Por si acaso, el botón funciona
							        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							          Información personal
							        </button>
							      -->
								     	<p>
										    Información personal
										  </p>
							      </h5>
							    </div>

							    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							      <div class="card-body">
							        
							      	<!-- Fila para los inputs de la cédula -->
							        <div class="row justify-content-center">
												<div class="col-12 col-md-3 col-xl-2 margin-bottom-sm">
													<span>Cédula de Identidad</span>					
												</div>
												<div class="col-4 col-md-2 margin-bottom-sm">
													<select name="nacionalidad" id="" class="form-control">
														<option value="V">V</option>
														<option value="E">E</option>
													</select>					
												</div>
												<div class="col-8 col-md-7 col-xl-6 margin-bottom-sm">
													<input type="number" name="ci" placeholder="Escribe la cédula" class="form-control" required="true">
												</div>
							        </div>

							        <div class="row justify-content-center">
							        	<div class="col-12 col-md-6 col-xl-3 margin-bottom-sm">
													<span>Primer nombre</span>
													<input type="text" name="primer_nombre" placeholder="Escribe el primer nombre" class="form-control" required="true">
												</div>

												<div class="col-12 col-md-6 col-xl-3 margin-bottom-sm">
													<span>Segundo nombre</span>
													<input type="text" name="segundo_nombre" placeholder="Escribe el segundo nombre" class="form-control" required="true">
												</div>

												<div class="col-12 col-md-6 col-xl-3 margin-bottom-sm">
													<span>Primer apellido</span>
													<input type="text" name="primer_apellido" placeholder="Escribe el primer apellido" class="form-control" required="true">
												</div>

												<div class="col-12 col-md-6 col-xl-3 margin-bottom-sm">
													<span>Segundo apellido</span>
													<input type="text" name="segundo_apellido" placeholder="Escribe el segundo apellido" class="form-control" required="true">
												</div>										
							        </div>
											
										<!-- el boton funciona
							        <button class="btn btn-link collapsed"  type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							          Siguiente
							        </button>
							      -->
								      <div class="row justify-content-right">
									      <div class="col text-center margin_sm_t">
									      	<a class="btn btn-success font-color-white semi-bold" data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="false" aria-controls="collapseTwo">
												    Siguiente
												    <i class="fa fa-angle-right"></i>
												  </a>
									      </div>
								      </div>
							      </div>
							    </div>
							  </div>

							  <!-- #2 Información académica -->
							  <div class="card no-margin">
							    <div class="card-header cursor-pointer bg-gris" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							      <h5 class="mb-0">
							      <!--
							        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							          Información académica
							        </button>
							      -->
							      <p>
							      	Información académica
							      </p>

							      </h5>
							    </div>
							    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							      <div class="card-body">

							       	<div class="row">
												<div class="col-12">
													<span class="no-padding">
													Selecciona tu(s) carrera(s):
												</span>
												</div>

											<?php foreach ($this->Carrera_model->obtenerCarreras() as $carrera): ?>
												<div class="col-12 col-md-4 checkbox-radios">

													<div class="form-check">
														<label class="form-check-label" for="carrera_<?= $carrera->carrera_id ?>">
															<input class="form-check-input" type="checkbox" name="carreras[]" value="<?= $carrera->carrera_id ?>" id="carrera_<?= $carrera->carrera_id ?>">
															<span class="form-check-sign"></span>
															 <?= $carrera->nombre ?>
														</label>
													</div>	
												</div>											
											<?php endforeach; ?>

											</div>


											<div class="row justify-content-right">

									      <div class="col text-center margin_sm_t">

									      	<a class="btn btn-secondary font-color-white semi-bold" data-toggle="collapse" href="#collapseOne" role="button" aria-expanded="true" aria-controls="collapseOne">
											    <i class="fa fa-angle-left"></i>
									          Anterior
												  </a>

									        <a class="btn btn-success font-color-white semi-bold" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">
												    Siguiente
												    <i class="fa fa-angle-right"></i>
												  </a>

									      </div>
										<!--
											<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							          Siguiente
							        </button>
										-->
							      </div>
							    </div>
							  </div>

							  <!-- #3 Información del usuario -->
							  <div class="card no-margin">
							    <div class="card-header cursor-pointer bg-gris" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							      <h5 class="mb-0">
							      <!--
							        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							          Información del usuario
							        </button>
							      -->
							      	<p>
							      		Información del usuario
							      	</p>

							      </h5>
							    </div>
							    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							      <div class="card-body">
							        <div class="row justify-content-center">
							        	<!-- Clave del usuario -->
							        	<div class="col-12 col-md-6 col-xl-3 margin-bottom-sm">
													<span>Clave</span>
													<input type="password" name="clave" value="" placeholder="Escribe la clave para iniciar sesión" class="form-control" required="true">
												</div>        	
							        </div>

							        <div class="row justify-content-right">
									      <div class="col text-center margin_sm_t">
									      	<a class="btn btn-secondary font-color-white semi-bold" data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="true" aria-controls="collapseTwo">
											    <i class="fa fa-angle-left"></i>
									          Anterior
												  </a>

												  <input type="submit" class="btn btn-success semi-bold" value="Enviar">
									      </div>
								      </div>

							      </div>
							    </div>
							  </div>
							</div>
						</form>
					</div>
				</div>

			</div>			
		</div>

	</body>
<!--   Core JS Files   -->
<script src="<?= base_url() ?>assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
<!--  Chartist Plugin  -->
<script src="<?= base_url() ?>assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-notify.js"></script>
<!--  Share Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.sharrre.js"></script>
<!--  jVector Map  -->
<script src="<?= base_url() ?>assets/js/plugins/jquery-jvectormap.js" type="text/javascript"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?= base_url() ?>assets/js/plugins/moment.min.js"></script>
<!--  DatetimePicker   -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-datetimepicker.js"></script>
<!--  Sweet Alert  -->
<script src="<?= base_url() ?>assets/js/plugins/sweetalert2.min.js" type="text/javascript"></script>
<!--  Tags Input  -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-tagsinput.js" type="text/javascript"></script>
<!--  Sliders  -->
<script src="<?= base_url() ?>assets/js/plugins/nouislider.js" type="text/javascript"></script>
<!--  Bootstrap Select  -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  jQueryValidate  -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.validate.min.js" type="text/javascript"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--  Bootstrap Table Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/bootstrap-table.js"></script>
<!--  DataTable Plugin -->
<script src="<?= base_url() ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<!--  Full Calendar   -->
<script src="<?= base_url() ?>assets/js/plugins/fullcalendar.min.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!-- Light Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= base_url() ?>assets/js/demo.js"></script>
<script>
	$(document).ready(function() {
		demo.checkFullPageBackgroundImage();

		setTimeout(function() {
			// after 1000 ms we add the class animated to the login/register card
			$('.card').removeClass('card-hidden');
		}, 700)
	});
</script>
<script type="text/javascript">
    $(document).ready(function() {
        // Init Wizard
        demo.initLBDWizard();
    });
</script>
<!-- Facebook Pixel Code Don't Delete -->
<script>
	! function(f, b, e, v, n, t, s) {
		if (f.fbq) return;
		n = f.fbq = function() {
			n.callMethod ?
				n.callMethod.apply(n, arguments) : n.queue.push(arguments)
		};
		if (!f._fbq) f._fbq = n;
		n.push = n;
		n.loaded = !0;
		n.version = '2.0';
		n.queue = [];
		t = b.createElement(e);
		t.async = !0;
		t.src = v;
		s = b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t, s)
	}(window,
		document, 'script', '//connect.facebook.net/en_US/fbevents.js');

	try {
		fbq('init', '111649226022273');
		fbq('track', "PageView");

	} catch (err) {
		console.log('Facebook Track Error:', err);
	}
</script>
<noscript>
	<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=111649226022273&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->

</html>