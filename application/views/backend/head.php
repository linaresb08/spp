
<!DOCTYPE html>
<html lang="es">

<head>
  <!-- Título de la ventana -->
  <title>Sistema de Prácticas Profesionales</title>

  <!-- Favicon -->
  <link href='<?= base_url() ?>assets/img/portal/logo.png' rel='shortcut icon' type='image/png'/>

  <!-- Meta etiquetas -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' />

  <!--     Fonts     -->
  <link href="https://fonts.googleapis.com/css?family=Dosis:200,400,600" rel="stylesheet">

  <!--     Icons     -->

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
  
<!-- 
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
-->

  <!-- CSS Files -->
  <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?= base_url() ?>assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/mystyles.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/containers.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/stepper.css">


  <!--   Core JS Files   -->
  <script src="<?= base_url() ?>assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/js/stepper.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="<?= base_url() ?>assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Chartist Plugin  -->
  <script src="<?= base_url() ?>assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?= base_url() ?>assets/js/plugins/bootstrap-notify.js"></script>
  <!--  Share Plugin -->
  <script src="<?= base_url() ?>assets/js/plugins/jquery.sharrre.js"></script>
  <!--  jVector Map  -->
  <script src="<?= base_url() ?>assets/js/plugins/jquery-jvectormap.js" type="text/javascript"></script>
  <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
  <script src="<?= base_url() ?>assets/js/plugins/moment.min.js"></script>
  <!--  DatetimePicker   -->
  <script src="<?= base_url() ?>assets/js/plugins/bootstrap-datetimepicker.js"></script>
  <!--  Sweet Alert  -->
  <script src="<?= base_url() ?>assets/js/plugins/sweetalert2.min.js" type="text/javascript"></script>
  <!--  Tags Input  -->
  <script src="<?= base_url() ?>assets/js/plugins/bootstrap-tagsinput.js" type="text/javascript"></script>
  <!--  Sliders  -->
  <script src="<?= base_url() ?>assets/js/plugins/nouislider.js" type="text/javascript"></script>
  <!--  Bootstrap Select  -->
  <script src="<?= base_url() ?>assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
  <!--  jQueryValidate  -->
  <script src="<?= base_url() ?>assets/js/plugins/jquery.validate.min.js" type="text/javascript"></script>
  <!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="<?= base_url() ?>assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--  Bootstrap Table Plugin -->
  <script src="<?= base_url() ?>assets/js/plugins/bootstrap-table.js"></script>
  <!--  DataTable Plugin -->
  <script src="<?= base_url() ?>assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--  Full Calendar   -->
  <script src="<?= base_url() ?>assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?= base_url() ?>assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
  <!-- Light Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?= base_url() ?>assets/js/demo.js"></script>

  <!-- ACTIVA LOS TOOLTIPS -->

  <script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
  </script>

  <!-- SCRIPT NECESARIO PARA PODER USAR LAS TABLAS -->

  <script type="text/javascript">
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [5,10, 25, 50, -1],
          [5,10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar registros",
        }
      });
        
      var table = $('#datatables').DataTable();
        // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');

        if ($tr.hasClass('child')) {
            $tr = $tr.prev('.parent');
        }

        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });
    });
  </script>

  <!-- CIERRE DEL SCRIPT NECESARIO PARA PODER USAR LAS TABLAS -->

  <!-- Facebook Pixel Code Don't Delete -->
  <script>
      ! function(f, b, e, v, n, t, s) {
          if (f.fbq) return;
          n = f.fbq = function() {
              n.callMethod ?
                  n.callMethod.apply(n, arguments) : n.queue.push(arguments)
          };
          if (!f._fbq) f._fbq = n;
          n.push = n;
          n.loaded = !0;
          n.version = '2.0';
          n.queue = [];
          t = b.createElement(e);
          t.async = !0;
          t.src = v;
          s = b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t, s)
      }(window,
          document, 'script', '//connect.facebook.net/en_US/fbevents.js');

      try {
          fbq('init', '111649226022273');
          fbq('track', "PageView");

      } catch (err) {
          console.log('Facebook Track Error:', err);
      }
  </script>
  <noscript>
      <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=111649226022273&ev=PageView&noscript=1" />
  </noscript>
  <!-- End Facebook Pixel Code -->
      
</head>

<body>
