    <div class="wrapper">
      <div class="sidebar" data-color="verde-uvm" data-image="../assets/img/sidebar-5.jpg">
        <!--
          Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

          Tip 2: you can also add an image using data-image tag
        -->

        <!-- Sidebar -->
        <div class="sidebar-wrapper">

          <!-- Logo -->
          <div class="logo text-center">
            <img src="<?= base_url() ?>assets/img/portal/logo.png" alt="Logo UVM" width="60px;" height="60px;">
            <a href="http://uvm.edu.ve/" class="simple-text logo-normal">
              Universidad Valle del Momboy
            </a>
          </div>

          <!-- Datos del usuario -->
          <div class="user">

            <!-- Foto del usuario -->
            <div class="photo">
              <?php if ($this->perfil_session->foto): ?> 
                <img src="<?= base_url() ?>profile/images/<?= $this->perfil_session->foto; ?>" alt="Foto de perfil de <?= $this->perfil_session->primer_nombre ?> <?= $this->perfil_session->primer_apellido ?>" width="34" height="34">
              <?php else: ?>
                <i class="fa fa-user-circle text-size-23"></i>
              <?php endif; ?>
            </div>

            <!-- Información del usuario -->
            <div class="info ">
              <a data-toggle="collapse" href="#collapsePerfil" class="collapsed">
                <span><?= $this->perfil_session->primer_nombre ?> <?= $this->perfil_session->primer_apellido?>
                  <b class="caret"></b>
                </span>
              </a>
              <div class="collapse" id="collapsePerfil">
                <ul class="nav">

                  <!-- Mi perfil -->
                  <li>
                    <a class="profile-dropdown" href="<?= base_url()?>perfiles">
                      <span class="sidebar-mini">MP</span>
                      <span class="sidebar-normal">Mi perfil</span>
                    </a>
                  </li>

                  <!-- Editar perfil -->
                  <li>
                    <a class="profile-dropdown" href="<?= base_url()?>perfiles/editar">
                      <span class="sidebar-mini">EP</span>
                      <span class="sidebar-normal">Editar perfil</span>
                    </a>
                  </li>

                  <!-- Subir/Cambiar foto de perfil -->
                  <li>
                    <a class="profile-dropdown" href="<?= base_url()?>imagenes/seleccionar">
                      <?php if ($this->perfil_session->foto): ?> 
                        <span class="sidebar-mini">CF</span>
                        <span class="sidebar-normal">Cambiar foto de perfil</span>
                      <?php else: ?>
                        <span class="sidebar-mini">SF</span>
                        <span class="sidebar-normal">Subir foto de perfil</span>
                      <?php endif; ?>
                    </a>
                  </li>

                  <!-- Cambiar clave -->
                  <li>
                    <a class="profile-dropdown" href="<?= base_url()?>usuarios/cambioClave">
                      <span class="sidebar-mini">CC</span>
                      <span class="sidebar-normal">Cambiar clave</span>
                    </a>                    
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <!-- Barra de navegación Vertical -->
          <ul class="nav">

            <!-- Panel de control -->
            <li class="nav-item ">
              <a class="nav-link" href="<?= base_url()?>dashboard">
                <i class="fa fa-tachometer"></i>
                <p>Panel de control</p>
              </a>
            </li>

            <!-- Gestor de censos (admin) / Censos (estudiante) -->
            <?php if($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador'): ?>
              <li class="nav-item ">
                <a class="nav-link" href="<?= base_url()?>censos">
                  <i class="fa fa-list-alt"></i>
                  <p>Gestor de censos</p>
                </a>
              </li>
            <?php else: ?>
              <li class="nav-item ">
                <a class="nav-link" href="<?= base_url()?>censos">
                  <i class="fa fa-list-alt"></i>
                  <p>Censos</p>
                </a>
              </li>
            <?php endif; ?>

            <!-- Gestor de actividades (estudiantes) -->
            <?php if($this->session->userdata('rol') == 'Estudiante'): ?>
              <?php if($this->Pasantia_model->obtenerPasantias_por_estudiante($this->perfil_session->usuario_id)): ?>
                <li class="nav-item ">
                  <a class="nav-link" href="<?= base_url()?>actividades">
                    <i class="fa fa-book"></i>
                    <p>Gestor de actividades</p>
                  </a>
                </li>
              <?php endif; ?>
            <?php endif; ?>

            <!-- Gestor de pasantías (admin) / (estudiantes) -->
            <?php if($this->session->userdata('rol') != 'Invitado'): ?>
              <li class="nav-item ">
                <a class="nav-link" href="<?= base_url()?>pasantias">
                  <i class="fa fa-briefcase"></i>
                  <p>Gestor de pasantías</p>
                </a>
              </li>
            <?php endif; ?>

            <!-- Gestor de publicaciones (admin) -->
            <?php if($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador'): ?>
              <li class="nav-item ">
                <a class="nav-link" href="<?= base_url()?>publicaciones">
                  <i class="fa fa-files-o"></i>
                  <p>Gestor de publicaciones</p>
                </a>
              </li>
            <?php endif; ?>

            <!-- Gestor de requisitos (admin) -->                        
            <?php if($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador'): ?>
              <li class="nav-item ">
                <a class="nav-link" href="<?= base_url()?>requisitos">
                  <i class="fa fa-list-ol"></i>
                  <p>Gestor de requisitos</p>
                </a>
              </li>
            <?php endif;?>

            <!-- Gestor de requisitos (estudiante) -->                       
            <?php if($this->session->userdata('rol') == 'Estudiante'): ?>
              <li class="nav-item ">
                <a class="nav-link" href="<?= base_url()?>requisitos/estudiante">
                  <i class="fa fa-tasks"></i>
                  <p>Gestor de requisitos</p>
                </a>
              </li>
            <?php endif; ?>

            <!-- Gestor de usuarios -->
            <?php if($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador'): ?>
              <li class="nav-item ">
                <a class="nav-link" href="<?= base_url()?>usuarios">
                  <i class="fa fa-users"></i>
                  <p>Gestor de usuarios</p>
                </a>
              </li>
            <?php endif; ?>

            <!-- OTRAS CONFIGURACIONES -->
            <?php if($this->session->userdata('rol') == 'Coordinador' || $this->session->userdata('rol') == 'Administrador'): ?>
              <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#configuracionSistema">
                  <i class="fa fa-cogs"></i>
                  <p>
                    Configuración
                    <b class="caret"></b>
                  </p>
                </a>
                <div class="collapse " id="configuracionSistema">
                  <ul class="nav">

                    <!-- Carreras Ofertadas -->
                    <li class="nav-item ">
                      <a class="nav-link" href="<?= base_url()?>carreras">
                        <span class="sidebar-mini">CO</span>
                        <span class="sidebar-normal">Carreras ofertadas</span>
                      </a>
                    </li>

                    <!-- Categorías -->
                    <li class="nav-item ">
                      <a class="nav-link" href="<?= base_url()?>categorias">
                        <span class="sidebar-mini">C</span>
                        <span class="sidebar-normal">Categorías</span>
                      </a>
                    </li>

                    <!-- Empresas Registradas -->
                    <!--
                    <li class="nav-item ">
                      <a class="nav-link" href="<?= base_url()?>empresas">
                        <span class="sidebar-mini">ER</span>
                        <span class="sidebar-normal">Empresas registradas</span>
                      </a>
                    </li>
                  -->
                    
                    <!-- Información de Contacto -->                    
                    <li class="nav-item ">
                      <a class="nav-link" href="<?= base_url()?>informaciones/editar/1">
                        <span class="sidebar-mini">IC</span>
                        <span class="sidebar-normal">Información de contacto</span>
                      </a>
                    </li>

                    <!-- Períodos Académicos -->
                    <li class="nav-item ">
                      <a class="nav-link" href="<?= base_url()?>periodos">
                        <span class="sidebar-mini">PA</span>
                        <span class="sidebar-normal">Períodos académicos</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            <?php endif;?>
          </ul>
        </div>
      </div>
      <div class="main-panel">