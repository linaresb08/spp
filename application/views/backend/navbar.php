			<!-- Navbar -->
			<nav class="navbar navbar-expand-lg ">
				<div class="container-fluid">
					<div class="navbar-wrapper">
						<!-- Botón para maximizar o minimizar el navbar -->
						<div class="navbar-minimize">
							<button id="minimizeSidebar" class="btn btn-success btn-fill btn-round btn-icon d-none d-lg-block">
								<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
								<i class="fa fa-navicon visible-on-sidebar-mini"></i>
							</button>
						</div>

						<!-- Título del encabezado  -->
						<a class="navbar-brand" href="<?= base_url()?><?= $controlador ?>"> 
						<?= $seccion ?> 
						</a>
					</div>
					<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-bar burger-lines"></span>
						<span class="navbar-toggler-bar burger-lines"></span>
						<span class="navbar-toggler-bar burger-lines"></span>
					</button>

					<!-- Search & Opciones -->						
					<div class="collapse navbar-collapse justify-content-end">
						<?php if($this->session->userdata('rol') != 'Estudiante'): ?>
							<ul class="nav navbar-nav mr-auto">
								<form action="<?= base_url() ?>perfiles/busqueda" method="POST" class="navbar-form navbar-left navbar-search-form" role="search">
									<div class="input-group">
										<i class="fa fa-search"></i>
										<input type="text" name="ci" class="form-control" placeholder="Buscar usuario por cédula" style="width: 200px !important;">
									</div>
								</form>
							</ul>
						<?php endif; ?>
						<ul class="navbar-nav">
							
							<!-- Para ir al gestor de mensajes -->
							<?php if(in_array($this->session->userdata('rol'), array("Administrador", "Coordinador"))): ?>
								<li class="nav-item dropdown">
									<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-envelope"></i>
										<?php 
											$notificaciones = $this->Contacto_model->contarMensajes(); 
										?>
										<?php if ($notificaciones > 0): ?>
											<span class="notification">											
												<?= $notificaciones ?>
											</span>
										<?php endif; ?>
										<span class="d-lg-none">Bandeja de mensajes</span>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<?php $mensajes = $this->Contacto_model->obtenerUltimos5(); ?>
										<?php if($mensajes): ?>
											<?php foreach ($mensajes as $mensaje): ?>
												<a class="dropdown-item" href="<?= base_url() ?>contactos"><?= $mensaje->correo ?></a>
											<?php endforeach; ?>
										<?php else: ?>
											<a class="dropdown-item" href="<?= base_url() ?>contactos">¡No hay mensajes pendientes!</a>											
										<?php endif; ?>	
										<a class="dropdown-item" href="<?= base_url() ?>contactos">
												<i class="fa fa-mail-forward"></i> Ir a bandeja de mensajes
											</a>										
									</ul>
								</li>
							<?php endif; ?>

							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i>
									<span class="d-lg-none">Opciones</span>
								</a>
								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
									<a href="<?= base_url()?>dashboard/salir" class="dropdown-item">
										<i class="fa fa-sign-out"></i> Cerrar sesión
									</a>
								</div>
							</li>
						</ul>
					</div>					
				</div>
			</nav>
			<!-- End Navbar -->
			<!-- Content -->
			<div class="content">
        <div class="container-fluid">

        	<?php if($this->session->flashdata('status')): ?>
		        <div class="alert <?= $this->session->flashdata('color') ?> border-radius-10" role="alert">
		         <?= $this->session->flashdata('status') ?>
		        </div>
		      <?php endif; ?>