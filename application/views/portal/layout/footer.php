				<footer class="footer footer-bg bg-light text-center border-superior-verde" style="position: absolute;width: 100%;bottom: 0;">
					<span class="section-description ">
						<strong>
							<a href="http://uvm.edu.ve/" target="_blank" class="font-color-black semi-bold">
								Universidad Valle del Momboy
							</a>
						</strong> | Rif J-30403287-0
					</span>
				</footer>
			</div>
		</div>
	</body>
	
	<!-- JS's y JQuery's -->

	<script src="<?= base_url() ?>assets/js/jquery-3.2.1.slim.min.js"></script>

<!-- 
	<script src="<?= base_url() ?>assets/js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
 -->
<!-- 
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> 
-->
	<script src="<?= base_url() ?>assets/js/popper.min.js"></script>

<!-- 	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> 
-->
	<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<!-- 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->

	<script>
    window.addEventListener('load', function () {
        $('#pagination li a').each(function () {
          $(this).addClass( "page-link" );
        });
      });
  </script>
</html>