
		<!-- Barra de navegación -->

		<!-- 
			Cambiar:
				navbar-light bg-light
			por:
				navbar-transparent navbar-absolute
		-->
		<nav class="navbar navbar-expand-md navbar-light bg-light border-inferior-verde">
			<!-- Marca o empresa -->
			<a class="navbar-brand semi-bold" href="<?= base_url() ?>portal">
		    <img src="<?= base_url() ?>assets/img/portal/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
		    Prácticas Profesionales
		  </a>

		  <!-- Botón para menú en resoluciones bajas -->
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPortal" aria-controls="navbarPortal" aria-expanded="false" aria-label="Toggle navigation" style="border: none;">
		    <span class="navbar-toggler-icon"></span>
		  </button>
	
			<!-- Lista de navegación -->
		  <div class="collapse navbar-collapse" id="navbarPortal">	
		  	<ul class="navbar-nav">

		  		<!-- INICIO -->
		  		<li class="nav-item">
		        <a class="nav-link" href="<?= base_url()?>portal">
		        	Inicio
		        </a>
		      </li>

		      <!-- CONTACTO -->
		      <li class="nav-item">
		        <a class="nav-link" href="<?= base_url()?>portal/contacto">Contacto</a>
		      </li>

		      <!-- DOCUMENTOS -->
		      <li class="nav-item">
		        <a class="nav-link" href="<?= base_url()?>portal/documentos">Documentos</a>
		      </li>

		      <!-- PUBLICACIONES DROPDOWN -->

		      <!-- Dropdown de Publicaciones -->
					<li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="dropdownPublicaciones" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Publicaciones
		        </a>

		        <!-- Lista de Links del Dropdown de Publicaciones -->
		        <div class="dropdown-menu" aria-labelledby="dropdownPublicaciones">
		          <?php $categorias = $this->Categoria_model->obtenerCategorias(); ?>
								<?php foreach ($categorias as $categoria): ?>
									<?php if ($categoria->categoria_id != 3): ?>
					    			<a href="<?= base_url()?>portal/categoria/<?=$categoria->categoria_id?>" class="dropdown-item">
					    				<?= $categoria->nombre ?>				    				
					    			</a>
					    		<?php endif; ?>
								<?php endforeach; ?>
								<div class="dropdown-divider"></div>
					    <a class="dropdown-item" href="<?= base_url()?>portal/publicaciones">Ver todas</a>
		        </div>
		      </li>

		      <!-- ACCESOS INSTITUCIONALES DROPDOWN -->
		      
		      <!-- Dropdown de Accesos Institucionales -->
					<li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="dropdownAccesosInstitucionales" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Accesos institucionales
		        </a>

		        <!-- Lista de Links del Dropdown de Accesos Institucionales -->
		        <div class="dropdown-menu" aria-labelledby="dropdownAccesosInstitucionales">

		        	<!-- Link del Campus Virtual -->
		        	<a href="http://campusvirtual.uvm.edu.ve/login/index.php" class="dropdown-item" target="_blank">
		        		Campus virtual
		        	</a>
							
							<!-- Link para consultar pasantes a través del portal-->
<!--		        	<a href="#" class="dropdown-item" target="_blank">
		        		Consulta de pasantes
		        	</a>
-->							
							<!-- Link del Correo electrónico @uvm -->
		        	<a href="https://accounts.google.com/AddSession?continue=http%3A%2F%2Fmail.google.com%2Fa%2Fuvm.edu.ve%2F&service=mail&hd=uvm.edu.ve&sacu=1#identifier" class="dropdown-item" target="_blank">
		        		Correo electrónico
		        	</a>
							
							<!-- Link del Sistema académico -->
		        	<a href="https://inscripcion.uvm.edu.ve/app.php" class="dropdown-item" target="_blank">
		        		Sistema académico
		        	</a>
							
							<!-- Link del Sistema de prácticas profesionales -->
		        	<a href="<?= base_url()?>dashboard/login" class="dropdown-item" target="_blank">
		        		Sistema de prácticas profesionales
		        	</a>

		        	<div class="dropdown-divider"></div>
							
							<!-- Link de la página principal de la UVM -->
		        	<a href="http://uvm.edu.ve/" class="dropdown-item" target="_blank">
		        		Universidad Valle del Momboy
		        	</a>

		        </div>
		      </li>

		  	</ul>
		  </div>
		</nav>
