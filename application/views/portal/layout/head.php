<!DOCTYPE html>
<html lang="es">
	<head>

		<!-- Título de la ventana -->
    <title>Prácticas Profesionales - Universidad Valle del Momboy</title>

    <!-- Favicon -->
    <link href='<?= base_url() ?>assets/img/portal/logo.png' rel='shortcut icon' type='image/png'/>
		

		<!-- Meta etiquetas -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <!--     Fonts     -->
    <link href="https://fonts.googleapis.com/css?family=Dosis:200,400,600" rel="stylesheet">

    <!--     Icons     -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" /> -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>


    <!-- CSS Files -->
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/mystyles.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/containers.css">



    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?= base_url() ?>assets/css/demo.css" rel="stylesheet" />

	</head>
	<body>	
    <div style="min-height: 100vh;position: relative;padding-bottom: 50px;<?= $this->uri->segment(1) == "portal" && ($this->uri->segment(2) == "" || $this->uri->segment(2) == "index" || $this->uri->segment(2) == "contacto") ? "background-image: url('http://localhost/spp/assets/img/portal/home2.jpg');background-repeat: no-repeat;background-size: cover;" : "" ?>">