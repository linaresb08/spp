		<div class="container">
			<div class="row">
				<?php foreach ($publicaciones->result() as $publicacion): ?>

					<?php foreach($this->Categoria_publicacion_model->obtenerCategorias_por_Publicacion($publicacion->publicacion_id) as $categoria): ?>
						<?php $categoria = $this->Categoria_model->obtenerCategoria($categoria->categoria_id); ?>	
		  		<?php endforeach; ?>

		  		<div class="col-12 col-sm-6 col-md-4 col-lg-3 margin-top-sm">
				  	<div class="card" style="border-top: 2.5px solid <?= $categoria->color ?>">

		  			<!-- CARD BODY -->
			      	<div class="card-body padding-12">
				        <h5 class="card-title text-left semi-bold card-titulo padding-bottom-3 margin-bottom-4"><?= $publicacion->titulo ?></h5>
				        <h6 class="card-subtitle mb-2 text-muted text-right">
				        	<?php $autor = $this->Perfil_model->obtenerPerfil_usuario_id($publicacion->usuario_id); ?>
				        	<span class="text-size-11">
				        		por <?= $autor->primer_nombre ?> <?= $autor->primer_apellido ?>
				        	</span>
				        </h6>
				        <p class="card-text text-justify parrafo-card extra-light">
				        	<?php if(strlen($publicacion->descripcion) > 120): ?>
				        		<?= substr($publicacion->descripcion, 0, 120); ?>...
				        	<?php else: ?>
				        		<?= $publicacion->descripcion ?>
				        	<?php endif;?>
				        </p>
				        <?php $fecha = new DateTime($publicacion->fecha); ?>
		      			<span class="text-left">
		      				<?= $fecha->format('d-m-Y'); ?>	
		      			</span>									
				      	<a href="
				      		<?php if ($categoria->categoria_id != 3): ?>
				      			<?= base_url() ?>portal/publicacion/<?= $publicacion->publicacion_id ?>
				      		<?php else: ?>
				      			<?= base_url() ?>portal/documentos
				      		<?php endif; ?>
				      		" class="btn btn-primary float-right">Ver más
				      	</a>
				     	</div>	
			    	</div>
			    </div>
			  <?php endforeach; ?>
	  	</div>
		</div>
		
		<div class="row padding_t">
			<div class="col-12">
				<?= $paginacion ?>  
			</div>
		</div>