			<div class="row justify-content-center">
				<?php $autor = $this->Perfil_model->obtenerPerfil_usuario_id($publicacion->usuario_id); ?>
				<?php $fecha = new DateTime($publicacion->fecha); ?>

				Publicado por <?= $autor->primer_nombre ?> <?= $autor->primer_apellido ?>,
				el <?= $fecha->format('d-m-Y'); ?>	
			</div>

			<div class="row justify-content-center">
				<?php if ($publicacion->foto): ?>
					<div class="col-sm-12 col-md-4 text-center">
						<img src="<?= $publicacion->foto ?>" class="img-fluid rounded" alt="Imagen alusiva a la publicación N° <?= $publicacion->publicacion_id ?> - <?= $publicacion->titulo ?>" width="100%">
					</div>

					<div class="col-sm-12 col-md-7">
				<?php else: ?>

					<div class="col-sm-10">
				<?php endif; ?>					
					<p class="card-text text-justify parrafo-card extra-light">
	        	<?= $publicacion->descripcion ?>	        	
	        </p>
				</div>

				<div class="col-12 text-left ">
					<strong>Categoría: </strong>
					<?php foreach($this->Categoria_publicacion_model->obtenerCategorias_por_Publicacion($publicacion->publicacion_id) as $categoria): ?>
						<a class="btn" style="color: white; background-color: <?= $this->Categoria_model->obtenerCategoria($categoria->categoria_id)->color ?>" href="<?= base_url() ?>portal/categoria/<?= $this->Categoria_model->obtenerCategoria($categoria->categoria_id)->categoria_id ?>" target="_blank" role="button">
							<i class="fa fa-tag"></i> <?= $this->Categoria_model->obtenerCategoria($categoria->categoria_id)->nombre ?>
						</a>
					<?php endforeach; ?>
				</div>		

			</div>

			