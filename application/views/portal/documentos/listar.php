		<div class="container">
			
			<?php
				/* Estos 3 arrays guardan los ID's  de la publicacion de los archivos correspondientes a cada tipo de archivo */
				$alumno_documentos = array();
				$tutor_academico_documentos = array();
				$tutor_laboral_documentos = array();

				foreach ($this->Categoria_publicacion_model->obtenerPublicaciones_por_Categoria($id) as $publicacion_categoria) {
					$publicacion = $this->Publicacion_model->obtenerPublicacion($publicacion_categoria->publicacion_id);
					if ($publicacion->tipo == 'Alumno') {
						array_push($alumno_documentos, $publicacion->publicacion_id);
					}
					elseif ($publicacion->tipo == 'Tutor laboral') {
						array_push($tutor_laboral_documentos, $publicacion->publicacion_id);
					}
					else{
						array_push($tutor_academico_documentos, $publicacion->publicacion_id);
					}
				}
			?>

			<div class="row justify-content-center">
				<div class="col-lg-10">
					<div id="accordion">

						<!-- #1 -->
					  <div class="card">
					    <div class="card-header cursor-pointer bg-gris" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					      <h5 class="mb-0">
					        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne">
					        	<div class="font-color-gray-d cursor-pointer">
					        		<strong>Carpeta del alumno</strong>
					        	</div>			          
					        </button>
					      </h5>
					    </div>

					    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
					      <div class="card-body row justify-content-md-center">
					      	<?php foreach ($alumno_documentos as $alumno_documento): ?>
										<?php $documento = $this->Publicacion_model->obtenerPublicacion($alumno_documento); ?>
										<div class="col-md-auto margin_b">
								      <a class="text-no-underline" href="<?= $documento->archivo ?>" target="_blank">
												<i class="fas fa-file-word"></i>
												<span class="font-color-black">
													<?= $documento->titulo ?>	
												</span>										
											</a>
								    </div>																	
									<?php endforeach; ?>
					      </div>
					    </div>
					  </div>

					  <!-- #2 -->
					  <div class="card">
					    <div class="card-header cursor-pointer bg-gris" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo">
					      <h5 class="mb-0">
					        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					        	<div class="font-color-gray-d cursor-pointer">
					        		<strong>Sobre del tutor académico</strong>
					        	</div>			          
					        </button>
					      </h5>
					    </div>

					    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
					      <div class="card-body row justify-content-md-center">
					        <?php foreach ($tutor_academico_documentos as $tutor_academico_documento): ?>
										<?php $documento = $this->Publicacion_model->obtenerPublicacion($tutor_academico_documento); ?>
										<div class="col-md-auto margin_b">
											<a class="text-no-underline" href="<?= $documento->archivo ?>" target="_blank">
												<i class="fas fa-file-word"></i>
												<span class="font-color-black">
													<?= $documento->titulo ?>	
												</span>		
											</a>
										</div>
									<?php endforeach; ?>
					      </div>
					    </div>
					  </div>

					  <!-- #3 -->
					  <div class="card">
					    <div class="card-header cursor-pointer bg-gris" id="headingThree" data-toggle="collapse" data-target="#collapseThree">
					      <h5 class="mb-0">
					        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
					          <div class="font-color-gray-d cursor-pointer">
					        		<strong>Sobre del tutor laboral</strong>
					        	</div>
					        </button>
					      </h5>
					    </div>

					    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
					      <div class="card-body row justify-content-md-center">
					        <?php foreach ($tutor_laboral_documentos as $tutor_laboral_documento): ?>
										<?php $documento = $this->Publicacion_model->obtenerPublicacion($tutor_laboral_documento); ?>
										<div class="col-md-auto margin_b">
											<a class="text-no-underline" href="<?= $documento->archivo ?>" target="_blank">
												<i class="fas fa-file-word"></i>
												<span class="font-color-black">
													<?= $documento->titulo ?>	
												</span>		
											</a>	
										</div>
									<?php endforeach; ?>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
			</div>
			
		</div>
		<br>