		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-3 margin-auto padding margin_b bg-blanco-b-radius">
					<strong>Horario de atención</strong><br>
			<!--		<span>Lunes a Viernes</span><br>					
					<span>8:00am - 12:00pm</span><br>					
					<span>1:00pm - 5:00pm</span><br>	-->				
					 <?= $informacion->horario	?> 
				</div>

				<div class="col-12 col-sm-5 margin-auto padding margin_b bg-blanco-b-radius">
					<strong>Contacto</strong><br>
					<span>Números telefónicos</span><br>
					<?= $informacion->numeros_telefonicos	?>
					<br>
					<span>Redes sociales</span><br>
					<?= $informacion->redes_sociales	?>
				</div>

				<div class="col-12 col-sm-3 margin-auto padding margin_b bg-blanco-b-radius">
					<strong>Ubicación</strong><br>
					<?= $informacion->ubicacion	?>
				</div>
			</div>

			 <div class="row">
				<div class="col text-center font-color-white">
					¿Tienes alguna duda? 
					<button type="button" class="btn btn-primary margin-l-r-10" data-toggle="modal" data-target="#exampleModalLong">
					  ¡Escríbenos!
					</button>
				</div>
			</div>

			<div class="row justify-content-center">
				<!-- Mensaje flash para comprobar enviado -->
					<?php if($this->session->flashdata('status')): ?>
		        <div class="alert <?= $this->session->flashdata('color') ?> margin-top-10" role="alert">
		         <?= $this->session->flashdata('status') ?>
		        </div>
		      <?php endif; ?>
		      <!-- Fin del mensaje flash -->
			</div> 
		</div>
					
					<!-- <div class="row">
						<div class="col-11">
							<span>Descripción general</span>
							<?= $informacion->descripcion_general	?>
						</div>
					</div>
					

					<div class="row justify-content-around">
						<div class="col-5">
							<span>Misión</span>
							<?= $informacion->mision	?>
						</div>

						<div class="col-5">
							<span>Visión</span>
							<?= $informacion->vision	?>
						</div>						
					</div>

					<div class="row">
						<div class="col">
							<span>Objetivos</span>
							<?= $informacion->objetivos	?>
						</div>
					</div> -->			
		

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      	<img src="<?= base_url() ?>assets/img/portal/logo.png" alt="Logo UVM" width="115" height="115" class="margin-auto">
<!--	     <h5 class="modal-title" id="exampleModalLongTitle"></h5> -->
        <button type="button" class="close" data-dismiss="modal">
          <i class="fa fa-times"></i>
        </button>
      </div>
      <div class="modal-body">
  	<form action="<?= base_url()?>portal/enviarMensaje"" method="POST">
	  		<div class="row">
	  			<div class="col">
	  				<input type="email" name="correo" placeholder="Escribe tu correo electrónico" class="form-control" required="true">
	  			</div>
	  		</div>
	    	<div class="row">
	    		<div class="col">
	    			<input type="text" name="asunto" placeholder="Asunto del correo" class="form-control" required="true">
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col">
	    			<textarea name="descripcion" cols="30" rows="8" placeholder="Escribe tu mensaje..." class="form-control" required="true"></textarea>
	    		</div>
	    	</div>
        
      </div>
      <div class="modal-footer">
		  	<div class="row justify-content-around">
		  		<div class="col">
		  			<button type="button" class="btn btn-secondary" data-dismiss="modal">
		  				Cerrar
		  			</button>
		  		</div>
		  		<div class="col">
		  			<input type="reset" value="Limpiar campos" class="btn btn-warning" style="background-color: #f5b909 !important;border-color: #f5b909 !important;color:white !important;">
		  		</div>
		      <div class="col">
		      	<input type="submit" value="Enviar" class="btn btn-success">
		      </div>		        
		  	</div>
      </div>
    </form>
    </div>
  </div>
</div>