	
		<div style="margin-bottom: 0px;padding-top: 25vh;padding-left: 15%;padding-right: 15%;">
			<div class="container align-items-center padding bg-blanco-b-radius">
				<p class="text-center section-sub-title">
					Universidad Valle del Momboy
				</p>					
				<h1 class="semi-bold text-center section-title">
					Coordinación de Prácticas Profesionales
				</h1>
				<p class="text-center extra-light section-description">
					<?= $informacion->descripcion_breve	?>
				</p>				
			</div>
		</div>
		