		<div class="container">
			<div class="row justify-content-center">
				<?php if ($publicaciones_categoria): ?>
					<?php foreach ($publicaciones_categoria as $publicacion_categoria): ?>
						
						<?php 
							$publicacion = $this->Publicacion_model->obtenerPublicacion($publicacion_categoria->publicacion_id); 
						?>

						<?php foreach($this->Categoria_publicacion_model->obtenerCategorias_por_Publicacion($publicacion->publicacion_id) as $categoria): ?>
							<?php $categoria = $this->Categoria_model->obtenerCategoria($categoria->categoria_id); ?>	
						<?php endforeach; ?> 	
						
						<div class="col-md-6 col-lg-4 margin-top-sm">
							<div class="card" style="border-top: 2.5px solid <?= $categoria->color ?>">

			  			<!-- CARD BODY -->
				      	<div class="card-body padding-12">
					        <h5 class="card-title text-left semi-bold card-titulo padding-bottom-3 margin-bottom-4"><?= $publicacion->titulo ?></h5>
					        <h6 class="card-subtitle mb-2 text-muted text-right">
					        	<?php $autor = $this->Perfil_model->obtenerPerfil_usuario_id($publicacion->usuario_id); ?>
										<span class="text-size-11">
					        		por <?= $autor->primer_nombre ?> <?= $autor->primer_apellido ?>
					        	</span>
					        </h6>
					        <p class="card-text text-justify parrafo-card extra-light">
					        	<?php if(strlen($publicacion->descripcion) > 120): ?>
        							<?= substr($publicacion->descripcion, 0, 120); ?>...
        						<?php else: ?>
        							<?= $publicacion->descripcion ?>
        						<?php endif;?>		        	
					        </p>
					        <?php $fecha = new DateTime($publicacion->fecha); ?>
			      			<span class="text-left">
			      				<?= $fecha->format('d-m-Y'); ?>	
			      			</span>									
					      	<a href="<?= base_url() ?>portal/publicacion/<?= $publicacion->publicacion_id ?>" class="btn btn-primary float-right">Ver más</a>
					      </div>
				    	</div>
				  	</div>	
				  <?php endforeach; ?>	
				<div class="col-12 padding_t">
					<?= $paginacion ?>  
				</div>
				<?php else: ?>
					<div class="alert alert-warning border-radius-10" role="alert">
					  ¡Aún no hay publicaciones en esta categoría!
					</div>
				<?php endif; ?>  
			</div>
		</div>



