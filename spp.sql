-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
<<<<<<< HEAD
-- Tiempo de generación: 12-05-2018 a las 20:45:51
=======
-- Tiempo de generación: 12-05-2018 a las 20:46:55
>>>>>>> master
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `spp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE IF NOT EXISTS `actividades` (
`actividad_id` int(11) NOT NULL,
  `pasantia_id` int(11) NOT NULL,
  `dia` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `observacion` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE IF NOT EXISTS `carreras` (
`carrera_id` int(11) NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`carrera_id`, `nombre`, `descripcion`) VALUES
(1, 'Ingeniería De Computación', 'Área de los tecnólogos en formación'),
(2, 'Ingeniería Industrial', ''),
(3, 'Administración de Empresas', ''),
(4, 'Contaduría Pública', ''),
(5, 'Derecho', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera_estudiantes`
--

CREATE TABLE IF NOT EXISTS `carrera_estudiantes` (
`carrera_estudiante_id` int(11) NOT NULL,
  `carrera_id` int(11) NOT NULL,
  `estudiante_id` int(11) NOT NULL,
  `pasantia_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
`categoria_id` int(11) NOT NULL,
  `color` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`categoria_id`, `color`, `nombre`) VALUES
(1, '#dd0529', 'Noticias'),
(2, '#008000', 'Cursos'),
(3, '#0080ff', 'Documentos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_publicaciones`
--

CREATE TABLE IF NOT EXISTS `categoria_publicaciones` (
`categoria_publicacion_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `publicacion_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categoria_publicaciones`
--

INSERT INTO `categoria_publicaciones` (`categoria_publicacion_id`, `categoria_id`, `publicacion_id`) VALUES
(21, 3, 1),
(22, 3, 2),
(23, 3, 3),
(24, 3, 4),
(25, 3, 5),
(26, 3, 6),
(27, 3, 7),
(28, 3, 8),
(29, 3, 9),
(30, 3, 10),
(31, 3, 11),
(32, 3, 12),
(33, 3, 13),
(34, 3, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `censos`
--

CREATE TABLE IF NOT EXISTS `censos` (
`censo_id` int(11) NOT NULL,
  `fecha_taller` date NOT NULL,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `lugar` text COLLATE utf8_spanish_ci NOT NULL,
  `duracion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `limite` int(11) NOT NULL,
  `estatus` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `coordinador_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `censos`
--

INSERT INTO `censos` (`censo_id`, `fecha_taller`, `nombre`, `descripcion`, `lugar`, `duracion`, `limite`, `estatus`, `coordinador_id`) VALUES
(1, '2018-05-09', 'Censo de prueba', 'Esta es la descripcion de un censo de prueba', 'Universidad Valle del Momboy', '4 horas', 10, 'Cerrado', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `censo_usuarios`
--

CREATE TABLE IF NOT EXISTS `censo_usuarios` (
`censo_usuario_id` int(11) NOT NULL,
  `censo_id` int(11) NOT NULL,
  `estudiante_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE IF NOT EXISTS `contactos` (
`contacto_id` int(11) NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `asunto` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(10) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`contacto_id`, `descripcion`, `correo`, `asunto`, `estado`) VALUES
(1, 'Este es un mensaje de prueba en la bandeja de entrada del sistema.', 'linaresproanobi@uvm.edu.ve', '¡Saludos!', 'No leido');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
`empresa_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `rif` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` text COLLATE utf8_spanish_ci NOT NULL,
  `telefono1` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `telefono2` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `jr_apellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `jr_nombres` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `jr_correo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `jr_telefono` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `pasantia_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante_requisitos`
--

CREATE TABLE IF NOT EXISTS `estudiante_requisitos` (
`estudiante_requisito_id` int(11) NOT NULL,
  `requisito_id` int(11) NOT NULL,
  `estudiante_id` int(11) NOT NULL,
  `estatus` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `archivo` text COLLATE utf8_spanish_ci NOT NULL,
  `observacion` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informacion_contacto`
--

CREATE TABLE IF NOT EXISTS `informacion_contacto` (
`informacion_id` int(11) NOT NULL,
  `descripcion_breve` text COLLATE utf8_spanish_ci NOT NULL,
  `descripcion_general` text COLLATE utf8_spanish_ci NOT NULL,
  `mision` text COLLATE utf8_spanish_ci NOT NULL,
  `vision` text COLLATE utf8_spanish_ci NOT NULL,
  `objetivos` text COLLATE utf8_spanish_ci NOT NULL,
  `numeros_telefonicos` text COLLATE utf8_spanish_ci NOT NULL,
  `ubicacion` text COLLATE utf8_spanish_ci NOT NULL,
  `redes_sociales` text COLLATE utf8_spanish_ci NOT NULL,
  `horario` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `informacion_contacto`
--

INSERT INTO `informacion_contacto` (`informacion_id`, `descripcion_breve`, `descripcion_general`, `mision`, `vision`, `objetivos`, `numeros_telefonicos`, `ubicacion`, `redes_sociales`, `horario`) VALUES
(1, 'Unidad adscrita al vicerrectorado que diseña, planifica, ejecuta y evalúa los procesos relacionados con el área de prácticas profesionales, uno de los pilares del curriculum integral, base filosófica de la acción educativa de la Universidad Valle del Momboy.', '', '', '', '', '0271-4144664 / +58 424 7125081', 'Av. Principal de Carvajal, Sector La Llanada Frente a Residencias "Los Manguitos", Universidad Valle del Momboy Sede "Estovacuy", Edificio 1, en la Coordinación de CIDIFI', 'practicasprofesionales@uvm.edu.ve (Correo ) <br>\r\n@practicas_puvm (Twitter) <br>\r\npracticas profesionales (Facebook) <br>', 'Lunes a Viernes <br>\r\n08:00am - 12:00pm <br>\r\n01:00pm - 05:00pm <br>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE IF NOT EXISTS `notas` (
`nota_id` int(11) NOT NULL,
  `eval_desemp_tutor_l` int(11) NOT NULL,
  `eval_informe_tutor_l` int(11) NOT NULL,
  `nota_final_tutor_l` int(11) NOT NULL,
  `eval_desemp_tutor_a` int(11) NOT NULL,
  `eval_informe_tutor_a` int(11) NOT NULL,
  `nota_final_tutor_a` int(11) NOT NULL,
  `definitiva` int(11) NOT NULL,
  `pasantia_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasantias`
--

CREATE TABLE IF NOT EXISTS `pasantias` (
`pasantia_id` int(11) NOT NULL,
  `informe` text COLLATE utf8_spanish_ci,
  `departamento_estudiante` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `seccion` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `estudiante_id` int(11) NOT NULL,
  `tutor_academico_id` int(11) DEFAULT NULL,
  `inscripcion_periodo_id` int(11) NOT NULL,
  `realizacion_periodo_id` int(11) NOT NULL,
  `tl_apellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tl_nombres` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tl_ci` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `tl_cargo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tl_correo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tl_telefono` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_reporte_actividad` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE IF NOT EXISTS `perfiles` (
`perfil_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `nacionalidad` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `ci` int(11) NOT NULL,
  `primer_nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `segundo_nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `primer_apellido` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `segundo_apellido` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `cumpleaños` date NOT NULL,
  `direccion_residencial` text COLLATE utf8_spanish_ci NOT NULL,
  `correo_institucional` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `correo_personal` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_movil` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_residencial` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `foto` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfil_id`, `usuario_id`, `nacionalidad`, `ci`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `cumpleaños`, `direccion_residencial`, `correo_institucional`, `correo_personal`, `telefono_movil`, `telefono_residencial`, `descripcion`, `foto`) VALUES
(2, 2, 'V', 0, 'Administrador', '', 'Del Sistema', '', '0000-00-00', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodo_academicos`
--

CREATE TABLE IF NOT EXISTS `periodo_academicos` (
`periodo_academico_id` int(11) NOT NULL,
  `nombre` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_finalizacion` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `periodo_academicos`
--

INSERT INTO `periodo_academicos` (`periodo_academico_id`, `nombre`, `fecha_inicio`, `fecha_finalizacion`) VALUES
(1, '2018A', '2018-01-08', '2018-04-13'),
(2, '2018B', '2018-04-30', '2018-07-30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicaciones`
--

CREATE TABLE IF NOT EXISTS `publicaciones` (
`publicacion_id` int(11) NOT NULL,
  `foto` text NOT NULL,
  `fecha` date NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descripcion` text NOT NULL,
  `archivo` text NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `tipo` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `publicaciones`
--

INSERT INTO `publicaciones` (`publicacion_id`, `foto`, `fecha`, `titulo`, `descripcion`, `archivo`, `usuario_id`, `tipo`) VALUES
(1, '', '2018-05-12', 'Tabla de requisitos', '', 'https://drive.google.com/open?id=1M2knHNmXuyCzsfYcdFwDMxXFmVxJd4E7', 2, 'Alumno'),
(2, '', '2018-05-12', 'Planilla de registro de pasante', '', 'https://drive.google.com/open?id=12AxeqSgR2m4R_1y83mk4qXZ5p_8wUqBB', 2, 'Alumno'),
(3, '', '2018-05-12', 'Consideraciones para estudiantes', '', 'https://drive.google.com/open?id=19iGld-RF4SfQODgGc0v7oAdcZHKs0ASf', 2, 'Alumno'),
(4, '', '2018-05-12', 'Control de asistencia', '', 'https://drive.google.com/open?id=1LH4WwZwm2ZYaT3_LVhUAsGas3eanmwr0', 2, 'Alumno'),
(5, '', '2018-05-12', 'Control diario y semanal de actividades', '', 'https://drive.google.com/open?id=1eFa0M91-q8Be47_whuLy0i-HnskH_dez', 2, 'Alumno'),
(6, '', '2018-05-12', 'Etiqueta', '', 'https://drive.google.com/open?id=1xiWMc89XFpPR4E2HLoqU1CIPAhb-uAqR', 2, 'Alumno'),
(7, '', '2018-05-12', 'Lineamientos del informe (Estudiantes)', '', 'https://drive.google.com/open?id=1bPuVSbR82XSEAMZTl0F_sMDUIZxPO5gx', 2, 'Alumno'),
(8, '', '2018-05-12', 'Plan de prácticas profesionales', '', 'https://drive.google.com/open?id=1LP_s8IBAWqXhVXKDeLTAlbE3eDr5vkLG', 2, 'Alumno'),
(9, '', '2018-05-12', 'Consideraciones para tutores académicos', '', 'https://drive.google.com/open?id=1lH2EqxfyA-Rcgo0cT3rLC9Dc4FQBkwWd', 2, 'Tutor académico'),
(10, '', '2018-05-12', 'Formato de evaluación supervisión académica', '', 'https://drive.google.com/open?id=1s7Ty189lIqa67EwcqGA7YC1yzxmtPOrH', 2, 'Tutor académico'),
(11, '', '2018-05-12', 'Lineamientos del informe (Tutor académico)', '', 'https://drive.google.com/open?id=1AR2aw9ALBDtzB3BS_RMOveG5t4wgsM9m', 2, 'Tutor académico'),
(12, '', '2018-05-12', 'Consideraciones para tutores laborales', '', 'https://drive.google.com/open?id=1S4ISJVcqwzxR4IqW4JAlgOgr6VUJUDaX', 2, 'Tutor laboral'),
(13, '', '2018-05-12', 'Formato de evaluación tutores laborales', '', 'https://drive.google.com/open?id=14YK5-nEe9nh39D8zpHEI_ncQzC67escR', 2, 'Tutor laboral'),
(14, '', '2018-05-12', 'Lineamientos del informe (Tutor laboral)', '', 'https://drive.google.com/open?id=1tSav4OtoTJ8PsU5lBZ5vAKQvl2YRjkMi', 2, 'Tutor laboral');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `requisitos`
--

CREATE TABLE IF NOT EXISTS `requisitos` (
`requisito_id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `requisitos`
--

INSERT INTO `requisitos` (`requisito_id`, `nombre`, `descripcion`) VALUES
(1, 'Registro del pasante', 'Aquí debe adjuntar la planilla de registro de pasante llena con todos sus datos, se recomienda que sea adjuntada en formato .pdf'),
(2, 'Carta de solicitud de prácticas profesionales', 'Esta carta debe estar dirigida a la Coordinación de Prácticas Profesionales'),
(3, 'Resumen curricular', 'El cual debe tener una foto digitalizada y reciente en tamaño carnet'),
(4, 'Cédula de identidad (Vigente)', 'Adjuntar la cédula de identidad ampliada y vigente'),
(5, 'Constancia de asistencia al taller (Obligatorio)', 'Digitalizar y adjuntar la constancia de asistencia al taller de prácticas profesionales firmada y sellada por el Coordinador del Departamento de Prácticas Profesionales'),
(6, 'Inscripción académica de prácticas profesionales', 'La planilla de inscripción debe estar firmada y sellada por el coordinador de carrera. (No aplica para estudiantes de la carrera de Derecho)'),
(7, 'Relación de materias aprobadas', 'Deben estar firmadas y selladas por el coordinador de carrera'),
(8, 'Pensum de la carrera a color', 'Debe tener las materias aprobadas marcadas, bajado del sistema académico, firmado y sellado por el coordinador de carrera. (UC aprobadas incluidas las equivalencias). En el caso de los estudiantes de la carrera de Derecho deben tener aprobado hasta el 3er. Año'),
(9, 'Constancia aprobatoria de inglés', 'Debe estar firmada y sellada por la Coordinadora del Centro de Idiomas. (No aplica para estudiantes de la carrera de Derecho)'),
(10, 'Constancia de aprobación del servicio comunitario', 'Firmada y sellada por la encargada del departamento. (No se requiere si aparece registrada la aprobación en la relación de materias aprobadas)'),
(11, 'Acreditación de materias por equivalencia', '(No se requiere si aparece registrada la aprobación en la relación de materias aprobadas. Si fuere el caso debe aparecer firmada y sellada por la oficina de equivalencias). No aplica para estudiantes de la carrera de Derecho'),
(12, 'Pasantías en micro-empresa, empresa familiar', 'Presentar RIF, organigrama, copia acta constitutiva y plan que justifique la realización de las prácticas profesionales en esa organización'),
(13, 'Carta de aceptación de la organización', ''),
(14, 'Plan de prácticas profesionales', ''),
(15, 'OPCIONALES', 'Algunas empresas exigen los siguientes requisitos: Certificado de salud, Carnet militar, RIF personal y seguro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `supervisiones`
--

CREATE TABLE IF NOT EXISTS `supervisiones` (
`supervision_id` int(11) NOT NULL,
  `fecha_supervision` text COLLATE utf8_spanish_ci NOT NULL,
  `actividades_realizadas` text COLLATE utf8_spanish_ci NOT NULL,
  `personales_aspectos_positivos` text COLLATE utf8_spanish_ci NOT NULL,
  `personales_puntos_mejoras` text COLLATE utf8_spanish_ci NOT NULL,
  `habilidades_aspectos_positivos` text COLLATE utf8_spanish_ci NOT NULL,
  `habilidades_puntos_mejoras` text COLLATE utf8_spanish_ci NOT NULL,
  `calidad_aspectos_positivos` text COLLATE utf8_spanish_ci NOT NULL,
  `calidad_puntos_mejoras` text COLLATE utf8_spanish_ci NOT NULL,
  `rasgos_aspectos_positivos` text COLLATE utf8_spanish_ci NOT NULL,
  `rasgos_puntos_mejoras` text COLLATE utf8_spanish_ci NOT NULL,
  `observaciones` text COLLATE utf8_spanish_ci NOT NULL,
  `pasantia_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`usuario_id` int(11) NOT NULL,
  `nombre_usuario` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `rol` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(12) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario_id`, `nombre_usuario`, `clave`, `rol`, `estado`) VALUES
(2, '0', 'JAo9DYYSt0D+5T6T0WUj2vZE3C+KEDMSlSoWiAOqpUlXn44XLviLqjwmQR6wRfjKXonLwqPQSZl/guYhI5bzlg==', 'Administrador', 'Habilitado');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividades`
--
ALTER TABLE `actividades`
 ADD PRIMARY KEY (`actividad_id`), ADD KEY `pasantia_id` (`pasantia_id`);

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
 ADD PRIMARY KEY (`carrera_id`);

--
-- Indices de la tabla `carrera_estudiantes`
--
ALTER TABLE `carrera_estudiantes`
 ADD PRIMARY KEY (`carrera_estudiante_id`), ADD KEY `carrera_id` (`carrera_id`,`estudiante_id`), ADD KEY `estudiante_id` (`estudiante_id`), ADD KEY `pasantia_id` (`pasantia_id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
 ADD PRIMARY KEY (`categoria_id`);

--
-- Indices de la tabla `categoria_publicaciones`
--
ALTER TABLE `categoria_publicaciones`
 ADD PRIMARY KEY (`categoria_publicacion_id`), ADD KEY `categoria_id` (`categoria_id`,`publicacion_id`), ADD KEY `publicacion_id` (`publicacion_id`);

--
-- Indices de la tabla `censos`
--
ALTER TABLE `censos`
 ADD PRIMARY KEY (`censo_id`), ADD KEY `coordinador_id` (`coordinador_id`);

--
-- Indices de la tabla `censo_usuarios`
--
ALTER TABLE `censo_usuarios`
 ADD PRIMARY KEY (`censo_usuario_id`), ADD KEY `censo_id` (`censo_id`,`estudiante_id`), ADD KEY `estudiante_id` (`estudiante_id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
 ADD PRIMARY KEY (`contacto_id`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
 ADD PRIMARY KEY (`empresa_id`), ADD KEY `pasantia_id` (`pasantia_id`);

--
-- Indices de la tabla `estudiante_requisitos`
--
ALTER TABLE `estudiante_requisitos`
 ADD PRIMARY KEY (`estudiante_requisito_id`), ADD KEY `requisito_id` (`requisito_id`,`estudiante_id`), ADD KEY `estudiante_id` (`estudiante_id`);

--
-- Indices de la tabla `informacion_contacto`
--
ALTER TABLE `informacion_contacto`
 ADD PRIMARY KEY (`informacion_id`);

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
 ADD PRIMARY KEY (`nota_id`), ADD KEY `pasantia_id` (`pasantia_id`);

--
-- Indices de la tabla `pasantias`
--
ALTER TABLE `pasantias`
 ADD PRIMARY KEY (`pasantia_id`), ADD KEY `inscripcion_periodo_id` (`inscripcion_periodo_id`,`realizacion_periodo_id`,`estudiante_id`,`tutor_academico_id`), ADD KEY `realizacion_periodo_id` (`realizacion_periodo_id`), ADD KEY `tutor_academico_id` (`tutor_academico_id`), ADD KEY `estudiante_id` (`estudiante_id`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
 ADD PRIMARY KEY (`perfil_id`), ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `periodo_academicos`
--
ALTER TABLE `periodo_academicos`
 ADD PRIMARY KEY (`periodo_academico_id`);

--
-- Indices de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
 ADD PRIMARY KEY (`publicacion_id`), ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `requisitos`
--
ALTER TABLE `requisitos`
 ADD PRIMARY KEY (`requisito_id`);

--
-- Indices de la tabla `supervisiones`
--
ALTER TABLE `supervisiones`
 ADD PRIMARY KEY (`supervision_id`), ADD KEY `pasantia_id` (`pasantia_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`usuario_id`), ADD UNIQUE KEY `nombre_usuario` (`nombre_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividades`
--
ALTER TABLE `actividades`
MODIFY `actividad_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `carreras`
--
ALTER TABLE `carreras`
MODIFY `carrera_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `carrera_estudiantes`
--
ALTER TABLE `carrera_estudiantes`
MODIFY `carrera_estudiante_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
MODIFY `categoria_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `categoria_publicaciones`
--
ALTER TABLE `categoria_publicaciones`
MODIFY `categoria_publicacion_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `censos`
--
ALTER TABLE `censos`
MODIFY `censo_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `censo_usuarios`
--
ALTER TABLE `censo_usuarios`
MODIFY `censo_usuario_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
MODIFY `contacto_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
MODIFY `empresa_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estudiante_requisitos`
--
ALTER TABLE `estudiante_requisitos`
MODIFY `estudiante_requisito_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `informacion_contacto`
--
ALTER TABLE `informacion_contacto`
MODIFY `informacion_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `notas`
--
ALTER TABLE `notas`
MODIFY `nota_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pasantias`
--
ALTER TABLE `pasantias`
MODIFY `pasantia_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
MODIFY `perfil_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `periodo_academicos`
--
ALTER TABLE `periodo_academicos`
MODIFY `periodo_academico_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
MODIFY `publicacion_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `requisitos`
--
ALTER TABLE `requisitos`
MODIFY `requisito_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `supervisiones`
--
ALTER TABLE `supervisiones`
MODIFY `supervision_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividades`
--
ALTER TABLE `actividades`
ADD CONSTRAINT `actividades_ibfk_1` FOREIGN KEY (`pasantia_id`) REFERENCES `pasantias` (`pasantia_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `carrera_estudiantes`
--
ALTER TABLE `carrera_estudiantes`
ADD CONSTRAINT `carrera_estudiantes_ibfk_1` FOREIGN KEY (`carrera_id`) REFERENCES `carreras` (`carrera_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `carrera_estudiantes_ibfk_2` FOREIGN KEY (`estudiante_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `carrera_estudiantes_ibfk_3` FOREIGN KEY (`pasantia_id`) REFERENCES `pasantias` (`pasantia_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `categoria_publicaciones`
--
ALTER TABLE `categoria_publicaciones`
ADD CONSTRAINT `categoria_publicaciones_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`categoria_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `categoria_publicaciones_ibfk_2` FOREIGN KEY (`publicacion_id`) REFERENCES `publicaciones` (`publicacion_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `censos`
--
ALTER TABLE `censos`
ADD CONSTRAINT `censos_ibfk_1` FOREIGN KEY (`coordinador_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `censo_usuarios`
--
ALTER TABLE `censo_usuarios`
ADD CONSTRAINT `censo_usuarios_ibfk_1` FOREIGN KEY (`censo_id`) REFERENCES `censos` (`censo_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `censo_usuarios_ibfk_2` FOREIGN KEY (`estudiante_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `empresas`
--
ALTER TABLE `empresas`
ADD CONSTRAINT `empresas_ibfk_1` FOREIGN KEY (`pasantia_id`) REFERENCES `pasantias` (`pasantia_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `estudiante_requisitos`
--
ALTER TABLE `estudiante_requisitos`
ADD CONSTRAINT `estudiante_requisitos_ibfk_1` FOREIGN KEY (`requisito_id`) REFERENCES `requisitos` (`requisito_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `estudiante_requisitos_ibfk_2` FOREIGN KEY (`estudiante_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `notas`
--
ALTER TABLE `notas`
ADD CONSTRAINT `notas_ibfk_1` FOREIGN KEY (`pasantia_id`) REFERENCES `pasantias` (`pasantia_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pasantias`
--
ALTER TABLE `pasantias`
ADD CONSTRAINT `pasantias_ibfk_1` FOREIGN KEY (`inscripcion_periodo_id`) REFERENCES `periodo_academicos` (`periodo_academico_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pasantias_ibfk_2` FOREIGN KEY (`realizacion_periodo_id`) REFERENCES `periodo_academicos` (`periodo_academico_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pasantias_ibfk_3` FOREIGN KEY (`estudiante_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pasantias_ibfk_4` FOREIGN KEY (`tutor_academico_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `perfiles`
--
ALTER TABLE `perfiles`
ADD CONSTRAINT `perfiles_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
ADD CONSTRAINT `publicaciones_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `supervisiones`
--
ALTER TABLE `supervisiones`
ADD CONSTRAINT `supervisiones_ibfk_1` FOREIGN KEY (`pasantia_id`) REFERENCES `pasantias` (`pasantia_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
